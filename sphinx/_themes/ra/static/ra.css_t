/*
 * ra.css_t
 * ~~~~~~~~~~~~
 *
 * :copyright: Copyright 2010 by Armin Ronacher. Modifications by Kenneth Reitz and Rohit Agarwal.
 * :license: Flask Design License, see LICENSE for details.
 */

{% set page_width = '1170px' %}
{% set reflow_width = '1000px' %}
{% set min_width = '480px' %}

@import url("basic.css");

/* -- page layout ----------------------------------------------------------- */

body {
    font-family: 'Open Sans', sans-serif;
    background-color: #f9f9f9;
    color: #000;
    margin: 0;
    padding: 0;
    min-width: {{ min_width }};
}

div.document {
    max-width: {{ page_width }};
    margin: 30px auto;
}

div.documentwrapper {
    float: right;
    width: 75%;
}

div.bodywrapper {
}

div.sphinxsidebar {
    width: 25%;
    float: left;
    margin: auto;
    font-size: 90%;
    line-height: 1.5;
}

hr {
    border: 1px solid #B1B4B6;
}

div.body {
    background-color: #f9f9f9;
    color: #3E4349;
    padding: 0 30px;
}

div.footer {
    max-width: {{ page_width }};
    margin: 30px auto;
    font-size: 90%;
    color: #888;
    text-align: right;
}

div.sphinxsidebar a {
    color: #444;
    text-decoration: none;
    border-bottom: 1px dotted #999;
}

div.sphinxsidebar a:hover {
    border-bottom: 1px solid #999;
}

div.sphinxsidebarwrapper {
    padding: 10px;
}

div.sphinxsidebarwrapper li {
    padding: .2em 0;
}

div.sphinxsidebar h3,
div.sphinxsidebar h4 {
    color: #444;
    font-weight: normal;
    margin: 0 0 5px 0;
    padding: 0;
}

div.sphinxsidebar h3 {
    font-size: 150%;
}

div.sphinxsidebar h4 {
    font-size: 125%;
}

div.sphinxsidebar h3 a {
    color: #444;
}

div.sphinxsidebar h3 a,
div.sphinxsidebar h3 a:hover {
    border: none;
}

div.sphinxsidebar p {
    color: #555;
    margin: 10px 0;
}

div.sphinxsidebar ul {
    margin: 10px 0;
    padding: 0;
    color: #000;
}

div.sphinxsidebar input {
    border: 1px solid #ccc;
    font-size: 100%;
}

/* -- body styles ----------------------------------------------------------- */

a {
    color: #004B6B;
    text-decoration: underline;
}

a:hover {
    color: #6D4100;
    text-decoration: underline;
}

div.body h1,
div.body h2,
div.body h3,
div.body h4,
div.body h5,
div.body h6 {
    font-weight: normal;
    margin: 30px 0px 10px 0px;
    padding: 0;
}

div.body h1 { margin-top: 0; padding-top: 0; font-size: 200%; }
div.body h2 { font-size: 175%; }
div.body h3 { font-size: 150%; }
div.body h4 { font-size: 125%; }
div.body h5 { font-size: 100%; }
div.body h6 { font-size: 100%; }

a.headerlink {
    color: #ddd;
    padding: 0 4px;
    text-decoration: none;
}

a.headerlink:hover {
    color: #444;
    background: #eaeaea;
}

div.body p, div.body dd, div.body li {
    line-height: 1.4em;
    text-align: justify;
    padding: .2em 0;
}

div.admonition {
    background: #fafafa;
    margin: 20px 0px;
    padding: 10px 10px;
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
}

div.admonition tt.xref, div.admonition a tt {
    border-bottom: 1px solid #fafafa;
}

div.admonition p.admonition-title {
    font-weight: normal;
    font-size: 150%;
    margin: 0 0 10px 0;
    padding: 0;
    line-height: 1;
}

div.admonition p.last {
    margin-bottom: 0;
}

div.highlight {
    background-color: white;
}

dt:target, .highlight {
    background: #FAF3E8;
}

div.note {
    background-color: #eee;
    border: 1px solid #ccc;
}

div.seealso {
    background-color: #ffc;
    border: 1px solid #ff6;
}

div.topic {
    background-color: #eee;
}

p.admonition-title {
    display: inline;
}

p.admonition-title:after {
    content: ":";
}

pre, tt {
    font-family: 'Consolas', 'Menlo', 'Deja Vu Sans Mono', 'Bitstream Vera Sans Mono', monospace;
    font-size: 90%;
}

tt.descname, tt.descclassname {
    font-size: 95%;
}

tt.descname {
    padding-right: 0.08em;
}

img.screenshot {
    -moz-box-shadow: 2px 2px 4px #eee;
    -webkit-box-shadow: 2px 2px 4px #eee;
    box-shadow: 2px 2px 4px #eee;
}

table.docutils {
    border: 1px solid #888;
    -moz-box-shadow: 2px 2px 4px #eee;
    -webkit-box-shadow: 2px 2px 4px #eee;
    box-shadow: 2px 2px 4px #eee;
}

table.docutils td, table.docutils th {
    border: 1px solid #888;
    padding: 0.25em 0.7em;
}

table.field-list, table.footnote {
    border: none;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}

table.footnote {
    margin: 15px 0;
    width: 100%;
    border: 1px solid #eee;
    background: #fdfdfd;
    font-size: 90%;
}

table.footnote + table.footnote {
    margin-top: -15px;
    border-top: none;
}

table.field-list th {
    padding: 0 0.8em 0 0;
}

table.field-list td {
    padding: 0;
}

table.footnote td.label {
    width: 0px;
    padding: 0.3em 0 0.3em 0.5em;
}

table.footnote td {
    padding: 0.3em 0.5em;
}

dl {
    margin: 0;
    padding: 0;
}

dl dd {
    margin-left: 30px;
}

blockquote {
    margin: 0 0 0 30px;
    padding: 0;
}

ul, ol {
    margin: 10px 0 10px 30px;
    padding: 0;
}

pre {
    background: #eee;
    padding: 7px 10px;
    margin: 15px 0px;
    line-height: 1.3em;
}

tt {
    background-color: #ecf0f3;
    color: #222;
    /* padding: 1px 2px; */
}

tt.xref, a tt {
    background-color: #FBFBFB;
    border-bottom: 1px solid white;
}

a.reference {
    text-decoration: none;
    border-bottom: 1px dotted #004B6B;
}

a.reference:hover {
    border-bottom: 1px solid #6D4100;
}

a.footnote-reference {
    text-decoration: none;
    font-size: 0.7em;
    vertical-align: top;
    border-bottom: 1px dotted #004B6B;
}

a.footnote-reference:hover {
    border-bottom: 1px solid #6D4100;
}

a:hover tt {
    background: #EEE;
}

a.previous-button {
    float: left;
    text-decoration: none;
    border-bottom: 1px dotted #004B6B;
}

a.next-button {
    float: right;
    text-decoration: none;
    border-bottom: 1px dotted #004B6B;
}

a.next-button:hover,
a.previous-button:hover {
    text-decoration: none;
    border-bottom: 1px solid #6D4100;
}

/* Top bar */
.top-bar {
    background-color: #003b63;
}

.top-bar-div {
    min-height: 44px;
    color: white;
    margin: 0px auto;
    max-width: {{ page_width }};
}

.telephone {
    float: left;
    line-height: 44px;
    font-weight: bolder;
    padding: 0px 10px;
}

.top-bar-links {
    float: right;
    line-height: 44px;
}

.top-bar-links .top-bar-link {
    margin-left: 10px;
    margin-right: 10px;
    font-size: 75%;
}

a.top-bar-link {
    text-decoration: none;
    color: white;
}

/* Site header */
.site-header {
  background-color: #fff;
}

.wrap {
    min-height: 60px;
    margin: auto;
    padding: 20px 0px;
    max-width: {{ page_width }};
}

.site-title,
.site-title:hover,
.site-title:visited {
    padding-left: 10px;
    display: block;
    float: left;
    line-height: 60px;
    position: relative;
    z-index: 1;
}

/******************************************
.site-nav {
    float: right;
    padding-right: 10px;
    line-height: 60px;
}

.site-nav .menu-icon { display: none; }

.site-nav .page-link {
    margin-left: 20px;
    color: #444;
}

a.page-link {
    text-decoration: none;
}

#sign-up {
    color: #0172b4;
    border: 1px solid;
    padding: 5px 10px;
}
******************************************/

@media (max-width: {{ reflow_width }}) {

    /******************************************
    .site-nav {
        z-index: 10;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        background-color: #fff;
    }

    .site-nav .menu-icon {
        display: block;
        font-size: 150%;
        color: #505050;
        float: right;
        text-align: center;
    }

    .site-nav a.menu-icon {
        text-decoration: none;
    }

    .site-nav .trigger {
        clear: both;
        margin-bottom: 5px;
        display: none;
    }

    .site-nav:hover .trigger { display: block; }

    .site-nav .page-link {
        display: block;
        text-align: right;
        line-height: 1.25;
        padding: 5px 10px;
        margin: 0;
    }

    #sign-up {
        border: none;
    }
    ******************************************/

    div.sphinxsidebar {
        display: block;
        float: none;
        width: 100%;
        margin: 50px 0px -20px 0px;
        background: #00426e;
        color: white;
    }

    div.body {
        padding: 0 10px;
    }

    div.sphinxsidebar h3,
    div.sphinxsidebar h4,
    div.sphinxsidebar p,
    div.sphinxsidebar h3 a {
        color: white;
    }

    div.sphinxsidebar a {
        color: #abd7f0;
        border-bottom: 1px dotted #abd7f0;
    }

    div.sphinxsidebar a:hover {
        border-bottom: 1px solid #abd7f0;
    }

    div.document {
        width: 100%;
        margin: 0;
    }

    div.documentwrapper {
        width: 100%;
        float: none;
        background: white;
        margin: 0;
    }

    div.bodywrapper {
        margin: 0;
    }

    .footer {
        width: auto;
    }

    .rtd_doc_footer {
        display: none;
    }

    .github {
        display: none;
    }

}

/* misc. */

.revsys-inline {
    display: none!important;
}
