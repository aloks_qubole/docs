.. _qds-doc-index:

.. Qubole Data Service documentation master file, created by
   sphinx-quickstart on Mon May 19 11:44:45 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Qubole Data Service Documentation
=================================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    quick-start-guide/index.rst
    changelog/index.rst
    release-notes/index.rst
    driver-release-notes/index.rst
    user-guide/index.rst
    admin-guide/index.rst
    rest-api/index.rst
    troubleshooting-guide/index.rst
    security-guide/index.rst
    faqs/index.rst
    partner-integration/index.rst
    runbook/index.rst
    feature-avail.rst

