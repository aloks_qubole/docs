.. _Changelog-index:

#########
Changelog
#########

.. toctree::
    :maxdepth: 1
    :titlesonly:

    apiqubole.rst
    eu-central.rst
    in.qubole.rst
    us.qubole.rst
    wellness.qubole.rst
    azure.qubole.rst

