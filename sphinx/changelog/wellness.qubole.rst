.. _wellness-qubole-log:

=================================
Changelog for wellness.qubole.com
=================================

+--------------------------------------+-------------------+-------------------+-------------------------------------------------------------------+
|  **Date and time of release**        |     **Version**   |  **Change type**  |                            **Change**                             |
+======================================+===================+===================+===================================================================+
| 24th January, 2019 (05:44 PM PST)    | 54.0.58           | Bug fix           | SPAR-3207: The Spark shuffle cleanup feature is not supported in  |
|                                      |                   |                   | Spark 2.2.1. However, the Spark shuffle cleanup feature continues |
|                                      |                   |                   | to be supported in Spark 2.3.1 and later versions.                |
+--------------------------------------+-------------------+-------------------+-------------------------------------------------------------------+
| 20th December, 2018 (2:30 PM PST)    | 54.0.30           | Enhancement       | SPAR-3151: Qubole now supports the latest Apache Spark 2.4.0      |
|                                      |                   |                   | version. It is displayed as 2.4 latest (2.4.0) in the Spark       |
|                                      |                   |                   | Version field of the **Create New Cluster** page on QDS UI.       |
|                                      |                   |                   |                                                                   |
|                                      |                   |                   | :ref:`Learn more <enh-spark2.4>`                                  |
+--------------------------------------+-------------------+-------------------+-------------------------------------------------------------------+
| 28th November, 2018 (2:36 AM PST)    |      54.0.31      | :ref:`Major release <release-notes-r54-index>`                                        |
|                                      |                   |                                                                                       |
|                                      |                   | For the list of changes until 28th November, 2018, see :ref:`apiqubole`.              |
+--------------------------------------+-------------------+---------------------------------------------------------------------------------------+