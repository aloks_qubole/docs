.. _eu-central:

===================================
Changelog for eu-central.qubole.com
===================================

+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|  **Date and time of release**        |     **Version**   |  **Change type**  |                            **Change**                                                                                           |
+======================================+===================+===================+=================================================================================================================================+
| 03rd April, 2019 (01:40 AM PST)      | 55.0.37           | Enhancement       | PRES-1350: Qubole supports configuring the required number of worker nodes during autoscaling. It is a cluster configuration    |
|                                      |                   |                   | override, ``query-manager.required-workers``. You can set it to denote the number of worker nodes that must be in the cluster   |
|                                      |                   |                   | before a query is scheduled to be run on the cluster. This enhancement is only supported with Presto 0.193 and later versions.  |
|                                      |                   |                   | For more information, see :ref:`presto-required-worker-nodes`.                                                                  |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2397: Qubole supports escaping newline ``\n`` and carriage return (``\r``) characters in data for correctly parsing on the |
|                                      |                   |                   | QDS UI. This enhancement is not available by default and it is only supported with Presto 0.193 and later versions. Create a    |
|                                      |                   |                   | ticket with the `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable it on the QDS account.                       |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2417: Presto clusters do not terminate while actively running Presto notebook paragraphs. The enhancement is               |
|                                      |                   |                   | not available by default. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to use it.              |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2474: The optimization to speed up queries on ``system.jdbc.tables`` with filter on a single table name. This speeds up    |
|                                      |                   |                   | the extract operation in Business Intelligence tools such as DBeaver, which query ``system.jdbc.tables`` with filter on a       |
|                                      |                   |                   | single table name.                                                                                                              |
|                                      |                   +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|                                      |                   | Bug fix           | PRES-1373: This is a fix for an issue where the retry operation was not working on the failed Presto queries. Retries work on   |
|                                      |                   |                   | the failed Presto queries now. As part of the fix, retries are configurable for Presto queries that run on the QDS platform.    |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | .. caution:: Configuring retries will just do a blind retry of a Presto query. This may lead to data corruption for             |
|                                      |                   |                   |              non-Insert Overwrite Directory (IOD) queries.                                                                      |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2306: Fixed a bug where the **Usage** page on the QDS UI did not show the Presto query's bytes read statistics.            |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2493: Fixed the data loss issue in the Hive connector when writing bucketed sorted table in Qubole Presto 0.208 queries.   |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2567: Fixed a bug in the user-overridden IAM Role feature where stale credentials were being used in a long-running        |
|                                      |                   |                   | cluster.                                                                                                                        |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2571: Fixed a concurrency related issue in autoscaling on Qubole Presto 0.193 and 0.208 versions due to which the          |
|                                      |                   |                   | cluster's size permanently exceeds its maximum size.                                                                            |
|                                      |                   +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|                                      |                   | Bug fix           | HIVE-3740: The Hive metastore API call has been replaced with a less expensive API call to resolve the issue of false alerts    |
|                                      |                   |                   | that were triggered on Datadog.                                                                                                 |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | HIVE-4040: It is a fix for an issue in ``DelimitedJSONSerde`` that caused NullPointerException when                             |
|                                      |                   |                   | ``hive.qubole.escape.crlf.in.result`` was enabled.                                                                              |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | HIVE-4053: It is a fix for an issue where the ``hive.qubole.drop.table.metastore.client.socket.timeout`` configuration was not  |
|                                      |                   |                   | honored when a Hive query containing the DROP Table statement was run.                                                          |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | HIVE-4280: Hive jobs were failing with a Metaexception that included a timeout message. To resolve this issue, the SSH          |
|                                      |                   |                   | configuration parameters' maximum value is increased on QDS servers.                                                            |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | QTEZ-335: It resolves an issue where the Tez Offline UI was inaccessible when ``yarn.timeline-service.store-class`` was set     |
|                                      |                   |                   | to ``RollingLevelDBTimelineStore`` in the cluster's Hadoop overrides.                                                           |
|                                      |                   +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|                                      |                   | Bug fix           | TOOLS-1139: The previous docker version had security vulnerabilities, which could allow malicious containers to                 |
|                                      |                   |                   | gain root-level privileges on the host. To resolve security vulnerabilities of the previous docker version, the                 |
|                                      |                   |                   | latest patch version of ``docker-18.06.1ce`` is installed.                                                                      |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|  13th March, 2019 (07:30 AM PST)     | 55.0.22           | Enhancement       | SPAR-3005: For the offline Spark clusters, only the event log files that are less than 400 MB are processed in                  |
|                                      |                   |                   | the offline Spark History Server (SHS). This prevents high CPU utilization on the internal servers due to SHS.                  |
|                                      |                   +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|                                      |                   | Bug fix           | SPAR-3371: Dynamic filtering now works for join keys of type Int, String, Long, and Short.                                      |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|  13th March, 2019 (07:30 AM PST)     | 55.0.22           | Bug fix           | ZEP-3247: Zeppelin failed to start due to the ``interpreter.json._COPYING_`` file in defloc. As a result, the                   |
|                                      |                   |                   | notebooks failed to load. With this fix, Zeppelin starts successfully and the ``interpreter.json._COPYING_``                    |
|                                      |                   |                   | file is not seen on defloc.                                                                                                     |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | ZEP-3252: On a cluster restart, the latest dashboards are displayed after the scheduled runs.                                   |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | ZEP-3230: Zeppelin server in Java 7 clusters was failing due to some ciphers that were disabled for security.                   |
|                                      |                   |                   | Now, the Zeppelin server starts successfully as the required ciphers are added when starting the Zeppelin                       |
|                                      |                   |                   | server.                                                                                                                         |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | ZEP-3243: A notebook with the ERROR status on a cluster failed to associate back to the cluster even after                      |
|                                      |                   |                   | switching to another cluster. This issue is fixed.                                                                              |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | ZEP-3234: Notebooks failed to load in case of higher latencies when fetching notebook permissions.                              |
|                                      |                   |                   | This issue is fixed.                                                                                                            |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
| 11th March, 2019 (12:30 AM PST)      | 55.0.17           |  :ref:`R55 Upgrade  <release-notes-r55-index>`                                                                                                      |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
| 7th January, 2019 (10:54 PM PST)     | 54.0.46           | Bug fix           | SPAR-3207: The Spark shuffle cleanup feature is not supported in                                                                |
|                                      |                   |                   | Spark 2.2.1. However, the Spark shuffle cleanup feature continues                                                               |
|                                      |                   |                   | to be supported in Spark 2.3.1 and later versions.                                                                              |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
| 20th December, 2018 (2:30 PM PST)    | 54.0.30           | Enhancement       | SPAR-3151: Qubole now supports the latest Apache Spark 2.4.0                                                                    |
|                                      |                   |                   | version. It is displayed as 2.4 latest (2.4.0) in the Spark                                                                     |
|                                      |                   |                   | Version field of the **Create New Cluster** page on QDS UI.                                                                     |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | :ref:`Learn more <enh-spark2.4>`                                                                                                |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
| 6th December, 2018 (4:10 PM PST)     | 54.0.38           | Bug fix           | EAM-1502: Fixed an issue in the Automatic Statistics Collection                                                                 |
|                                      |                   |                   | framework due to which the fresh statistics were not collected for                                                              |
|                                      |                   |                   | a few tables.                                                                                                                   |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
| 29th November, 2018 (4:21 AM  PST)   | 54.0.33           | Enhancement       | PRES-2380: Presto notebooks are GA and Presto 0.208 (beta) version                                                              |
|                                      |                   |                   | is available for use.                                                                                                           |
|                                      |                   +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|                                      |                   |  Bug fix          | PRES-2338: Fixed a bug in Presto 0.180 and 0.193 to adhere to                                                                   |
|                                      |                   |                   | fallback to the On-Demand configuration in case of Spot loss.                                                                   |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2268: Fixed a bug in Presto 0.193 which caused SELECT                                                                      |
|                                      |                   |                   | queries with THE ROW type output to fail.                                                                                       |
|                                      |                   |                   |                                                                                                                                 |
|                                      |                   |                   | PRES-2269: Fixed a bug in Presto 0.193 due to which queries on                                                                  |
|                                      |                   |                   | Hive views from Presto were failing.                                                                                            |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
|                                      |                   |                   | AD-1752: Bug fix to restrict the Service user from logging in via                                                               |
|                                      |                   |                   | Gmail or SAML.                                                                                                                  |
| 21st November, 2018 (9:42 AM PST)    |      54.0.26      |    Bug fix        |                                                                                                                                 |
|                                      |                   |                   | AD-1763: Bug fix to enable updating the feature status in an                                                                    |
|                                      |                   |                   | internal table.                                                                                                                 |
+--------------------------------------+-------------------+-------------------+---------------------------------------------------------------------------------------------------------------------------------+
| 20th November, 2018 (4:53 AM PST)    |      54.0.23      | :ref:`Major release <release-notes-r54-index>`                                                                                                      |
|                                      |                   |                                                                                                                                                     |
|                                      |                   | For the list of changes until 20th November, 2018, see :ref:`apiqubole`.                                                                            |
+--------------------------------------+-------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------+

