.. _availability-features:

=========================================
Launch Stage and Availability of Features
=========================================
All the features are classified by the launch stage, availability, and the default state as:

* Launch stage: (General Availability or Beta)
* Availability: (Self-serve or via Support)
* Default state: (Enabled or Disabled)

.. note:: Unless stated otherwise, features are generally available, available as self-service and enabled by default.

This table describes the two different launch stages of a feature.

+---------------------------+----------------------------------------------------------------+
| Launch Stage              | Description                                                    |
+===========================+================================================================+
| Beta                      | It implies that the features are appropriate for limited       |
|                           | production use. There is no SLA adherence for a beta feature.  |
+---------------------------+----------------------------------------------------------------+
| General Availability (GA) | It implies that the features can run production workloads      |
|                           | with the SLA adherence.                                        |
+---------------------------+----------------------------------------------------------------+

This table describes the two types of the feature availability.

+---------------------------+----------------------------------------------------------------+
| Availability              | Description                                                    |
+===========================+================================================================+
| Self-serve                | It implies that the feature has a user-configurable property.  |
+---------------------------+----------------------------------------------------------------+
| Via Support               | It implies that the feature can be enabled by the Support.     |
|                           | The feature may have a configurable property to disable it but |
|                           | only the Support can enable it for the first time.             |
+---------------------------+----------------------------------------------------------------+

This table describes the two types of a feature's default state.

+---------------------------+----------------------------------------------------------------+
| Default State             | Description                                                    |
+===========================+================================================================+
| Enabled                   | It implies that Qubole enables this feature by default.        |
+---------------------------+----------------------------------------------------------------+
| Disabled                  | It implies that the user must enable the feature using the     |
|                           | configurable properties.                                       |
+---------------------------+----------------------------------------------------------------+

.. _cluster-restart-op:

Cluster Restart Operation
-------------------------
Certain configurations that you change at the cluster level would require a cluster restart for the new configuration to
be effective.