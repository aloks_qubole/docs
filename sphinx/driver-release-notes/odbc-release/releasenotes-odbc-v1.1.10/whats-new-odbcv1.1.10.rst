.. _what-new-odbc-v1.1.10:

What is New and List of Changes
===============================
The new features and enhancements are listed in the section below:

.. note:: Unless stated otherwise, features are generally available, available as self-service and enabled by default.
          For more information, see :ref:`availability-features`.

* The ODBC drivers version 1.1.10 for Windows and Mac have been released.
* The ODBC driver configuration allows you to filter to a single schema while fetching metadata so that only the related
  ``TABL_SCHEM`` configured is exposed.
  For more details, see :ref:`odbc-advanced-config`.
* A view for other databases (catalogs) is supported When Presto is used with the ODBC driver.
* The ODBC driver for the Windows 32-bit edition is not supported with this version. If you do not have installed a 64-bit
  version of your BI tool, you may want to install the 64-bit version of your tool before upgrading to the ODBC driver
  version 1.1.10.

  :ref:`Learn more <odbc-changesv1.1.10>`

.. _odbc-changesv1.1.10:

List of Changes
---------------

Enhancements
............
* **ODBC-217**: There have been improvements in fetching the metadata for Presto. A new option is added in the
  advanced driver configuration to filter to a single schema while fetching metadata so that only the related
  ``TABL_SCHEM`` configured is exposed.
* **ODBC-225**: A view for other databases (catalogs) is supported When Presto is used with the ODBC driver.

Bug Fixes
.........
* **ODBC-229**: The ODBC driver supports SSL-enabled S3 buckets.