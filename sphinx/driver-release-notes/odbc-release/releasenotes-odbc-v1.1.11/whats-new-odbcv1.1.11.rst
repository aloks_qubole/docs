.. _what-new-odbc-v1.1.11:

What is New and List of Changes
===============================
The new features and enhancements are listed in the section below:

.. note:: Unless stated otherwise, features are generally available, available as self-service and enabled by default.
          For more information, see :ref:`availability-features`.

* The ODBC drivers version 1.1.11 for Windows, Mac, and Linux have been released.
* The connection time in Tableau on Windows is now reduced to less than a minute.
* The need to mention the schema name in a query is removed if the schema is added in the ODBC driver's advanced configuration.


  :ref:`Learn more <odbc-changesv1.1.11>`

.. _odbc-changesv1.1.11:

List of Changes
---------------

Enhancements
............
* **ODBC-243**: The need to mention the schema name in a query is removed if the schema is added in the ODBC driver's
  advanced configuration.

Bug Fixes
.........

* **ODBC-247**: Fixed the issue where the certificate file was not traceable when a new DSN is created using the
  Driver Manager.
* **ODBC-255**: Fixed the issue where the connection time in Tableau on Windows used to take around 8 minutes. The
  connection time is now reduced to less than a minute.
* **ODBC-264**: Fixed the issue where quotes got added to the data type when the continuous data type was selected
  on Tableau.