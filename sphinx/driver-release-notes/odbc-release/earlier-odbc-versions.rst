.. _earlier-odbc-versions:

============================
Earlier ODBC Driver Versions
============================
To see the release notes for the ODBC driver releases prior to the ODBC driver version 1.1.10, click
`Earlier ODBC Driver Versions <https://s3.amazonaws.com/paid-qubole/odbc/ReleaseNotesODBC.txt>`__.