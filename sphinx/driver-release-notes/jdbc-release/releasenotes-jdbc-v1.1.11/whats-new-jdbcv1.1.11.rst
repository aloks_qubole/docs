.. _whats-news-jdbcv1.1.11:

===============================
What is New and List of Changes
===============================
This is a new enhancement:

* The JDBC driver provides the list of catalogs for a Presto data source. The catalogs contain schemas
  that define a set of tables, which can be queried. The view helps you to see the different schemas categorized into
  the catalog. The view also eliminates the repetition of schemas, which may belong to different catalogs.

List of Changes
---------------

Enhancements
............
* **JDBC-129**: The JDBC driver now provides the list of catalogs for a Presto data source. The catalogs contain the
  corresponding schemas.

Bug Fixes
.........
* **JDBC-127**: Earlier the unicode characters were not supported in a query. Now, the JDBC driver supports unicode
  characters in queries.
