.. _whats-news-jdbcv1.1.12:

===============================
What is New and List of Changes
===============================
These are the changes in this version:

* The JDBC driver now supports VALUES for defining an inline table in an SQL query. For more information,
  see `SQL Values <https://prestodb.github.io/docs/current/sql/values.html>`__.
* The JDBC driver now supports retries in case of unsuccessful HTTP GET requests for successfully executed queries.

List of Changes
---------------

Bug Fixes
.........
* **JDBC-137**: A Presto query failed as it had an inline table defined by VALUES. The JDBC driver now supports VALUES
  for defining an inline table in an SQL query. For more information,
  see `SQL Values <https://prestodb.github.io/docs/current/sql/values.html>`__.
* **JDBC-138**: The JDBC driver could not display correct information for some successful queries. To resolve this issue,
  the JDBC driver now supports retries in case of unsuccessful HTTP GET requests for successfully executed queries.