.. _jdbc-relnotes-v1.1.12-index:

==========================
JDBC Driver Version 1.1.12
==========================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    whats-new-jdbcv1.1.12.rst