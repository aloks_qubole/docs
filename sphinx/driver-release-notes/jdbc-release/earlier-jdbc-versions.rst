.. _earlier-jdbc-versions:

============================
Earlier JDBC Driver Versions
============================
To see the release notes for the ODBC driver releases prior to the JDBC driver version 1.1.11, click
`Earlier JDBC Driver Versions <https://s3.amazonaws.com/paid-qubole/jdbc/ReleaseNotesJDBC.txt>`__.