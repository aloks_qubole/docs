.. _Driver-Release-Notes-index:

###################################
ODBC and JDBC Drivers Release Notes
###################################

.. toctree::
    :maxdepth: 1
    :titlesonly:

    jdbc-release/index.rst
    odbc-release/index.rst