.. _set-object-permission:

===========================================================
How do I set access control to a specific object in Qubole?
===========================================================
Qubole allows you to set access control to a specific cluster and notebook currently through REST APIs. Qubole also
allows you to control access to a notebook through the **Notebooks** UI.

For more information on managing access control to a specific cluster, see :ref:`set-object-policy-cluster`.

For more information on managing access control to a specific notebook, see:

* :ref:`set-object-policy-note` through API
* :ref:`manage-notebook-permissions` and :ref:`manage-folder-permissions` through UI




