.. _avoid-others-to-see-commands:

How do I avoid other users of the QDS account from seeing my commands?
----------------------------------------------------------------------
To avoid other users from seeing your commands, assign other users a policy for ``Command Resource`` with
only ``create`` permission in **Control Panel** > **Manage Roles**. With this policy, other users can create
a command but denied access to see other users' commands. For more information, see :ref:`manage-roles-user-resources-actions`.