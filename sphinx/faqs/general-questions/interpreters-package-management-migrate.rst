.. _interpreters-packaged-management-migrate:

=============================================================
Why is my Spark application not using the Package Management?
=============================================================
If the Spark application is using the existing Spark interpreters, then it would still use system Python and R versions.
To make your Spark application to use Package Management, you must migrate the interpreter property values as described
in :ref:`migrate-interpreters-pm`.