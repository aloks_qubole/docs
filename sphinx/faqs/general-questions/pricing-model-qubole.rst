.. _pricing-model-qubole:

What is the pricing model for Qubole?
=====================================

Please see the :ref:`billing-qubole-index` for a description of our pricing model. 

Updated pricing plans are available at the `Pricing page <http://www.qubole.com/pricing/>`__.

We also offer custom plans according to the customer requirements.
For more information on the contact details, see the `Contact US page <http://www.qubole.com/company/contact-us/>`__.


