.. _supported-browser-and-OS:

=============================================================================
What are the mimimum Operating System and browser requirements for using QDS?
=============================================================================
QDS is supported on latest versions of Google Chrome and Mozilla Firefox. Though not officially supported, most of QDS
features work reasonably well on Apple Safari.
