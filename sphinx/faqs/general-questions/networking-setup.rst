.. _networking-setup:

How should I set up networking for my Cloud accounts?
=====================================================

.. _networking-setup-aws:

Setting Up Networking for AWS
-----------------------------

* In an EC2-Classic Qubole account, QDS launches clusters with a default security group that controls traffic among
  the cluster nodes and acts as a virtual firewall to the outside world. The port settings are described
  :ref:`here <cluster-network-security-characteristics>`.

* In an EC2-VPC Qubole account, by default QDS launches clusters in the AWS default VPC for the
  :ref:`AWS region <aws-regions-supported-qubole>`. The default configuration is described
  `here <http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/default-vpc.html>`__.

   * You can override this default behavior and launch clusters into a specific AWS VPC; use the **Advanced** tab on
     the QDS **Clusters** page to specify the VPC and subnet. See :ref:`networking-setup-aws-VPC-requirements`.

   * To create an AWS VPC with public and private subnets, and launch a cluster in that VPC, follow
     :ref:`these instructions <clusters-in-vpc>`.

..  _networking-setup-aws-VPC-requirements:

QDS Requirements for an AWS VPC
...............................

If you decide to create a VPC for your QDS clusters, make sure that the VPC meets these requirements:

* Has an `internet gateway <https://aws.amazon.com/premiumsupport/knowledge-center/create-attach-igw-vpc/>`__.
* Has a route table with a rule that specifies the internet gateway as the destination of CIDR block 0.0.0.0/0 (allowing traffic
  between the VCN and internet).
* Has a subnet with an ACL (Access Control List) that

  * Allows SSH access to all
  * Allows all traffic for all protocols of all port ranges to destination 0.0.0.0/0


.. _networking-setup-azure:

Setting Up Networking for Azure
-------------------------------

To create a virtual network (**VNet**) for your Qubole Azure VMs, navigate to **Virtual Networks** in the Azure portal,
and create your network following
`this Azure documentation <https://azure.microsoft.com/en-us/documentation/articles/virtual-networks-create-vnet-arm-pportal/>`__.

Use the **Azure Settings** section under the **Advanced Configuration** tab of the **Clusters** page in the QDS UI
to configure your QDS clusters to use the VNet.

.. _networking-setup-oracle:

Setting Up Networking for Oracle OCI
------------------------------------

To enable QDS to bring up clusters in Oracle OCI, you must have
`an Oracle VCN <https://console.us-az-phoenix-1.oracleiaas.com/#/a/networking/vcns>`__ with the following characteristics:

   * Has an `internet gateway <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Tasks/managingIGs.htm?Highlight=VCN%20gateway>`__.
   * Has a route table with a rule that specifies the internet gateway as the target of CIDR block 0.0.0.0/0 (allowing traffic between the VCN and internet).
   * Has `subnets <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Tasks/managingsubnets.htm>`__ for each
     OCI `Availability Domain <https://cloud.oracle.com/en_US/bare-metal-compute/faq>`__ in which you intend to launch
     QDS clusters:

     * The `security list <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Tasks/managingsecuritylists.htm?Highlight=security%20list#To>`__
       for the subnets must have the following rules at a minimum:

       * `Stateful <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Concepts/securitylists.htm?Highlight=stateful%20rule>`__
         ingress rules, specifying each subnet's CIDR as the source CIDR, allowing all protocols (and hence all ports).
       * A stateful ingress rule, specifying 0.0.0.0/0 as the source CIDR, allowing ssh access (TCP protocol, port 22).
       * A stateful egress rule, specifying 0.0.0.0/0 as the destination CIDR, allowing all protocols (and hence all ports).

These Oracle documents provide explanations and instructions:

* `Overview of the Networking Service <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Concepts/overview.htm>`__
* `Managing Subnets <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Tasks/managingsubnets.htm>`__
* `Managing Route Tables <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Tasks/managingroutetables.htm>`__

Use the **Advanced Settings** tab of the **Clusters** page in the QDS UI to configure your QDS clusters to use the VCN.

To configure and use a private subnet for your Oracle OCI clusters, follow :ref:`these instructions <private-subnet-oracle>`.