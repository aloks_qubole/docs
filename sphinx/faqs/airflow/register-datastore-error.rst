.. _register-datastore-error:

==================================================================================================
What does this error - **Data store was created successfully but it could not be activated** mean?
==================================================================================================
This error appears when you register the data store in the **Explore** UI page. It usually comes because QDS does not
have access to the data store. You can safely ignore this error and associate the data store with the Airflow cluster.
This is a known limitation and Qubole plans to address it in the near future.