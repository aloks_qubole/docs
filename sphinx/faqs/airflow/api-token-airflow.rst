.. _api-token-airlfow-password-relationship:

===============================================================
How do I put the AUTH_TOKEN into the Qubole Default connection?
===============================================================

1. From the Airflow page, go to **Administration** > **Connections** > **qubole_default**.

.. image:: images/QuboleDefault.png

2. Add the API token or AUTH_TOKEN in the password's text box. (A password is the QDS authentication token for a QDS account user. See :ref:`manage-my-account` for more information on API tokens.)
   Based on the schedule mentioned in the DAG, the next run must pick it up automatically. Try loading the DAG with a different name and schedule to check out the behavior.

.. image:: images/AuthTool.png

.. note:: If you want to specify a Qubole Auth Token other than qubole_default for a task, you can provide the
          qubole_conn_id value in the task parameters after creating a new connection with same name from the Connections tab.
