.. _run-dag-button:

============================================
Is there any button to run a DAG on Airflow?
============================================
There is no button to run a DAG in the Qubole UI, but the Airflow 1.8.2 web server UI provides one.