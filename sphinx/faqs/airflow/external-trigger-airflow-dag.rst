.. _external-trigger-airflow-dag:

Can I create a configuration to externally trigger an Airflow DAG?
==================================================================
No, but you can trigger DAGs from the QDS Analyze UI using the shell command ``airflow trigger_dag <DAG>...``.

If there is no connection password, the ``qubole_example_operator`` DAG will fail when it is triggered.