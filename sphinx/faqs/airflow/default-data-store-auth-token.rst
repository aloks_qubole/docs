.. _default-data-store-auth-token:

Why must I reenter the database password/AUTH-token at a cluster restart?
=========================================================================
When the data store is set to default, then the connection authorization password (which is the AUTH-Token) is stored
directly on the Airflow cluster in the default data store, which is also on the cluster. That means that when you restart
the cluster, you must re-add the password (AUTH-Token) because it would be erased since the database gets deleted when
the cluster is offline.