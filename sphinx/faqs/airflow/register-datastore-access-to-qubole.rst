.. _register-datastore-access-to-qubole:

=================================================================================
Do I need to provide access to Qubole while registering Airflow datastore in QDS?
=================================================================================
No, QDS does not need access to the Airflow datastore.
