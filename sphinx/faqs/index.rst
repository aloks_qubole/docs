.. _faqs:

####
FAQs
####
The topics that follow provide answers to commonly asked questions:

* :ref:`differences-presto-hive` provides answers to commonly asked questions on Presto.

.. toctree::
    :maxdepth: 2
    :titlesonly:

    general-questions/index.rst
    airflow/index.rst
    hive/index.rst
    hadoop-clusters/index.rst
    aws-questions/index.rst
    security/index.rst
    package-management/index.rst

