.. _install-dependencies-package-faq:

Does the Qubole Package Management upgrade the underlying dependent libraries if they are already installed?
============================================================================================================
The Qubole Package Management (PM) only installs the dependent libraries during the package's installation. If there is
a failure in the dependent libraries' upgrade, an appropriate error message is displayed. However, the PM does not upgrade
the dependent libraries automatically.