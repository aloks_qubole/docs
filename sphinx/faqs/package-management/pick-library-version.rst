.. _pick-library-faq:

Does the Qubole Package Management install dependencies of a package?
=====================================================================
The Qubole Package Management installs all dependencies of a package. If it cannot install the dependencies, then that
package installation fails.