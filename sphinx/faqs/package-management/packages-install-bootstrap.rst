.. _packages-install-bootstrap:

How do I install packages that are not available in a Package Management environment?
=====================================================================================
If you want to use a specific package which is unavailable in a Package Management environment, then create a ticket with
`Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to get an alertnative solution to install the missing packages.