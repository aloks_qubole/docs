.. _pm-upgrade-libraries-faq:


How does Qubole Package Management pick a library version? Is it dependent on the Conda version or does it pick the latest version?
===================================================================================================================================
The Qubole Package Management (PM) only does a ``conda install <package>``. If this fails, then it does a ``pip install <package>``.
If you mention a specific version of package while adding it, then the PM installs that version. Otherwise, the Conda does its
own calculation in picking up the version that is compatible with the other existing installations in the PM environment.

.. note:: The PM uses the **R channel** to install the R packages.

:ref:`package-management` describes how to use the Package Management to create an environment and add or remove packages
through the UI. :ref:`add-packages-api` describes how to add packages in a PM environment through the API.