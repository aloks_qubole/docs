.. _faqs-aws-questions:

###################
Questions about AWS
###################


The topics that follow provide answers to questions commonly asked about using QDS with Amazon Web Services (AWS):

.. toctree::
    :maxdepth: 2
    :titlesonly:
    :numbered: 2


    aws-regions-supported-qubole.rst
    can-use-aws-spot-instances-qubole.rst
    use-reserved-instances-with-qubole.rst
    difference-purchasing-option-slave-nodes.rst
    disadvantage-spot-only-cluster.rst
    support-EBSVolume-KMS-encryption.rst