What are the purchasing options for AWS cluster nodes?
======================================================

For a full discussion, see :ref:`spot-nodes` and :ref:`auto`.
