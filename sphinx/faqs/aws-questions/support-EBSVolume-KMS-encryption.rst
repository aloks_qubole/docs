.. _support-ebs-volume-kms-encryption:

===============================================
Can I enable SSE-KMS encryption on EBS Volumes?
===============================================
Qubole does not directly support SSE-KMS encryption on EBS volumes. However, Qubole supports custom EBS snapshots
that can belong to an SSE-KMS encrypted EBS volume. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
to use encrypted EBS volumes. Provide the EBS snapshot ID of the EBS volume to Qubole Support.