.. _aws-regions-supported-qubole:

What AWS regions are supported by Qubole?
=========================================
Qubole currently supports following AWS regions:

-  US East (N.Virginia) [us-east-1]
-  US East (Ohio) [us-east-2]
-  US West (Seattle) [us-west-1]
-  US West (Oregon) [us-west-2]
-  Canada (Central) [ca-central-1]
-  EU (Ireland) [eu-west-1]
-  EU (London) [eu-west-2]
-  Asia Pacific (Mumbai) [ap-south-1]
-  Asia Pacific (Singapore) [ap-southeast-1]
-  Asia Pacific (Tokyo) [ap-northeast-1]
-  Asia Pacific (Seoul) [ap-northeast-2]
-  South America (Sao Paulo) [sa-east-1]

Qubole also supports EU (Frankfurt); this requires a separate Qubole account, as explained below.

.. _eu-ap:

How do I use QDS in the EU (Frankfurt) AWS Region?
--------------------------------------------------

This section provides a brief introduction to using `eu-central-1.qubole.com <https://eu-central-1.qubole.com>`__.

This version of QDS:

* Runs entirely in the local EU (Frankfurt) `AWS region <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions>`__ .
* Requires a separate Qubole account. If you already have an `api.qubole.com <https://api.qubole.com>`__ account, you need to create a new account to use eu-central-1.qubole.com.

Why Use this QDS version?
.........................

eu-central-1.qubole.com ensures that all the data you work on stays in the local AWS region. Examples of why this
may be useful or necessary include:

* Compliance: you may need to meet data residency requirements.
* Latency: if you primarily work in this region, improved latency may be a consideration: the QDS REST endpoint
  may be closer to your computation and storage resources.

Restrictions
............

The following restrictions apply to eu-central-1.qubole.com:

* Hadoop MRv1 is not supported.
* eu-central-1-qubole.com uses the AWS `S3A file system <https://wiki.apache.org/hadoop/AmazonS3>`__, whereas
  api.qubole.com uses S3N `by default <http://docs.qubole.com/en/latest/user-guide/hadoop/hadoop2/enable-s3a-filesystem.html?highlight=s3a>`__.

Useful Documents
................

Make yourself familiar with the following documents before setting up a cluster in the EU:

**AWS documentation:**

* `Global Infrastructure <http://aws.amazon.com/about-aws/global-infrastructure/>`__
* `Regions and Availability Zones <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html>`__
   * `AWS Regions <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions>`__
   * `Describing Your Regions and Availability Zones <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#using-regions-availability-zones-describe>`__
* `AWS Security Credentials <http://docs.aws.amazon.com/general/latest/gr/aws-security-credentials.html>`__
* `Setting Up with Amazon EC2 <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/get-set-up-for-amazon-ec2.html>`__
* `Your VPC and Subnets <http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html>`__
* `Amazon Elastic Block Store (Amazon EBS) <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonEBS.html>`__
* `Amazon EC2 FAQs <https://aws.amazon.com/ec2/faqs/>`__

**Qubole documentation:**

* `Configuring Clusters <http://docs.qubole.com/en/latest/user-guide/clusters/configuring-clusters.html>`__
* `Configuring a Cluster in a VPC with Public and Private Subnets <http://docs.qubole.com/en/latest/user-guide/clusters/clusters-in-vpcs.html>`__
* `Understanding Cluster Network Security Characteristics <http://docs.qubole.com/en/latest/user-guide/clusters/cluster-network-security-characteristics.html>`__

Guidelines for Setting Up a Cluster
...................................

The following guidelines apply to setting up any QDS cluster:

* Make sure your cluster routing table is set up as described under `Understanding Cluster Network Security Characteristics <http://docs.qubole.com/en/latest/user-guide/clusters/cluster-network-security-characteristics.html>`__ (this is the default configuration).
   * In particular, if you make changes, make sure TCP port 22 remains open to allow SSH connections from QDS tunnel servers.
     :ref:`private-subnet-whitelist-IP` lists the IP addresses of the Qubole tunnel servers.
* Use the default security configuration if possible.
   * If it’s not possible for your organization to use the default configuration, make sure you thoroughly understand the issues described in the links on this page and know how to proceed. If you need a custom security configuration and are not fully confident you can configure it correctly, contact `Qubole Support <mailto:help@qubole.com>`__ before you start.
* If you move the cluster from `EC2-Classic to a VPC <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-vpc.html>`__, or *vice versa*, make sure you delete the old security group. (If the old group exists with the same name, QDS will use it and the cluster will fail to start.)
* Make sure that starting the cluster will not put your AWS account over the `quota <http://aws.amazon.com/ec2/faqs/#How_many_instances_can_I_run_in_Amazon_EC2>`__ for the instance type you’ve chosen. (Remember the quota is AWS-account-wide; it includes any non-Qubole instances you may have.)
* Make sure your `AWS credentials <http://docs.aws.amazon.com/general/latest/gr/aws-security-credentials.html>`__, and `IAM credentials <http://docs.qubole.com/en/latest/faqs/general-questions/policy-use-qubole-use-my-iam-credentials.html>`__ if any, are properly configured.
* If you use AWS instances with limited local storage (such as *c3*), be careful about configuring `Elastic Block Store <http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonEBS.html>`__ (EBS) volumes for the cluster (via **EBS volume count** on the **Add New Cluster** page of the **QDS Control Panel**):
   * If you don’t configure enough storage, jobs may run slowly or fail.
   * If you configure too much, you could possibly exceed your AWS account’s `volume limit <http://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html#limits_ebs>`__. (You can `request an increase <http://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html>`__ if necessary.)

