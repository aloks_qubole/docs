Can I use AWS Spot instances with Qubole?
=========================================

Yes. See :ref:`spot-nodes` for a full discussion.
