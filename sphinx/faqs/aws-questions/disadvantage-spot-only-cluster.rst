.. _disadvantage-spot-only-cluster:

===========================================================================================
What is the disadvantage of creating a Spot-only cluster or a cluster with 100% Spot nodes?
===========================================================================================
As Spot node prices change very frequently, it would result in poor performance of the cluster. For more details on
the disadvantages and recommendations, see :ref:`spot-only`.