.. _destination-for-hadoop-archive-extraction:

Are files from a Hadoop archive extracted to a specific folder by default?
==========================================================================
When you extract files from a Hadoop archive, by default, the files are extracted to a folder with a name that matches
the Hadoop archive's name exactly. For example, files from a **user/zoo/test.tar** archive are extracted into
a **user/zoo/test.tar** folder.




