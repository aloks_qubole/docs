.. _hive-command-spark-cluster:

==================================================================
Can I submit Hive Commands to a Spark Cluster and is it supported?
==================================================================
You can submit Hive commands to a Spark cluster and expect it to run correctly. However, Qubole does not recommend using
this scenario, where multiple engines run on the same cluster.

In this specific case, Hive (using Tez or MapReduce execution engine) and Spark target different use cases. So the
Hadoop cluster configurations optimized for each use case could be unrelated to each other.

QDS does not block this behavior out of the consideration for convenience and experimental purpose. For example, you
may submit Hive commands for adhoc purpose such as DDL or schema change.