Can the data stored on a Cloud instance be encrypted?
=====================================================



AWS
---

Yes, you can improve security by encrypting the data at rest on ephemeral drives of a Cloud instance.
(A QDS Hadoop cluster uses ephemeral drives for HDFS and for the intermediate output of MapReduce
jobs.) Select **Enable Encryption** when :ref:`configuring the cluster <configuring-clusters>`.

See also :ref:`encrypt-HDFS-data`.

Azure
-----

See :ref:`data-management-azure`.