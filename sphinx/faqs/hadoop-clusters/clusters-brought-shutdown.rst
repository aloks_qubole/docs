When are clusters brought up and shut down?
===========================================

Qubole’s clusters are ephemeral, though of course the data persists in
your Cloud storage.

When you submit a query that requires a cluster, QDS brings up the cluster automatically if
a suitable one is not already running, and uses it for subsequent queries, if any. Once the cluster
has been idle for some time, QDS :ref:`brings it down <auto-works-downscaling-shutdown>`.

You can also bring up and shut down a cluster :ref:`manually <cluster-operations>`.
