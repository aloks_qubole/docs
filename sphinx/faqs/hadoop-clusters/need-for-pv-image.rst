.. _need-for-pv-image:

Do I need to bake a Paravirtual Image for bringing up the clusters?
===================================================================
Qubole will use a single hardware virtual machine (HVM) image across all instance types going forward and stop publishing
the  paravirtual (PV) image. HVM image is even supported on older instance types such as m1, m2, and m3 instance families,
with PVonHVM driver, there is no need of a separate PV image.

Qubole does not support the PV custom image from now on. So, you do not have to bake PV custom images. The API which
accepts custom images will continue accepting the PV image to maintain the backward compatibility but the PV image would
not be used to bring up the clusters.