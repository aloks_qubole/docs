.. _check-master-slave-node:

How do I check if a node is a master node or a worker node?
===========================================================
Use either of these two methods:

* Run the ``nodeinfo`` command as described in :ref:`run-utility-commands-in-cluster`.
* Add the following code in the :ref:`node bootstrap script <nodebootstrapscript>`:

  .. sourcecode:: bash

      #!/bin/bash

      source /usr/lib/hustler/bin/qubole-bash-lib.sh
      is_master=`nodeinfo is_master`
      if [[ "$is_master" == "1" ]]; then
      -----your code here--

