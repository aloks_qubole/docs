.. _faqs-hadoop-clusters:

############################
Questions about QDS Clusters
############################

The topics that follow provide answers to questions commonly asked about QDS clusters:

.. toctree::
    :maxdepth: 2
    :titlesonly:
    :numbered: 2

    aws-instance-region.rst
    long-qubole-hadoop-cluster-take-come.rst
    whose-aws-account-hadoop-clusters-launched.rst
    clusters-brought-shutdown.rst
    clusters-auto-scaled.rst
    one-cluster-or-many.rst
    do-files-get-extracted-to-a-specifc-folder-while-extracted-from-hadoop-archive.rst
    will-hdfs-affected-cluster-auto-scaling.rst
    check-master-slave-node.rst
    qubole-store-data.rst
    can-data-stored-ec2-instance-encrypted.rst
    can-use-python-2-7-hadoop-tasks-2.rst
    why-are-so-many-idle-mappers-seen-on-a-hadoop-cluster.rst
    hive-command-spark-cluster.rst
    need-for-pv-image.rst



.. can-use-aws-reserved-instances-qubole.rst
