How long does a Qubole Hadoop Cluster take to come up?
======================================================

It can take up to a few minutes for a cluster to come up. If a cluster fails to come up, see
:ref:`troubleshoot-cluster-startup` for help.