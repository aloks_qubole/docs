.. _jdbc-storage-handler-split-group:

=====================================================================================
How do I handle the exception displayed with a Tez Query while querying a JDBC table?
=====================================================================================
You would see the the following exception message when you are querying a JDBC table using Tez.

.. sourcecode:: bash

    java.io.IOException: InputFormatWrapper can not support RecordReaders that don't return same key & value objects.

You get the exception as the JDBC Storage handler does not work when **Input Splits Grouping** is enabled in Hive-on-Tez.
``HiveInputFormat`` is enabled by default in Tez to support **Splits Grouping**.

You can avoid the issue by setting the input format as ``CombineHiveInputFormat`` using this command that disables the
**Splits Grouping**.

.. sourcecode:: bash

    set hive.tez.input.format=org.apache.hadoop.hive.ql.io.CombineHiveInputFormat;