.. _faqs-hive:

####################
Questions about Hive
####################

The topics that follow provide answers to common questions about Hive in Qubole:

.. toctree::
    :maxdepth: 2
    :titlesonly:
    :numbered: 2

    version-hive-qubole-provide.rst
    data-directory-structure-s3-can-access.rst
    difference-external-table-managed-table.rst
    qubole-opensource-sessions.rst
    can-create-table-hdfs.rst
    file-formats-quboles-hive-support-box.rst
    default-inputformat-used-quboles-hive.rst
    qubole-remember-tables-even-cluster-goes-away.rst
    data-rds-can-use-hive-process-data.rst
    can-use-exceltableaubi-tools-top-quboles-hive-tables.rst
    can-plug-udfs-serdes.rst
    jdbc-storage-handler-splits-group.rst
    unsupportedhivefeatures.rst
