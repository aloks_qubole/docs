.. _qubole-opensource-sessions:

=========================================================================
How different is a Qubole Hive Session from the Open Source Hive Session?
=========================================================================
In a Qubole Hive session, when a user logs into QDS, all query history commands executed when this specific user
is active earlier, are in the same Qubole Hive Session.

In open-source Hive, each Hive CLI represents a Hive session. Session-level commands and properties are not effective
across open-source Hive sessions.

The difference in how commands run differently in a Qubole Hive session and an open-source session are described in
this table.

.. note:: When you run ``set hive.on.master`` and ``set hive.use.hs2`` in the corresponding **Qubole Hive** session,
          the two commands are not saved automatically.

+-----------------------------------------+-------------------------------------+----------------------------------------+
| Session-Level Commands                  | Qubole Hive Session                 | Open-source Hive Session               |
+=========================================+=====================================+========================================+
|``set <key>=<value>;``                   | All four commands are effective     | All four commands are only valid in    |
+-----------------------------------------+ throughout the entire Qubole Hive   + the corresponding Hive sessions.       +
|``add FILES <filepath> <filepath>*;``    | session. The commands are           |                                        |
+-----------------------------------------+ automatically saved.                +                                        +
| ``add JARS <filepath> <filepath>*;``    |                                     |                                        |
+-----------------------------------------+                                     +                                        +
|``add ARCHIVES <filepath> <filepath>*;`` |                                     |                                        |
+-----------------------------------------+-------------------------------------+----------------------------------------+