What is the difference between an external table and a managed table?
=====================================================================

The main difference is that when you drop an external table, the
underlying data files stay intact. This is because the user is expected
to manage the data files and directories. With a managed table, the
underlying directories and data get wiped out when the table is dropped.
