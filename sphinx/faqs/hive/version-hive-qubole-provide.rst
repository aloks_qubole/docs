What version of Hive does Qubole provide?
=========================================

See :ref:`os-version-support`.




.. .. In addition to stock functionality from Apache, Qubole adds a number of
.. additional enhancements that include:
..
.. .-  Faster access to Data in Amazon S3
.. .-  Ability to recover partitions from Amazon S3
.. .-  JVM reuse across Hive queries to allow faster and more efficient
.. .   query execution
.. .-  Ability to query MongoDB from Hive directly
.. .-  A responsive always-on Hive server for fast interactive queries
.. . -  Automatic caches HDFS to speed up access to json data

.. .. ..
  .. Qubole's Hive service also benefits from a highly optimized and tuned deployment of Hadoop that provides faster and more
.. . efficient job scheduling as well as auto-scaling benefits.
