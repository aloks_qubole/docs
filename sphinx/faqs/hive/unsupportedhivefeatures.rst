.. _unsupportedhivefeatures:

=================================================
What are the unsupported features in Qubole Hive?
=================================================
Qubole Hive does not support:

* ACID transactions
* The LOAD file command

