What file formats does Qubole’s Hive support out of the box?
============================================================

Qubole’s Hive supports:

-  Text files (compressed or uncompressed) containing delimited, CSV or
   JSON data.
-  It can support Binary data files stored in RCFile and SequenceFile
   formats containing data serialized in Binary JSON, Avro, ProtoBuf and
   other binary formats

Custom File Formats (InputFormats) and Deserialization libraries (SerDe)
can be added to Qubole. Please contact `Qubole
Support <mailto:help@qubole.com>`__ if you have a query in this
regard.
