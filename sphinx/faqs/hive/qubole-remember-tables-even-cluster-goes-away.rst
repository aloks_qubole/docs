Does Qubole remember my tables even when my cluster goes away?
==============================================================

For every account in Qubole – we create a persistent MySql backed Hive
metastore automatically. All table definitions are stored in this
metastore – and hence table metadata is available across cluster
restarts.

(Note that *tmp* tables are an exception as they are automatically
deleted at the end of a user session)
