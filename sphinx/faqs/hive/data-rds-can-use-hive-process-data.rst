I have my data in RDS. Can I use Hive to process the data?
==========================================================

Yes. You can use “Data Import” command to fetch data from RDS and many
other non rdbms databases in a hive table. Then normal hive queries can
be run on the hive table to do processing.

As a final step you can run “Data Export” command on the processed
data to export it back to a database of your choice for visualization
purposes.
