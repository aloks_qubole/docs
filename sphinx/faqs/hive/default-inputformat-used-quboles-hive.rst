What is the default InputFormat used by Qubole’s Hive?
======================================================

Qubole’s hive uses *CombineHiveInputFormat* by default and treats
underlying files as text-files by default. During table definition –
users can indicate that the file type of of an alternative format (such
as a Sequence or RC File).
