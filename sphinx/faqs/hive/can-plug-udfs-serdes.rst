Can I plug in my own UDFs and SerDes?
=====================================

Yes you can use your own user-defined functions (UDFs) and Serializer/Deserializers (SerDes). But because Qubole
is a multi-tenant service, it must approve such requests on a per-account basis.
Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ if there is a data format that is not
supported or if you want to use custom UDFs.
