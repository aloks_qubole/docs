Can I use Excel/Tableau/BI tools on top of Qubole’s Hive tables?
================================================================

Qubole provides an ODBC driver that you can :ref:`download <download-odbc-driver>` and
:ref:`install <install-odbc-driver>` on a Microsoft Windows server. This allows Excel,
Tableau and other BI tools to talk to Qubole’s Hive service.