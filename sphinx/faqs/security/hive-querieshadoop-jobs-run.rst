Where do my Hive queries/Hadoop jobs run?
=========================================

* All Hadoop commands run on a Hadoop cluster. If no cluster is already running, a Hadoop command brings one up
  automatically.

* Hive commands run on QDS machines, launching a Hadoop cluster only to run MapReduce jobs; metadata queries
  such as  “show table” and “recover partitions” don’t bring up a Hadoop cluster.
