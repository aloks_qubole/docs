.. _does_Qubole_store_my_data:

Does Qubole store any of my data on its machines?
=================================================

Qubole doesn’t store any of our users’ data. QDS
clusters use your credentials for your
Cloud storage account, and this is where the results of your jobs and queries are stored.
Your credentials are encrypted in our database.

Our HDFS cache, which is ephemeral and used only while running queries, supports
:ref:`encrypted storage <encrypt-HDFS-data>`. We may temporarily cache some query results but these
are destroyed when the node is :ref:`decommisioned <auto-works-downscaling>`.