.. _hadoop-component-logs:

=======================================
Accessing the Logs of Hadoop Components
=======================================
Here are the log locations of Hadoop components:

* The logs of ResourceManager/NodeManager are saved in ``/media/ephemeral0/logs/yarn``.
* The logs of NameNode/DataNode are saved in ``/media/ephemeral0/logs/hdfs``.
* The logs of the EBS upscaling are saved in ``/media/ephemeral0/logs/others/disk_check_daemon.log``.
* The logs written by the node bootstrap script are saved in ``/media/ephemeral0/logs/others/node_bootstrap.log``.
* The logs of the autoscaling-nodes are saved in ``/media/ephemeral0/logs/yarn/autoscaling.log or /media/ephemeral0/logs/yarn/scaling.log``.

:ref:`Hadoop Logs Location <hadoop-mapreduce-configuration-logs>` provides the location of YARN logs, daemon logs, and
the MapReduce history files.