.. _analyzing-notebook-failures:

===================
Accessing the Logs
===================

You can access the Zeppelin logs that reside on the cluster master node at ``/media/ephemeral0/logs/zeppelin/logs``.
When analyzing the Zeppelin start up logs, you should also check the ``<zeppelin-root-ip>.out`` file.

You can access the interpreter logs and the logs for the Spark jobs that run on the **Notebooks** page.


Accessing the Interpreter Logs
------------------------------

1. From the QDS UI, navigate to the **Notebooks** page.
2. Click on **Interpreters**.
3. From the **Interpreters** page, click **Logs** on the top right corner.

   The following figure shows the **Interpreters** page.

   .. image:: images/int-logs.png

4. The logs files are displayed in a separate tab. Click on the files for the details.

   The following figure shows the log files that are displayed.

    .. image:: images/int-logs-details.png

For information about accessing the **Spark Application UI**, see :ref:`Accessing the Spark Application UI from the **Notebooks** page <spark-app-ui-notebook>`.