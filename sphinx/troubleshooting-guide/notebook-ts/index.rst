.. _debug-notebooks:

===============================
Troubleshooting Notebook Issues
===============================

When troubleshooting a failed paragraph in the notebook, you can analyze the Zeppelin logs and Spark logs to identify the errors and exceptions to understand the root cause of the failure.

.. toctree::
    :maxdepth: 2
    :titlesonly:

    analyze-notebooks-logs
    troubleshoot-notebook