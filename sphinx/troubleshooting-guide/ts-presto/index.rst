.. _debug-presto-index:

=============================
Troubleshooting Presto Issues
=============================
This topic describes common Presto issues with suitable workarounds. It consists of the issues that are categorized as
below:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    debug-presto
    presto-server
    presto-tuning