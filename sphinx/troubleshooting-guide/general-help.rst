.. _general-help:

=========================
Using Qubole General Help
=========================
For reporting an issue related to QDS, create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
which logs an Helpdesk ticket. The Qubole Support team ensures that each helpdesk ticket is attended and resolved as
quickly as possible.

:ref:`troubleshooting.rst` describes a checklist that you can do to gather the information before logging a helpdesk
ticket/contacting the Qubole Support team.

:ref:`qds-helpcenter.rst` describes how to use the Helpcenter to know more information such as documentation and the
support helpdesk.