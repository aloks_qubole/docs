.. _debugging-spark:

==============================
Troubleshooting Spark Issues
==============================

When troubleshooting a failed Spark job or Spark application, you can analyze the command logs to identify the errors and exceptions to understand the root cause of the job failure.

.. toctree::
    :maxdepth: 2
    :titlesonly:

    analyze-spark-failure
    access-additional-logs
    troubleshoot-spark