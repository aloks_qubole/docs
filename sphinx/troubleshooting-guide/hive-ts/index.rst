.. _debugging-hive-index:

==============================
Troubleshooting Hive Issues
==============================

When troubleshooting a failed Hive job or Hive application, you can analyze the command logs to identify the errors and exceptions to understand the root cause of the job failure.

.. toctree::
    :maxdepth: 2
    :titlesonly:

    analyze-hive-failure
    troubleshoot-hive
