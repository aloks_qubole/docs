.. _troubleshoot-cluster-startup-Oracle:

===================================================
Troubleshooting Oracle OCI Cluster Startup Failures
===================================================


* :ref:`diagnose-and-fix-Oracle`
* :ref:`prevent-Oracle`

.. _diagnose-and-fix-Oracle:

Diagnosing and Fixing Problems
------------------------------

The table that follows lists some common error messages that may be logged when a cluster fails to start,
describes the underlying causes, and provides remedies:

+-----------------------------------+-------------------------------------+----------------------------------------+
|Error message text                 |Cause                                |What to do                              |
+===================================+=====================================+========================================+
|``Hadoop Bring up failed. File     |                                     |                                        |
|<filename> could only be           |                                     |Make sure you have configured the subnet|
|replicated to 0 nodes...``         |                                     |so as to allow communication among all  |
|                                   |                                     |nodes: see :ref:`oracle-configure`.     |
|                                   |Master daemon cannot talk to worker  |                                        |
|                                   |daemon, or worker is down or out of  |                                        |
|                                   |disk space.                          |                                        |
|                                   |                                     |                                        |
|                                   |                                     |                                        |
+-----------------------------------+-------------------------------------+----------------------------------------+
|``The limit for this tenancy has   |Bringing up this cluster would exceed|Decrease the cluster size, or change the|
|been exceeded``                    |this tenancy's limit for instances   |instance type, and try again. If that   |
|                                   |of this type.                        |fails, ask Oracle support for a `higher |
|                                   |                                     |limit <https://docs.us-phoenix-1.       |
|                                   |                                     |oraclecloud.com/Content/General/        |
|                                   |                                     |Concepts/servicelimits. htm>`__.        |
|                                   |                                     |                                        |
|                                   |                                     |                                        |
|                                   |                                     |                                        |
|                                   |                                     |                                        |
|                                   |                                     |                                        |
|                                   |                                     |                                        |
|                                   |                                     |                                        |
+-----------------------------------+-------------------------------------+----------------------------------------+
|``HEALTH-CHECK-FAILED. Reason:     |QDS cannot contact the cluster master|Make sure you have whitelisted port 22  |
|Failed to create socks proxy for   |node via SSH.                        |for the QDS NAT (52.44.223.209); use    |
|cluster...``                       |                                     |the subnet's `security list             |
|                                   |                                     |<https://docs.us-phoenix-1.oraclecloud. |
|                                   |                                     |com/Content/Network/Tasks/              |
|                                   |                                     |managingsecuritylists.htm>`__ to do     |
|                                   |                                     |this.                                   |
+-----------------------------------+-------------------------------------+----------------------------------------+

.. _prevent-Oracle:

Preventing Problems
-------------------

Here are some guidelines to help you prevent similar problems in the future.

* Make sure you’ve read and understood the relevant Qubole and Cloud documentation, in particular:
    * :ref:`configuring-clusters`
    * `Setting Up Your Tenancy <https://docs.us-phoenix-1.oraclecloud.com/Content/GSG/Concepts/settinguptenancy.htm>`__
    * `Networking Service <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Concepts/overview.htm>`__
    * `Security Lists <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Tasks/managingsecuritylists.htm>`__
*  Make sure you have :ref:`configured <oracle-configure>` each subnet so as to allow communication among all nodes.
*  Make sure you have
   `whitelisted <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Tasks/managingsecuritylists.htm>`__
   port 22 for the QDS NAT (52.44.223.209).
*  Make sure that starting the cluster will not put you over the `limit
   <https://docs.us-phoenix-1.oraclecloud.com/Content/General/Concepts/servicelimits.htm>`__ for your tenancy.

