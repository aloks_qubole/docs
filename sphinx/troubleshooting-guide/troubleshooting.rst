.. _troubleshooting.rst:

============================================================
Troubleshooting Query Problems -- Before You Contact Support
============================================================
To make sure your problem is resolved as quickly as possible, you should gather all the pertinent information
before you :ref:`create a support ticket <qds-helpcenter.rst>`. The following checklist will help you organize
this information:

- *Fill in mandatory fields in the ticket*.
- *What is the account ID which is impacted*?

  You can find this on the My Accounts page of the Control Panel.

- *Which command ID(s) or Notebook ID(s) are impacted*?

  You can find it on respective QDS UI.

- *Can we rerun command or Notebook Paragraph in case of failure*?  If not, please state the reason in the ticket description.

- *Which environment is the issue occurring in*?

  You can share the URL in case you do not know the environment.

- *Is the issue happening intermittently or always*? If intermittent, provide successful and failed command ID(s).

- *Was it running before successfully*?

- *Have there been any changes to your environment lately which could contribute to the issue*?  *If so, what did you changed in your query environment (QDS, Cloud, and so on) before you ran this query*?

- *What troubleshooting steps (if any) have you tried*?

- *What is the business impact of this problem*? Provide a description to further describe how it matches the selected ticket priority.

  For example:

  * Impact on internal SLA or production workloads?
  * Impact on new development with <deadline> (please specify)?
  * Testing only (low urgency)?

-  *Provide reference of previous tickets with the same issue if you have raised them before*.
-  *If there are any other observations about this issue, please provide details*.