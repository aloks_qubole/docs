.. _QDS-clusters-azure:

Configure Default QDS Clusters
==============================
Each account includes pre-defined clusters for Hadoop2, Spark and Presto. You can use these to run your jobs,
but first you need to modify them to add the storage account, virtual network, and instances types you want to use.

Choose **Clusters** from the **Control Panel** in the QDS UI and do the following for each cluster type you plan to use:

1. Click Edit in the row for the cluster type you want to use:

    .. image:: ../images/QDS-update-clusters.jpg

2. Under the Configuration tab:

   * Choose the **Master Node Type** (Azure instance type) and **Worker Node Type**.

   * Choose the minimum and maximum number of nodes the cluster can run at any one time.

      .. image:: ../images/QDS-update-clusters-2.jpg
  
3. Under the **Advanced Configuration** tab:

   * Set the cluster Location (the Azure region the cluster instances will run in).

   * Set the Managed Disk Type – Standard or Premium (Premium disks are SSDs).

   * Set the Data Disk Size (in gigabytes; default is 256). You can let the Data Disk Count default to zero for now.

   * Add the Virtual Network Resource Group and Subnet from :ref:`Azure Step 3 <Azure-steps-3>`.


     .. image:: ../images/QDS-update-clusters-3.jpg

**Setup is complete**. Now you can :ref:`run your first query <data-analytics>`.

