.. _azure-scheduler:

=====================================
Creating a Schedule through Scheduler
=====================================
On the QDS UI, navigate to **Scheduler** to view any existing schedule and create a schedule.

For information on how to create a schedule, see :ref:`create-new-schedule`.