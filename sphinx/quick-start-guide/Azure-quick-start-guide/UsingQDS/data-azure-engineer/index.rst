.. _data-azure-engineer-index:

================
Data Engineering
================

.. toctree::
   :maxdepth: 1
   :titlesonly:

   azure-data-engineering.rst
   azure-scheduler.rst