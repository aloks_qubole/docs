.. _Azure-UsingQDS:

=========
Using QDS
=========

.. toctree::
   :maxdepth: 1
   :titlesonly:

   azure-update-clusters.rst
   azure-data-analytics.rst
   datascience-azure/index.rst
   data-azure-engineer/index.rst