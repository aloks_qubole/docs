.. _notebook-azure:

Notebooks
=========

To use notebooks, perform these steps:

1.	Choose **Notebooks** from the **Control Panel** and click on the link for **Examples**.

      .. image:: ../../images/QDS-Notebooks-intro.jpg

 
2.	Choose the Getting Started notebook:

      .. image:: ../../images/QDS-Notebooks-Getting-Started.jpg

3.	Copy the notebook:

       .. image:: ../../images/QDS-Copy-Notebook-button.jpg


You’ll see a dialog window:

      .. image:: ../../images/QDS-Copy-Notebook.jpg

4.	Give the notebook a new name or let it default.

5.	Choose a location:

      .. image:: ../../images/QDS-Notebooks-browse.jpg

 
Choose the cluster and click Copy:

      .. image:: ../../images/QDS-Notebooks-cluster.jpg


This creates and runs the new notebook. You’ll see a message that the notebook is in read-only mode while the cluster
is starting up. This can take a few minutes.

