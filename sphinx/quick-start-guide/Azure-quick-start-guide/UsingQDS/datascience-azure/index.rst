.. _datascience-azure-index:

============
Data Science
============

.. toctree::
   :maxdepth: 1
   :titlesonly:

   azure-data-science.rst
   azure-note-dashboard.rst