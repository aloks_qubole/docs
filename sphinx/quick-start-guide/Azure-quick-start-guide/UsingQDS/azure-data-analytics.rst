.. _data-analytics:


Data Analytics
==============
Perform these steps to run your first query:

1.	Choose Analyze from the **Control Panel**, and click on the **Workspace** tab.
2.	Choose **Hive Select Query Example** and click **Run**:

      .. image:: ../images/QDS-run-Hive-query.jpg

You should see results like this:

      .. image:: ../images/QDS-Hive-query-result.jpg

After successfully running the query, you may want to learn to :ref:`use QDS notebooks <notebook-azure>`.

If your cluster fails to start with the message

``The subscription is not registered to use namespace Microsoft.Compute``

the cause is probably that this is a new subscription that has not yet been registered to use ``Microsoft.Compute``.
You should be able to fix the problem by bringing up a new virtual machine (VM) manually from the
`Azure Portal <https://portal.azure.com/>`__.