.. _data-store-azure:

Connecting to a Data Store
==========================
Through the QDS UI, you can access the object storage and the Qubole Hive Metastore, and add a data store.

Accessing the Object Storage
----------------------------
On the QDS UI, navigate to the **Explore** page and pull the drop-down list to see the object storage. It is Blob store or
the Azure Data Lake store that you have configured. For more information, see :ref:`handling-data-index`.

Adding a Data Store
-------------------
On the QDS UI, navigate to the **Explore** page and pull the drop-down list and click **Add a Data Store** to connect to
the data store. For more information, see :ref:`db-tap`.