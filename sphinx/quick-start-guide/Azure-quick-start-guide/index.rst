.. _azure-quick-start-guide-index:

#######################
Azure Quick Start Guide
#######################

The topics in this section are intended to give you a quick introduction to the Qubole Data Service (QDS) on
`Microsoft Azure <https://azure.microsoft.com/en-us/>`__:

:ref:`Connect Qubole to Your Azure Account <qubole-connect-azure>`
----------------------------------------------------------------------


.. toctree::
    :maxdepth: 3
    :titlesonly:

    QuboleConnect/index.rst

:ref:`Connect to a Data Store <data-store-azure>`
--------------------------------------------------------

* :ref:`data-store-azure`


:ref:`Use QDS <Azure-UsingQDS>`
---------------------------------

.. toctree::
    :maxdepth: 3
    :titlesonly:

    UsingQDS/index.rst



For more comprehensive information, see:

* :ref:`admin-guide-index`
* :ref:`user-guide-index`
* :ref:`faqs`

