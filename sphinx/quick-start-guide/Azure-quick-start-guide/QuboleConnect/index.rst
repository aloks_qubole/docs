.. _qubole-connect-azure:

=======================================
Connecting Qubole to your Azure Account
=======================================

.. toctree::
   :maxdepth: 1
   :titlesonly:

   azure-quick-guide.rst
   detailed-azure-qubole-steps/index.rst

