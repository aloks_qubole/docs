.. _quick-guide:

Quick Guide
===========

Steps 1 and 2 below provide a quick overview of the process.

Step 1: Azure Portal
--------------------

a.	Navigate to the `Azure Portal <https://portal.azure.com/>`__.
b.	Follow the instructions in :ref:`Getting Set Up on Azure <Azure-steps>` (in this Quick Start Guide). Or you can use the `Azure documentation <https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal>`__ if you prefer.

.. note:: When prompted for the SIGN-ON URL and APP ID URI enter *https://azure.qubole.com*.

c.	Make a note of the following credentials:

    * *Account:* **Application ID** (also known as Client ID), **Authentication Key**, and **Tenant ID**.
    * *Storage:* EITHER:

      *	For Azure Data Lake: **Storage Client ID**, **Storage Client Secret**, **Storage Tenant ID**, and **Subscription ID**; OR
      *	For Azure Blob Storage: storage account **Name** and the **Access Keys**.

Step 2: QDS Account Settings
----------------------------

a.	Log in to the QDS UI and navigate to Account Settings.
b.	On the Account Settings page, scroll down to Storage Settings.

    *	For **Data Lake**: Add the **Storage Client ID**, **Storage Client Secret**, **Storage Tenant ID**, and **Subscription ID**.
    *	For **Blob Storage**: Add the **Storage Account Name**, container (**Default Location**) and a **Storage Access Key**.

c.	Scroll down to the **Compute Settings** and add the **Compute Client ID** (also known as Application ID), **Compute Client Secret** (also known as Authentication Key) and **Compute Tenant ID**.
d.	Check the box to Push Compute Settings to all clusters.

More details :ref:`here <QDS-steps>`.

