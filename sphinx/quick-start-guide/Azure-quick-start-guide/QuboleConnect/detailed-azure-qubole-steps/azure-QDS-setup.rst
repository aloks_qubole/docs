.. _QDS-steps:

Qubole Step (5)
===============

.. note:: If you are following the detailed steps, the steps below follow on from Steps 1-4 in the :ref:`Azure Setup <Azure-steps>`.

.. _QDS-steps-5:

Step 5 (QDS): Configure QDS Account Settings
--------------------------------------------


.. _QDS-steps-5a:

Step 5a: Add the storage account, container and storage keys
............................................................

Log in to the `QDS UI <https://azure.qubole.com>`__ and navigate to **Account Settings**. On the Account Settings page, scroll
down to **Storage Settings**.

You need to enter information for *either* Azure Blob storage *or* Azure Data Lake storage:

*EITHER:* **For Azure Blob storage:**

  .. image:: ../../images/QDS-Azure-Blob-storage.jpg

Add the **Storage Account Name**, container (**Default Location**) and a **Storage Access Key** from :ref:`Step 4 <Azure-steps-4>`.
 
*OR:* **For Azure Data Lake:**

  .. image:: ../../images/QDS-Azure-Data-Lake.jpg


For **Storage Client ID**, **Storage Client Secret**, and **Storage Tenant ID**, use the values you saved in
:ref:`Step 1b <Azure-steps-1b>`. You will use the same values for **Compute Settings** (:ref:`Step 5b <QDS-steps-5b>`).
 
.. _QDS-steps-5b:

Step 5b: Add the Client ID, Authentication Key and Tenant ID
............................................................

Add the **Compute Client ID** (also known as Application ID), **Compute Client Secret** (also known as Authentication Key),
and **Compute Tenant ID** from :ref:`Step 1b <Azure-steps-1b>`.

.. _QDS-steps-5c:

Step 5c: Specify the Default Resource Group
...........................................

You need to specify the default Azure **Resource Group** in which QDS will bring up your clusters. Proceed as follows:

1. Set the **Default Resource Group** field set to **Use Existing**.

2. Choose the Resource Group you specified in :ref:`Step 1c <Azure-steps-1c>`:

  .. image:: ../../images/QDS-choose-RG.png


3. Click **Save**.

You have configured your QDS account. Now proceed to :ref:`updating your clusters <QDS-clusters-azure>`.
