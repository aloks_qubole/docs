.. _detailed-azure-qds-setup-index:

##############
Detailed Guide
##############

.. toctree::
    :maxdepth: 1
    :titlesonly:

    azure-Azure-setup.rst
    azure-QDS-setup.rst
