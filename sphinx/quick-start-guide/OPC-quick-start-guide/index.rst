####################################
Oracle OCI Classic Quick Start Guide
####################################

For a quick introduction to the Qubole Data Service (QDS) on
`Oracle OCI Classic <https://cloud.oracle.com/home>`__, start here:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    OPC.rst
