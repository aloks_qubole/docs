.. _Oracle-quick-start-guide-index:

############################
Oracle OCI Quick Start Guide
############################

For a quick introduction to the Qubole Data Service (QDS) on
`Oracle OCI <https://cloud.oracle.com/en_US/bare-metal>`__, start here:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    oracle.rst

The following topics provide more detailed information.

    * Platform Support:

      * :ref:`os-version-support`

    * For administrators:

      * :ref:`users-and-accounts`
      * :ref:`manage-sessions`
      * :ref:`auto`
      * :ref:`modify-Oracle-cluster`
      * :ref:`troubleshoot-cluster-startup-Oracle`
      * :ref:`nodebootstrapscript`
      * :ref:`managing-access-permissions`
      * :ref:`using-saml-sso`
      * :ref:`run-utility-commands-in-cluster`
      * :ref:`set-data-compression`
      * :ref:`shade-java-with-maven`

    * For users:

      * :ref:`hive-qubole-index`
      * :ref:`hive-analyze-data`
      * :ref:`compose-hive-query`
      * :ref:`hive-data-export`
      * :ref:`spark-qubole-index`
      * :ref:`compose-spark-command`
      * :ref:`notebook-index`
      * :ref:`hadoop-2-important-parameters`
      * :ref:`hadoop-mapreduce-configuration`
      * :ref:`compose-hadoop-query`
      * :ref:`compose-shell-command`
      * :ref:`compose-dataexport`
      * :ref:`compose-data-import`
      * :ref:`compose-dbquery`
      * :ref:`compose-workflow`
      * :ref:`download-complete-raw-result`
      * :ref:`scheduler-qubole-index`
      * :ref:`performance-monitoring-ganglia`

    * Getting help:

      * :ref:`diagnose-and-fix-Oracle`
      * :ref:`troubleshooting.rst`
      * :ref:`qds-helpcenter.rst`
