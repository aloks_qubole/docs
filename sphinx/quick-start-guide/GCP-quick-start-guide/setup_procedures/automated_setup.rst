.. _automated_setup:

##########################################
Automated Setup of a Qubole Account on GCP
##########################################

This section describes the automated process for setting up a Qubole account on GCP.

******************************
Required Permissions for Setup
******************************

* Default Storage Location (**defloc**): QDS must have read/write access to a default storage location in Google Cloud Storage where you want QDS to save log files and write the results of the queries you run. You will enter the **defloc** location during the setup process.

* QDS must have read access to the buckets in Cloud Storage where you will store the data you want to process with QDS.

* To perform the automated account setup, certain permissions must be assigned to the Qubole service account (QSA), as described in :ref:`step f of the Setup Process <step-f>` below.

.. note:: For information on creating a service account, see `Creating and managing service accounts <https://cloud.google.com/iam/docs/creating-managing-service-accounts>`__ in the GCP documentation. For information on assigning roles to a service account, see `Granting roles to service accounts <https://cloud.google.com/iam/docs/granting-roles-to-service-accounts>`__ in the GCP documentation.

*************
Setup Process
*************

1. In the QDS UI, go to **Control Panel** > **Account Settings** > **Access Settings**. For **Access Mode Type**, select **Automated**.

2. In this step, you will assign the required GCP permissions for your QDS account.

   a. Log in to the GCP console and navigate to the **IAM & admin** page (`https://console.cloud.google.com/iam-admin/iam <https://console.cloud.google.com/iam-admin/iam>`__).
   b. At the top of the IAM & admin page, select the project that you want to associate with your QDS account.
   c. Click the **Add** button to assign an IAM policy to the project.

      .. image:: images/40-gcp-console-IAM-page.png

   d. In the QDS UI, go to **Control Panel > Account Settings > Access Settings**, and copy the Qubole service account's (QSA) email address:

       .. image:: images/41-autosetup-sa-eamail.png

   e. Paste the copied email address into the **New members** text box on the **IAM & admin** page in the GCP console:

       .. image:: images/42-gcp-new-member-field.png

.. _step-f:

   f. Add the roles or permissions below on the Qubole service account (QSA). You can either assign predefined GCP roles or create custom granular roles. Note that when you assign predefined roles, you might be assigning broader permissions than what QDS requires. If any of the required permissions are missing, however, account setup may fail.

      i. Predefined roles:

         1. **Service Account Admin:** This role includes permissions for working with service accounts.
         2. **Project IAM Admin:** This role contains permissions to access and administer a project's IAM policies.
         3. **Storage Legacy Bucket Owner/Storage Admin:** At least one of these two roles should appear in the IAM section of your GCP console. Add either of these roles to provide read and write access to existing buckets with object listing, creation, and deletion.
         4. **Role Administrator role:** Create custom roles.

      ii. Custom granular roles:

       To apply granular permissions, you must first create a custom role and then assign it to the Qubole service account (QSA). For information about creating custom roles, see `Creating and managing custom roles <https://cloud.google.com/iam/docs/creating-custom-roles>`__ in the GCP documentation. Include the following permissions in your custom role:

          1. iam.roles.create
          2. iam.roles.delete
          3. iam.roles.get
          4. iam.roles.list
          5. iam.roles.undelete
          6. iam.roles.update
          7. iam.serviceAccounts.create
          8. iam.serviceAccounts.delete
          9. iam.serviceAccounts.get
          10. iam.serviceAccounts.getIamPolicy
          11. iam.serviceAccounts.list
          12. iam.serviceAccounts.setIamPolicy
          13. iam.serviceAccounts.update
          14. resourcemanager.projects.get
          15. resourcemanager.projects.getIamPolicy
          16. resourcemanager.projects.list
          17. resourcemanager.projects.setIamPolicy
          18. storage.buckets.getIamPolicy
          19. storage.buckets.list
          20. storage.buckets.setIamPolicy

       iii. Click **Save** to complete the assigning of the IAM permissions.

    g. It might take a few seconds for the permission changes to take effect.

3. In the QDS UI, go to **Control Panel > Account Settings > Access Settings** and reload the page.

4. From the **Projects** dropdown, select the project ID for the project to which you gave IAM permissions to the Qubole service account (QSA).

5. In the **Default Location** field, enter the name of the bucket (without the prefix (``gs://``) that will serve as your default location (defloc) in Cloud Storage.

6. Provide a comma-separated list of data buckets (without the ``gs://`` prefix) to read data from the **Data Bucket(s)** field. You can provide up to a maximum of five data buckets.

7. Click **Save**. Appropriate error messages are displayed if there are errors.

8. Validation of credentials after **Save**:

    * If your settings were saved successfully, you will see a message at the top of the page saying, “Please wait while we validate your settings. It may take up to a few minutes." Upon completion of the validation, your account will be fully operational.

    * Qubole validates your settings in the background, allowing you to use the application while the settings are being validated, but you will not be allowed to update the access settings or perform operations that interact with GCP, such as starting a cluster.

    * Validation may take up to 5 minutes.

    * If validation is successful, you will see green check marks in the **Access Settings** section next to the **Default Location** and **Data Bucket(s)** fields. If validation fails, you will see a red X next to the respective field(s).

9. Troubleshooting:

    a. Try re-saving the access settings.

    b. If the problem persists, contact Qubole Support.

10. Changing project ID for the QDS account:

    a. If you update your project ID in the QDS **Access Settings** UI, you must assign QSA the required permissions (as described above) again on the new project.

    b. The project can be changed when there are no running clusters.

.. note:: In automated setup (as with manual setup), Qubole creates two custom roles in your project: ``qbol_compute_role`` and ``qbol_storage_role``. Do not modify or delete these roles from the project as doing so might lead to unexpected behavior.