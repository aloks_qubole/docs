.. _setup_procedures_index:

#####################################
Setting up your Qubole Account on GCP
#####################################

There are two ways to set up Qubole on GCP:

* **Automated setup:** The automated setup procedure makes it easy to get started with Qubole on GCP. Qubole automatically creates the service accounts you need and assigns the required roles and permissions needed to run your Qubole workloads on GCP.

* **Manual setup:** The manual setup procedure provides a guided experience to setup your account using a script provided by Qubole that you can download and execute to setup your account.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    automated_setup.rst
    manual_setup.rst

