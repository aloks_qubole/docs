.. _gcp-spark-query:

Running a Spark Query
=====================

Perform the following steps to run a Spark query:

1. If you don't have a Spark cluster running, go to the **Clusters** page and create a Spark cluster by clicking **New** and clicking the Spark icon.

2. Start your Spark cluster.

3. Go to the **Analyze** page in the QDS UI.

4. Choose the Spark engine and your Spark cluster, then click on the link for **Examples** and click the **Python Pi Example**:

   .. image:: ../images/36-QDS-Spark-query.png

5. Click **Run** to run the **Python Pi Example** query:

  .. image:: ../images/37-QDS-run-Spark-query.png

6. The result will be displayed in the **Results** tab when the query has finished running:

 .. image:: ../images/38-QDS-Spark-query-results.png
