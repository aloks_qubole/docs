.. _gcp-hive-query:


Running a Hive Query
====================

Perform the following steps to run a Hive query:

1. On the **Cluster** page in the QDS UI, make sure the cluster you want to use is running. If the cluster is stopped, click **Start**, or click **New** to create a new Hive cluster.

2. On the **Analyze** page in the QDS UI, select the Hive engine and the cluster you want to use.

3. Click **Examples**, **Hive Select Query Example**, and then click **Run** or **Run Again**.

   .. image:: ../images/31-GCP-new-Hive-query-example.png

When the Hive query is completed, you should see results like this:

   .. image:: ../images/33-GCP-Hive-results.png
