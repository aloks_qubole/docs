.. _gcp-notebook:

Using a Notebook
================

To use notebooks, perform these steps:

1. If you don't have a Spark cluster running, go to the **Clusters** page and create a Spark cluster by clicking **New** and clicking the Spark icon.
2. Start your Spark cluster.
3. Open the **Notebooks** page in the QDS UI, and click on the link for **Examples**.
4. Click **Getting Started** > **Getting Started**:

   .. image:: ../images/34-QDS-Notebooks-Examples.png

5. Click **Copy Notebook**. In the dialog window, give the notebook a new name or leave the default name. Choose a location or leave the default location. Choose the cluster and click **Copy**.

   .. image:: ../images/35-QDS-new-Copy-Notebook.png

6. This creates and runs the new notebook. You’ll see a message that the notebook is in read-only mode while the cluster is starting up. This can take a few minutes.

   .. image:: ../images/30-QDS-Notebooks-cluster.jpg

7. When your notebook is ready, you can use it to increase your knowledge of Notebooks:

   a. Look at the many examples included in the **Examples** section of the **Notebooks** page to help you get started quickly.
   b. For more information on managing Notebooks, see the Qubole documentation section `Notebooks <https://docs.qubole.com/en/latest/user-guide/notebooks-and-dashboards/notebooks/index.html>`__.
