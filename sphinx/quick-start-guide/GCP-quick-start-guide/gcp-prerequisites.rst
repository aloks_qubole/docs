.. _gcp-prerequisites:

########################
Prerequisites and Signup
########################

=============
Prerequisites
=============

Ensure that you have the following prerequisites before beginning the setup process:

* **Google account:** You must have a Google account to begin the setup process. You will use this Google account to sign up for QDS on GCP.

* **GCP project:** You must have a GCP project that is tied to a valid billing account to use QDS on GCP. All of the GCP resources you create to use with QDS will be contained within this project. You can either create a new project or use an existing project. For information on creating a project, see `Creating and Managing Projects <https://cloud.google.com/resource-manager/docs/creating-managing-projects>`__ in the GCP documentation.

.. note:: In the current version of QDS on GCP, each QDS account can be associated with only one GCP project.

* Your GCP project must have the following APIs enabled:

    * Compute Engine API
    * Cloud Resource Manager API
    * Identity and Access Management (IAM) API
    * `Big Query Storage API <https://cloud.google.com/bigquery/docs/reference/storage/>`__ (optionally, to use BigQuery)

* Your GCP project can already have existing service accounts in it, but must have remaining quota to create two more service accounts, as Qubole will create two new service accounts in the project. By default, GCP has a limit of 100 service accounts per project.

=========================
Sign up for Qubole on GCP
=========================

1. Make sure that your GCP project meets the :ref:`prerequisites <gcp-prerequisites>` described above.

2. Click **Sign up with Google** on the signup page:

   .. image:: images/01-SignupPage.png

3. Click the Google account to be used for the signup process:

   .. image:: images/02b-SelectGoogleAccount.png

4. Provide additional details and click **Save And Start Authentication with Google Cloud**:

   .. image:: images/03-ProvideMoreDetails.png

You have signed up for Qubole on GCP. Now let’s setup your Qubole account.
