.. _AWS-quick-start-guide-index:

########################
AWS Quick Start Guide
########################

The topics in this section are intended to give you a quick introduction to the Qubole Data Service (QDS) on `Amazon Web Service (AWS) <https://aws.amazon.com>`__:

.. toctree::
    :maxdepth: 1
    :titlesonly:


    aws.rst
    runquery.rst
    running-hadoop-job.rst
    running-mahout-job.rst
    running-dumbo-job.rst
    run-airflow-command.rst


For more comprehensive information, see the :ref:`admin-guide-index`, the :ref:`user-guide-index`, and the :ref:`faqs`. QDS
functions can also be called programmatically; see the :ref:`rest-api-index`.
