.. _runquery:

Running a Hive Query, Extracting Sample Rows, and Analyzing Data
----------------------------------------------------------------
After authenticating AWS using IAM Keys or IAM Role, perform the following steps:

1. Navigate to the **Analyze** page, click the **Compose** button. A command editor on the right side is displayed with a list of
   commands. By default, **Hive Query** is selected.

   .. image:: images/command-editor.png

2. To run a Hive query, ensure that the **Hive Query** is selected from the drop-down list. In the **Compose** editor
   window, type a simple query. For example:

   .. sourcecode:: sql

    show tables;

3. Click **Run** to execute the query.
4. The Results are shown in the **Results** tab.

   .. note:: If the query is successful, the **Log** tab shows the status of the query as OK and displays the time taken to
             run the query. Also, next to the Query, a green dot indicates that the query **Succeeded**. You can also
             click the History tab to see the query status.

5. To execute another query, click **Compose**. This clears the command window.
6. Now type and execute any other query in the **Compose** editor window. For example:

   .. sourcecode:: sql

    select * from default_qubole_memetracker limit 10;

7. Click **Run** to execute the query.

   .. note:: The query takes a little time if a large amount of data has to be fetched.

8. To analyze the data, for example to find the total number of rows in a table corresponding to August 2008, submit the
   following query:

   .. sourcecode:: sql

    select count(*) from default_qubole_memetracker where month="2008-08";

   .. note:: This query is more complex than the previous queries and requires additional resources. In the background,
             Qubole Data Service provisions a Hadoop cluster.

   This can take a couple of minutes. When the query is being processed, the status of the query is shown with a spinning
   circle that indicates that it is in progress. Once it is processed successfully, the query result is displayed in the
   **Results** tab.

**Congratulations!** You have just executed your first Hive query on the Qubole Data Service.
Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__. The Qubole support team gets back to you
and help you on board.