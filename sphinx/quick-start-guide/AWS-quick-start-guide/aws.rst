.. _aws-quick-start-guide-ctabs:

##################################
Setting-up the Qubole Data Service
##################################

Prerequisites
-------------

* An AWS account (Sign up for a free account `here <https://aws.amazon.com/free/>`__) and relevant permissions to create roles and policies
* An AWS S3 bucket for a default storage location (Create one `here <https://s3.console.aws.amazon.com/s3/home>`__)
* A QDS account (Sign up for a free account `here <https://us.qubole.com/users/sign_up>`__)

.. image:: images/infographic.png

Click the respective :ref:`tab <tabs>` (below) to follow the instructions under each of these steps.

.. _tabs:

.. content-tabs::

    .. tab-container:: tab1
        :title: Copy Credentials from QDS

        1. On the Home page, click **Account Settings** under **Control Panel**.

           .. image:: images/1.png

           Scroll down to the Access Settings section.
        2. From the Access Settings section, select **IAM Role**.
        3. Copy the **Trusted Principal Account ID** and **External ID**.

            .. image:: images/AccessSettings-IAMRoles.png


    .. tab-container:: tab2
        :title: Create IAM Policies on AWS

        1. On the AWS Console, enter IAM in the search bar and click **Enter**.
           The Identity and Access Management Dashboard page appears.
        2. On the left pane, click **Policies**.
        3. On the right pane, click **Create policy**.
           This opens on a separate page.
        4. On the Create policy page, click the **JSON** tab.
        5. Copy and paste a JSON file from `this <https://docs.qubole.com/en/latest/user-guide/managing-accounts/aws/iam-roles/create-iam-roles.html#creating-an-aws-ec2-policy>`__ page.
        6. Click **Review policy**.

            .. image:: images/createpolicy.png

        7. In the **Name** field, enter the name you wish to save the policy with.
        8. Click **Create policy**. This creates your AWS EC2 policy.
        9. Repeat steps 2 through 7 using `this <https://docs.qubole.com/en/latest/user-guide/managing-accounts/aws/iam-roles/create-iam-roles.html#creating-an-aws-s3-policy>`__  JSON code to create the AWS S3 policy.

        .. note:: Remember to replace **<bucketpath>** in the JSON text to match your AWS S3 bucketname (from the prerequisites).


    .. tab-container:: tab3
        :title: Create IAM Roles on AWS

        1. On the left pane, click **Roles**.
        2. On the right pane, click **Create role**.
           The Create role page appears.
        3. Click **AWS service**, and then select **EC2**. Click **Next: Permissions**.

            .. image:: images/selectec2.png

        4. In the search field beside Filter policies, search for and add both the polices you have created.

            .. image:: images/attachpolicies.png

        5. Click **Next: Tags**.
        6. Add any tags required (**optional**),  click **Next: Review**.
        7. Enter a role name and click **Create role**.
        8. On the Roles page, select the newly created role.

            .. image:: images/EditTrustRelationship.png

        9. Click the **Trust Relationship** tab, then click **Edit trust relationship**.
        10. Copy and paste `this <https://docs.qubole.com/en/latest/faqs/general-questions/policy-use-qubole-use-my-iam-credentials.html#sample-trust-relationship-policy>`__ JSON text.

             .. note:: Remember to replace **<AccountID>** and **<ExternalID>** in the JSON text to match the ones you saved in step 3 of **Copy Credentials from QDS**.

        11. Click **Update trust policy**. This takes you back to the Summary page.
        12. Copy the **Role ARN**.

    .. tab-container:: tab4
        :title: Link AWS & QDS accounts

        1. On the Home page, click **Account Settings** under **Control Panel**.
        2. Scroll down to the Access Settings section and select **IAM Role**.
        3. In the Role ARN field, paste the Role ARN that you copied in step 12 of the **Create IAM Roles on AWS**.
        4. In the Default Location field, enter the AWS S3 bucket path you had created and **Save**.

           .. image:: images/defloc.png

        That’s it! Your account is set up. :ref:`Run <runquery>` your first query.

|


Go back to :ref:`Tabs <tabs>`.

|


