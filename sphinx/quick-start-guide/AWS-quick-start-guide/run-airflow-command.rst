.. _run-airflow-command:

===================================================
Setting up an Airflow Cluster and an ETL in Airflow
===================================================
Airflow is a platform for programmatically authoring, scheduling, and monitoring workflows. It supports integration with
third-party platforms. You can author complex directed acyclic graphs (DAGs) of tasks inside Airflow. It comes packaged
with a rich feature set, which is essential to the ETL world. The rich user interface and command-line utilities make it
easy to visualize pipelines running in production, monitor progress, and troubleshoot issues as required. See
:ref:`airflow-introduction` for more information.

Creating a Data Store
---------------------
Airflow uses a data store to maintain status of jobs/tasks and other related information. Qubole recommends using a
persistent data store for for archiving in a history and recovery purpose. See :ref:`set-up-data-store` for more
information. See :ref:`db-tap` for more information on how to create a data store using the QDS UI and
:ref:`create-a-dbtap` to create a data store using a REST API call.

Creating an Airflow Cluster
---------------------------
See :ref:`configuring-airflow-cluster` to create a cluster using the QDS UI and :ref:`create-airflow-cluster` to create a
new cluster using a REST API call.

Registering a DAG within Airflow
--------------------------------
You can submit Airflow commands through a QDS Shell command on an Airflow cluster. You can execute all Airflow commands
available in CLI through the shell command. See `Qubole Operator DAG <https://github.com/apache/incubator-airflow/blob/master/airflow/contrib/example_dags/example_qubole_operator.py>`__
to author a DAG using Qubole Operator.

See :ref:`register-airflow-dag` for more information.


