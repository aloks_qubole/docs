.. _quick-start-guide-index:

#################
Quick Start Guide
#################

This Guide provides a quick introduction to the Qubole Data Service (QDS). For more
comprehensive information, see the :ref:`admin-guide-index`, the :ref:`user-guide-index`, and the :ref:`faqs`. QDS
functions can also be called programmatically; see the :ref:`rest-api-index`.

The Guides for each platform are:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    AWS-quick-start-guide/index.rst
    Azure-quick-start-guide/index.rst
    GCP-quick-start-guide/index.rst
    Oracle-quick-start-guide/index.rst
    OPC-quick-start-guide/index.rst
..    gce.rst
..    old-AWS-quick-start-guide/index.rst


