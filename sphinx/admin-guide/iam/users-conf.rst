.. _manage-users:

Managing Users
==============
Use this tab to add and manage users in an account.

You can add users using any of the following three methods:

* Use the **Manage Users** in the **Control Panel** as described in `Managing Users through the Control Panel`_.
* Use the API as described in :ref:`users-api`.
* Sign up as a new user and own a QDS account as described in :ref:`signup-qds`.

Understanding How Qubole Adds a New User
----------------------------------------
Qubole adds you into a QDS account by either of the following ways:

* When you sign up as a new user:

  1. Qubole sends you an email with a confirmation link.
  2. After you click the confirmation link, Qubole activates you as a new user and activates your account as well.

* When you invite a user to a QDS account:

  1. Qubole sends the owner of that QDS account an email to approve the new user.
  2. The owner of that QDS account or system-admin approves the new user in the **Manage Users** > **Pending Users** tab.

Managing Users through the Control Panel
----------------------------------------
To manage users, choose **Manage Users** in the **Control Panel**:

.. image:: ../../user-guide/ug-images/ManageUsers.png

* Click **Action** to modify or disable a user's access.

* Click the user icon |InviteUser| to invite a new user.

  .. |InviteUser| image:: ../../user-guide/ug-images/AddUserIcon.png

  The following page appears:

  .. image:: ../../user-guide/ug-images/InviteUser.png

  Enter the user's email address and select a group from the available **Groups**.
  Click **Send** to send the invitation, or **Cancel** to change or discard it.


Managing Users through the API
------------------------------
Qubole has introduced a new Service user type. This user is automatically validated and immediately added to the account.
Any user having user management permissions can create a Service user and update the authentication token. However, note that
these users cannot login from the UI and can be used only through APIs. Additionally, they cannot be added as regular users in
any other account after being created as a Service user.