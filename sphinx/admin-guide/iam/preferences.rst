.. _manage-preferences:

Managing Preferences
====================
To edit your account preferences, click **My Preferences** on the **Control Panel** page. The **My Preferences** page
is displayed as shown below.

.. image:: ../../user-guide/ug-images/MyPreferences.png

The **Keyboard shortcuts** option under the **General** section allows you to enable/disable keyboard shortcuts. By default,
the option is enabled. Currently, only the :ref:`Scheduler <scheduler-keyboard-shortcuts>` and :ref:`Analyze <analyze-shortcuts>`
pages support keyboard shortcuts.

The **Command permissions** drop-down list under the **General** section lets you specify whether a command is public (select
the **Anyone in this account** option) or private (select the **Only me** option). This lets you control which command runs other
users in the account have access to. By default, the **Anyone in this account** option is enabled. You can overwrite these settings
from the **Analyze** page for a particular command run.

.. image:: ../../user-guide/ug-images/History1.png

The **Composer Auto-suggestions** option under the **Analyze Interface** section allows you to enable/disable Composer
auto-suggestions. By default, the option is enabled. You can also enable this feature from the **Analyze** page for SQL commands
only.
