.. _manage-profile:

Managing Profile
================
To edit your profile and change the password, click **My Profile** in the **Control Panel** page. The **My Profile** page
is displayed as shown in the following figure.

.. image:: ../../user-guide/ug-images/MyProfile.png

Modify the name in the **Full Name** text field. To change the password as well, click **Change Password**;
otherwise, click **Save**.

When you click **Change Password**, you get additional text fields to change
the password, as shown in the following figure.

.. image:: ../../user-guide/ug-images/MyProfileCPwd.png

Fill in the **Current Password** and **New Password** text fields. Make sure your password meets the requirements
described :ref:`below <password-renewal>`.

Confirm the new password in the **Confirm Password** text field. Click **Save** to save your name and password changes.

.. _password-renewal:

Handling QDS Account Password Expiration and Renewal
----------------------------------------------------

After 6 months, your password expires and a new password is required. QDS sends periodic alerts about
password expiration, prompting you to renew the password.

If you forget to change the password before its expiration and try to log in, Qubole redirects you to the
**Reset Password** page.

Qubole has added a password strength indicator on the **Sign Up**, **Change Password**, and **Forgot Password** pages. The password must contain:

* A minimum of 8 characters
* At least one alphabet in uppercase
* At least one special character (all special characters are accepted)
* At least one number

Here is an example of the password strength indicator on the **Change Password**  page.

.. image:: ../../user-guide/ug-images/PasswordStrength.png

Here is an example of the password strength on the **User Activation**  page.

.. image:: ../../user-guide/ug-images/UserActivation.png

