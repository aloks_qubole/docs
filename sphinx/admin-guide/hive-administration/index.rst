.. _hive-administration:

===================
Hive Administration
===================
This section contains information on administering the Hive service. It is intended for system administrators and users managing Hive in QDS.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    configure-hive-server2.rst
    hive-cluster-master.rst
    hive-authorization.rst
    use-hive-authorization.rst

For more information, see the :ref:`hive-qubole-index`.