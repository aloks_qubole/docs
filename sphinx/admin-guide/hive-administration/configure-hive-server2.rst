.. _configure-hive-server2:

=======================================
Configuring a HiveServer2 Cluster (AWS)
=======================================
QDS supports `HiveServer2 <https://cwiki.apache.org/confluence/display/Hive/HiveServer2+Overview>`__ on Hadoop 2 clusters
(:ref:`Hive versions <use-hive-versions>` 1.2 and later). QDS uses
`Beeline <https://cwiki.apache.org/confluence/display/Hive/HiveServer2+Clients#HiveServer2Clients-Beeline–CommandLineShell>`__
as the client.

Enable HiveServer2 in the QDS UI, in the **Hive Settings** section under the **Advanced Configuration** tab of the **Clusters** page.

Enabling HiveServer2
--------------------

All queries run on a HiveServer2-enabled cluster use HiveServer2. If you want to enable HiveServer2 only for a
specific query, without enabling it on a cluster, add this to the query:

.. sourcecode:: bash

   set hive.use.hs2=true;

When HiveServer2 is enabled, all Hive queries are executed on the cluster Master node, including queries running DDL
statements such as ``ALTER TABLE RECOVER PARTITIONS``.

.. note:: Once Qubole has enabled Hive Authorization in your account:

          * QDS sets ``hive.security.authorization.enabled`` to ``true``, and adds it to Hive's `Restricted List <https://cwiki.apache.org/confluence/display/Hive/Configuration+Properties#ConfigurationProperties-Restricted/Hidden/InternalListandWhitelist>`__.
            This prevents users from bypassing Hive authorization when they run a query.
          * If you want to change the setting of ``hive.security.authorization.enabled`` at the cluster level, you can do so in the QDS UI: set it in the **Override Hive Configuration** field in the **Hive Settings** section under the **Advanced Configuration** tab of a Hadoop 2 (Hive) cluster, then restart the cluster.
          * To change the setting at the account level, `create a Qubole support ticket <https://qubole.zendesk.com/hc/en-us>`__.


Verifying that Hive Queries are using HiveServer2
-------------------------------------------------
To verify that queries are being directed to HiveServer2, look in the query logs for statements similar to the following:

.. sourcecode:: bash

   2016-11-08 04:19:22,485 INFO hivecli.py:412 - getStandaloneCmd - Using HS2
   Connecting to jdbc:hive2://<master-dns>:10003
   Connected to: Apache Hive (version 1.2.0)
   Driver: Hive JDBC (version 1.2.0)
   Transaction isolation: TRANSACTION_REPEATABLE_READ

Understanding Memory Allocation to HiveServer2
----------------------------------------------
When HiveServer2 is enabled, QDS reserves memory for it when the cluster starts, allocating an approximate value of 25-30%
(depending on the memory given to other daemons on the cluster) of the memory obtained from the YARN ResourceManager to
HiveServer2. This has an impact on how many concurrent queries users can run on the cluster:
QDS configures the concurrency limitation in the node bootstrap script, setting it to 500MB for one query.

HiveServer2 is deployed on the master node, so you should configure a powerful instance type for it; the
total RAM size must be at least 15 GB.

Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ for help if you are not sure about
the optimal configuration.

.. note:: You can enable HiveServer2, with additional settings, through a Hadoop 2 API call as described in :ref:`hive-server-api-parameter`.

.. _hive-metadata-cache:

Understanding Hive Metadata Caching (AWS)
-----------------------------------------

Hive Metadata Caching, supported on on Hadoop 2 clusters and currently available only on AWS, reduces the split computation
time for ORC files currently by caching the meta data required in split computation on Redis running on the master node.
It is very useful when the data contains many ORC files. Qubole plans to extend this feature support to Parquet files
in the near future. Configure it in the QDS UI, in the **Hive Settings** section under the **Advanced Configuration** tab
of the **Clusters** page:

.. image:: hive-images/HiveServer2.png


If you do not see the **Enable Hive Metadata Cache** option for a Hadoop 2 cluster, create a ticket with
`Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable it for your QDS account.

.. note:: As a **prerequisite**, enable :ref:`Hive on Master <hive-on-cluster-master>`
          or :ref:`Hive on HiveServer2 <configure-hive-server2>` before enabling Hive Metadata Cache.

Once metadata caching is enabled on a QDS account, it is enabled by default only on new Hadoop 2 (Hive) clusters;
it remains disabled on existing clusters.

Enabling metadata caching installs a Redis server on the cluster. You can turn caching on and off at the
query level by setting ``hive.qubole.metadata.cache`` to ``true`` or ``false``. You can also add this setting
in the Hive bootstrap script.

You can also enable Hive Metadata caching through a REST API call as described in :ref:`hive-server-api-parameter`.

Setting Time-To-Live in the JVMs for DNS Lookups on a Running Cluster
---------------------------------------------------------------------
Qubole now supports configuring Time-To-Live (TTL) JVMS for DNS Lookups in a running cluster (except Airflow and Presto).
This feature is not enabled by default; create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to
enable it. The recommended value of TTL is ``60`` and its unit is seconds.

