.. _using-saml-sso:

=========================================================
Using SAML Single SignOn and Google Authorization Service
=========================================================
You can log into QDS using your username and password, through the Google authorization service (available OOTB), or through SAML Single
Sign-On (create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__).

You must provide some information to enable the SAML Single Sign-On (SSO) service on your account. See `Enabling SAML Single SignOn Authorization Service`_.

The following figure shows the QDS login page.

   .. image:: ../quick-start-guide/images/Login_screen.JPG


Enabling SAML Single SignOn Authorization Service
-------------------------------------------------

To enable the SAML Single SignOn (SSO) authorization service, contact Qubole customer support and provide the following details:

* SAML Single SignOn endpoint
* SAML Single SignOn cert
* SAML Single SignOn fingerprint
* Email/domain details of users who would be using SAML login credentials to use Qubole.

.. note:: User email addresses need to be provided only if a specific set of users need SAML enabled. SAML is supported at the domain level too.

Qubole uses SAML version 2.0. Qubole's entity ID on SAML service providers such as Okta, OneLogin, and LastPass is
``qubole``. After using the entity ID on the SAML service provider, select **api** or **api2** as the subdomain. After
this, you can figure out the SSO endpoint and the cert/pemfile.

Setup SAML with Custom Qubole Application
-----------------------------------------

You can connect to the Qubole application through multiple SAML service providers such as Okta, OneLogin, LastPass, and so on. For the
https://api.qubole.com endpoint, a Qubole community application exists with most SAML service providers. For providers where such an
application does not exist or if you want to connect to a QDS endpoint other than https://api.qubole.com (See :ref:`qubole-endpoints` ),
you can create a custom application.

Use the following settings to create a custom application with Okta or with your SAML provider:

* Single SignOn endpoint  = <env_endpoint>/saml/callback
* Audience URI (SP Entity ID) = ``qubole``
* Name ID format = <emailaddress>
* Application username = <email>
