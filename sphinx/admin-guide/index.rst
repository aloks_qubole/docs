.. _admin-guide-index:


####################
Administration Guide
####################

This guide explains how to configure and manage Qubole accounts and clusters. It also explains core concepts such as
:ref:`auto-scaling <auto>`.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    intro-admin-guide.rst
    cluster-admin/index.rst
    iam/index.rst
    cost-saving.rst
    saml-sso.rst
    managing-roles-and-access.rst
    enabling-encryption/index.rst
    hive-administration/index.rst
    presto-admin/index.rst
    spark-admin/index.rst
    osversionsupport.rst
    how-to-topics/index.rst
