.. _configuring-your-presto-cluster:

Configuring a Presto Cluster
============================
A single Qubole account can run multiple clusters. By default, Qubole provides a Presto cluster, along with
Hadoop and Spark clusters, for each account.

.. note:: Presto is not currently supported on all Cloud platforms; see :ref:`os-version-support`.

The following topics explain Presto custom configuration and the presto catalog properties:

* :ref:`custom-configuration` that describes:

  - :ref:`jvm-config`
  - :ref:`presto-config-properties`

* :ref:`presto-catalog-properties`

.. note:: QDS provides the Presto Ruby client for better overall performance, such as processing DDL queries much faster
          and quickly reporting errors that a Presto cluster generates. This client is available for Beta access;
          create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable it for
          your QDS account. For more information, see this `blog <https://www.qubole.com/blog/product/presto-ruby-client-in-qds/>`__.

To view or edit a Presto cluster's configuration, navigate to the **Clusters** page and select the cluster with the label **presto**.

Click the edit icon in the **Action** column against a **Presto** cluster to edit the
configuration.

.. note:: Presto queries are memory-intensive. Choose instance types with ample memory for both the master and worker nodes.

You can select the **Presto Version** on the cluster configuration page. These are the supported versions:

* **0.142** is the deprecated version. It is unavailable for spawning new clusters or version change. Although existing
  clusters remain to work until the configuration is changed.
* **0.157** is a stable version.
* **0.180** is the default and it is a stable version.
* **0.193** is the latest stable version.
* **0.208** (beta) is a beta version.


See :ref:`os-version-support` for the latest version information for your platform.

.. note:: Qubole can automatically terminate a Presto cluster with an invalid configuration. This capability is available for
          Beta access; Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable it for your account.

          Check the logs in ``/usr/lib/presto/logs/server.log`` if there is a cluster failure or configuration error. See
          :ref:`differences-presto-hive` for more information about Presto logs.

The following figure shows Hadoop and Presto configuration override for a Presto cluster.

 .. image:: images/cluster-settings.png

On AWS or Azure, select **Enable Rubix** to enable RubiX. See :ref:`using-Rubix` for more information.

See :ref:`manage-clusters` for more information on cluster configuration options that are common to all cluster types.

About Presto System Monitoring
------------------------------
:ref:`presto-system-metrics` describes the list of metrics that can be seen on the Datadog monitoring service. It
also describes the abnormalities and actions that you can perform to handle abnormalities.

Avoiding Stale Caches
---------------------
The cache parameters are useful to tweak if you expect data to change rapidly.

Fo example, if a Hive table adds a new partition, it may take Presto 20 minutes to discover it. If you plan on changing existing
files in the Cloud, you may want to make `fileinfo` expiration more aggressive. If you expect new files
to land in a partition rapidly, you may want to reduce or disable the `dirinfo` cache.