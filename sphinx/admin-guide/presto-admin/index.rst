.. _presto-admin-index:

=====================
Presto Administration
=====================
This section explains the topics related to the Presto administration.

.. toctree::
    :maxdepth: 1
    :titlesonly:


    configuring-presto-cluster
    config-properties
    catalog-hive-properties
    auto-scaling
    spill-disk-config
    presto-server-bootstrap
    connect-to-mysql-jdbc-sources
    best-practices

For more information, see the :ref:`presto-qubole-index`.