.. _auto-scaling-presto:

===============================
Auto-scaling in Presto Clusters
===============================

For information about auto-scaling in Presto clusters, see the section on :ref:`Presto <auto-works-practice-Presto>`
under :ref:`Auto-scaling in Qubole Clusters <auto>`.

.. note:: Presto is not currently supported on all Cloud platforms; see :ref:`os-version-support`.