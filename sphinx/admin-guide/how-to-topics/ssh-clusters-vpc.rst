.. _ssh-clusters_vpc:

================================================
Log into Qubole Clusters Hosted in an Amazon VPC
================================================

.. note:: You must log in as the **ec2-user**.

For logging into Qubole clusters hosted in Amazon EC2, see :ref:`ssh-clusters-amazon-ec2`.

As a prerequisite, ensure that cluster in the VPC contains all settings as described in :ref:`clusters-in-vpc`. Perform
the following steps to log into a cluster hosted in an Amazon VPC through SSH:

1. Navigate to the **Clusters** page and find the server hostname of the running cluster into which you want to log in.
   The following figure shows an example of a cluster page with one running cluster.

   .. image:: ../../user-guide/clusters/images/ClusterResources.png

   For master node details:

   * Click **View Master DNS** on the specific running cluster.

2. Create a :ref:`persistent security group <persistent-security-group>` and specify the SSH port access (port 22) for the machines from which you would
   access a Qubole cluster using SSH. Add the **name** of the persistent security group in the cluster's configuration.
   See :ref:`modify-security-settings` for more information.

3. Add the public SSH key of the machines (from where you would use SSH to access clusters) in the **Customer SSH Key**
   text field on the cluster configuration. See :ref:`modify-security-settings` for more information.

4. On a terminal, run an SSH command as mentioned below:

   ``$ ssh -i <path to your private-key> ec2-user@<master-dns of server>``