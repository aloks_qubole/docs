.. _ssh-clusters-amazon-ec2:

==============================================
Log in to Qubole Clusters Hosted in Amazon EC2
==============================================
You must configure the public key on the cluster to log into it. Set the public key if you have it already. Else,
see `Generating SSH Keys <https://help.github.com/articles/generating-ssh-keys>`__ or
`SSH Key <http://www.ece.uci.edu/~chou/ssh-key.html>`__ or any other online resources to create a pair of keys.

For instructions on logging into Qubole clusters hosted in an Amazon VPC, see :ref:`ssh-clusters_vpc`.

Configuring a Public SSH Key on a Qubole Cluster
------------------------------------------------
Navigate to the **Clusters** page and select the cluster you want to log in to. Perform the following steps:

1. Click the edit button. The **Edit Cluster** page is displayed. See :ref:`modify-clusters` for more information.

2. In the **Security settings** section, type the public key in the **Customer public ssh key** text field. See
   :ref:`modify-security-settings` for more information.

   If the cluster is already running, restart it to apply the new setting.

Logging in through SSH
----------------------
.. note:: Note that you must log in as the **ec2-user**.

After configuring public SSH key, you can log in using a private key by performing the following steps:

1. Navigate to Control Panel > `Clusters <https://api.qubole.com/v2/control-panel#clusters>`__ and find the server
   hostname of the running cluster into which you want to log in. The following figure shows an example of a cluster page
   with one running cluster.

   .. image:: ../../user-guide/clusters/images/ClusterResources.png


   For master node and worker node details:

   * Click **View Master DNS** on the specific running cluster.
   * Click the number in the **Nodes** column and copy the **Public DNS** value of the worker nodes.

2. On a terminal, run an SSH command as mentioned below:

    `$ ssh -i <path to your private key> ec2-user@<public dns of server>`
