.. _provision-a-new-account:

=============================
Set up a New Account with QDS
=============================

The :ref:`quick-start-guide-index` describes how to sign up on QDS. Once you sign up, you can access the
QDS user interface (UI) to change your account settings and select the authentication type.

QDS supports AWS IAM-based keys and roles authentication for each account on QDS. See :ref:`manage-accounts` for
more information.

Authorizing a QDS Account using AWS IAM Access Keys
---------------------------------------------------
See :ref:`iam-keys` to configure a QDS account for getting it authenticated using AWS IAM Access Keys.
:ref:`manage-accounts` describes how to configure and modify a QDS account's settings.

Authorizing a QDS Account using AWS IAM Role Credentials
--------------------------------------------------------
See :ref:`manage-accounts` to configure a QDS account for getting it authenticated using AWS IAM Role credentials.
:ref:`manage-accounts` describes how to configure and modify a QDS account's settings.




