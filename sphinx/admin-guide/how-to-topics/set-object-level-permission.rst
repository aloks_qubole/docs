.. _set-object-level-permissions:

======================================
Set Object-level Permissions in Qubole
======================================
Qubole allows you to set object-level permissions for a cluster and a notebook through REST APIs as described in
:ref:`object_policy-api`.

Qubole allows you to set cluster-level permissions through the UI as described in :ref:`cluster-permissions`.

Qubole allows you to set object-level permissions for a notebook through the Notebooks UI as described in
:ref:`manage-notebook-permissions` and :ref:`manage-folder-permissions`.

At the QDS account level, Qubole allows you to control access to objects/resources by creating roles that are set in
**Control Panel** > **Manage Roles**. For more information, see :ref:`manage-roles-user-resources-actions` and
:ref:`manage-roles`.


