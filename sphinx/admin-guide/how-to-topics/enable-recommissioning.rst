.. _enable-recommissioning:

======================
Enable Recommissioning
======================

:ref:`Recommissioning <auto-works-downscaling-recommission>` allows clusters to reactivate nodes that are in the process of being decommissioned if the workload requires it.


Recommissioning on clusters is not enabled by default. It can be enabled as an Hadoop override setting:

``mapred.hustler.recommissioning.enabled=true``.

To disable the feature, set ``mapred.hustler.recommissioning.enabled=false``.


