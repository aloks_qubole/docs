.. _connect-redshift-in-vpc:

==============================================================================================
Connect to a Redshift/RDS Instance in a VPC (AWS) or Connect to a Data Store in a VNet (Azure)
==============================================================================================

QDS supports adding data stores to import data and export analyzed data to external databases such as MySQL and Redshift.
See :ref:`db-tap` for more information on data stores. See the :ref:`Data Import <data-import>` and 
:ref:`Data Export <data-export>` for more information on importing and exporting data.

Connecting to a Redshift/RDS instance on AWS
--------------------------------------------
**Prerequisite:** You must be a system administrator to add a data store unless your system administrator has granted you specific
permission by means of an Access Control List (ACL).

To connect to an AWS Redshift/RDS instance in a VPC, perform the following steps in the QDS UI:

1. Navigate to the **Explore** page and pull down the drop-down list at the top of the left pane
   (it defaults to **Qubole Hive**):

   .. image:: ../../user-guide/ug-images/ExploreDropdown.png

2. Select the **Redshift** as the **Database Type**.

3. Enter the name in the **Database Name** text field.

4. Enter the IP address of the database server in the **Server Address** field.

5. In the **Port** field, enter the port number for access to the server, or accept the default.

6. Enter the username to be used on the database server in the **Username** field.

7. Enter the password to be used on the database server in the **Password** field.

8. Check the **Skip Validation** box if you do not want QDS to attempt to validate the connection when when you click **Save**.

9. Select **on-premise/other** from the AWS **Region** drop-down list to indicate that the database is in a VPC.
   **Details of gateway machine** fields appear:

.. image:: ../ag-images/RedshiftVPC.png

10. Enter the IP address of the gateway machine that has access to the RedShift/RDS instance.

11. Enter the user name to access the gateway machine in the **Username** text field.

12. Enter the private key to access the gateway machine in the **Private Key** text field.

13. Click **Save**. QDS will attempt to validate the connection unless you checked the **Skip Validation** box.

Connecting to a Data Store in a VNet on Azure
---------------------------------------------

**Prerequisites:**

1. You must be a system administrator to add a data store unless your system administrator has granted you specific
   permission by means of an Access Control List (ACL).
2. Make sure that IP address ``52.44.223.209`` has access to the database server.

To add the data store, navigate to the **Explore** page, pull down the drop-down list at the top of the left pane
(it defaults to **Qubole Hive**), choose **Add Data Store**, and follow prompts for the remaining fields. Because the data
store is in a VNet, you must check the **Bastion Node** box and supply additional information as prompted.

See :ref:`db-tap` for detailed instructions.

