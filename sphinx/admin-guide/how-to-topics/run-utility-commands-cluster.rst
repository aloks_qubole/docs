.. _run-utility-commands-in-cluster:

=================================
Run Utility Commands in a Cluster
=================================
You can run the **nodeinfo** command on a cluster node to get information about it.

  Syntax:

::

    nodeinfo [is_master | master_ip | master_public_dns]

``nodeinfo is_master`` reports if the node is a Master or Worker in boolean format (1/0).

``nodeinfo master_ip`` reports the public IP address of the master node.

``nodeinfo master_public_dns`` reports the public DNS address of the master node.

Add *$(nodeinfo master_public_dns)* to a node bootstrap script or any shell script to get the public
DNS hostname of the Master node.

See :ref:`nodebootstrapscript` for more information.

You can also use the QDS GUI to see the public and private IP addresses of worker and Master nodes: navigate to the **Control Panel >
Clusters** and click on the number in the **Nodes** column of a running cluster.

On a cluster node, run the `nodeinfo` command to check if the node is a worker or Master or get the
master node's IP address and public DNS address. Here is an example illustration of running the `nodeinfo` command on an
AWS cluster node:

::

    [ec2-user@ip-10-111-11-11 /]$ nodeinfo is_master
    1
    [ec2-user@ip-10-111-11-11 /]$ nodeinfo master_ip
    10.111.11.11
    [ec2-user@ip-10-111-11-11 /]$ nodeinfo master_public_dns
    ec2-54-54-544-544.compute-1.amazonaws.com
    [ec2-user@ip-10-111-11-11 /]$

You can also see :ref:`check-master-slave-node`.




