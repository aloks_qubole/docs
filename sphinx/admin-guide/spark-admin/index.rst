.. _spark-admin-index:

====================
Spark Administration
====================

This section explains the topics related to the Spark administration.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    spark-cluster-configurations.rst
    autoscale-spark.rst
    encrypt-spark-data.rst
    spark-hive-authorization.rst
    spark-job-server.rst

For more information about Spark, see :ref:`spark-qubole-index`.
