.. _data-encryption-matrix:

========================================
Data Encryption Mapping within QDS (AWS)
========================================
The QDS Control Plane denotes all the components except the clusters. :ref:`defloc-folder-qdsaccess` provides the list
of folders in the account's default location into which QDS has access to write data. Here is a table that maps the
different types of data encryption with QDS Control Plane and Hadoop, Hive, Presto, and Spark engines.

+------------------------+-------------------+---------------------+---------------------+----------------------------+-------------------------+
|Type                    | Filesystem        | QDS Account         | QDS Control Plane   | Hadoop/Hive/Spark Engines  | Presto Engine           |
+========================+===================+=====================+=====================+============================+=========================+
| Server-side Encryption | S3n               | S3n                 | NA                  | SSE-S3                     | NA                      |
|                        +-------------------+---------------------+---------------------+----------------------------+-------------------------+
|                        | S3a               | SSE-S3 (via Support)| SSE-KMS (via API)   | SSE-S3, SSE-KMS,           | NA                      |
|                        |                   |                     |                     | and SSE-C                  |                         |
|                        +-------------------+---------------------+---------------------+----------------------------+-------------------------+
|                        |PrestoS3Filesystem | NA                  | NA                  | NA                         | SSE-S3 and SSE-KMS, and |
|                        |                   |                     |                     |                            | SSE-C (custom Key is    |
|                        |                   |                     |                     |                            | only supported in 0.157)|
+------------------------+-------------------+---------------------+---------------------+----------------------------+-------------------------+
| Client-side Encryption | S3a               | NA                  | NA                  | Supported only on the S3a  | NA                      |
|                        |                   |                     |                     | filesystem                 |                         |
+------------------------+-------------------+---------------------+---------------------+----------------------------+-------------------------+

The expanded form of the supported encryption keys in the above table are:

* **SSE-S3**: Amazon S3-managed encryption keys
* **SSE-KMS**: Amazon S3-KMS Managed encryption keys
* **SSE-C**: Server-Side Encryption with Customer-Provided encryption keys

.. note:: The encryption keys in Presto have different configuration parameters. A custom
          encryption key is also supported in Presto 0.157. For more information, see :ref:`catalog-hive-properties`.

          You can enable SSE-S3 on the QDS account only by creating a ticket with the `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.