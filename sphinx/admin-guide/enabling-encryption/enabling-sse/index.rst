.. _enabling-encryption-data-at-rest:

================================================
Enabling the Server-side Encryption in QDS (AWS)
================================================
QDS supports data encryption to protect data when the data in Cloud storage and HDFS. Qubole leverages on Amazon S3’s
server-side encryption (SSE). For more information, see this `reference <http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingServerSideEncryption.html>`__.

:ref:`defloc-folder-qdsaccess` provides the list of folders in the account's default location into which QDS
has access to write data.

On the S3a filesystem, QDS supports these types of SSE that are categorized based on the encryption keys they use:

* SSE-S3: Amazon S3-managed encryption keys
* SSE-KMS: Amazon S3-KMS Managed encryption keys
* SSE-C: Server-Side Encryption with Customer-Provided encryption keys



These topics provide more information, and explain how to enable data encryption on AWS:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    enabling-sse-s3
    enabling-sse-kms
    enabling-sse