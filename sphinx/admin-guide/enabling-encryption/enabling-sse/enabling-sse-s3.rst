.. _enabling-sse-s3:

===============
Enabling SSE-S3
===============
QDS supports the Amazon S3-Managed Encryption Keys (SSE-S3). This type of SSE uses ``AES-256`` to encrypt the data.

Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ in case you want to enable the SSE-S3 on
the QDS account.
