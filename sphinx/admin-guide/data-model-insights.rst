.. _data-model-insights-stub:

=============================
Getting Usage Recommendations
=============================

QDS can examine your data layout and usage and make recommendations that improve productivity and reduce cost; see
:ref:`data-model-insights`.

