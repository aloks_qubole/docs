.. _performance-monitoring-ganglia:

Performance Monitoring with Ganglia
===================================

`Ganglia <http://ganglia.info/>`__ is a scalable, distributed system
designed to monitor clusters and grids while minimizing the impact on
the performance. When you enable Ganglia monitoring on a cluster,
you can view the performance of the cluster as a whole as well as
inspect the performance of individual node instances. You can also view
various Hadoop metrics for each node instance.

How to Enable Ganglia Monitoring
--------------------------------
Perform the following steps to enable Ganglia Monitoring:

#. Sign in to your Qubole account.

#. Navigate to **Control Panel**; the **Clusters** tab is displayed by default. Click the edit button for the cluster
   for which you want to enable Ganglia monitoring.

#. On the **Edit Cluster** page, select **Enable Ganglia Monitoring** in the **Cluster Settings** section.
   The setting is applied when the cluster is restarted.


How to View Ganglia Metrics
---------------------------
Navigate to https://<your_platform>.qubole.com/ganglia-metrics-<cluster_id> to see the Ganglia metrics for a specific
cluster; for example, for an AWS cluster with ID 18, go to https://api.qubole.com/ganglia-metrics-18.


Collecting Cluster Metrics
--------------------------
When Ganglia monitoring is enabled on a cluster, you can also collect the cluster metrics using the
:ref:`Cluster Metrics <metrics-cluster>` API.

