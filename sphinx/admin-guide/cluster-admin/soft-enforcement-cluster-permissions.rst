.. _soft-enforcement-cluster-permissions:

===================================================
Understanding the Soft-enforced Cluster Permissions
===================================================
QDS supports soft enforcement of cluster permissions at the object level. On the **Manage Permissions** dialog of a
specific cluster, when you select one permission, then additional cluster permissions are automatically selected. You
can still disable those additional permissions in the UI before saving.

Qubole highly recommends a user to accept the enforced permissions. For example, the **Read** permission is enforced with
the **Start** permission. If you decide to uncheck the **Read** permission on the UI, Qubole warns you that the product
experience is not optimal with this warning: ``Unchecking these permissions will disable certain capabilities and might lead to errors.``

The permissions that are enforced on the cluster are provided in this table.

+-------------+-------------------------------------------------------+
|             | Enforced Permissions                                  |
+=============+=======================================================+
| Read        | Not applicable                                        |
+-------------+-------------------------------------------------------+
| Start       | Read                                                  |
+-------------+-------------------------------------------------------+
| Update      | Read and Start                                        |
+-------------+-------------------------------------------------------+
| Clone       | Read, Manage, Update                                  |
+-------------+-------------------------------------------------------+
| Terminate   | Read, Manage, Update, and Clone                       |
+-------------+-------------------------------------------------------+
| Delete      | Read, Manage, Update, Clone, and Terminate            |
+-------------+-------------------------------------------------------+

The different cluster permissions are:

   * **Read** - This permission allows/denies a user to view the specific cluster. The UI does not display a cluster for
     which a user does not have read permission. This implies that all other actions even if granted are ineffective on the UI.
   * **Update** - This permission allows/denies a user to edit the cluster configuration. When no update permission is
     set, it also means that the cluster cannot be cloned from the UI.
   * **Delete** - This permission allows/denies a user to delete the cluster.
   * **Start** - This permission allows/denies a user to start the cluster. A user is not allowed to run a command from
     the **Analyze** page on a cluster on which the **Start** permission is denied to that user even when he has
     access permission to run commands. However, this behavior is not enabled by default and you can enable this behavior
     by creating a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.
   * **Terminate** - This permission allows/denies a user to terminate the cluster.
   * **Clone** - This permission allows/denies a user to clone the cluster. The **Update** permission must be granted to
     clone a cluster.
   * **Manage** - This permission allows/denies a user to manage this cluster's permissions.

   .. note:: If you a allow a user with a permission who is part of the group that has restricted access, then that user
             is allowed access and vice versa. For more information, see :ref:`cluster-permission-precedence`.