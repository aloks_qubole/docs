.. _cluster-admin-index:

======================
Cluster Administration
======================
This section explains the topics related to the cluster administration.

.. toctree::
   :maxdepth: 1
   :titlesonly:

   clusters-setting.rst
   configuring-clusters.rst
   soft-enforcement-cluster-permissions.rst
   cluster-permissions.rst
   auto-scaling.rst
   autoscaling-logs.rst
   spot-nodes.rst
   aggressive-downscaling.rst
   aggressive-downscaling-azure.rst
   cluster-network-security-characteristics.rst
   heterogeneous-clusters.rst
   use-heterogeneous-nodes.rst
   clusters-in-vpcs.rst
   private-subnet.rst
   vpc-multiple-subnets.rst
   ec2-classic-cost.rst
   aws-rate-limit.rst
   performance-monitoring-ganglia.rst

For more information, see the :ref:`qds-clusters-index`.