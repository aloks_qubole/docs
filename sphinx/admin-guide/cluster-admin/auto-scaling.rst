.. _auto:

###############################
Auto-scaling in Qubole Clusters
###############################

.. _auto-whatis:


********************
What Auto-scaling Is
********************

Auto-scaling is a mechanism built in to QDS that adds and removes cluster nodes while the cluster is running,
to keep just the right number of nodes running to handle the workload. Auto-scaling automatically adds resources when
computing or storage demand increases, while keeping the number of nodes at the minimum needed to meet your processing
needs efficiently.

.. _auto-works:

**********************
How Auto-scaling Works
**********************

When you :ref:`configure a cluster <configuring-clusters>`, you choose the minimum and maximum
number of nodes the cluster will contain (**Minimum Worker Nodes** and **Maximum Worker Nodes**, respectively). While the cluster is running, QDS continuously monitors the progress of tasks and the number of active nodes to make sure that:

* Tasks are being performed efficiently (so as to complete within an amount of time that is set by a configurable default).
* No more nodes are active than are needed to complete the tasks.

If the first criterion is at risk, and adding nodes will correct the problem, QDS adds as many nodes as are needed,
up to the **Maximum Worker Nodes**. This is called *upscaling*.

If the second criterion is not being met, QDS removes idle nodes, down to the **Minimum Worker Nodes**. This is called
*downscaling*, or *decommissioning*.

The topics that follow provide details:

* :ref:`auto-works-nodes`
* :ref:`auto-works-upscaling`
* :ref:`auto-works-downscaling`
* :ref:`auto-works-spot`
* :ref:`auto-works-preemptible`
* :ref:`auto-works-practice`

  * :ref:`Hadoop MRv1 (Hadoop 1) <auto-works-practice-MRv1>`
  * :ref:`Hadoop MRv2 (Hadoop 2) <auto-works-practice-MRv2>`
  * :ref:`Presto <auto-works-practice-Presto>`
  * :ref:`Spark <auto-works-practice-Spark>`
  * :ref:`Tez <auto-works-practice-Tez>`



.. _auto-works-nodes:

Types of Nodes
==============

Auto-scaling operates only on the nodes that comprise the difference between the **Minimum Worker Nodes** and
**Maximum Worker Nodes** (the values you specified in the QDS **Cluster UI** when you
:ref:`configured the cluster<configuring-clusters>`),
and affects worker nodes only; these are referred to as *auto-scaling nodes*.

The *Master Node(s)*, and the nodes comprising the **Minimum Worker Nodes**, are the stable core of the cluster;
they normally remain running as long as the cluster itself is running; these are called *core nodes*.

Stable and Volatile Nodes on AWS
--------------------------------

In AWS clusters, core nodes are *stable* instances -- normally AWS `On-Demand <https://aws.amazon.com/ec2/purchasing-options/>`__
or high-priced `Spot <https://aws.amazon.com/ec2/spot/>`__ instances -- while auto-scaling
nodes can be stable or *volatile* instances. Volatile instances are Spot instances that can be lost at any time;
stable instances are unlikely to be lost. See :ref:`cluster_composition`
for a more detailed explanation, and :ref:`auto-works-spot` for further discussion of how QDS manages Spot instances
when auto-scaling a cluster.

Preemptible VMs and On-demand Instances on GCP
----------------------------------------------

`Preemptible VM instances <https://cloud.google.com/compute/docs/instances/preemptible>`__ on GCP have the following characteristics:

    * The cost for preemptible VMs is much lower than the cost for on-demand instances.
    * The price is fixed, based on the instance type, and does not fluctuate. For details about the pricing of preemptible instances, see `Google Compute Engine Pricing <https://cloud.google.com/compute/pricing>`__ in the GCP documentation.
    * GCP Compute Engine can terminate (preempt) your preemptible VMs at any time if it needs to use them for other tasks.
    * Compute Engine always terminates preemptible VMs after they run for 24 hours, if not sooner. But certain actions reset the 24-hour counter for a preemptible VM, for instance, stopping and restarting the instance.

For more information, see :ref:`auto-works-preemptible` below.

.. _auto-works-upscaling:

Upscaling
=========

Launching a Cluster
-------------------

QDS launches clusters automatically when applications need them. If the application needs a cluster that is not running,
QDS launches it with the minimum number of nodes, and scales up as needed toward the maximum.

.. _auto-works-upscaling-criteria:

Upscaling Criteria
------------------

QDS bases upscaling decisions on three factors:

* The rate of progress of the jobs that are running.
* Whether faster throughput can be achieved by adding nodes.
* Whether the :ref:`HDFS storage <auto-works-HDFS>` currently configured for the cluster will be sufficient for all tasks
  to complete.

  .. note:: This storage criterion currently applies only on AWS clusters.

Assuming the cluster is running fewer than the configured maximum number of nodes, QDS activates more nodes if, and only
if, the configured SLA (Service Level Agreement) will not be met at the current rate of progress, and adding the nodes
will improve the rate.

Even if the SLA is not being met, QDS does not add nodes if the workload cannot be distributed more efficiently across
more nodes. For example, if three tasks are distributed across three nodes, and progress is slow because the tasks are
large and resource-intensive, adding more nodes will not help because the tasks cannot be broken down any further. On the
other hand, if tasks are waiting to start because the existing nodes do not have the capacity to run them, then QDS will
add nodes.

.. _auto-works-HDFS-MRV2:

EBS Upscaling on Hadoop MRv2 AWS Clusters
-----------------------------------------

EBS upscaling dynamically adds EBS volumes to AWS instances that are approaching the limits of their storage capacity.
You can enable EBS upscaling on :ref:`Hadoop MRv2 <auto-works-practice-MRv2>` clusters running on Amazon AWS;
these include clusters running :ref:`Spark <auto-works-practice-Spark>` and :ref:`Tez <auto-works-practice-Tez>`
jobs as well as those running MapReduce jobs. See :ref:`auto-works-hdfs-ebs` for details.


EBS upscaling works as follows.

* The underlying mechanism is `Logical Volume Manager (LVM) <https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux)>`__.
* When you :ref:`configure EBS storage <ec2_cluster_settings>` for a node, QDS configures the EBS volumes into an LVM volume
  group comprising a single logical volume.
* When free space on that volume falls below the free-space threshold, QDS adds EBS volumes to the logical volume and
  resizes the file system.
* QDS auto-scaling adds EBS storage but does not remove it directly, because this involves reducing the filesystem
  size, a risky operation. The storage is removed when the node is
  :ref:`decommissioned <auto-works-downscaling>`.

See `Auto-scaling in Qubole With AWS Elastic Block Storage <https://www.qubole.com/blog/auto-scaling-in-qubole-with-aws-elastic-block-storage/>`__
for a full discussion.

.. _auto-works-Azure-disk:

Disk Upscaling on Hadoop MRv2 Azure and GCP Clusters
----------------------------------------------------

Disk upscaling in Azure and GCP clusters works similarly to :ref:`EBS upscaling <auto-works-HDFS-MRV2>` on AWS.

When you enable disk upscaling for a node, you also specify:

* The maximum number of disks that QDS can add to a node (**Maximum Data Disk Count** on the **Clusters** page in the QDS UI).
* The minimum percentage of storage that must be available on the node (**Free Space Threshold %**). When available storage drops
  below this percentage, QDS adds one or more disks until free space is at or above the minimum percentage, or the node
  has reached its **Maximum Data Disk Count**. The default is 25%.
* The absolute amount of storage that must be available on the node, in gigabytes (**Absolute Free Space Threshold**).
  When available storage drops below this amount, QDS adds one or more disks until free space is at or above the minimum
  amount, or the node has reached its **Maximum Data Disk Count**. The default is 100 GB.

In addition, QDS monitors the rate at which running Hadoop jobs are using up storage, and from this computes when more
storage will be needed.

QDS auto-scaling adds storage but does not remove it directly, because this involves reducing the filesystem
size, a risky operation. The storage is removed when the node is :ref:`decommissioned <auto-works-downscaling>`.

Reducers-based Upscaling on Hadoop MRv2 Clusters
------------------------------------------------

Hadoop MRv2 clusters can upscale on the basis of the number of Reducers. This configuration is disabled by default.
Enable it by setting ``mapred.reducer.autoscale.factor=1`` as a Hadoop override.

.. _auto-works-HDFS:

HDFS-based Upscaling on Hadoop 1 Clusters (AWS)
-----------------------------------------------

You can enable HDFS-based auto-scaling on :ref:`Hadoop MRv1 <auto-works-practice-MRv1>`.

.. note:: :ref:`Hadoop MRv1 <auto-works-practice-MRv1>` is not supported on Azure, GCP, or Oracle OCI. For more information, see :ref:`QDS Components: Supported Versions and Cloud Platforms <os-version-support>`.

You can enable HDFS-based auto-scaling by setting

``mapred.hustler.dfs.autoscale.enable = true``

in the **Override Hadoop Configuration Variables** field on the **Add Cluster** and **Cluster Settings** screens in the
**Cluster UI**, or in your cluster’s ``mapred-site.xml`` file.

If HDFS-based auto-scaling is enabled, QDS continuously monitors the cluster’s HDFS storage to ensure that the available
capacity remains sufficient for the jobs that are running to complete, and launches more nodes if necessary.

There are two triggers for launching more nodes; these are discussed under **Rate-based Auto-scaling**
and **Threshold-based Auto-scaling** below.


**Rate-based Auto-scaling:** As Hadoop jobs are running, QDS monitors the rate at which they
are using up storage, and from this computes when to launch
additional nodes (taking into account how long it is taking for newly launched nodes to become operational).

Bringing up additional instances is the most efficient and reliable way to increase storage, but it is expensive,
so QDS ensures that this is done only if it has to be, and is done as late as possible. Nodes are launched so they, and
their storage volumes, come online just in time to prevent the cluster from running out of storage, and no sooner.

**Threshold-based Auto-Scaling:** In addition to monitoring the rate at which the storage is filling up, QDS also compares current usage against a
configurable threshold. If usage exceeds this threshold, QDS launches more nodes until usage is below the threshold.



.. _auto-works-downscaling:

Downscaling
===========

QDS bases downscaling decisions on the following factors.


Downscaling Criteria
--------------------

A node is a candidate for decommissioning only if:

* The cluster is larger than its configured minimum size.
* No tasks are running.
* The node is approaching an hourly boundary. By default, QDS runs nodes for multiples of one hour. (This is true unless
  you enable *Aggressive Downscaling*; see :ref:`aggressive-downscaling` and :ref:`aggressive-downscaling-azure` for
  more information, and note that you must currently create a `Qubole Support ticket <https://qubole.zendesk.com/hc/en-us>`__
  to enable Aggressive Downscaling.)
* The node is not storing any shuffle data (data from Map tasks for Reduce tasks that are still
  running).
* Enough cluster storage will be left after shutting down the node to hold the data that must be kept (including HDFS
  replicas).

  .. note:: This storage consideration does not apply to :ref:`Presto <auto-works-practice-Presto>` clusters.

.. note:: In Hadoop MRv2, you can control the maximum number of nodes that can be downscaled simultaneously
          by setting ``mapred.hustler.downscaling.nodes.max.request`` to the maximum you want; the default
          is 500.

**Downscaling Exception for Hadoop 2 and Spark Clusters:** Hadoop 2 and Spark clusters do not downscale
to a single worker node once they have been upscaled. When
**Minimum Worker Nodes** is set to **1**, the cluster starts with a single worker node, but once upscaled, it never
downscales to fewer than two worker nodes. This is because decommissioning slows down greatly if there is only one usable
node left for HDFS, so nodes doing no work may be left running, waiting to be decommissioned. You can override this
behaviour by setting ``mapred.allow.single.worker.node`` to ``true`` and restarting the cluster.

.. _auto-works-downscaling-container:

Container Packing in Hadoop 2 and Spark
---------------------------------------

Qubole allows you to pack YARN containers on Hadoop MRv2 (Hadoop 2) and Spark clusters. You must
:ref:`enable <container-packing-hadoop2>` this capability; it is disabled by default.

When it is enabled, container packing causes the scheduler to pack containers on a subset of
nodes instead of distributing them across all the nodes of the cluster. This increases the probability of some nodes
remaining unused; these nodes become eligible for downscaling, reducing your cost.

Packing works by separating nodes into three sets:

* Nodes with no containers (the *Low* set)
* Nodes with memory utilization greater than the threshold (the *High* set)
* All other nodes (the *Medium* set)

YARN schedules each container request in this order: nodes in the *Medium* set first, nodes in the *Low* set next,
nodes the *High* set last. For more information, see :ref:`container-packing-hadoop2`.

.. _auto-works-offloading:

Offloading (Hadoop MRv1)
------------------------

.. note:: Offloading is supported in :ref:`Hadoop MRv1 <auto-works-practice-MRv1>` clusters only. These are not supported
          on :ref:`all Cloud platforms <os-version-support>`.

Normally MapReduce requires a Mapper’s hosting cluster node, which stores the Mapper output, to remain up until the
Reducer finishes and the job completes. Only then can the Mapper process quit. This means that you may be paying for Cloud
instances that are idle and merely waiting for Reducers to finish.

QDS can optionally remove this bottleneck, and reduce your costs, by offloading the Mapper data either to HDFS or to
Cloud storage outside the cluster. When you enable offloading, a Mapper needs to run only until its data has been offloaded;
once the data data has been copied, and all other :ref:`criteria <auto-works-downscaling>` are met, the Mapper node can
be decommissioned.

To enable offloading, use the **Cluster Configuration UI** to set ``qubole.offload.enable`` to ``true`` in the
**Override Hadoop Configuration Variables** field in the **Hadoop Cluster Settings** section when you add or modify the
cluster.

By default, the data is offloaded to a Cloud storage location (``s3.log.location`` for AWS). You can use the
**Override Hadoop Configuration Variables** field to change this to:

* a different Cloud storage location, by setting the path in ``qubole.offload.path``, *or*
* an HDFS path, by setting ``qubole.offload.protocol`` to ``hdfs://`` and ``qubole.offload.path`` to the HDFS path.


.. _auto-works-downscaling-graceful:

Graceful Shutdown of a Node
---------------------------

If all of the downscaling criteria are met, QDS starts decommissioning the node. QDS ensures a graceful shutdown by:

* Waiting for all tasks to complete.
* Ensuring that the node does not accept any new tasks.
* Transferring HDFS block replicas to other nodes.

.. note:: Data transfer is not needed in :ref:`Presto <auto-works-practice-Presto>` clusters.


.. _auto-works-downscaling-recommission:

Recommissioning a Node
----------------------

If more jobs enter the pipeline while a node is being decommissioned, and the remaining nodes cannot handle them, the
node is recommissioned -- the decommissioning process stops and the node is reactivated as a member of the cluster and
starts accepting tasks again.

Recommissioning takes precedence over launching new instances: when handling an upscaling request, QDS launches new
nodes only if the need cannot be met by recommissioning nodes that are being decommissioned.

Recommissioning is preferable to starting a new instance because:

* It is more efficient, avoiding bootstrapping a new node.
* It is cheaper than provisioning a new node.

.. _auto-works-dynamic:

Dynamic Downscaling
-------------------

Dynamic downscaling is triggered when you reduce the maximum size of a cluster while it's running. The
subsections that follow explain how it works. First you need to understand what happens when you decrease (or increase)
the size of a running cluster.

**Effects of Changing Worker Nodes Variables while the Cluster is Running:** You can change the **Minimum Worker Nodes**
and **Maximum Worker Nodes** while the cluster is running. You do this via the
**Cluster Settings** screen in the QDS UI, just as you would if the cluster were down.

To force the change to take effect dynamically (while the cluster is running) you must *push* it, as described :ref:`here
<push-config-cluster>`. Exactly what happens then depends on the current state of the cluster and configuration settings.
Here are the details for both variables.

**Minimum Worker Nodes:** An increase or reduction in the minimum count takes effect dynamically by default. (On a Hadoop
MRv2 cluster, this happens because ``mapred.refresh.min.cluster.size`` is set to ``true`` by default. Similarly, on a Presto
cluster, the configuration reloader mechanism detects the change.)

**Maximum Worker Nodes:**

* An increase in the maximum count always takes effect dynamically.
* A reduction in the maximum count produces the following behavior:

  * If the current cluster size is smaller than the new maximum, the change takes effect dynamically. For example, if
    the maximum is 15, 10 nodes are currently running and you reduce the maximum count to 12, 12 will be the maximum from
    now on.
  * If the current cluster size is greater than the new maximum, QDS begins reducing the cluster to the new maximum,
    and subsequent upscaling will not exceed the new maximum. In this case, the default behavior for reducing the number
    of running nodes is **dynamic downscaling**.

**How Dynamic Downscaling Works:**

If you decrease the **Maximum Worker Nodes** while the cluster is running, and more
than the new maximum number of nodes are actually running, then QDS begins dynamic downscaling.

If dynamic downscaling is triggered, QDS selects the nodes that are:

* closest to completing their tasks
* closest to their hourly boundary (unless :ref:`auto-works-aggressive-d` is enabled)
* (in the case of Hadoop MRv2 clusters) closest to the time limit for their containers

Once selected, these nodes stop accepting new jobs and QDS shuts them down :ref:`gracefully <auto-works-downscaling-graceful>`
until the cluster is at its new maximum size (or the maximum needed for the current workload, whichever is smaller).

.. note:: In a Spark cluster, a node selected for dynamic downscaling may not be removed
          immediately in some cases-- for example, if a Notebook or other long-running Spark application
          has executors running on the node, or if the node is storing Shuffle data locally.

.. _auto-works-aggressive-d:

Aggressive Downscaling
----------------------

**Aggressive Downscaling** refers to a set of QDS capabilities that are currently available only by request from
`Qubole Support <https://qubole.zendesk.com/hc/en-us>`__. See :ref:`aggressive-downscaling` and
:ref:`aggressive-downscaling-azure` for more information.

.. _auto-works-downscaling-shutdown:

Shutting Down an Idle Cluster
-----------------------------

By default, QDS shuts the cluster down completely if both of the following are true:

* There have been no jobs in the cluster over a configurable period.
* At least one node is close to its hourly boundary (unless :ref:`auto-works-aggressive-d` is enabled) and no tasks are running on it.

You can change this behavior by :ref:`disabling automatic cluster termination <modify-cluster-settings>`,
but Qubole recommends that you leave it enabled -- inadvertently allowing an idle cluster to keep running can become an
expensive mistake.


.. _auto-works-spot:

Spot-based Auto-scaling in AWS
==============================

You can specify that a portion of the cluster’s :ref:`auto-scaling nodes <auto-works-nodes>` can be potentially volatile
AWS `Spot <https://aws.amazon.com/ec2/spot/>`__ instances (as opposed to higher-priced, more stable Spot instances, or
`On-Demand <https://aws.amazon.com/ec2/purchasing-options/>`__ instances, which are stable but still more expensive.) You
do this via the **Spot Instances Percentage** field that appears when you choose the **Autoscaling Node Purchasing Option**
on the **Add Cluster** and **Cluster Settings** screens in the QDS **Cluster UI**. The number you put in that field actually
specifies the *maximum* percentage of auto-scaling nodes that QDS can launch as volatile Spot instances.

For example, if you specify a **Minimum Worker Nodes** of 2 and a **Maximum Worker Nodes** of 10, and you choose **Spot
Instance** as the **Autoscaling Node Purchasing Option**, you might specify a **Spot Instances Percentage** of 50. This
would allow a maximum of 4 nodes in the cluster (50% of the difference between 2 and 10) to be volatile Spot instances. The
*non-auto-scaling nodes* (those represented by the **Minimum Worker Nodes**, 2 in this case) are always stable instances
(On-Demand or high-priced Spot instances).

Opting for volatile Spot instances has implications for the working of your cluster, because Spot instances can be lost at any
time. To minimize the impact of losing a node in this way, Qubole implements the *Qubole Placement Policy*, which is in
effect by default and makes a best effort to place one copy of each data block on a stable node. You can change the
default in the QDS **Cluster UI** on the **Add Cluster** and **Cluster Settings screens**, but Qubole recommends that you
leave the policy enabled (**Use Qubole Placement Policy** checked in the **Cluster Composition** section). For more
information about this policy, and Qubole’s implementation of Spot instances in general, see the blog post
`Riding the Spotted Elephant <https://www.qubole.com/blog/product/riding-the-spotted-elephant/>`__ .

.. _auto-works-preemptible:

Preemptible VM-based Auto-scaling in Google Cloud Platform
==========================================================

For clusters on GCP, you can create and run preemptible VMs for a much lower price than you would pay for on-demand instances.

In the QDS UI, you can configure a percentage of your instances to be preemptible. You do this via the **Composition** tab in either the **New Cluster** or **Edit Cluster** screen. In the **Summary** section, click **edit** next to **Composition**. The number you put in the **Preemptible Nodes (%)** field specifies the maximum percentage of auto-scaling nodes that QDS can launch as preemptible VMs:

      .. image:: images/GCPSetPercentPreemptibleVMs.png

Qubole recommends using one of the following approaches to combining on-demand instances with preemptible VMs:

* Use on-demand instances for your core nodes and a combination of on-demand instances and preemptible VMs for the autoscaling nodes.

* Use preemptible VMs for both core nodes and autoscaling nodes.

Normally, the core nodes in a cluster are run on stable on-demand instances, except where an unexpected termination of the entire cluster is considered worth risking in order to obtain lower costs. Auto-scaling nodes, on the other hand, can be preemptible without a risk that the cluster could be unexpectedly terminated.  For more information on preemptible instances, see `Preemptible VM Instances <https://cloud.google.com/compute/docs/instances/preemptible>`__ in the GCP documentation.

.. _auto-works-spot-rebalance:

Spot Rebalancing
----------------

Using volatile instances--Spot instances on AWS or preemptible VMs on GCP--significantly reduces your cloud spending. But fluctuations in the market may mean that QDS cannot always obtain as many volatile instances as your cluster specification
calls for. (QDS tries to obtain the volatile instances for a configurable number of minutes before giving up.) To pursue the
example in the previous section, suppose your cluster needs to scale up by four additional nodes, but only two volatile
instances that meet your requirements (out of the maximum of four you specified) are available. In this case, QDS will
launch the two volatile instances, and (by default) make up the shortfall by also launching two on-demand instances,
meaning that you will be paying more than you had hoped in the case of those two instances. (You can change this default
behavior in the QDS **Cluster UI** on the **Add Cluster** and **Cluster Settings** screens, by un-checking
**Fallback to on demand** in the **Cluster Composition** section.)

Whenever the cluster is running a greater proportion of on-demand instances than you have specified, QDS works to remedy
the situation by monitoring the volatile market, and replacing the on-demand nodes with volatile instances as soon as suitable
instances become available. This is called *Spot Rebalancing*.

.. note:: Spot rebalancing is supported in :ref:`Hadoop MRv1 <auto-works-practice-MRv1>`, :ref:`Hadoop MRv2 <auto-works-practice-MRv2>`,
          and :ref:`Spark <auto-works-practice-Spark>` clusters only.

For a more detailed discussion of this topic, see `Rebalancing Hadoop Clusters for Higher Spot Utilization
<https://www.qubole.com/blog/product/rebalancing-hadoop-higher-spot-utilization/?nabe=5695374637924352:1>`__.

.. _auto-works-practice:

How Auto-scaling Works in Practice
==================================

.. _auto-works-practice-MRv1:

Hadoop MRv1
-----------

Here’s how auto-scaling works on Hadoop MRv1 (Hadoop 1 clusters):

* Each node in the cluster reports its launch time to the JobTracker, which keeps track of how long each node has been
  running.
* Whenever a node approaches its hourly boundary, the JobTracker continuously monitors the pending and current work in
  the system and computes the amount of time required to finish it. If that time exceeds the pre-configured threshold
  (for example, two minutes) and there is :ref:`sufficient parallelism in the workload <auto-works-upscaling-criteria>`,
  then the JobTracker adds more nodes to the cluster.
* Whenever a node approaches its hourly boundary, the JobTracker checks to see if there is enough work in the system to
  justify continuing to run this node. If not, and the criteria described above under :ref:`Downscaling Criteria <auto-works-downscaling>`
  are met, the JobTracker :ref:`decommissions <auto-works-downscaling-graceful>` the node.

.. note:: Hadoop MRv1 clusters are not supported on Azure, GCP, or Oracle OCI. For more information, see :ref:`QDS Components: Supported Versions and Cloud Platforms <os-version-support>`.

.. _auto-works-practice-MRv2:

Hadoop MRv2
-----------

Here’s how auto-scaling works on Hadoop MRv2 (Hadoop 2 (Hive) clusters):

.. note:: :ref:`Offloading  <auto-works-offloading>` is not supported in Hadoop 2 (Hive) clusters.

In Hadoop MRv2, you can control the maximum number of nodes that can be downscaled simultaneously by means of
``mapred.hustler.downscaling.nodes.max.request``. Its default value is 500.

* Each node in the cluster reports its launch time to the ResourceManager, which keeps track of how long each node has
  been running.
* YARN ApplicationMasters request YARN resources (:ref:`containers <hadoop-mapreduce-configuration>`) for each Mapper
  and Reducer task. If the cluster does not have enough resources to meet these requests, the requests remain pending.
* On the basis of a pre-configured threshold for completing tasks (for example, two minutes), and the number of pending
  requests, ApplicationMasters create special auto-scaling container requests.
* The ResourceManager sums the ApplicationMasters’ auto-scaling container requests, and on that basis adds more nodes
  (up to the configured Maximum Worker Nodes).
* Whenever a node approaches its hourly boundary, the ResourceManager checks to see if any task or shuffle process is
  running on this node. If not, the ResourceManager :ref:`decommissions <auto-works-downscaling-graceful>` the node.

.. note::
          * You can improve auto-scaling efficiency by enabling :ref:`container packing <container-packing-hadoop2>`.

          * You can control the maximum number of nodes that can be downscaled simultaneously
            by setting ``mapred.hustler.downscaling.nodes.max.request`` to the maximum you want; the default
            is 500.

.. _auto-works-practice-Presto:

Presto
------
Here’s how auto-scaling works on a Presto cluster:

.. note:: :ref:`Spot Rebalancing <auto-works-spot-rebalance>` is not yet supported on Presto clusters. :ref:`HDFS-based Auto-scaling  <auto-works-HDFS>`
          and :ref:`Offloading  <auto-works-offloading>` are not applicable to Presto because it has an in-memory query
          execution model that does not require persisting intermediate results to disk.
          Presto is not supported on :ref:`all Cloud platforms <os-version-support>`.

* The Presto Server (running on the master node) keeps track of the launch time of each node.
* At regular intervals (10 seconds by default) the Presto Server takes a snapshot of the state of running queries,
  compares it with the previous snapshot, and estimates the time required to finish the queries. If this time exceeds a
  threshold value (set to one minute by default and configurable through ``ascm.bds.target-latency``),
  the Presto Server adds more nodes to the cluster. For more information on ``ascm.bds.target-latency`` and other autoscaling
  properties, see :ref:`presto-config-properties`.
* If QDS determines that the cluster is running more nodes than it needs to complete the running queries
  within the threshold value, it begins to :ref:`decommission <auto-works-downscaling-graceful>` the excess nodes.

.. note:: Because all processing in Presto is in-memory and no intermediate data is written to HDFS, the HDFS-related
          :ref:`decommissioning <auto-works-downscaling-graceful>` tasks are not needed in a Presto cluster.

After new nodes are added, you may notice that they sometimes are not being used by the queries already in progress. This is
because new nodes are used by queries in progress only for certain operations such as **TableScans** and **Partial Aggregations**.
You can run ``EXPLAIN (TYPE DISTRIBUTED)`` (see `EXPLAIN <https://prestosql.io/docs/current/sql/explain.html>`__)
to see which of a running query's operations can use the new nodes: look for operations that are part
of **Fragments** and appear as ``[SOURCE]``.

If no query in progress requires any of these operations, the new nodes remain unused initially. But all new queries started
after the nodes are added can make use of the new nodes (irrespective of the types of operation in those queries).

.. _presto-required-worker-nodes:

Configuring the Required Number of Worker Nodes
-----------------------------------------------
You can configure ``query-manager.required-workers`` as a cluster override to set the number of worker nodes that
must be present in the cluster before a query is scheduled to be run on the cluster. This allows cluster admins to reduce
the minimum size of Presto clusters to 1 without causing query failures due to limited resources. Qubole Presto's
autoscaling feature ensures that ``query-manager.required-workers``, which denotes the number of worker nodes brought up before
the query is scheduled to be run on the cluster. While nodes are being requested from the cloud provider and added to the
cluster, queries are queued on the Presto's coordinator node. These queries are visible as queries in the **Waiting for resources**
state in the Presto web UI.

.. note:: This enhancement is only supported with Presto 0.193 and later versions.

Queries waits for a maximum time of upto ``query-manager.required-workers-max-wait`` (with a default value of 5 minutes)
before getting scheduled on the cluster if the required number of nodes (configured for ``query-manager.required-workers``)
could not be provisioned. The nodes brought up to match the value of ``query-manager.required-workers`` size are
autoscaling nodes and adheres to the existing spot ratio configuration. This allows higher usage of spot nodes in the cluster.
Queries  such as queries on JMX, system, and information schema connectors or queries such as ``SELECT 1`` and ``SHOW CATALOGS``,
which do not require multiple worker nodes to execute, are immediately executed on the cluster. While queries are running
on the cluster, the required number of worker nodes set through ``query-manager.required-workers`` are maintained in the
cluster. The cluster downscales back to the minimum configured size when there are no active queries.

.. _decommission-presto-worker-node:

Decommissioning a Worker Node
.............................
Qubole allows you to gracefully decommission a worker node during autoscaling through the cluster's master node. If you
find a problematic worker node, then you can manually decommission it.

To get the instance ID of the worker node that you want to decommission, run this API from the cluster's master node
that returns a list of all node instance IDs.

.. sourcecode:: bash

   curl localhost:8081/v1/cm/list/

From the list in the API response, pick and note the instance ID of the worker node that you want to decommission and
add it in the API (below).

From the cluster master node, run this command to decommission the worker node below:

.. sourcecode:: bash

   curl -X DELETE localhost:8081/v1/cm/worker/instance-id

Where ``instance-id`` is the worker node's instance ID.

This puts the specified worker node in a decommissioning state but it still completes its ongoing job (if any) and only
after that it shuts down gracefully. This node is not used for executing any new queries which are submitted after you call
the node-decommissioning API.

.. _auto-works-practice-Spark:

Spark
-----
Here’s how auto-scaling works on a Spark cluster:

.. note:: :ref:`Offloading  <auto-works-offloading>` is not supported on Spark clusters.

* You can configure Spark auto-scaling at the cluster level and at the job level.
* Spark applications consume YARN resources (containers); QDS monitors container usage and launches new nodes (up to the
  configured **Maximum Worker Nodes**) as needed.
* If you have enabled job-level auto-scaling, QDS monitors the running jobs and their rate of progress, and launches
  new executors as needed (and hence new nodes if necessary).
* As jobs complete, QDS selects candidates for :ref:`downscaling <auto-works-downscaling>` and initiates
  :ref:`Graceful Shutdown <auto-works-downscaling-graceful>` of those nodes that meet the criteria.

For a detailed discussion and instructions, see :ref:`Autoscaling in Spark <autoscale-spark>`.

.. note:: You can improve auto-scaling efficiency by enabling :ref:`container packing <container-packing-hadoop2>`.

.. _auto-works-practice-Tez:

Tez on Hadoop 2 (Hive) Clusters
-------------------------------

Here’s how auto-scaling works on a Hadoop 2 (Hive) cluster where Tez is the execution engine:

.. note:: :ref:`Offloading  <auto-works-offloading>` is not supported on Hadoop 2 (Hive) clusters.
          Tez is not supported on :ref:`all Cloud platforms <os-version-support>`.

* Each node in the cluster reports its launch time to the ResourceManager, which keeps track of how long each node has
  been running.
* YARN ApplicationMasters request YARN resources (containers) for each Mapper and Reducer task. If the cluster does not
  have enough resources to meet these requests, the requests remain pending.
* ApplicationMasters monitor the progress of the DAG (on the Mapper nodes) and calculate how long it will take to finish
  their tasks at the current rate.
* On the basis of a pre-configured threshold for completing tasks (for example, two minutes), and the number of pending
  requests, ApplicationMasters create special auto-scaling container requests.
* The ResourceManager sums the ApplicationMasters’ auto-scaling container requests, and on that basis adds more nodes
  (up to the configured **Maximum Worker Nodes**).
* Whenever a node approaches its hourly boundary, the ResourceManager checks to see if any task or shuffle process is
  running on this node. If not, the ResourceManager :ref:`decommissions <auto-works-downscaling-graceful>` the node.

For More Information
--------------------
For more information about configuring and managing QDS clusters, see:

* :ref:`Configuring Clusters <configuring-clusters>`
* :ref:`Managing Clusters <manage-clusters>`
