.. _private-subnet:

============================
Configuring a Private Subnet
============================

Use these instructions to configure a private subnet for your clusters:

* :ref:`clusters-in-vpc`
* :ref:`private-subnet-whitelist-IP`
* :ref:`private-subnet-azure`
* :ref:`private-subnet-oracle`



.. _private-subnet-whitelist-IP:

Tunnelling with Bastion Nodes for Private Subnets in an AWS VPC
----------------------------------------------------------------
You must whitelist IP addresses/NAT gateways to tunnel with bastion nodes for private subnets. The IP addresses/NAT gateways
to be whitelisted are different in various QDS envrionments:

* `US Non-SOC-compliant Environment`_
* `US SOC-compliant Environment`_
* `Europe Region Environment`_
* `India Region Environment`_

US Non-SOC-compliant Environment
................................
In the https://api.qubole.com QDS environment, an administrator must ensure that these IP addresses have access
privileges to the Bastion node for private subnets in the Virtual Private Cloud (VPC):

* 54.204.43.32/32
* 54.243.39.255/32
* 23.23.178.159/32
* 54.243.29.190/32
* 54.243.128.178/32
* 23.21.153.231/32
* 23.21.191.84/32
* 34.205.91.155/32
* 34.205.91.156/30
* 34.205.91.160/28
* 34.205.91.176/29
* 34.205.91.184/31
* 34.205.91.186/32

US SOC-compliant Environment
............................
In the https://us.qubole.com QDS environment, an administrator must ensure that this NAT gateway has access
privileges to the Bastion node for private subnets in the VPC:

* 52.44.223.209/32

Europe Region Environment
.........................
In the https://eu-central-1.qubole.com QDS environment, an administrator must ensure that these NAT gateways
have access privileges to the Bastion node for private subnets in the VPC:

* 18.195.234.160
* 18.195.249.80

India Region Environment
........................
In the https://in.qubole.com QDS environment, an administrator must ensure that this NAT gateway has access
privileges to the Bastion node for private subnets in the VPC:

* 35.154.109.184/32


.. _private-subnet-azure:

Configuring a Private Subnet for Azure Clusters
-----------------------------------------------

Proceed as follows:

#. If your private subnet does not already exist, create it in the same
   `VNet <https://docs.microsoft.com/en-us/azure/virtual-network/virtual-networks-create-vnet-arm-pportal>`__
   where you will launch the Bastion host.

   The subnet should have rules that are no more restrictive than these (Qubole recommends setting rules that are less
   restrictive):

   * A rule to allow ingress via all protocols to all ports from the Bastion node's private IP address.
   * A rule that allows all communication via all protocols to all ports on all hosts within the subnet.

   You can configure the rules by means of a **Network Security Group** if you prefer (see :ref:`modify-Azure-cluster`).

#. Whitelist the IP address of Qubole's NAT gateway for the `US SOC-compliant Environment`_ to allow SSH access to the
   Bastion host. The Bastion host address and the whitelisted address must be in the same subnet.

#. Decide how to allow QDS access to the Bastion host you are going to create:

   * *EITHER*: use the default public key. To see the key in the QDS UI, navigate to **Clusters**, choose the **Advanced**
     tab, and scroll down to **SECURITY SETTINGS**.

   * *OR*: to use a unique SSH key for your account (for communication between QDS and the Bastion node), and also instruct QDS to
     generate a unique key each time a cluster is launched (for communication between the Bastion Node and the cluster),
     `create a support ticket  <https://qubole.zendesk.com/hc/en-us>`__ to enable that capability and obtain the account-level
     key. Then authorize that key on the Bastion host as described below.

#. Create a Bastion node, bring it up, and :ref:`configure SSH access <private-subnet-azure-Bastion>`.

   * Qubole recommends using an Azure instance running the latest version of CentOS Linux. The instance must be able
     to handle high network throughput and the workload of communicating with multiple clusters: the **Standard_A4_v2**
     instance type (with 4 cores and 8 GB of RAM) is a good choice.
   * Create the Bastion node in a public subnet, in the same VNet as the private subnet (from step 1 above).
   * Make sure the Bastion node's network security group (or the public subnet's network security group, which the Bastion
     node inherits by default) opens the SSH port (22 by default) for access from Qubole's NAT gateway (see step 2 above).
   * You (as the system administrator) also need SSH access to the Bastion node.

#. :ref:`Configure a QDS cluster <private-subnet-azure-cluster>` to use the private subnet and Bastion node.

.. [replace step 3 above with this once we have the Qubole image] ..#. Use Qubole's Bastion image to create a Bastion node.
   The Bastion node must be in the same
   `VNet <https://docs.microsoft.com/en-us/azure/virtual-network/virtual-networks-create-vnet-arm-pportal>`__
   as the private subnet.

.. add: (search Azure portal for QB image-- waiting for Harsh or Ajay--- see JIRA)


.. _private-subnet-azure-Bastion:

Configuring SSH on the Bastion Node
...................................

**Prerequisite**: The Bastion node must allow SSH access via the loopback address. This is usually the default for SSH access in a Linux
environment, but can be configured differently.

.. ..--commenting out the following subsection until we publish the Bastion image--

.. Bringing up the Bastion Host using the Qubole Bastion Image

.. ..~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. Search the Azure portal for the image labelled ``qubole-bastion-??`` *[need name]*.

.. The image contains a Qubole Public Key, which you can use to start an Azure instance for the Bastion host.

.. .. note:: If you create a cluster with a custom SSH key (see :ref:`modify-Azure-cluster`), replace the Qubole Public Key
..           with your account-level SSH key. To get a custom SSH key for your account, create a ticket with
.. ..`Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.


.. Bringing up the Bastion Host using another Image

.. .. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. .. If you decide not to use Qubole's image for the Bastion node,

.. ..--end of commented-out text--

Do the following after bringing up your Bastion node:

1. Create a user on the Bastion node for SSH access by Qubole. This can be the default user shown in the QDS UI, or a name
   you choose.

   To see the default name in the QDS UI, navigate to **Clusters**, choose the **Advanced** tab, and enter an IP address
   for the Bastion node.

   If you choose a different user name, make sure you change the user name in the QDS UI also. Make a note of this user ID; you'll need
   it when you :ref:`Configure your QDS cluster <private-subnet-azure-cluster>` to use the private subnet and Bastion node.

2. Log in to the Bastion node as the user you created in step 1.

3. Modify the Bastion node's SSH configuration in the SSH demon configuration file, ``/etc/ssh/sshd_config`` to set
   ``GatewayPorts`` to ``yes``. (This file is also where you can change the SSH port from the default (22) to something
   else if you need to. Remember that this port must be open for access from Qubole's NAT gateway, as described
   :ref:`above <private-subnet-azure>`).

4. After editing ``/etc/ssh/sshd_config``, restart the SSH demon service. For example, on CentOS, run
   ``sudo systemctl restart sshd.service``.

5. Add the SSH key from step 3 :ref:`above <private-subnet-azure>` to ``~/.ssh/authorized_keys``
   in the home directory of the user you created in step 1.


.. _private-subnet-azure-cluster:

Configuring a QDS Cluster to use the Private Subnet and Bastion Node
....................................................................

Follow :ref:`these instructions <modify-Azure-cluster>` to create or modify a QDS cluster.
Under the **Advanced** tab, configure the following fields:

- **Virtual Network**: From the drop-down menu, choose the Vnet in which the both the Bastion host and the cluster will be
  launched.

- **Subnet**: Choose your :ref:`private subnet <private-subnet-azure>` from the drop-down menu.

- **Bastion Node**: Provide the public IP address of your :ref:`Bastion node <private-subnet-azure-Bastion>`.

- **Bastion Node Port**: Let this default to port 22 unless you have changed it on the Bastion host
  (see step 3 :ref:`above <private-subnet-azure-Bastion>`).

- **Bastion Node User**: The user you :ref:`created <private-subnet-azure-Bastion>` for QDS access.

- **Network Security Group**: If you configured :ref:`rules <private-subnet-azure>` by means of a security group, choose
  that group from the drop-down menu.

When you are satisfied with your changes, choose **Save** or **Update**.





.. _private-subnet-oracle:

Configuring a Private Subnet for Oracle OCI Clusters
----------------------------------------------------

Proceed as follows:

#. `Create a Qubole support ticket <https://qubole.zendesk.com/hc/en-us>`__ to get a tunnel IP address and public key to allow
   Qubole access to the Bastion host you are going to create.

#. To create a Bastion node, launch an instance using the Oracle-Linux-7.3 image. This instance must be in the same
   VCN as the private subnet.

#. Add the public key provided by Qubole support to the authorized key list of user ``opc`` on the Bastion node.
   To do this, append the public key to ``/home/opc/.ssh/authorized_keys`` on the Bastion node.

#. Run the following commands in the ``bash`` shell on the Bastion node.

   .. sourcecode:: bash

      useradd ec2-user -p ''
      mkdir -p /home/ec2-user/.ssh
      cp /home/opc/.ssh/authorized_keys /home/ec2-user/.ssh/
      chown -R ec2-user:ec2-user /home/ec2-user/.ssh
      # Need to open port 7000 to private subnet CIDR block using firewall if required.
      firewall-cmd --permanent --zone=public --add-rich-rule='rule family=ipv4 source address=<private subnet cidr block> port port=7000 protocol=tcp   accept'
      firewall-cmd --reload
      bash -c 'echo "GatewayPorts yes" >> /etc/ssh/sshd_config'
      sudo service sshd restart


#. If your private subnet does not already exist, create it in the same VCN where you launched the Bastion host.

   At a minimum, the subnet should have these rules:

   * A `stateful <https://docs.us-phoenix-1.oraclecloud.com/Content/Network/Concepts/securitylists.htm?Highlight=stateful%20rule#Stateful>`__ rule to allow ingress via all protocols to all ports from the private subnet's CIDR block.
   * A stateful rule to allow ingress via all protocols to all ports from the Bastion node's private IP address.
   * A stateful rule to allow egress via all protocols and all ports to destination 0.0.0.0/0


#. Configure the Bastion host's security list. This list should have these rules at a minimum:

   * A stateful rule to allow ingress via TCP to port 22 for ssh access from the tunnel
     server IP address specified by Qubole support.
   * A stateful rule to allow ingress via TCP protocols to port 22 for ssh access from the public IP address of the
     Bastion node.
   * A stateful rule to allow ingress via TCP to port 7000 for metastore access from the private subnet's CIDR block
     (see step 5 above).
   * A stateful rule to allow egress via all protocols and all ports to destination 0.0.0.0/0.


#. `Create a Qubole support ticket <https://qubole.zendesk.com/hc/en-us>`__
   to provide your Bastion host's public IP address.
   Qubole will enable private subnet support for your clusters.

.. ..
.. ..#. Edit each of your clusters to add the Bastion host's Public IP address.
