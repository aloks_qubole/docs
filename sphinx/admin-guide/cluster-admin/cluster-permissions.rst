.. _cluster-permissions:

===========================================
Managing Cluster Permissions through the UI
===========================================
QDS supports setting permissions for a specific cluster in the **Clusters** UI page in addition to the Object Policy
REST API. For more information on the API, see :ref:`set-object-policy-cluster`. You can allow/deny the cluster's access
to a user/group.

This feature is not enabled by default. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
to get this feature enabled for the QDS account.

.. note:: :ref:`soft-enforcement-cluster-permissions` provides the list of cluster permissions that would be enforced
          with one cluster permission.

To allow/deny permissions to a specific cluster through the UI, perform these steps:

1. Navigate to the **Clusters** page and click or do a mouse hover on the ellipsis that is against the specific cluster
   to which you want to restrict permissions. You must be a system-admin, owner of the cluster, or a user who has the
   **Manage** permission to set permissions and even see **Manage Permissions**. Clicking or a mouse hover on the ellipsis
   displays this drop down list for a running cluster.

   .. image:: ../../user-guide/ug-images/cloneNdefaultCluster.png

2. Click **Manage Permissions** (it is also displayed in the drop-down list in the ellipsis for an inactive cluster). The
   **Manage Permissions** dialog is displayed as shown here.

   .. image:: images/ClusterPermission.png

3. Select a user/group that you want to allow or deny access to. Some permissions are set by default and the current
   permissions are displayed for the selected user/group. To allow select the checkbox below the cluster policy action,
   and to deny, uncheck the checkbox below the cluster policy action or do not select the checkbox. The different
   cluster permissions are:

   * **Read** - This permission allows/denies a user to view the specific cluster. The UI will not display a cluster for
     which a user does not have read permission. This implies that all other actions even if granted are ineffective on the UI.
   * **Update** - This permission allows/denies a user to edit the cluster configuration. When no update permission is
     set, it also means that the cluster cannot be cloned from the UI.
   * **Delete** - This permission allows/denies a user to delete the cluster.
   * **Start** - This permission allows/denies a user to start the cluster. A user is not allowed to run a command from
     the **Analyze** page on a cluster on which the **Start** permission is denied to that user even when he has
     access permission to run commands. However, this behavior is not enabled by default and you can enable this behavior
     by creating a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.
   * **Clone** - This permission allows/denies a user to clone the cluster. The **Update** permission must be granted to
     clone a cluster.
   * **Manage** - This permission allows/denies a user to manage this cluster's permissions.

   .. note:: If you a allow a user with a permission who is part of the group that has restricted access, then that user
             is allowed access and vice versa. For more information, see `Understanding the Precedence of Cluster Permissions`_.

4. Click **Add New Permission** to assign permissions to another user/group. Allow/deny cluster permissions as described
   in the step 3. Specific cluster permissions for a user and a group are illustrated in this sample.

   .. image:: images/ClusterPermission1.png

5. Click **Save** after assigning cluster permissions to the user(s)/group(s).


.. _cluster-permission-precedence:

Understanding the Precedence of Cluster Permissions
---------------------------------------------------
The precedence of cluster permissions are mentioned below:

* The cluster owner and system-admin have all permissions that cannot be revoked.
* Users take precedence over groups.
* A user who is not assigned with any specific permissions inherits them from the group that he is part of.
* If the cluster ACL permissions are defined by a user, who is the current owner, then that user has all access by
  default. Even if there is a access control set for deny. Basically QDS honors ownership over object ACLs.
* If the cluster ACL permissions are not defined by a user, who is the current owner, QDS allows that user to do cluster
  operations if there is no explicit deny permission set for that user. But if a READ permission is denied to the user,
  then the user cannot see that specific cluster in the **Clusters** list. The read permission is denied as shown in this
  example.

  .. image::  images/ClusterPermsn2.png