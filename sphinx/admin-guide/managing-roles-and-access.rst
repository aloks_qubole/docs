.. _managing-access-permissions:

=====================================
Managing Access Permissions and Roles
=====================================
QDS allows system administrators to create custom roles to control access to specific resources. A role has
a list of policies that determine which QDS capabilities a user has access to.
See :ref:`Manage Roles <manage-roles>` for more information.

:ref:`role-types` briefly describes the system-defined roles and custom roles. :ref:`role-precedence` briefly
describes how assigning multiple roles to a group works.


.. _role-types:

Types of Roles
--------------
There are two types of roles:

* **System-defined roles**:

  - System-admin - A system-defined role, which has full access to all resources. User who have this role have full
    access to all resources in the Qubole account.

  - System-user - A system-defined role whose access is restricted. Users who have
    this role can read all Qubole resources (such as commands and templates), create commands in **Analyze**, create,
    clone and run Templates, create and clone Schedules, and kill and re-run Scheduler Instances. Create, update, and delete
    operations on data stores in **Explore** are denied. Create, clone, update, and delete cluster operations are also
    denied.

* **Custom roles**: A system administrator can create custom roles that allow granular
  control over QDS resources. These roles have specific permissions identified by a
  user-defined name.

.. _role-precedence:

Precedence of Roles
-------------------
When two roles in the same group allow different access to the same resource, the least
restrictive policy applies. For example, suppose **Role1** denies read access to commands and **Role2** allows read access to
commands. If **Role1** and **Role2** are part of group **Group1**, **Group1** users have read access to comands.

To add, modify, and delete roles, choose **Manage Roles** from the **Control Panel**.

Create a Role
-------------
:ref:`Creating a Role <manage-roles-user-operations-create>` explains the steps to add a new role.

Adding a Policy to a Role
-------------------------
Perform the following steps to set a policy with access and resource permissions for a role. You must have system
administrator (**system-admin**) privileges.

1. Navigate to **Manage Roles** on the **Control Panel** page. Click the **+** button to create a new role. Select
   the **Modify** action from the drop-down list in the **Action** column of an existing role that you want to edit.

   :ref:`Creating a Role <manage-roles-user-operations-create>` explains how to add a new role. :ref:`Modifying a Role <manage-roles-user-operations-modify>`
   explains how to modify the access permissions of a role. You can modify the policies of an existing role or
   set a new policy for a new role.
2. Set the following policy elements:

   a. **Access**: Denotes the type of access. Select **Allow** or **Deny**.
   b. **Resources**: You can select **All** from the drop-down list, or any specific resource such as **Clusters** or
      **App**. By default, the drop-down list shows the **Account** resource. The following figure shows the available
      resources for which you can add access policies for a role.

      .. image:: ag-images/Role-Resources.png

   c. Depending on the resources, you can set policy actions in the **Action** text field. Click in the text
      field to see available actions for that resource, such as **create** and **update**. Select the action
      that you want to set for the role.
   d. Click **Add Policy** to add the policy for the role.

Deleting a Role
---------------
You can delete only custom roles; system-defined roles cannot be deleted. See :ref:`role-types` for more information.

To remove a custom role, click the down-arrow in the **Action** column on the **Manage Roles** page, and select **Delete**.


