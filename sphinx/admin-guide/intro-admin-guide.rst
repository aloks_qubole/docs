.. _`intro-admin-guide`:

============
Introduction
============

This guide contains information on administering the Qubole Data Service (QDS). It is intended for system
administrators and users managing QDS.