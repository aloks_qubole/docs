.. _os-version-support:

======================================================
QDS Components: Supported Versions and Cloud Platforms
======================================================

* :ref:`supported-versions`
* :ref:`deprecated-versions`

.. _supported-versions:

Supported Versions
...................

The following table shows the currently supported open source versions of QDS Components,
and the Cloud platforms on which they run.

+---------------------------+----------------------------------------------+-------------------------------------+
|QDS Component              |Currently Supported Versions                  |Supported on (Cloud Platforms)       |
+===========================+==============================================+=====================================+
|Airflow                    |1.7.0, 1.8.2, 1.10.0 (beta)                   |AWS, Azure, Oracle OCI, GCP          |
+---------------------------+----------------------------------------------+-------------------------------------+
|Cascading                  |All                                           |AWS, Azure, Oracle OCI               |
+---------------------------+----------------------------------------------+-------------------------------------+
|Hadoop 1                   |0.20.1 :ref:`(deprecated) <hadoop1-dep>`      |AWS                                  |
+---------------------------+----------------------------------------------+-------------------------------------+
|Hadoop 2                   |2.6.0                                         |AWS, Azure, Oracle OCI, GCP          |
+---------------------------+----------------------------------------------+-------------------------------------+
|Hive                       |1.2, 2.1.1, 2.3 (beta)                        |AWS                                  |
|                           +----------------------------------------------+-------------------------------------+
|                           |1.2, 2.1.1                                    |Azure, Oracle OCI                    |
|                           +----------------------------------------------+-------------------------------------+
|                           |2.1.1                                         |GCP                                  |
+---------------------------+----------------------------------------------+-------------------------------------+
|Java                       |* Hadoop 1 supports Java 1.7                  |AWS                                  |
|                           |                                              |                                     |
|                           +----------------------------------------------+-------------------------------------+
|                           |* Hadoop 2 and Spark support Java 1.7 as the  |AWS, Azure, Oracle OCI               |
|                           |  default version, but 1.8 can be enabled     |                                     |
|                           |  through the node bootstrap.                 |                                     |
|                           +----------------------------------------------+-------------------------------------+
|                           |* Presto supports only 1.8                    |AWS, Azure                           |
|                           +----------------------------------------------+-------------------------------------+
|                           |* GCP supports only 1.8                       |GCP                                  |
+---------------------------+----------------------------------------------+-------------------------------------+
|MapReduce                  |0.20.1                                        |AWS                                  |
|                           +----------------------------------------------+-------------------------------------+
|                           |2.6.0                                         |AWS, Azure, Oracle OCI, GCP          |
+---------------------------+----------------------------------------------+-------------------------------------+
|Pig                        |0.11, 0.15                                    |AWS                                  |
|                           +----------------------------------------------+-------------------------------------+
|                           |0.11                                          |Azure, Oracle OCI                    |
+---------------------------+----------------------------------------------+-------------------------------------+
|Presto                     |                                              |AWS, Azure                           |
|                           |                                              |                                     |
|                           |0.157, 0.180, 0.193, and 0.208 (beta)         |                                     |
+---------------------------+----------------------------------------------+-------------------------------------+
|Python                     |2.6, 2.7, and 3.5                             |AWS                                  |
|                           |                                              |                                     |
|                           |                                              |See :ref:`python-version-hadoop`     |
|                           |Airflow supports only 2.7 and later           |for more information                 |
|                           +----------------------------------------------+-------------------------------------+
|                           |2.7 and 3.5                                   |Azure, Oracle OCI                    |
|                           +----------------------------------------------+-------------------------------------+
|                           |3.6.x and 3.7                                 |GCP                                  |
+---------------------------+----------------------------------------------+-------------------------------------+
|R                          |3.3.3                                         |AWS, Oracle OCI                      |
|                           +----------------------------------------------+-------------------------------------+
|                           |3.3.2                                         |Azure                                |
|                           +----------------------------------------------+-------------------------------------+
|                           |3.5.2                                         |GCP                                  |
+---------------------------+----------------------------------------------+-------------------------------------+
|RubiX                      |0.2.11                                        |AWS, Azure                           |
+---------------------------+----------------------------------------------+-------------------------------------+
|Scala                      |2.10 for Spark versions older than 2.0.0      |AWS                                  |
|                           +----------------------------------------------+-------------------------------------+
|                           |2.11 for Spark 2.0.0 and later                |AWS, Azure, Oracle OCI, GCP          |
+---------------------------+----------------------------------------------+-------------------------------------+
|Spark: see                 |                                              |AWS                                  |
|                           |                                              |                                     |
|:ref:`spark-support`       |                                              |                                     |
|                           |1.6.2, 2.0.2, 2.1.1,                          |                                     |
|                           |2.2.0, 2.2.1, 2.3.1, 2.4.0                    |                                     |
|                           +----------------------------------------------+-------------------------------------+
|                           |2.0.2, 2.2.1, 2.3.1                           |Azure, Oracle OCI                    |
|                           +----------------------------------------------+-------------------------------------+
|                           |2.3.2, 2.4                                    |GCP                                  |
|                           +----------------------------------------------+-------------------------------------+
|                           |                                                                                    |
|                           | .. note:: In the **Spark Version** drop-down list on the **Clusters** page of the  |
|                           |           QDS UI, ``latest`` means the latest open-source maintenance version.     |
|                           |           If you choose ``latest``, Qubole Spark is automatically upgraded when a  |
|                           |           new maintenance version is released. For example, if ``2.x latest``      |
|                           |           currently points to Spark 2.x.y, then when 2.x.(y+1) is released,        |
|                           |           QDS clusters running ``2.x latest`` automatically start using 2.x.(y+1)  |
|                           |           on a cluster restart.                                                    |
+---------------------------+----------------------------------------------+-------------------------------------+
|Sqoop                      |1.4.7                                         |AWS                                  |
|                           +----------------------------------------------+-------------------------------------+
|                           |1.4.6                                         |AWS, Azure, Oracle OCI               |
+---------------------------+----------------------------------------------+-------------------------------------+
|Tez                        | 0.7                                          |AWS, Azure, Oracle OCI               |
|                           +----------------------------------------------+-------------------------------------+
|                           |0.8.4                                         |GCP                                  |
+---------------------------+----------------------------------------------+-------------------------------------+
|Zeppelin (notebooks)       |0.6.2                                         |AWS, Azure, Oracle OCI, GCP          |
+---------------------------+----------------------------------------------+-------------------------------------+

.. _deprecated-versions:

Deprecated Versions
...................

The following table shows the deprecated versions of the engines and the corresponding timelines.

.. note::

   * Deprecation Timeline: Deprecated version is marked as (deprecated) on the UI. Existing clusters with this version function normally, however, new features and bug fixes are not available. It is not recommended for production use, therefore, you should upgrade the clusters to the later versions of engines. For assistance, contact `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.

   * Removal Timeline: Version in the removal timeline is completely removed from the cluster image. Existing clusters with this version function when they are in running state. After the next restart, these clusters come up with the default version on the available version list.

+----------------+-----------------------------------+-----------------------------------------------+--------------------------------------------+
| Engines on QDS | Deprecated Versions               | Deprecation Timeline                          | Removal Timeline                           |
+================+===================================+===============================================+============================================+
| Spark          | 1.3.1, 1.4.0, 1.4.1, 1.5.0        | 2017                                          | Yet to be removed. Estimated release: R56  |
|                +-----------------------------------+-----------------------------------------------+--------------------------------------------+
|                | 1.5.1, 1.6.0, 1.6.1, 2.0.0, 2.1.0 | R54 (November 2018)                           | Yet to be removed. Estimated release: R56  |
+----------------+-----------------------------------+-----------------------------------------------+--------------------------------------------+
| Hive           | 0.13                              | Not supported for new customers.              | Removed in R55                             |
|                |                                   | For legacy customers, support ended in R55.   |                                            |
+----------------+-----------------------------------+-----------------------------------------------+--------------------------------------------+
| Presto         | 0.142                             | R52 (January 2018)                            | Yet to be removed. Estimated release: R56  |
|                +-----------------------------------+-----------------------------------------------+--------------------------------------------+
|                | 0.157                             | R56                                           | Yet to be removed. Estimated release: R57  |
+----------------+-----------------------------------+-----------------------------------------------+--------------------------------------------+

.. _spark-support:

Spark Version Support
---------------------
QDS supports two major versions of Spark, 1.x and 2.x. As of April 2017, Qubole began phasing out
support for older versions. Those versions are marked ``deprecated`` in the drop-down list of available versions on the
**Clusters** page of the QDS UI. You can still launch clusters running a version marked ``deprecated``, but:

  * No new features or bug fixes will be applied to this version.
  * The version will no longer be eligible for Qubole Customer Support; tickets will not be addressed.

.. _hadoop1-dep:

Hadoop 1 is Deprecated
----------------------
Qubole has deprecated Hadoop 1 as-a-service. Qubole will support Hadoop 1 on the existing clusters until 31 December 2018.
Creating a new Hadoop 1 cluster or cloning an existing Hadoop 1 cluster is not supported through the API and the UI.

If you are using Hadoop 1 clusters, then you must plan to migrate the workloads to Hadoop 2 clusters.
Creating a new Hadoop 2 (Hive) cluster is one of the easiest ways to migrate the workload. As an alternative
option, you can migrate the workloads to a Presto or a Spark cluster. For more information, contact the `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
or the Customer Success Manager associated with the account.


..
.. Explanation of Terms
.. .. ....................

.. .. * **Active**: Fully supported; no current timeline for obsolescence.

.. .. * **Deprecated**: You can still launch clusters running this version, but:

.. ..  * No new features or bug fixes will be applied to this version.
.. ..  * The version will no longer be eligible for Qubole Customer Support; tickets will not be addressed.
.. ..  * The version will sunset 90 days from the deprecation date.

.. .. * **Sunset**: The version is no longer available in the QDS UI, and no longer eligible
.. ..   for Qubole Customer Support. You can no longer launch clusters running this version. Qubole will make a best effort
.. ..  not to break compatibility with versions in this state, but compatibility is not guaranteed.


.. .. Spark 1.x Deprecation
.. .. .....................

.. .. For Spark major version 1.x, QDS is deprecating and will eventually sunset versions older than 1.6.2, starting April
.. .. 14, 2017:

.. .. +-------------------------------------------------------+-----------------+-----------------+
.. .. |Spark Versions                                         |Deprecation Date |Sunset Date      |
.. .. +=======================================================+=================+=================+
.. .. |1.3.0, 1.3.1, 1.4.0, 1.4.1, 1.5.0, 1.5.1, 1.6.0, 1.6.1 | April 14, 2017  |July 31, 2017    |
.. .. +-------------------------------------------------------+-----------------+-----------------+

.. ..
.. .. Spark 2.x Deprecation
.. .. .....................

.. .. Qubole will support two versions of a Spark 2.x release at any given time:

.. .. * **Latest**: The most recent Spark version will be marked **Latest** in the QDS UI. If you choose this
.. ..  version when creating or modifying a cluster, the cluster will always run the latest Spark version QDS supports;
.. ..   as soon as Qubole releases a Spark version update, that version will be installed on the cluster nodes and the cluster
.. ..  will run it after the next cluster restart.

.. .. * **Static**: If you choose the Spark version marked **Static** when creating or modifying a cluster, the cluster
.. ..  will continue to run that version until it becomes obsolete; QDS will not automatically upgrade Spark in this cluster
.. ..  when a new version is released. Static versions will become obsolete in two phases, as described
.. ..   :ref:`above <spark-obsolescence>`.

.. As of April 15, 2017, the supported 2.x versions of Spark will be:

.. ..

.. ..* Spark 2.0 Versions:

.. ..  * Static version 2.0.0

.. ..  * Latest version 2.0.2

.. ..* Spark 2.1 Versions:

.. ..  * Static version 2.1.0

.. ..  * Latest version 2.1.0
