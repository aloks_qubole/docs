.. _partner-integration-guides-index:

==========================
Partner Integration Guides
==========================

Qubole offers new functionalities for Big Data users by partnering with other solutions, such as Business Intelligence (BI) and Data Warehouse solutions.

In the BI domain, Qubole has partnered with BI solution providers such as Looker, Tableau Software, and Power BI to provide fast data access while removing the complexities of provisioning, managing, and scaling the Big Data infrastructure.

In the data warehouse domain, Qubole has partnered with Snowflake to enable users to use Qubole Spark to run advanced data transformation and machine learning on Snowflake data warehouses.

In the ETL domain, Qubole has partnered with Talend to enable users to create data jobs and pipelines using Talend and automatically execute them at scale on Qubole’s platform.

The partner integration information are covered in:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    looker-integration/index.rst
    tableau-integration/index.rst
    snowflake-integration/index.rst
    talend_integration_guides/index.rst
    powerbi-integration/index.rst
    sagemaker_integration/index.rst


