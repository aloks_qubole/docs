..  _sagemaker-prerequisites:

=============
Prerequisites
=============
These are the prerequisites:

    * Qubole Spark 2.2 using Hadoop 2.8
    * AWS SageMaker
    * Qubole Env: https://api.qubole.com, https://us.qubole.com.
    * Scala, Python on Qubole Analyze, Qubole Zeppelin Notebook, and Sagemaker Notebook
