..  _sagemaker-use_case2:

==============================================================================
Use Case 2: Executing  First AWS SageMaker Model with Python on Qubole Analyze
==============================================================================

The script uses MNIST data provided by SageMaker to build a Machine Learning (ML) model. This model can be used to predict
the output for MNIST test data.

Prerequisites
-------------

Ensure that the instructions mentioned under :ref:`sagemaker-configuring_spark` and :ref:`sagemaker-configuring_sagemaker`
sections are completed.

Executing  First AWS SageMaker Model with Python on Qubole Analyze
------------------------------------------------------------------

1. Navigate to the **Analyze** page on the QDS UI.
2. On the **Analyze** page, select **Spark Command** as the **Command Type** in the query editor. Select the Spark Cluster
   that you have configured under :ref:`sagemaker-configuring_spark` section. Ensure that the language is set as **Python**.

   .. image:: images/sparkcommand.png

3. Copy and paste  the program mentioned under the :ref:`sagemaker-appendix3` and enter the required information in the
   **region** and role **ARN** (as configured under Step 3 of :ref:`sagemaker-configuring_sagemaker`) for Python program.
4. Use the following code under **Spark Submit Command Line Operations**:

   .. sourcecode:: bash

      --packages com.amazonaws:sagemaker-spark_2.11:spark_2.1.1-1.0

5. Click **Run**. It starts training a job and creates an endpoint to host the model. At the end, it runs the Test Data
   and displays the result, as shown below.

   .. image:: images/testdata2.png