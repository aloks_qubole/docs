..  _sagemaker-configuring_sagemaker:

=========================
Configuring AWS SageMaker
=========================

Prerequisites
-------------

Ensure that you have access to the AWS SageMaker and permission to create an IAM role with createRole.

Configuring AWS SageMaker
-------------------------

1. Click the link below and login to the AWS SageMaker:
   `AWS SageMaker Console <https://console.aws.amazon.com/sagemaker/home?region=us-east-1#/dashboard>`_
2. Create an IAM Role with AWS SageMaker Full Access Policy.
3. Copy the ARN Role and paste it in the scala code.

The configuration is completed. You can now use AWS SageMaker with Qubole.