..  _sagemaker-appendix2:

=====================
Appendix 2: For Scala
=====================

.. sourcecode:: bash

   import org.apache.spark.sql.SparkSession
   import scala.sys.process._
   //val spark = SparkSession.builder.getOrCreate

   val region = "<region>"
   val trainingData = spark.read.format("libsvm")
   .option("numFeatures", "784")
   .load(s"s3://sagemaker-sample-data-$region/spark/mnist/train/")

   val testData = spark.read.format("libsvm")
   .option("numFeatures", "784")
   .load(s"s3://sagemaker-sample-data-$region/spark/mnist/test/")

   val roleArn = "<role_Arn>"

   import com.amazonaws.services.sagemaker.sparksdk.IAMRole
   import com.amazonaws.services.sagemaker.sparksdk.algorithms
   import com.amazonaws.services.sagemaker.sparksdk.algorithms.KMeansSageMakerEstimator

   val estimator = new KMeansSageMakerEstimator(
   sagemakerRole = IAMRole(roleArn),
   trainingInstanceType = "ml.p2.xlarge",
   trainingInstanceCount = 1,
   endpointInstanceType = "ml.c4.xlarge",
   endpointInitialInstanceCount = 1)
   .setK(10).setFeatureDim(784)

   val model = estimator.fit(trainingData)

   val transformedData = model.transform(testData)
