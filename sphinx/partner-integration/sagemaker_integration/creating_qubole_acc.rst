..  _sagemaker-creating_qubole_acc:

=========================
Creating a Qubole Account
=========================
This section is applicable only when you don’t have a Qubole account and want to use SageMaker with a new Qubole account.

`Click here <https://www.qubole.com/get-qubole/>`__ to create a Qubole Free Trial account.

You can also upgrade your account to Qubole Enterprise Edition which provides actionable Alerts, Insights, and
Recommendations to optimize reliability, performance, and costs. To upgrade your account to QDS Enterprise Edition, `click here <https://www.qubole.com/products/pricing/enterprise-edition-pricing/>`_.

To create a Qubole account, see :ref:`aws-quick-start-guide`.

