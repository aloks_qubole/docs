.. _supported-environments-with-looker:

==================================================================
Understanding Additional Considerations related to the Integration
==================================================================
The Qubole Looker integration is supported only on the Presto engine and these Qubole environments:

* AWS US SOC compliant environment: https://us.qubole.com
* AWS US non-SOC compliant environment: https://api.qubole.com
* Azure: https://azure.qubole.com