.. _using-qubole-and-looker:

===========================
Using Qubole through Looker
===========================
After the connection between Qubole and Looker is established, you can use it to create dashboards in the Looker UI.
As a first step, let us get all tables in the schema available in Qubole and  create a dashboard out of one of the tables
in the schema. Viewing tables and creating a dashboard are described in:

* `Viewing Tables in the Schema`_
* `Creating a Dashboard on Looker`_

Viewing Tables in the Schema
----------------------------
To view tables in the schema, perform these steps:

1. On **Looker**, click **Admin**. In the **Admin** tab, click **Connections** under **Database**, which is on the left side.
2. For the connection created in :ref:`config-looker`, click the **settings** (gear) icon,  which is present on the right
   side and it displays a list as shown below.

   .. image:: images/ConnectionSettings.png

3. Click **Explore** from the list and it will run queries on Qubole’s Presto cluster to get the table information for
   the Schema provided while creating the connection in Step 4 of :ref:`config-looker`. There could be a delay in loading
   the Explore page based on the time taken by the queries to run. (If the cluster is not started, then additional time
   is required to start the cluster.)

4. The following screen is displayed once the query execution is completed on Qubole.

   .. image:: images/QueryExecution.png

5. Click the tables that you want to work with in **Looker** and you are redirected to the **Explore** page for the
   selected table.

Creating a Dashboard on Looker
------------------------------
To create a dashboard on **Looker**, perform these steps:

1. Select one of the fields from the left side of the scroll bar in the **Panel** as shown below and click the **Filter**
   tag beside it.

   .. image:: images/LookerDashboard.png

2. In the **SQL** sub-section under **Data** in **Looker**, you can see the SQL which is formed out of the selected filters.
3. Click **Run** available at the the top-right corner. It starts running a query on Qubole, which can be seen on the
   **Analyze** page of Qubole.

   .. image:: images/PrestoQueryonAnalyze.png

4. The results are populated for the selected columns and shown under **Visualizations** and **Data** sections on the
   **Looker** UI.