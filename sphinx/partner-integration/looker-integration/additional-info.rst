.. _additional-info-looker:

============================================
Additional Information about the Integration
============================================
To learn more about Looker, visit https://looker.com/demo. To report issues to Looker email support@looker.com or use
in-app chat.

To learn more about Qubole, visit https://www.qubole.com. To report issues to Qubole, use any of the options below:

* On the Qubole UI, click the **Help** icon at the top right corner and select **Submit Support Ticket**. For more
  information, see :ref:`qds-helpcenter.rst`.
* Go to https://qubole.zendesk.com/hc/en-us.
* Call (855) 423-6674 and select option 2.
