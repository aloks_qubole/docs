.. _looker-prerequisites:

===================================================
Understanding the Prerequisites for the Integration
===================================================
There are certain prerequisites for the Looker Qubole Integration as listed below:

* Looker Version: 6.x
* Latest version of Qubole JDBC driver. For download instructions, see :ref:`download-jdbc-driver`.
* Set up a Looker instance as described `here <https://docs.looker.com/setup-and-management>`__. Qubole JDBC driver
  would be available with the Looker instance.

  **You must have a web link to login to your Looker server. The JDBC driver is packaged with the Looker instance.**


Ensuring the Use of Supported Versions
--------------------------------------
In addition to the Looker instance and Qubole JDBC driver, you must also know use the supported versions, which are:

* Looker Version: 6.x
* Latest version of the Qubole JDBC drivers
* Supported Presto Versions with QDS
