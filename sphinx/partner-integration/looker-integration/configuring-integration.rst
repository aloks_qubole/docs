.. _looker-configuration-integration:

===========================
Configuring Looker with QDS
===========================
For the Qubole-Looker integration, you must first configure Qubole before configuring Looker as described in:

1. :ref:`config-qubole`
2. :ref:`config-looker`

.. _config-qubole:

Configuring Qubole
------------------
Configure Qubole as a first step by performing these steps:

1. Copy the API token from your QDS accounts. To get the API token, on the QDS UI, navigate to the **Control Panel** >
   **My Accounts**. In the **My Accounts** tab, copy the API token that is against the QDS account that would be used on
   Looker for running commands. To copy the API token, click **Show** and the token is displayed. Copy it and keep it
   handy on a notepad or word file. For more information on the **My Accounts** tab, see :ref:`manage-my-account`.
2. Presto cluster with 0.157 or higher version is required for the integration. Update the version in the Presto cluster
   configuration before configuring the Looker.

.. _config-looker:

Configuring Looker
------------------
After configuring the required settings on Qubole UI, perform these steps on the Looker web UI:

1. Once the Looker instance or server is ready, log in to your Looker server which was set up as part of
   :ref:`looker-prerequisites`.
2. Click the **Admin** link on the upper right corner and the **General Settings** page is displayed.

   .. image::  images/LookerAdmin.png

3. From the navigation pane on the left, click **Connections** under the **Database** section. The **Connections** page is displayed.

   .. image:: images/DatabaseConnections.png

4. On the **Connections** page, click the **New Connection** button on the top-left of the page. The **Connection Settings** page displayed as shown below.

   .. image:: images/NewDBConnection.png

   For the new connection:

   a. Enter a name for the connection in the **Name** field.
   b. Select the appropriate Qubole Presto cluster version from the **Dialect** dropdown list. For example, **Qubole Presto**.

      (To get this detail, on  Qubole UI, navigate to **Clusters**. Against the Presto cluster that you want to use, click
      **Edit**. Check the **Presto Version** in the **Configurations** tab.)

   c. Select the **Serverless Qubole** option if you want to send the queries to the Presto clusters run by Qubole.

   .. note:: The **Serverless Qubole** option is not enabled for all users by default. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
      to enable this feature on the QDS account.

   d. Enter the Qubole cluster label name in the **Cluster Label**  field.
   e. Enter **hive** in the **Database** field.
   f. Enter the API token in the **API key** field.
   g. Enter the database or schema name from Qubole in the **Schema** field. For example,  **tpcds_orc_1000**.

      The database or schema is necessary and sufficient information to get started with Qubole in case the Qubole account
      is on the api.qubole.com Qubole environment.

      .. note:: In JDBC and ODBC driver configurations, https://api.qubole.com is the default endpoint.

   h. If the Qubole account is any other Qubole environment or URL, enter the **Additional Params** with **endpoint=<Qubole URL>**. The endpoint represents the Qubole environment that
      you will connect to. Use any of the below values based on where your Qubole account is created:

      * Qubole-on-AWS SOC-compliant environment: endpoint=https://us.qubole.com
      * Qubole-on-AWS non-SOC-compliant environment: endpoint=https://api.qubole.com
      * Qubole-on-Azure: endpoint=https://azure.qubole.com

      Entering the endpoint is illustrated here

      .. image:: images/AdditionalParams.png

   To save tables that Looker fetches from Qubole, configure the below settings.
   Select the **Persistent Derived Tables** checkbox in Looker and fill in the **Temp Database**.

   .. image:: images/PersistentDerivedTables.png

5. Click **Test These Settings** to verify the connection.
   A **Testing…** message is displayed while **Looker** tries to run a query on Qubole to verify the connection.

   If the connection is successful, a message similar to the below message is displayed.

   .. image:: images/ConnectionSuccess.png

Troubleshooting Qubole Looker Integration Issues
------------------------------------------------
One common error related to the connection is displayed as follows.

.. image:: images/CommonConnectionError.png

The causes would be because:

* API Token is wrong
* User account is not on https://api.qubole.com and you have not added **endpoint=<Qubole_URL>** in the **Additional Params**
  field.

To fix this error, verify if the API token is correct or the Qubole environment is added in the Additional Params field.
For more information, see step 4 of :ref:`config-looker`.








