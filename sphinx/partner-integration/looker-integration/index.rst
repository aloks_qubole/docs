.. _looker-integration-guide-index:

###############################
Qubole Looker Integration Guide
###############################
Qubole Data Service (QDS) is a cloud-native autonomous Data Platform that removes the complexity and reduces the cost of
managing Big Data, allowing the data team to focus on business outcomes rather than on running infrastructure.

QDS self-manages and constantly analyzes and learns about the platform’s usage through a combination of heuristics and
machine learning, providing insights and recommendations to optimize reliability, performance and costs. QDS orchestrates
and manages Big Data clusters such as Hadoop, Spark, Presto, and Hive in any public cloud and allows  direct access to
Big Data services securely with user-access control through the QDS user interface (Analyzer and Notebooks), through a REST
API and ODBC/JDBC interfaces.

Looker is a business intelligence (BI) software  and a Big Data analytics platform that help users explore, analyze and
share real-time analytics easier than any other competitive product.

Organizations deploying Big Data face the challenge to provide timely data access to business users (Analysts,
Data Scientist, etc.) while running and managing the Big Data infrastructure efficiently and optimized to keep up with
the increasing amount of data and the demands to access it.

Qubole and Looker address those challenges, providing fast data access while removing the complexities of provisioning,
managing and scaling the Big Data infrastructure.

This guide describes the steps to configure Looker for use with the Qubole Data Service and other related info as
described in the following topics:


.. toctree::
    :maxdepth: 1
    :titlesonly:

    integration-benefits
    prerequisites
    configuring-integration
    using-the-looker-integration
    supported-qds-environments
    additional-info