.. _powerbi-driver-setup:

#############################################
Qubole ODBC Driver Setup for Power BI Desktop
#############################################

Installing the Qubole ODBC Driver:

  Download the updated driver at `https://s3.amazonaws.com/paid-qubole/odbc/1.1.10/qds-odbc-1.1.10.msi <https://s3.amazonaws.com/paid-qubole/odbc/1.1.10/qds-odbc-1.1.10.msi>`__.

  * The download instructions can be found here:
    `http://docs.qubole.com/en/latest/admin-guide/use-qubole-drivers/download-odbc-driver.html <http://docs.qubole.com/en/latest/admin-guide/use-qubole-drivers/download-odbc-driver.html>`__

  * The installation instructions can be found here:
    `http://docs.qubole.com/en/latest/admin-guide/use-qubole-drivers/install-odbc-driver.html <http://docs.qubole.com/en/latest/admin-guide/use-qubole-drivers/install-odbc-driver.html>`__
