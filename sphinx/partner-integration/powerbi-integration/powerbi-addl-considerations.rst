..  _powerbi-addl-considerations:

#########################
Additional considerations
#########################

* Access denied or failure in installing Qubole ODBC driver (Azure DSVM) can occur due to user access control (UAC), formerly known as limited user account (LUA). Disable it to install the Qubole driver successfully (source).

* The latest version of Power BI [above 2.60.5169.4101 64-bit (July 2018) ] includes a security check for uncertified custom connectors. Qubole is working to certify its connector with Microsoft. In order to use the Qubole custom connector before certification, please enable “Allow any extension to load without validation or warning” under the security tab in the **Options** dialog and restart Power BI.

     .. image:: images/37_options.png

* Power Query transformations that alter the original data source (for example, casting of data types) are not permitted. Formula or data analysis expressions [DAX], on the other hand, fall into one of two categories: **Optimized for DirectQuery** and **Not Optimized for DirectQuery**. You can find a link to the page that specifies which functions are optimized for DirectQuery here. Ex : Power BI switches to import mode (unsupported direct query mode) on heavy data transformations such as operations on floats and casting string to numbers.

* A discussion of the implications of using DirectQuery in Power BI can be found at `Implications of using DirectQuery <https://docs.microsoft.com/en-us/power-bi/desktop-directquery-about#implications-of-using-directquery>`__ on the Microsoft Power BI web site.

* A newly-created DSN will not be listed in the existing instance of Power BI. Power BI must be restarted in order to sync with the newly configured DSN in **ODBC Data Sources (64-bit)**.

* Enhancing the performance of query processing.

  Transformations and visualizations will execute multiple queries. If you share the report with others, that could result in a lot of traffic on your servers. To alleviate this problem, Power BI has implemented an optional feature called **Query Reduction** which adds an **Apply** button to all of your slicers. Go to **File > Options and Settings > Options > Query reduction**.

     .. image:: images/38_options_2.png
