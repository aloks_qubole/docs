.. _powerbi-qubole-account:

#####################################
Create and configure a Qubole account
#####################################

If you don’t have a Qubole account or you want to use Power BI with a new Qubole account, follow the instructions on this page. Otherwise, proceed with the next section below, :ref:`powerbi-setup`.

  1. Go to the Qubole endpoint where you want to define your Qubole account.
  2. For an Azure compliant environment, click on `https://azure.qubole.com <https://azure.qubole.com>`__.
  3. Click on the **Sign Up** link at the bottom and complete the information on the sign up page. An activation code will be sent to your email address along with a link to confirm the account. Follow the instructions in the email and choose your password. (You can skip activation and choose your password immediately if you sign up using Google or LinkedIn)
  4. Follow the instruction in **Getting Started with Qubole on Azure** to configure your new Qubole account.
