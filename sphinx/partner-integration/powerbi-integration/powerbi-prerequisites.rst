.. _powerbi-pre-requisites:

##########################################
Pre-requisites for using Power BI with QDS
##########################################

Supported Versions
------------------

* Power BI Desktop version 2.59.5135.601 64-bit (June 2018)
* Supported Engines in Qubole: Presto 0.180
* Supported Storage: Azure Blob Storage
* Qubole Endpoints: The endpoint represents the Qubole environment you will connect to. Currently, the endpoint to use for Azure deployments is https://azure.qubole.com.

Azure data science VM hardware requirements
-------------------------------------------
For more information about Azure data science virtual machines, see `Data Science Virtual Machines <https://azure.microsoft.com/en-us/services/virtual-machines/data-science-virtual-machines/>`__ on the Microsoft Azure web site.

Minimum Hardware Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* VM type: DS4_V2
* CPU: 8-core
* RAM: 28 GB
* Free disk space: 50 GB
