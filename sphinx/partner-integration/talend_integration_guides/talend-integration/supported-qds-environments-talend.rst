.. _talend-prerequisites:

#######################################################
Understanding Requirements Related to the Integration
#######################################################

You must review and understand the supported configuration for the Qubole Talend integration:

Supported Talend Version
------------------------

* Talend Real-time Big Data platform  7.0
* Talend JobServer 7.0 (same version as the Studio)

Supported Qubole Environments
-----------------------------

* AWS US SOC compliant environment: https://us.qubole.com
* AWS US non-SOC compliant environment: https://api.qubole.com
* AWS India region: https://in.qubole.com
* AWS Europe region: https://eu-central-1.qubole.com
* Azure: https://azure.qubole.com

.. note:: The endpoint represents the Qubole environment you will connect to. Use any of the values mentioned above depending on where your Qubole account is.


Supported Engines in Qubole
---------------------------

For AWS
.......

 * Hadoop 2.6
 * Hive 1.2
 * Spark 2.0


For Azure
.........

 * Hadoop 2.6
 * Spark 2.0

Supported Storage
-----------------

* AWS S3
* HDFS
* Azure Blob




