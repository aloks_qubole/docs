.. _talend-additional-info:

############################################
Additional Information about the Integration
############################################

To learn more about Talend, visit  https://www.talend.com. To report issues to
Talend, visit https://www.talend.com/support/contact-support/.

To learn more about Qubole, visit https://www.qubole.com and http://docs.qubole.com/en/latest/.

To report issues to Qubole use any of the options below:

* On the Qubole UI, click the **Help** icon at the top right corner and select **Submit Support Ticket**. For more information,
  see :ref:`qds-helpcenter.rst`.
* Go to https://qubole.zendesk.com/hc/en-us.
* Call (855) 423-6674 and select option 2.