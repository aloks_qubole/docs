.. _spark-job-create:

##################################
Sample Use Case for Creating a Job
##################################

This topic explains how to create a Spark job after setting up the cluster on Talend server. This sample use case explains how to create a job to filter out S3 data using Talend and Qubole.

-------------
Prerequisites
-------------

The Spark cluster must have been configured on the Talend server. See :ref:`talend-configuration-talend`.

.. note:: If you want to submit the jobs remotely, you must ensure that the Talend Jobserver is installed.


Parameters for this sample use case:

* Name of the cluster connecting to QDS: **qubole_hadoop_cluster**.
* HDFS connection: **qubole_hdfs**.
* Name of the sample job: **spark_sample_job**.

.. note:: Hive components are not supported on the Spark cluster. Therefore, you must not use Hive tables in Spark jobs.

------
Steps
------

Perform the following steps to create a Spark job:

1. In the Talend studio, navigate to **Repository >> Job Design**.
2. Right-click on **Big Data Batch**, and select **Create Big Data Batch Job**.
3. Enter name, purpose, and description for the job in the respective fields as shown in the following figure, and click **Finish**.

.. image:: images-talend/sample-spark-job.png

4. From the new studio editor, navigate to **Repository >> Metadata >> Hadoop Cluster**.
5. Select the Hadoop cluster that was configured. Example, **qubole_hadoop_cluster**.
6. Drag the HDFS connection (example, **qubole_hdfs**) to the **Designer** pane. The **Components** pop-up box is displayed as shown in the figure.

.. image:: images-talend/components.png

7. Select **tHDFSConfiguration** and click **OK**.
8. Click **Finish**.
9. On the **Designer** pane, search for **tS3Configuration** from the **Palette** side bar as shown in the figure.

.. image:: images-talend/palette.png

10. Configure the **tS3Configuration** component:

    a. Drag the **tS3Configuration** component to the **Designer** pane.
    b. Select **tS3Configuration**, and click on the **Component** tab.
    c. Enter the access key, secret key, bucket name, and temp folder in the respective fields as shown in the following figure.

    .. image:: images-talend/ts3config.png

11. On the **Designer** pane, search for **tFileInputDelimited** from the **Palette** side bar.
12. Configure **tFileInputDelimited** component:

    a. Drag the **tFileInputDelimited** component to the **Designer** pane.
    b. Select **tFileInputDelimited**, and click on the **Component** tab.
    c. Select **tS3Configuration_1** for AWS or **tAzureFSConfiguration** for Azure from **Define a storage configuration component**.
    d. Enter name of the file that has to be filtered in the **Folder/File** field.
    e. Select the appropriate **Row Separator** and **Field Separator**.

    The following figure shows the sample values

    .. image:: images-talend/tfileinput.png

..

    f. For Azure, update the storage account name, access key, and container for **tAzureFSConfiguration** as shown in the following figure.

    .. image:: images-talend/azure_fsconfig.png

13. On the **Designer** pane, search for **tFilterRow** from the **Palette** side bar.
14. Configure **tFilterRow** component:

    a. Drag the **tFilterRow** component to the **Designer** pane.
    b. Select **tFilterRow**, and click on the **Component** tab.
    c. Enter the details as shown in the figure.

    .. image:: images-talend/tfilterrow.png

15. On the **Designer** pane, search for **tLogRow** from the **Palette** side bar.
16. Configure **tLogRow** component:

    a. Drag the **tLogRow** component to the **Designer** pane.
    b. Select **tLogRow**, and click on the **Component** tab.
    c. Enter the details as shown in the figure.

    .. image:: images-talend/tlogrow.png

   The following figure shows the view of the sample workflow for AWS.

   .. image:: images-talend/sample-workflow.png

   The following figure shows the view of the sample workflow for Azure.

   .. image:: images-talend/sample-workflow-azure.png

17. From the **Designer** pane, click **Run (Job spark_sample_job)**.
18. Select **Target Exec** from the left pane. Select the custom job server that was configured as part of configuring Talend.
19. Select **Spark Configuration** and add Property Type, Spark Version, and Spark Mode as shown in the following figure.

    .. image:: images-talend/spark-config.png

20. Click on the **Run** tab. From the navigation pane, select **Basic Run**, and click **Run** as shown in the following figure.

    .. image:: images-talend/job-run.png

The job runs on the cluster and the results are displayed on the console.

