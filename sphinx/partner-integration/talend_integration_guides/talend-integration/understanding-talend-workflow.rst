.. _talend-workflow:

================================================
Workflow for using the Qubole-Talend integration
================================================

The Qubole-Talend integration workflow involves the following steps:

.. image:: images-talend/Talend-workflow.png