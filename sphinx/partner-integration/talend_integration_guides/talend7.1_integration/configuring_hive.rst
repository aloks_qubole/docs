.. _talend7.1_configuring_hive:

==============================================
Configuring a Sample Hive Job in Talend Studio
==============================================

This configuration enables a Talend connection with QDS Hive in order to show tables under
the default database. Follow the instructions below to configure a sample Hive job.

1. Open the Talend application.
2. Navigate to **Repository > Job Designs** at the left pane of the Talend window and create a **Standard Job**.

   .. image:: images/talend1.png

3. Name the job and click **Finish**. A standard job is created and opens up on the **Designer** window. It helps you to place
   components for creating a job.
4. Import **tHiveConnection** component from the **Palette** into the **Designer** window and click the component to setup a connection
   under the **Component** ribbon.
5. Select **Qubole** from the **Distribution** drop-down list and enter the supported version in the **Version** field.

   .. note:: This version (Hive 2.1.1) is configured under section :ref:`talend7.1_configuring_qds`.


   .. image:: images/thiveconnection1.png

6. Under the **Connection** section, enter the **API token**. If you want the execution on a configured cluster, enter the
   label name of the cluster in **Cluster label** field. API endpoint is https://api.qubole.com by default, however, you can
   specify the API endpoint based on your location by checking **Change API endpoint** and selecting the right endpoint.

   .. image:: images/api_token.png


   .. note:: **API-TOKEN** is the API token of your QDS account. To find it, go to **Control Panel > My Accounts** and click
             Show under the **API Token** column.

7. Import **tHiveRow** component from the **Palette** into the **Designer** window. It executes the HiveQL query stated
   in the specified database.
8. Select the component from the **Component List** drop-down list to setup a connection and click **Use an existing connection**.
   Select **tHiveConnection** from the **Component List**. Mention the query to be processed. In this example, the tables
   are listed under the default database.

   .. image:: images/thiverow1.png

   The Hive job is now displayed as shown below.

   .. image:: images/hive_job.png

9. Navigate to **Run(Job_hive_sample_job) > Basic Run** and click **Run**. It helps the job to trigger the query mentioned in
   **tHiveRow** on the QDS endpoint.

You have successfully configured and executed the sample Hive job.