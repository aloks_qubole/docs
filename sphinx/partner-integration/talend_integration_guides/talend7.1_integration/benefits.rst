.. _talend7.1_benefits:

=========================================
Benefits of Qubole-Talend 7.1 Integration
=========================================

Theses are the benefits of Qubole-Talend 7.1 integration:

* Reduce the data processing cost by 40%-70% compared to the on-premise solutions. This is achieved by:
    * Moving data workloads to the cloud, reduces the cost of infrastructure and management.
    * Optimizing cloud resources by automatically managing and scaling Big Data engines like Spark, Hadoop, and Hive.
    * Finding the best price-performance ratio by leveraging AWS Spot market by automatically bidding and managing
      cheaper instances when needed.

* Simplify data processing through the power of the cloud. This is achieved by:
    * Leveraging the cost efficiency of a cloud data lake in AWS and Azure.
    * Managing and scaling Big Data engines when Talend jobs are executed.
    * No additional administration overhead installing, configuring and maintaining Apache Spark or Hadoop.

* Increase productivity by eliminating reliance on IT for data preparation in Hadoop and Spark. This is achieved by:
    * Enabling self-service data preparation for non-technical users with Talend Cloud.
    * Focusing on creating data pipelines in Talend instead of managing Hadoop or Spark.
    * Eliminating the time spent in writing complex MapReduce or Spark code for Big Data processing.
    * Providing serverless experience. Users don’t need IT to provision Big Data infrastructure or compute resources for their data
      integration and preparation workflows.

