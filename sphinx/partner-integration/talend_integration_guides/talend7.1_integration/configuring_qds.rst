.. _talend7.1_configuring_qds:

==============================================
Configuring Qubole to Interact with Talend 7.1
==============================================

Follow the instructions for configuring QDS to interact with Talend 7.1:

1. Login to QDS and select a Spark cluster from the **Clusters** drop-down list to run the Spark jobs.

   .. image:: images/clusters.png

2. On the cluster details page, click **Edit** located at the right top corner of the UI.
3. In the **Spark Version** field, select **2.2** and click **Update**.
4. Select a Hadoop 2 cluster from the **Clusters** drop-down list to run the Hive jobs.
5. On the cluster details page, click **Edit** located at the right top corner of the UI.
6. In the **Hive Version** field, select **2.1.1** and click **Update**.

   .. note:: Make a note of these cluster labels as you will need them during the Talend Studio configuration.