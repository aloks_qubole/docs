.. _talend7.1_prerequisites:

Prerequisites
=============

These are the prerequisites:

* Talend Data Fabric  7.1
* Supported Engines in Qubole over AWS: Hive 2.1.1 Beta, Spark 2.2
* Supported Storage: S3
* Qubole Endpoints (either one listed below): https://us.qubole.com (AWS US SOC compliant environment), https://api.qubole.com (AWS US
  non-SOC compliant environment), https://in.qubole.com (AWS India region), and https://eu-central-1.qubole.com (AWS Europe
  region)
