.. _tableau-qds-integration:

==============================================
Understanding the Tableau Integration with QDS
==============================================
Using Qubole and Tableau, organizations can deliver faster analytics, visualization and business intelligence leveraging
Big Data stored in the cloud. Users can create and distribute an interactive and shareable dashboards and reports, with
trends, graphs and charts. This can be done without worrying about the Big Data infrastructure required to access and
process those queries. Qubole automatically provisions, manages, and scales
the Big Data infrastructure in the cloud so analysts and business users can do interactive visualization and analysis
using Tableau.

Tableau users connect to Qubole using Qubole’s ODBC driver and use Hive or Presto engines to analyze their data.

To define the connectivity between Tableau and Qubole, users must specify the Qubole API Token (which Qubole uses to
authenticate access), the cluster name (cluster Label) and the endpoint (Qubole platform in the cloud provider where the
customer has its Qubole account). The first time a query, a dashboard or a report is run, Qubole authenticates the Tableau
user and starts the big data engine (Presto or Hive) that Tableau requires, if it is not already running. After that,
Tableau sends SQL commands through the ODBC driver that Qubole passes to the right cluster.  Qubole manages and runs the
cluster with the right number of nodes and only for the required time, thus saving users up to 40% in infrastructure cost.

The Tableau and Qubole integration leverage the power of the cloud to allow users to connect, explore and visualize
their data. Qubole dramatically reduces the operating expense of running big data compared with traditional on-premise
Big Data infrastructures. Since Qubole automatically provisions clusters for Tableau users and scales them based on
workloads, it also eliminates the painful capacity planning required to expand on-premises clusters to keep up with the
SLA that business requires. Qubole starts and manages ephemeral clusters, therefore there is no resource competition and
performance impact among Tableau users that need to analyze the same data simultaneously. With Qubole, Data
Administrators can associate individual clusters to different Tableau users for self-service analysis without impacting
the performance of existing ad hoc analysis, ETL, or machine learning workload, unlike  on-premises big data deployments
where expanding user access to analyze data has a direct impact on all workloads already running in the cluster.

Here is the Tableau QDS integration architecture diagram.

.. image:: images-tableau/TableauQuboleIntegration.png

