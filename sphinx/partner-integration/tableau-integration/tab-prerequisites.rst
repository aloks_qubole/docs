.. _tableau-prerequisites:

===============================
Understanding the Prerequisites
===============================

There are two ways to connect Qubole Data Service (QDS) with Tableau:

* **Method 1:** :ref:`Using the Qubole Presto Connector (Recommended) <presto_connector>`
* **Method 2:** :ref:`Using Other Databases (ODBC) and Qubole ODBC Driver (Deprecated) <config-tableau-qds-integration>`

It is recommended to use  **Method 1** (:ref:`Using the Qubole Presto Connector <presto_connector>`) as
it provides a more seamless experience and better performance for the end users. This can be used only with Presto as
the backend engine.

**These are the prerequisites for Qubole-Tableau integration using Qubole Presto Connector:**

* `Tableau Desktop and Server version - 2019.1 or above <https://www.tableau.com/support/releases>`_
* Qubole ODBC Driver (For more information, see :ref:`Qubole ODBC Drivers <use-qubole-drivers>`)
* `Qubole Presto Connector Plugin <https://s3.amazonaws.com/paid-qubole/Tableau-Connector/presto/Qubole-Tableau-plugin.zip>`_

**These are the prerequisites when you use Other Databases and Qubole ODBC driver for Qubole-Tableau integration:**

* Tableau Desktop and Tableau Server (preferably 10.2+)
* Qubole ODBC Driver (For more information, see :ref:`Qubole ODBC Drivers <use-qubole-drivers>`)


