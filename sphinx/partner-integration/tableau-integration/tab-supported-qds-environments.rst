.. _tableau-additional-considerations:

====================================================================
Understanding Some Additional Information related to the Integration
====================================================================
The Qubole Tableau integration is supported on these Qubole environments:

* AWS US SOC compliant environment: https://us.qubole.com
* AWS US non-SOC compliant environment: https://api.qubole.com
* AWS India region: https://in.qubole.com
* AWS Europe region: https://eu-central-1.qubole.com
* Azure: https://azure.qubole.com

To learn more about Tableau, visit http://onlinehelp.tableau.com/current/pro/desktop/en-us/help.htm. To report issues to
Tableau, visit https://www.tableau.com/support.

To learn more about Qubole, visit https://www.qubole.com and http://docs.qubole.com/en/latest/.

To report issues to Qubole use any of the options below:

* On the Qubole UI, click the **Help** icon at the top right corner and select **Submit Support Ticket**. For more information,
  see :ref:`qds-helpcenter.rst`.
* Go to https://qubole.zendesk.com/hc/en-us.
* Call (855) 423-6674 and select option 2.
