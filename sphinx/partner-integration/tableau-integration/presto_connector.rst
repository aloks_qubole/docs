.. _presto_connector:

===============================================================
Configuring Tableau Using Qubole Presto Connector - Recommended
===============================================================

Follow the instructions below to configure QDS using the Qubole Presto connector:

A. Install Qubole ODBC Driver for Mac, Windows, and Linux:
-------------------------------------------------------------
Install ODBC driver for MAC/Windows/Linux. For more information, see :ref:`Qubole ODBC Drivers <use-qubole-drivers>`.

.. note:: You don't require to configure the ODBC API token for the QDS account as mentioned in :ref:`install-odbc-driver-switch`.

B. Download Qubole Presto Connector:
---------------------------------------

`Click here <https://s3.amazonaws.com/paid-qubole/Tableau-Connector/presto/Qubole-Tableau-plugin.zip>`_ to download the
Qubole Presto Connector.

C. Plug in the Qubole Presto Connector:
---------------------------------------
Follow the instructions below to plug in the Qubole Presto connector:

1. After downloading the Qubole Presto Connector, run the following command to extract plugin ``Qubole-Tableau-connector.zip``.

   .. sourcecode:: bash

      $ unzip Qubole-Tableau-plugin.zip

2. Create a directory for Tableau connectors in the following location:

   **For Tableau Desktop:**

     MacOS: ``/connector``

     Windows: ``C:\connector``

   **For Tableau Server:**

     Linux: ``/connector``

     Windows: ``C:\connector``

   .. Note:: The location of the connector must be in the root directory.

3. Copy the ``qubole_odbc`` directory containing your connector’s ``manifest.xml`` file in the newly created directory and
   use the following command to validate:

   .. sourcecode:: bash

      $ ls /connector/qubole_odbc
      Connection-dialog.tcd
      connectionBuilder.js
      connectionResolver.tdr
      manifest.xml

4. Run Tableau using ``-DConnectPluginsPath`` command line argument for Tableau Desktop pointing to your connector directory,
   as mentioned below:

   **For MAC:**

   .. sourcecode:: bash

      /Applications/Tableau\ Desktop\ 2019.1.app/Contents/MacOS/Tableau
      -DConnectPluginsPath=/connector/

   **For Windows:**

   .. sourcecode:: bash

      cd c:\Program Files\Tableau\Tableau 2019.1\bin
      tableau.exe -DConnectPluginsPath=C:\connector

5. Run TSM (Tableau Server Manager) with the following option to make it available for publishing:

   For Linux/Windows:

   .. sourcecode:: bash

      tsm configuration set -k native_api.connect_plugins_path -v <path_to_connector> --force-keys
      tsm pending-changes  apply

      This operation will perform a server restart. Are you sure you wish to continue?
      (y/n): y
      Starting deployments asynchronous job.

You have successfully plugged in the connector.

.. note::

   * If you mention an incorrect <path-to-directory> which doesn't have a plugin code, you will view the following errors:

     **For Tableau Desktop:** Qubole Presto Connector with the name “Qubole Presto” will not appear in the list of connectors
     when you start Tableau.

     **For Tableau Server:** Once you publish a report from Tableau Desktop, prompt appears with an error: “Tableau doesn’t
     recognize the data source type `qubole_odbc`”, as shown below:

    .. image:: images-tableau/error.png

D. Connecting to Qubole Data Service (QDS):
-------------------------------------------
Follow the instructions below to connect to QDS:

1. Open the Tableau and navigate to the menu.

   .. image:: images-tableau/tab-menu.png

2. Click **Qubole Presto** to add required parameters from QDS such as End Point, API Token, Catalog, and Cluster Label.

   .. image:: images-tableau/qubole_presto.png

3. Click **Sign In** to fire a query on the Qubole portal.

You have successfully connected to Qubole Data Service (QDS) via Qubole Presto Connector.

.. note::

   * Remove all the ``.tdc`` files (if present) from ``Documents/My\ Tableau\ Repository/Datasources``.
   * Open **Other Sessions** of Tableau from a primary instance that has the same connector. This ensures that all new
     sessions make use of Qubole Presto Connector.
