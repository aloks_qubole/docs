.. _configuring-tableau-server:

===================================================================================
Publishing a Workbook from Desktop and Scheduling Extract Refresh on Tableau Server
===================================================================================
Follow the instructions below to publish a workbook from Desktop and schedule an extract refresh on Tableau server:

1. Double-click **New Custom SQL** on the Tableau Desktop.

   .. image:: images-tableau/PrestoNewCustomSQL.png

2. Enter a query and click **OK**.
3. The query gets submitted to Qubole and can be seen on the QDS UI’s Analyze page.
4. On the right top corner, select **Extract** under **Connection**.
5. On the bottom-left corner, click **go to worksheet**.
6. A **Save Extract as** dialog appears asking to save the datasource. Retain the file name or change it if required and
   click **Save**. The dialog is illustrated below.

   .. image:: images-tableau/SavePrestoDataExtract.png

7. Customise the worksheet.
8. Go to Tableau Server. Enter the username and password and click **Sign In**.

   .. image:: images-tableau/PrestoTableauLoginPage.png

9. Click **Server** and click **Publish Workbook**.
10. Provide a name and description for the workbook.
11. Select edit that is next to **Datasource**.
12. Select **Embedded in workbook** and **Allow refresh access**.
13. Click **Publish**.

    .. image:: images-tableau/PrestoTableauServerPublish.png

14. A server page is launched.
15. Select **Schedule Refresh**.

    .. image:: images-tableau/PrestoTableauPublishingComplete.png

16. Select a time to auto-run refresh by server on the extract and click **Schedule a Refreshes**.

    .. image:: images-tableau/PrestoTableauScheduleExtractRefresh.png

17. To test run, you must manually start a refresh.
18. Go to the **Refresh Schedule** tab and click the **refresh...**.
19. Click **Run Now**.

    .. image:: images-tableau/RunExtractRefresh.png

20. Go to the **Status** tab.
21. Select **Background Tasks for Extracts**.

    .. image:: images-tableau/TableauServerStatus.png

22. Select the Process which is currently running.
23. It launches a page with timeline for the extract refresh run. Also on completion, the status is shown as success.
    Here is a snippet of a passing extract refresh.

    .. image:: images-tableau/SuccessfulExtractRefresh.png

24. Learn more details on the `Extracting Refresh <http://onlinehelp.tableau.com/current/pro/desktop/en-us/extracting_refresh.html>`__ page.
