.. _workbook:

===================
Creating a Workbook
===================

Follow the instructions to create a workbook.

1. Under the **Connections** at the top-left corner, search for a Schema in the **Schema** field. A query runs on Qubole
   to fetch all available Schemas.

   .. image:: images-tableau/ODBCTableauConnections.png

2. Select the **default** schema or the schema required for the case where you are trying to build visualization from your
   own tables/data.
3. Search for the table name in the **Table** field.

    .. image:: images-tableau/Tableau-HiveTable-Windows.png

4. Select ``default_qubole_airline_origin_destination`` (or any other table in the default schema) table to work with
   the data in it.
5. Click **Update Now**. Updating takes some time to complete depending on the cluster size and data.

    .. image:: images-tableau/Tableau-UpdateNow.png

6. Once the table gets updated, click **Sheet 1** to start designing. If you are configuring Tableau using the **Other Databases (ODBC)**,
   you will view a warning as shown below:

    .. image:: images-tableau/TableauODBCWarning.png

7. Click **OK** and the sheet gets loaded.
8. You can see the column names of the database on the left side panel under **Dimensions**. You can drag and drop the
   **Dimensions** and **Measures** to **Rows** and **Columns** as required to create a visualization/report.
9. Refer to the following figures that illustrate **Rows** and **Columns**.

    .. image:: images-tableau/TableauRowsandColumns.png

    .. image:: images-tableau/TableauFilters.png

    .. image:: images-tableau/TableauSheet3.png
