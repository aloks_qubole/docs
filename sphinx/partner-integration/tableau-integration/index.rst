.. _tableau-integration-guide-index:

================================
Qubole Tableau Integration Guide
================================
Qubole Data Service (QDS) is a cloud-native autonomous data platform that removes the complexity and reduces the cost of
managing Big Data, allowing the data team to focus on business outcomes rather than on managing infrastructure.

QDS self-manages and constantly analyzes and learns about the platform’s usage through a combination of heuristics and
machine learning, providing insights and recommendations to optimize reliability, performance and cost. QDS orchestrates
and manages Big Data clusters such as Hadoop, Spark, Presto, and Hive in any public cloud and allows direct access to Big
Data services securely with user-access control through the QDS user interface (Analyzer and Notebooks), via a REST API
and ODBC/JDBC interfaces.

Tableau is a business intelligence solution that integrates data analysis and reports into a continuous visual analysis
process that is easy to learn and use.

Organizations deploying Big Data face the challenge to provide timely data access to business users (Analysts, Data
Scientists, and so on) while running,  managing and optimizing the Big Data infrastructure to keep up with the
increasing amount of data.

Qubole and Tableau address those challenge by providing fast data access while removing the complexities of provisioning,
managing, and scaling the Big Data infrastructure.

This guide describes the steps to configure Tableau for using it with the Qubole Data Service.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    tab-integration-benefits.rst
    tab-prerequisites.rst
    presto_connector.rst
    configuring-tab-integration.rst
    workbook.rst
    configuring-tab-server.rst
    tab-supported-qds-environments.rst


