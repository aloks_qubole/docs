.. _qds-helpcenter.rst:

===============
QDS Help Center
===============
The top-right corner of the QDS user interface (UI) contains a help icon |helpicon|.

.. |helpicon| image:: ug-images/helpicon.png

Clicking the icon displays the QDS UI help center as shown below.

.. image:: ug-images/HelpCenterIcon.png

The **Help Center** contains the following assistance:

* A generic search field associated with a search button.
* **Announcements**
* **Resources**
* **Support**

Using the Search Field
----------------------
Type a word/phrase in the search field and click the search button. The following figure shows a search result sample.

.. image:: ug-images/HelpCenterSearchResult.png

The results from Qubole knowledge base, documentation, forums, and blogs are displayed separately. Click the individual
search result to see the word/phrase that you wanted to look up.

About the Announcements
-----------------------
This section highlights major changes related to the QDS platform and it is a copy of the **Announcements** section of
the Qubole Support website.

Using the Resources Assistance
------------------------------
The following resources are available:

* **Knowledge Base** - Clicking it directs you to the **Knowledge Base** section of the Qubole Support website.
* **Support Portal** - Clicking it directs you to the home page of the Qubole Support website.
* **Documentation** - Clicking it directs you to the Qubole documentation website.
* **Walk Through** - Clicking it gives you a walk through of the current main pages such as Analyze and Explore. For
  example, if you are on **Analyze** and click the **Walk Through** from the **Help Center**, you see the following
  tutorials based on **Analyze**.

  .. image:: ug-images/TutorialsAnalyze.png

  Similarly, when you are on the **Explore** page, click **Walk Through** from the **Help Center** to see the following
  tutorials based on **Explore**.

  .. image:: ug-images/TutorialsExplore.png

* **Video Tutorials** - Clicking it displays a list of tutorials on how to use QDS.
* **Quick Tour** - Clicking it gives you a quick tour of the QDS User interface as shown in the following figure.

  .. image:: ug-images/HelpCenterQuickTour.png

* **Download Drivers** - This takes you to the link where you can download Qubole ODBC and JDBC drivers.
* **Education** - This takes you to the Qubole Education website.

Using the Support Assistance
----------------------------
The **Support** section in the help center contains the following assistance URLs:

* **Ask in forums** - Clicking it redirects you to the **New Post** page of **Qubole Forums** in the **Qubole Support**
  website. If you have logged in as a user of the **Qubole Support** website, then you see the **New Post** page as
  shown in the following figure.

  .. image:: ug-images/AskinForum.png

  In the **New Post** page, write a title for a post (a mandatory field) and add details in the corresponding text boxes.
  Select a related topic from the **Topic** drop-down list (a mandatory field) and click **Submit**.

* **Submit Support Ticket** - Clicking it displays the **Submit Support Ticket** dialog as shown in the following figure.

  .. image:: ug-images/SubmitSupportTicket.png

  Write a subject and describe the issue in the corresponding text fields. Set a priority for the ticket if you want to
  change it from the default **Low** priority. Enter the command ID if it is related to a QDS command. Click **Submit** to
  create a helpdesk ticket. Click **Cancel** if you do not want to create a helpdesk ticket and return to the previous dialog.

