.. _running-shell-command:

=======================
Running a Shell Command
=======================

This document is intended to get a new user up and running with QDS, by
running a simple Shell Command. As a prerequisite, the user must have
signed up for Qubole (via the Sign Up page) and has a working
account in QDS (else, create one as described in :ref:`manage-my-account`).


Running a Shell Command from Analyze
------------------------------------

Perform the following steps to run a Shell Command:

#. Navigate to the `Analyze <https://api.qubole.com/v2/analyze>`__ page
   from the top menu and click the **Compose** button. A command composer and editor is displayed.
#. In the **Compose** command editor, select the command type as **ShellCommand** from the drop-down
   list.
#. Specify the shell command to be run. For example:

   ``hadoop dfs -ls s3://paid-qubole/``
#. Optionally specify other files or archives to be copied to the directory where the shell command executes.
#. Click **Run**. QDS brings up a cluster to run the job; this may take a few minutes. You can watch the progress of the
   job under the **Logs** tab; when it completes, you can see the query results under the **Results** tab.



**Congratulations!** You have executed your first shell command using the Qubole Data Service.




