.. _compose-redshift:

==========================
Composing a Redshift Query
==========================

Compose a Redshift query using the query composer on the **Analyze** page.

.. note:: Hadoop 1, Hadoop 2, Presto, and Spark clusters support Redshift queries, and the queries do not need run on a cluster. See :ref:`cluster-command-map` for more information.

Prerequisites
-------------
You must have an existing data store to query it. Create a data store in **Explore** if it does not exist.

Perform the following steps to compose a Redshift query:

.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts.

1. Navigate to the **Analyze** page and click **Compose**. Select **Redshift Query** from the **Command Type** drop-down list.
2. **Query Statement** is selected by default from the drop-down list.
   Depending on your choice, perform the appropriate actions:

   * If you want to use **Query Statement**, enter the RedShift query in the text field.
     The following figure shows a sample Redshift query.

     .. image:: analyze-images/ComposeRedshift.png

   * If you want to use **Query Path**, select **Query Path**, then specify the Cloud storage path that contains the Redshift query file.
     The following figure shows a sample Redshift query that is executed by using the **Query Path** option.

     .. image:: analyze-images/ComposeRedshift_1.png

3. Pick a data store from the **Redshift Data Store** drop-down list.
4. Click **Run** to execute the query. Click **Save** if you want to run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)
5. The query result is displayed in the **Results** tab and the query logs in the **Logs** tab. The **Logs** tab has
   the **Errors and Warnings** filter. For more information on how to download command results and logs, see
   :ref:`download-analyze-results`.