.. _compose-shell-command:

=========================
Composing a Shell Command
=========================
Use the command composer on the **Analyze** page to compose a shell command. See
:ref:`Running a Shell Command <running-shell-command>` for more information.

.. note:: Hadoop 1, Hadoop 2, Presto, and Spark clusters support shell commands. See :ref:`cluster-command-map` for more
          information. :ref:`Some Cloud platforms <os-version-support>` do not support all cluster types.

          It is not recommended to run a Spark application as a Bash command under the Shell command options because
          automatic changes such as increase in the Application Master memory based on the driver memory and debug options'
          availability **do not happen**. Such automatic changes occur when you run a Spark application through the
          **Command Line** option.

Perform the following steps to compose a shell command:

.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts.

1. Navigate to the **Analyze** page and click **Compose**.
2. Select **Shell Command** from the **Command Type** drop-down list.
3. From the drop-down list, choose **Bash Commands** (default); or specify the location of a script in Cloud storage.
4. To use a shell command, enter it into the text field.
   If you are using a script, specify **script parameters**, if any, in the text field.
5. In the **List of Files** text field, optionally list files (separated by a comma) to be copied from Cloud storage to
   the working directory where the command is run.
6. in the **List of Archives** text field, optionally list archive files (separated by a comma) to be uncompressed in the
   working directory where the command is run.
7. Click **Run** to execute the query. Click **Save** if you want to re-run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)

You can see the result under the **Results** tab, and the logs under the **Logs** tab. The **Logs** tab has the
**Errors and Warnings** filter. For more information on how to download command results and logs, see
:ref:`download-analyze-results`.
