.. _download-complete-raw-result:

================================================
Download Complete Raw Result from the Analyze UI
================================================
A successful query result can be downloaded from the **Analyze** > **History** tab by referring to the corresponding
command ID and query and also from the **Results** tab of that command. Some queries have larger result sets than the
default file size of 20 MB (or configured result file size limit). For more information on how to download
command results, see :ref:`download-analyze-results`.

.. note:: :ref:`analyze-index` describes composing different types of commands that you can run on the Analyze Query composer.

Click the download button of the query ID that has the large result set in the **History tab**. The options are as shown:

.. image:: ../../admin-guide/ag-images/DownloadCompleteRawResult1.png

Click **Download Complete Raw Results**. Alternatively, you can download the complete raw result by clicking **Download**
available at the right-most corner of the **Results** tab as shown in this example:

.. image:: ../../admin-guide/ag-images/CompleteRawResult-Tab.png

The Cloud storage bucket is displayed with the corresponding directory and files:

.. image:: ../../admin-guide/ag-images/FullRawResult.png

Select a file or expand the directory to select files, or download all folders and files.

.. note:: You can only download a single file at a time.

After making your selection, click **Download**. Click **Close** to go back to the **Analyze** main tab.


Merging Individual Files from a Complete Raw Result Download
------------------------------------------------------------
When you get multiple files as part of a complete raw result download, merge them to see the complete result.

To merge multiple files to write them into a single file, use any of the following commands:

* ``cd <folder>;`` create a folder and run the following command to merge the individual files into a single file:

    - ``cat .* | tr \\1 \\t > output.txt``

* ``cat <files> | tr \\1 \\t > query_output.txt``

