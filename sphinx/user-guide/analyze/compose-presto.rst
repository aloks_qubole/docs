.. _compose-presto-query:

========================
Composing a Presto Query
========================

.. note:: Presto is not currently supported on all Cloud platforms; see :ref:`os-version-support`.

You can compose a Presto query using the query composer available in the **Analyze** page.
See :ref:`Presto In Qubole <presto-qubole-index>` for more information.

.. note:: Presto queries run on Presto clusters. See :ref:`cluster-command-map` for more information. Presto supports
          querying tables backed by the storage handlers.

.. note:: QDS uses Presto Ruby client that provides better overall performance such as processing DDL queries much faster
          and quickly reporting errors that a Presto cluster generates. However, this feature is available for beta access.
          Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable Presto Ruby client for
          the Qubole account. For more information, see the `Presto Ruby Client in QDS  <https://www.qubole.com/blog/product/presto-ruby-client-in-qds/>`__ blog.

Perform the following steps to compose a Presto query:

.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts.

1. Navigate to the **Analyze** page and click **Compose**. Select **Presto Query** from the **Command Type** drop-down
   list.
2. **Query Statement** is selected by default from the drop-down list. Enter the Presto query in the text field.

   -or-

   If you want to run a query through a path, select **Query Path** from the drop-down list, then specify the cloud storage path that contains the Presto query file.

   A sample Presto query in the composer is shown in the following figure.

   .. image:: ../ug-images/ComposePresto.png

3. Click **Run** to execute the query. Click **Save** if you want to run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)

   .. caution:: Configuring retries will just do a blind retry of a Presto query. This may lead to data corruption for
                non-Insert Overwrite Directory (IOD) queries.

4. The query result is displayed in the **Results** tab and the query logs in the **Logs** tab. The **Logs** tab has
   the **Errors and Warnings** filter. For more information on how to download command results and logs, see
   :ref:`download-analyze-results`.

   .. note:: For a given Presto query, a new Presto Query Tracker is displayed in the **Logs** tab when:

             * A cluster instance that ran the query is still up.
             * The query info is still present in the Presto server in that cluster instance. The query information is
               periodically purged from the server.

             If any of the above 2 conditions is not met, the older Presto Query Tracker is displayed in the **Logs** tab.

For REST API-related information, see :ref:`Submit a Presto Command <submit-a-presto-command>`.

About Presto System Monitoring
------------------------------
**Presto clusters support Datadog monitoring when the Datadog monitoring is enabled at the QDS account level**.
**Datadog settings cannot be overridden at the cluster level for a Presto cluster**. Create your own dashboard for
the Presto cluster as Qubole Dashboards do not currently support Presto clusters. For more information on enabling
Datadog in **Control Panel** > **Account Settings**, see :ref:`iam-keys` or :ref:`manage-roles`.

:ref:`presto-system-metrics` describes the list of metrics that can be seen on the Datadog monitoring service. It
also describes the abnormalities and actions that you can perform to handle abnormalities.