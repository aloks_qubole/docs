.. _compose-pig-query:

=====================
Composing a Pig Query
=====================
You can compose a Pig query using the query composer available in the **Analyze** page. See
:ref:`Running a Pig Job <running-pig-job>` and :ref:`Pig in Qubole <pig-qubole-index>` for more information.

.. note:: Hadoop-1, Hadoop-2, and Presto clusters support Pig queries. See :ref:`cluster-command-map` for more
          information.

          You can configure the Pig version on an Hadoop 2 (Hive) cluster. Pig 0.11 is the default version. Pig 0.15 and
          Pig 0.17 (beta) are the other supported versions. You can also choose between MapReduce and Tez as the execution
          engine when you set the Pig 0.17 (beta) version. Pig 0.17 (beta) is only supported with Hive 1.2.0.

Perform the following steps to compose a Pig query:

.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts.

1. Navigate to the `Analyze <https://api.qubole.com/v2/analyze>`__ page and click **Compose**. Select
   **Pig Query** from the **Command Type** drop-down list.
2. **Query Statement** is selected by default from the drop-down list, which has **Query Path** as the other option.
   A sample Pig query in the composer is shown in the following figure.

   .. image:: ../ug-images/ComposePigQ.png

3. In the text field, enter the Pig query. If you select **Query Path**, then specify the S3 bucket directory path that
   contains the Pig query file. See :ref:`pig-query-path-compose` for more information.
4. Click **Run** to execute the query. Click **Save** if you want to run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)
5. The query result is displayed in the **Results** tab and the query logs in the **Logs** tab. The **Logs** tab has the
   **Errors and Warnings** filter. For more information on how to download command results and logs,
   see :ref:`download-analyze-results`.

For REST API-related information, see :ref:`Submit a Pig Command <submit-a-pig-command>`.

.. _pig-query-path-compose:

Composing a Pig Query by Specifying a Query Path
------------------------------------------------
1. Navigate to the `Analyze <https://api.qubole.com/v2/analyze>`__ page and click **Compose**. Select
   **Pig Query** from the **Command Type** drop-down list.
2. Select **Query Path** from the drop-down list that contains **Query Statement** selected by default.
   A sample query composer for a Pig query path is as shown in the following figure.

.. image:: ../ug-images/ComposePigQ1.png

3. In the text field, enter the S3 location that contains the pig query path. Enter the script parameters in the
   **Pig S3 script parameters** in this format, **key1=value1!key2=value2...**.
4. Click **Run** to execute the query. Click **Save** if you want to run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)
5. The query result is displayed in the **Results** tab and the query logs in the **Logs** tab. The **Logs** tab has
   the **Errors and Warnings** filter. For more information on how to download command results and logs, see
   :ref:`download-analyze-results`.