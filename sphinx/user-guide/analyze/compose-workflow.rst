.. _compose-workflow:

==========================
Composing a Workflow Query
==========================
Compose a workflow query when more than one command has to be executed a sequence; use the query composer
on the **Analyze** page. (For REST API-related information, see
:ref:`Submit a Composite Command <submit-a-composite-command>`.)

.. note:: Hadoop-1, Hadoop-2, Presto, and Spark clusters support workflow queries. However, the cluster support depends
          on the type of query that is in a sequence. See :ref:`cluster-command-map` for more information.

Perform the following steps to compose a workflow query:

.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts.

1. Navigate to the **Analyze** page and click **Compose**. Select **Workflow** from the **Command Type** drop-down list.
   The query composer is displayed as shown in the following figure.

   .. image:: analyze-images/ComposeWorkflowA.png

2. Click **+ Add Command** to create a new query. After you click it, the query composer is displayed as shown in the
   following figure.

   .. image:: analyze-images/ComposeWorkflowB.png

3. Pick the type of command that you want from the **Select Command Type** drop-down list.
4. Compose the query and click **+ Add Command** to add another query. Repeat steps 2-4 to add the next type of query.
   Similarly, you can add the required number of queries to be executed in a sequence.

   The following figure shows a workflow query composed to execute a data import, hive, and data export queries in the
   same order.

   .. image:: analyze-images/ComposeWorkflow.png

5. Click **Run** to execute the query. Click **Save** if you want to run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)
6. The query result is displayed in the **Results** tab and the query logs in the **Logs** tab. The **Logs** tab has
   the **Errors and Warnings** filter. For more information on how to download command results and logs, see
   :ref:`download-analyze-results`.