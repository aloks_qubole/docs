.. _compose-hadoop-query:

======================
Composing a Hadoop Job
======================
Use the command composer on the **Analyze** page to compose a Hadoop job. See the
:ref:`quick-start-guide-index` for an example.

You can use the query composer for these types of Hadoop job:

* Custom jar. See :ref:`Compose a Hadoop Custom Jar Query <compose-hadoop-jar>`.
* Streaming. See :ref:`Compose a Hadoop Streaming Job <compose-hadoop-stream>`.
* DistCp. See :ref:`Compose a Hadoop DistCp Command <compose-hadoop-s3distcp>`.

.. note:: Hadoop 1, Hadoop 2, and Presto clusters support Hadoop job queries. See :ref:`cluster-command-map` for more
          information.

          Ensure that the output directory is new and does not exist before running a Hadoop job.

          Qubole has deprecated Hadoop 1 as-a-service. For more information, see :ref:`hadoop1-dep`.

.. _compose-hadoop-jar:

Compose a Hadoop Custom Jar Query
---------------------------------
.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts. In new QDS accounts, QDS provides
          example saved queries of different command types. For more information, see :ref:`repo-tab`.

Perform the following steps to compose a Hadoop jar query:

1. Navigate to the **Analyze** page and click **Compose**. Select **Hadoop Job** from the **Command Type** drop-down list.
   **Custom Jar** is selected by default in the **Job Type** drop-down list, and this is what you want.
2. In the **Path to Jar File** field, specify the path of the directory that contains the Hadoop jar file.
3. In the **Arguments** text field, specify the main class, generic options, and other JAR arguments. The following figure
   shows an example using AWS S3:

   .. image:: ../ug-images/ComposeHadoopJob.png

4. Click **Run** to execute the query. Click **Save** if you want to re-run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)

For REST API-related information, see :ref:`Submitting a Hadoop Jar Command <submit-a-hadoop-jar-command>`. For developing
applications, see :ref:`use-cascading-with-qds`.

You can see the result under the **Results** tab, and the logs under the **Logs** tab. The **Logs** tab has the
**Errors and Warnings** filter. For more information on how to download command results and logs, see
:ref:`download-analyze-results`.

.. _compose-hadoop-stream:

Compose a Hadoop Streaming Query
--------------------------------
Perform the following steps to compose a Hadoop streaming job query:

1. Navigate to the **Analyze** page and click **Compose**.
2. Select **Hadoop Job** from the **Command Type** drop-down list.
3. Select **Streaming** from the **Job Type** drop-down list.
4. In the **Arguments** field, specify the streaming and generic options.
5. Click **Run** to execute the query. Click **Save** if you want to re-run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)

You can see the result under the **Results** tab, and the logs under the **Logs** tab. The **Logs** tab has the
**Errors and Warnings** filter. For more information on how to download command results and logs, see
:ref:`download-analyze-results`.

.. _compose-hadoop-s3distcp:

Compose a Hadoop DistCp Command
-------------------------------
Perform the following steps to compose a Hadoop DistCp command:

1. Navigate to the **Analyze** page and click **+ Create**. Select **Hadoop Job** from the **Command Type** drop-down list.
2. From the **Job Type** drop-down list, select **s3distcp** for AWS, or **clouddistcp** for Azure or Oracle.
3. In the **Arguments** text field, specify the generic and DistCp options.
4. Click **Run** to execute the command. Click **Save** if you want to re-run the same command later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)

You can see the result under the **Results** tab, and the logs under the **Logs** tab. The **Logs** tab has an
**Errors and Warnings** filter. For more information on how to download command results and logs, see
:ref:`download-analyze-results`.
