.. _analyze-index:

##############
Data Analytics
##############

This section explains how to use the QDS user interface to run commands. It covers the following
topics:

.. toctree::
   :maxdepth: 1

   qpal.rst
   executiontypes.rst
   commands.rst
   analyze-search.rst
   download-analyze-results.rst
   download-complete-raw-result.rst
   sessions.rst
   cluster-command-map.rst
   compose-airflow.rst
   compose-redshift.rst
   running-shell-command.rst
   compose-shell.rst
   compose-workflow.rst
   compose-hadoop.rst
   compose-hive.rst
   compose-query-export.rst
   compose-refresh-table.rst
   compose-data-export.rst
   compose-data-import.rst
   compose-db-query.rst
   compose-pig.rst
   compose-presto.rst
   compose-spark.rst
   tsql.rst


For more information, see :ref:`command-templates`.

