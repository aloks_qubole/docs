.. _compose-dataexport:

==============================================
Composing a Data Export Command through the UI
==============================================
Use the command composer on the **Analyze** page to compose a data export command. You can export Hive tables and
directories from Cloud storage. See :ref:`Data Export <data-export>` for more
information.

Prerequisites
-------------
You must have an existing data store to which the data is to be exported. Create a data store in **Explore** if it does
not exist.

.. note:: Hadoop 2, Presto, and Spark clusters support database commands; these commands can also be run without bringing
          up a cluster. See :ref:`cluster-command-map` for more information.

.. _compose-hivetable-export:

Composing a Hive Table Export Command
-------------------------------------

.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts.

Perform the following steps to compose a command to export a Hive table:

1. Navigate to the **Analyze** page and choose **Compose**.
2. Choose **Data Export** from the **Command Type** drop-down list. The **Mode** defaults to **HiveTableExport**, which
   is what you want.
3. Specify the Hive table in the **HiveTable** text field.
4. In the **Hive Table Partition Spec** text field, specify the Hive partitions if any. Leave the field blank if the table
   has no partitions.
5. Choose a data store from the **Data Store** drop-down list.
6. Choose a table from the **DbTable** drop-down list.
7. Choose a **DB Update Mode**. **Append Mode** is the default. The other two options are **Update Only Mode**
   and **Insert and Update Mode** (supported only for an Oracle MySQL database).
8. If you want to run the command on a Hadoop cluster, click the **Use Hadoop Cluster** checkbox
   and choose the cluster label from the drop-down list.
9. Click **Run** to execute the command. Click **Save** if you want to re-run the same command later.
   (See :ref:`Workspace <repo-tab>` for more information on saving commands and queries.)


You can see the result under the **Results** tab and the logs under the **Logs** tab. The **Logs** tab has the
**Errors and Warnings** filter. For more information on how to download command results and logs, see
:ref:`download-analyze-results`.

.. _compose-directory-export-query:

Composing a Directory Export Command
------------------------------------
Perform the following steps to export a directory from a Cloud storage bucket to a data store:

1. Navigate to the **Analyze** page and choose **Compose**.
2. Choose **Data Export** from the **Command Type** drop-down list. Change the **Mode** to **Directory Export**.
3. In the **Export Directory** text field, specify the path of the Cloud storage directory.
4. If necessary, change the default value of the field separator in the **Fields Terminated by** text field.
5. Choose a data store from the **Data Store** drop-down list.
6. Choose a table from the **DbTable** drop-down list.
7. Choose a **DB Update Mode**. **Append Mode** is the default. The other two options are **Update Only Mode**
   and **Insert and Update Mode** (supported only for an Oracle MySQL database).
8. If you want to run the command on a Hadoop cluster, click the **Use Hadoop Cluster** checkbox
   and choose the cluster label from the drop-down list.
9. Click **Run** to execute the command. Click **Save** if you want to re-run the same command later.
   (See :ref:`Workspace <repo-tab>` for more information on saving commands and queries.)


You can see the command result under the **Results** tab and the command logs under the **Logs** tab. The **Logs** tab
has the **Errors and Warnings** filter. For more information on how to download command results and logs, see
:ref:`download-analyze-results`.
