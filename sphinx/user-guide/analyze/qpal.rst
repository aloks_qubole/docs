.. _analyze-qpal:

================================
About the Analyze User Interface
================================
Use the **Analyze** page to compose and run queries and commands against datasets. You can also use **Analyze** to
exchange data between various databases and Qubole using the Data Import Commands and Data Export Commands.

This section explains how to use the Analyze page and its component tabs:

* :ref:`History <history-tab>`
* :ref:`Tables <tables-tab>`
* :ref:`S3 (AWS) <s3-tab>`

:ref:`analyze-shortcuts` describes the supported keyboard shortcuts.


.. _history-tab:

History Tab
-----------

.. note:: Commands older than 6 months from the ODBC, API, and SDK sources will not be available for review from the **Analyze** interface.
          However, these commands will continue to be archived into your configured cloud storage.

Navigating to the **Analyze** page brings up the **History** tab by default, as shown in the following figure.

.. image:: ../ug-images/History1.png

This shows previous commands, logs and results. Search for previous commands by providing the command ID, command type,
status, user, tags, name, and so on. You can re-run previous queries, or schedule them to run later, without edits. By
default, 4 filters are selected and the search filter text field format is changed. For example, you can enter ``type:hive status:failed``
as a search string in the search filter's text box to get the list of failed Hive commands.

QDS explicitly indicates commands that are waiting to execute. The |queue| icon indicates that the command is throttled and provides its
position in the queue so that users can distinguish these from commands that are already running. A warning message also appears
in the lower right pane under **Logs**.

.. |queue| image:: ../ug-images/QueueStatus.png

.. note:: To avoid other users from seeing your commands, assign other users a policy for ``Command Resource`` with
          only ``create`` permission in **Control Panel** > **Manage Roles**. With this policy, other users can create
          a command but are denied access to see other users' commands. For more information, see :ref:`manage-roles-user-resources-actions`.

The filter with all fields is as illustrated below.

.. image:: analyze-images/HistoryFilter.png

:ref:`analyze-search` describes the search experience for text/keywords searches and other search fields. For each word
search in the **Text** field, the **Search History** displays results in these two different ways:

* The default search history that displays search results for the words (specified in the **Text** field) from the last
  100 commands run by the current user. With this default search history, this condition is valid:

  The filter in the **History** tab has **Start Date** and **End Date** for filtering between a specific duration.
  Whenever you select a cluster label or tag in this filter, selecting a duration (a maximum of 30 days) is mandatory.

* The search history searches the words (specified in the **Text** field) in the last 6 months data and displays
  such search results. This provides a larger set of results. This feature is not enabled by default. To enable it,
  create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.


.. _tables-tab:

Tables Tab
----------
This tab shows all the Hive schemas and tables in your account; for example.

.. image:: ../ug-images/Analyze-Tables1.png

Click on a table to see its columns and their types. You can also refresh the schema to get metadata for any newly
created Hive tables. This view helps in composing Hive or Presto queries against the Hive tables.

.. _s3-tab:

S3 Tab (AWS)
------------
Use this tab to browse Amazon S3 buckets and examine the directories and files.

Uploading and Downloading a File to an S3 Location
..................................................
Using the S3 tab in the **Analyze** UI, you can upload a downloaded results file (in CSV or other format) to an S3 location.

.. note:: You must have the **Object Storage** resource permission for the role assigned to you for uploading and downloading
          data. For more information, see :ref:`manage-roles-user-resources-actions`.

To upload a file to an S3 location, follow these steps:

1. Go to the **S3** tab and select an S3 location to which you want to upload. Click the gear icon against
   the S3 location as illustrated here.

   .. image:: analyze-images/AnalyzeUploadButtontoS3.png

|

2. The Upload to S3 dialog appears as shown here.


   .. image:: analyze-images/UploadtoS3.png

   Click **Choose File** to browse to the location of the file and select it.

3. Click **Upload** for uploading the file to the S3 location. Click **Cancel** if you do not want to upload.

Similarly, you can download a file from an S3 location. Select the file that you want to download from an S3 location and
click the gear icon against the file. You can see **Download** as shown here.

.. image:: analyze-images/DownloadFromS3Analyze.png


.. _analyze-shortcuts:

Using the Supported Keyboard Shortcuts in Analyze
-------------------------------------------------
Press **Shift** + **/** (implies **?**) to see the list of available keyboard shortcuts. You can disable or enable the
keyboard shortcuts in **Control Panel** > **My Profile**. By default the shortcuts are enabled. For more information,
see :ref:`manage-profile`.

.. note:: The keys are case sensitive.

+----------------------------------------+------------------------------------------------------------------------------+
| Keyboard Shortcut                      | Functionality                                                                |
+========================================+==============================================================================+
| k                                      | Moves to the next command in the list                                        |
+----------------------------------------+------------------------------------------------------------------------------+
| j                                      | Moves to the previous command in the list                                    |
+----------------------------------------+------------------------------------------------------------------------------+
| m                                      | Loads more commands                                                          |
+----------------------------------------+------------------------------------------------------------------------------+
| h                                      | Focuses on the **History** tab                                               |
+----------------------------------------+------------------------------------------------------------------------------+
| p                                      | Focuses on the **Workspace** tab                                             |
+----------------------------------------+------------------------------------------------------------------------------+
| v                                      | Switches to **Version** in **Workspace**                                     |
+----------------------------------------+------------------------------------------------------------------------------+
| f                                      | Focuses on the **Workspace** search bar                                      |
+----------------------------------------+------------------------------------------------------------------------------+
| /                                      | Opens the **Filters** panel and focuses on the first field                   |
+----------------------------------------+------------------------------------------------------------------------------+
| **Esc** key from filters panel         | Same as clicking the **x** button at bottom of the form                      |
+----------------------------------------+------------------------------------------------------------------------------+
| **Esc** key from any command on the    | Removes focus from the text editor                                           |
| query composer                         |                                                                              |
+----------------------------------------+------------------------------------------------------------------------------+
| r                                      | Focuses on the **Results** tab of the current command or refreshes **Repo**  |
+----------------------------------------+------------------------------------------------------------------------------+
| l                                      | Focuses on the **Logs** tab of the current command                           |
+----------------------------------------+------------------------------------------------------------------------------+
| c                                      | Focuses on the **Comments** tab of the current command                       |
+----------------------------------------+------------------------------------------------------------------------------+
| o                                      | Focuses on the **Resource** tab of the current command                       |
+----------------------------------------+------------------------------------------------------------------------------+
| s                                      | Schedules the current command                                                |
+----------------------------------------+------------------------------------------------------------------------------+
| d                                      | Opens the **Downloads** menu on the left pane of the current command         |
+----------------------------------------+------------------------------------------------------------------------------+
| t                                      | Downloads tab-separated command results                                      |
+----------------------------------------+------------------------------------------------------------------------------+
| d then l                               | Clicks **Download Logs**                                                     |
+----------------------------------------+------------------------------------------------------------------------------+
| d then c                               | Clicks **Download Results as CSV**                                           |
+----------------------------------------+------------------------------------------------------------------------------+
| d then t                               | Clicks **Download Results as TSV**                                           |
+----------------------------------------+------------------------------------------------------------------------------+
| d then r                               | Shows the **Download Raw Results** dialog                                    |
+----------------------------------------+------------------------------------------------------------------------------+
| **Ctrl** + **Alt** + n                 | Opens a new query                                                            |
+----------------------------------------+------------------------------------------------------------------------------+
| **Shift** + **Enter**                  | Runs the query                                                               |
+----------------------------------------+------------------------------------------------------------------------------+
|**Ctrl** + **Alt** + s                  | Saves the query                                                              |
+----------------------------------------+------------------------------------------------------------------------------+
|**Ctrl** + **Alt** + c                  | Clears the query                                                             |
+----------------------------------------+------------------------------------------------------------------------------+
|  **Shift** + **Backspace**             | Kills the query                                                              |
+----------------------------------------+------------------------------------------------------------------------------+
