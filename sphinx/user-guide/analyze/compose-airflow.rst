.. _compose-airflow-command:

=======================================
Registering a DAG on an Airflow Cluster
=======================================

See :ref:`register-airflow-dag`.


