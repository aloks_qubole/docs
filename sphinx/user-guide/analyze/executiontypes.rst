.. _execution-types:

Command Execution Types
-----------------------

There are 3 ways you can execute commands in Qubole:

* The :ref:`User Interface <analyze-qpal>`
* :ref:`APIs <command-api>`
* SDKs:

  - `Python SDK <https://github.com/qubole/qds-sdk-py>`__
  - `Java SDK <https://github.com/qubole/qds-sdk-java>`__
