.. _compose-dbquery:

===================================
Composing a Db Query through the UI
===================================
You can query an existing data store by using the query composer available in the **Analyze** page.

Prerequisites
-------------
You must have an existing data store to query it. Create a data store in the **Explore** page if it does not exist.

.. note:: Hadoop-1, Hadoop-2, Presto, and Spark clusters support Db queries; the queries can be run without bringing up
          a cluster. See :ref:`cluster-command-map` for more information.

Perform the following steps to query an existing data store:

.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts.

1. Navigate to the **Analyze** page and click **Compose**. Select **Db Query** from the **Command Type** drop-down
   list.
2. **Query Statement** is selected by default from the drop-down list. Enter the Db query in the text field.

   -or-

   If you want to run a query through a path, select **Query Path** from the drop-down list, then specify the cloud storage path that contains the Db query file.

   A sample Db query in the composer is shown in the following figure.

   .. image:: ../ug-images/ComposeDBQuery.png

3. Select a data store to which the query is to be applied.
4. Click **Run** to execute the query. Click **Save** if you want to run the same query later.
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)
5. The query result is displayed in the **Results** tab and the query logs in the **Logs** tab. The **Logs** tab has the
   **Errors and Warnings** filter. For more information on how to download command results and logs, see
   :ref:`download-analyze-results`.