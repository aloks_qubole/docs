.. _compose-refresh-table:

=====================================
Composing a Refresh Table Query (AWS)
=====================================

You can compose a query to refresh Hive tables using the query composer available in the **Analyze** page. See
:ref:`Refresh Table <refresh-table>` for more information.

.. note:: Hadoop-1, Hadoop-2, Presto, and Spark clusters support refresh table queries, and the queries can run without
          clusters. See :ref:`cluster-command-map` for more information.

          You can configure the Pig version on an Hadoop 2 (Hive) cluster. Pig 0.11 is the default version. Pig 0.15 and
          Pig 0.17 (beta) are the other supported versions. You can also choose between MapReduce and Tez as the execution
          engine when you set the Pig 0.17 (beta) version. Pig 0.17 (beta) is only supported with Hive 1.2.0.

Perform the following steps to compose a Refresh Table query:

.. note:: :ref:`analyze-shortcuts` describes the supported keyboard shortcuts.

1. Navigate to the **Analyze** page and click **Compose**. Select **Refresh Table** from the **Command Type** drop-down
   list. The query composer for **Refresh Table** is shown in the following figure.

   .. image:: ../ug-images/ComposeRefreshTable.png

2. Select a Hive metastore from the **Hive Database** drop-down list.
3. Select the Hive table for which the data must be refreshed from the **Hive Table Name** drop-down list.
4. Set the **Stablity Interval** in minutes (numeric value).
5. Click **Run** to execute the query. Click **Save** if you want to run the same query later
   (See :ref:`Workspace <repo-tab>` for more information on saving queries.)
6. The query result is displayed in the **Results** tab and the query logs in the **Logs** tab. The **Logs** tab has
   the **Errors and Warnings** filter. For more information on how to download command results and logs, see
   :ref:`download-analyze-results`.
