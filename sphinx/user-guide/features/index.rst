.. _qds-features-index:

###################################
Using the Qubole Data Service (QDS)
###################################
QDS is supported on latest versions of Google Chrome and Mozilla Firefox. Though not officially supported, most of QDS
features work reasonably well on Apple Safari.

The following topics explain how to configure and use the Qubole Data Service (QDS):

* :ref:`command-templates`
* :ref:`handling-data-index`
* :ref:`analyze-index`
* :ref:`notebook-dashboard-index`
* :ref:`scheduler-qubole-index`

.. toctree::
   :maxdepth: 1
   :titlesonly:

   users-and-accounts
   smart-query/index.rst
   usage.rst





