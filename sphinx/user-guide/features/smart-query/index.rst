.. _smart-query-index:

##########################
Using the Smart Query Page
##########################

Smart Query is a QDS feature basically designed for data analysts, who are not familiar with using query languages such
as SQL. It can be used to compose Hive and Presto queries.

.. note:: Clicking the Qubole logo on the QDS UI displays the Qubole homepage.

The following topics explain how to compose Hive and Presto queries using the Smart Query feature:

.. toctree::
   :maxdepth: 1

   hive-smart-query.rst
   presto-smart-query.rst

