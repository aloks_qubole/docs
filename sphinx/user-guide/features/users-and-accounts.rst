.. _users-and-accounts:

Users and Accounts
==================

Qubole identifies a user by means of an email address. In the QDS UI, you can see the email address of the current user
by clicking on the profile icon at the far right of the top menu bar.

A new user can :ref:`sign up <quick-start-guide-index>`
on the Qubole website using an email address. You can either enter this address directly or authorize Qubole to obtain it
from a provider such as Google. An existing user can also invite a new user
to use Qubole using the new user's email address. See :ref:`manage-users` for more information on adding new users.

Each user can be a member of one or more accounts. All non-trivial user actions (such as creating a table, running a command,
importing or exporting data or even looking up older commands) are done in the context of an account. Here is some
important information about accounts:

* As a user, you can create a *free* account on the **New Account** page (click the plus sign on the **My Accounts** page
  in the **Control Panel**). Qubole may reasonably restrict the number of free accounts that you can be associated with.
* While creating an account, you must provide credentials that allow Qubole to orchestrate Cloud-provider resources on behalf
  of the account; for example, AWS credentials to read S3 buckets (Storage Credentials) and to launch instances in
  EC2 (Compute Credentials).
* Each account has one or more *admin* users. The user who created an account is an administrator by default.
* Administrators can invite more users by sending invitations to their email addresses. Similarly, they can remove users
  or change the administrative status of users from the **Manage Users** page in the **Control Panel**. See :ref:`manage-users`
  for more information.
* Each account has a distinct Hive warehouse and a common default clusters that are used for running user queries and background jobs.

QDS users with multiple accounts can see only the current account; this appears near the top-right
corner of each page. You can use the drop-down list to switch between accounts. See :ref:`manage-my-account`
for more information.

For API access using tokens, you must use tokens specific to a particular account. Manage tokens on the **My Accounts**
page in the **Control Panel**; see :ref:`manage-my-account`.

