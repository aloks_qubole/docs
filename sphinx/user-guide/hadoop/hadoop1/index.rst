.. _hadoop1-qubole-index:

########################
Hadoop 1 in Qubole (AWS)
########################

The sections that follow cover Qubole optimizations, and aspects of Hadoop 1 (:ref:`Hadoop 0.20.x <os-version-support>`)
that are especially important in Qubole clusters. Hadoop 1 is currently supported only on AWS clusters.

.. note:: Hadoop 1 is currently supported on AWS only. Qubole has deprecated Hadoop 1 as-a-service. For more information,
          see :ref:`hadoop1-dep`.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    fairscheduler-hadoop
    bad-job-protection
    develop-mapreduce-applications
    volume-configuration.rst
    manual-scale-hadoop-cluster
    speculation
    deal-OOM-errors
    retain-task-logs
