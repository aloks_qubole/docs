.. _manual-scale-a-cluster:

=================================
Manually Scaling a Hadoop Cluster
=================================

Manual scaling provides explicit cluster control for manually adding nodes to a Hadoop-1 cluster. Manual scaling is
useful when:

* You want to add more nodes than the Qubole's auto-scaling limit to process a lot of reduce tasks that are running.

* You want to scale up beyond the maximum limit specified in the configuration.

Qubole supports an add node API to manually scale a Hadoop cluster. Currently, this API supports only Hadoop 1 clusters.
See :ref:`add-node` for more information on the REST API.
