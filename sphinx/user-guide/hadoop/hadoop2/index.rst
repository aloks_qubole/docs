.. _hadoop2-qubole-index:

##################
Hadoop 2 in Qubole
##################

The sections that follow cover Qubole optimizations, and aspects of Hadoop 2 (:ref:`Hadoop 2.6.x <os-version-support>`)
that are especially important in Qubole clusters.


.. toctree::
   :maxdepth: 1
   :titlesonly:

   mapreduce-v2/index.rst
   yarn/index.rst


For more information, see :ref:`compose-hadoop-query`.

