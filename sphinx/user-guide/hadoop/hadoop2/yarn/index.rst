.. _yarn-in-qubole-index:

==============
YARN in Qubole
==============
The sections that follow cover Qubole optimizations, and aspects of Apache Hadoop YARN (:ref:`Hadoop 2.6.x <os-version-support>`),
which are especially important in Qubole clusters.


.. toctree::
   :maxdepth: 1
   :titlesonly:

   hadoop2-important-parameters.rst
   workload-scaling.rst
   yarn-metrics.rst

