.. _mapreduce2-in-hadoop2-index:

======================
MapReduce V2 in Qubole
======================
The section that follow covers MapReduce V2 and aspects of it in Qubole Hadoop 2 (:ref:`Hadoop 2.6.x <os-version-support>`),
which are especially important in Qubole clusters.

.. toctree::
   :maxdepth: 1
   :titlesonly:

   mapreduce-configuration.rst