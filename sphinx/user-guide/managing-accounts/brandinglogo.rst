.. _brandinglogo:

======================
Customise your Account
======================
You can add a logo for resellers, who are within the QDS account.

.. note:: This feature is not enabled by default. To enable it on the QDS account, create a ticket with
          `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.

          :ref:`brand-logo-docs` describes the API to add a logo.

After Qubole enables the branding feature on the QDS account, navigate to the **Control Panel** > **Branding Details**.

Under **Logo Details**, you have these options to add the logo:

* **Logo URL**: It is a publicly accessible logo URI image in a png/gif/svg format. The image size must be less
  than 100 KB. It must have the pixel dimension of (**120px x 48px**) It is mandatory for branding the logo.
* **Small Logo**: It can be a small logo that you can upload. It must have the pixel dimension of (**48px x 48px**).

After uploading the logo, click **Submit** to add the logo on the QDS UI. You can also remove the logo using the
**Remove Branding** button.

Here is a sample brand logo that is uploaded and yet to be submitted.

.. image:: ../ug-images/brandingdetails.png
