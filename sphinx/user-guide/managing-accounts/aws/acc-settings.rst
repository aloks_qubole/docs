.. _acc-settings:

Configuring your Account Settings
=================================

.. note:: ``Qubole`` and ``qubole`` are **reserved** by Qubole. Do not use them in any AWS configuration.
          For more information, see :ref:`cluster-tags`.


Account Details
^^^^^^^^^^^^^^^
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Detail                                                                                                      |
+===========================+=============================================================================================================+
|   Account name            | Enter a name in the text field.                                                                             |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
|   Domain Name Allowed     | Enter a domain, or a comma-separated list of domains, from which this account can be used; for example,     |
|   to Sign In/Up           | `qubole.net` or `qubole.net,example.com`.                                                                   |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
|   Idle Session Timeout    | Optionally specify how long (in minutes) QDS should wait to terminate an idle QDS UI session. The default   |
|                           | is 1440 minutes (24 hours). To change it, enter a number from 1 to 10080 (10080 minutes is a week).         |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
|   Idle Cluster Timeout    | The default cluster timeout is 2 hours. Optionally, you can configure it between 0-6 hours. The unit of time|
|                           | supported is only **hour**.                                                                                 |
|                           | If the timeout is set at the  account level, it applies to all clusters within that account. However, you   |
|                           | can override the timeout at the cluster level.                                                              |
|                           | The timeout is effective on the completion of all queries on the cluster. Qubole terminates a cluster       |
|                           | in an **hour** boundary.                                                                                    |
|                           | For example, when ``idle_cluster_timeout`` is 0, then if there is any node in the cluster near its hour     |
|                           | boundary (that is it has been running for 50-60 minutes and is idle even after all queries are executed),   |
|                           | Qubole terminates that cluster.                                                                             |
|                           |                                                                                                             |
|                           | .. note:: The Idle Cluster Timeout can be configured in hours and minutes. This feature is not enabled on   |
|                           |           all accounts by default. To enable this feature for your QDS account, create a ticket with        |
|                           |           `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.                                         |
|                           |                                                                                                             |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
|  Allow Qubole Access      | Check this to allow Qubole Support to log in to this account (helpful if you run into problems).            |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
|  Account Level Concurrent | Provides the maximum number of concurrent commands that can be run at the account level.                    |
|  Command Limit            |                                                                                                             |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
|  User Level Concurrent    | Provides the maximum number of concurrent commands that can be run at the user-level for the current        |
|  Command Limit            | account. This helps avoid a scenario where one user consumes the entire concurrent command limit. The user  |
|                           | limit remains the same for all the users.                                                                   |
|                           |                                                                                                             |
|                           | .. note:: This feature is disabled by default. To enable this feature for your account, create a ticket     |
|                           |                with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.                               |
|                           |                                                                                                             |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Notifications
^^^^^^^^^^^^^

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 |  Detail                                                                                                     |
+===========================+=============================================================================================================+
| **Email List for**        | Enter a list of email addresses to which notifications will be sent about releases, account/cluster         |
| **Account Updates**       | configuration, and feature changes.                                                                         |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **Command Timeout**       | (Optional) Enter the number of seconds to wait before triggering an alert that a query you ran (from the    |
|                           | **Compose** tab of the **Analyze** page) is still running.                                                  |
+---------------------------+-------------------------------------------------------------------------------------------------------------+


Datadog Settings
^^^^^^^^^^^^^^^^
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Detail                                                                                                      |
+===========================+=============================================================================================================+
| **Email List for**        | Enter a list of comma separated email addresses to which the datadog alerts should be sent.                 |
| **Cluster Alerts**        |                                                                                                             |
|                           | .. note:: Any change in the list of email addresses is effective only on the new clusters. If you want to   |
|                           |           change the list of email addresses for an existing cluster, you should update the email list on   |
|                           |           the Datadog UI manually.                                                                          |
|                           |                                                                                                             |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **API key**               | Enter the API token.                                                                                        |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **Application key**       | Enter the Datadog application key.                                                                          |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

* Qubole supports the Datadog cloud monitoring service on Hadoop 1, Hadoop 2 (Hive), Presto, and Spark clusters.
* If you configure Datadog settings, they are enabled by default on all Hadoop 1, Hadoop 2 (Hive), Presto, and Spark
  clusters in the QDS account. Ganglia monitoring is also enabled. You can override the settings for a particular cluster.

.. note:: Although Ganglia monitoring is enabled, its link may not be visible in the cluster's UI resources list.

Click **Save** to apply your changes or **Reset** to cancel them.

See :ref:`policy-use-qubole-use-my-iam-credentials` for the set of permissions to be given.


Qubole allows you to authorize Amazon Web Service (AWS) using:

* IAM Keys to access AWS resources. See :ref:`iam-keys` for more information.
* IAM Roles to access AWS resources. See :ref:`manage-roles` for more information. To learn more about IAM roles, click
  `here <http://docs.aws.amazon.com/IAM/latest/UserGuide/roles-toplevel.html>`__.