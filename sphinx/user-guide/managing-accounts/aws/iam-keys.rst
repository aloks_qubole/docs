.. _iam-keys:

###############################################
Configuring your Access Settings using IAM Keys
###############################################

By default, the selected **Access Mode Type** is **IAM Keys**.

Storage Settings
^^^^^^^^^^^^^^^^
+--------------------------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                                  | Detail                                                                                                      |
+============================================+=============================================================================================================+
|    AWS Access Key                          | Enter the AWS Access Key (must have 20 characters).                                                         |
+--------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|    AWS Secret Key                          | Enter the AWS Secret Key (must have 40 characters).                                                         |
+--------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|    Default Location                        | QDS recommends you change the **Default Location** (for any data created).                                  |
|                                            |                                                                                                             |
|                                            | .. warning:: To avoid adverse effects on the command latency and performance, specify a separate AWS S3     |
|                                            |              bucket for each QDS account,avoid providing long prefixes, and sharing the same AWS S3 bucket  |
|                                            |              for all accounts.                                                                              |
|                                            |                                                                                                             |
+--------------------------------------------+-------------------------------------------------------------------------------------------------------------+

See :ref:`Manage Roles <manage-roles>` for information on how access permissions are granted and restricted.


Compute Settings
^^^^^^^^^^^^^^^^

+-------------------------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                                 | Detail                                                                                                      |
+===========================================+=============================================================================================================+
|   Use Same Settings as Storage            | Selected by default. To use different compute settings, clear the checkbox.                                 |
+-------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|  Push Compute Settings to all clusters    | Select this option if you want to push compute settings to all clusters in an account. Qubole recommends    |
|                                           | selecting this option if you are setting up the compute keys for the first time. It is enabled by default   |
|                                           | for all new users and users, who have entered invalid credentials.                                          |
+-------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|   AWS Access Key                          | Enter the AWS Access Key (must have 20 characters).                                                         |
+-------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|   AWS Secret Key                          | Enter the AWS Secret Key (must have 40 characters).                                                         |
+-------------------------------------------+-------------------------------------------------------------------------------------------------------------+
|   Persistent Security Groups              | If you specify a :ref:`persistent security group <persistent-security-group>` here, it is attached to all   |
|                                           | clusters by default unless it is overridden in the cluster settings. (This field is optional; by default,   |
|                                           | no persistent security group is used.)                                                                      |
|                                           | **Qubole only uses the security group name for validation. So, do not provide the security group's ID**.    |
+-------------------------------------------+-------------------------------------------------------------------------------------------------------------+

You can improve the security of a cluster by authorizing Qubole to generate a unique SSH key every time a cluster is
started; `create a support ticket <https://qubole.zendesk.com/hc/en-us>`__ to enable this capability. Once it is enabled,
QDS will use the unique SSH key to interact with the cluster. If you want use this capability to control QDS access to a
Bastion node communicating with a cluster running on a :ref:`private subnet <private-subnet>`, Qubole support will provide
you with an account-level key that you must authorize on the Bastion node.

Click **Save** after making changes. Click **Reset** to change any setting.

