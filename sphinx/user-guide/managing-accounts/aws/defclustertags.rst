.. _default-cluster-tags-account:

Adding Account and User level Default Cluster Tags (AWS)
--------------------------------------------------------
At the end of **Account Settings** page, look for **Default Cluster Tags**. This feature is available on request.
Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable this feature on the QDS account.

As a prerequisite, you must have system administrator privileges to create and update default cluster tags. The default
cluster tags get applied to only new clusters of a specific account. The default cluster tags are applied to cluster
nodes at runtime. However, the default cluster tags do not apply to existing clusters.

.. image:: ma-images/DefaultClusterTags.png

By default, no tags would be present. Click **Add another tag** to start adding a cluster tag and its value. You can set the read-only access on the cluster tags by selecting
the **Read Only** checkbox against the cluster tag.

Qubole plans to provide API support to configure default cluster tags shortly.

Setting User-level Cluster Tags
-------------------------------
Qubole supports adding user-level cluster tags through SAML Login. The main use of user-level cluster tags is that it
helps in tracing the user's activity on a specific cluster. As a pre-requisite, the QDS account admin must share these details
with Qubole by creating a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__:

1. ``Attribute Name``: It refers to the request attribute in the SAML response.
2. ``Tag Name``: It corresponds to the name of the default cluster tag.

For example, if the QDS account admin shares the ``Attribute Name`` to be ``Email`` and ``Tag Name`` to be ``UserEmail``,
then in the SAML API response of a specific user, Qubole traces the ``Email`` and fetches its value. Qubole records
that value as the user-level cluster tag's value.

In this example, ``UserEmail`` is the user-level cluster tag that Qubole adds and ``<User's Email Address>`` is its value.

Qubole plans to support setting user-level cluster tags through REST API shortly.

Verifying the Account and User level Cluster Tags
-------------------------------------------------
If the account-level default cluster tags and user-level cluster tags exist, they would be prepopulated when a cluster is
created from the UI. These tags are visible when you create a cluster. The user-level and account-level cluster tags apply
to cluster nodes at run time the same way as the custom EC2 tags get applied to cluster nodes.

