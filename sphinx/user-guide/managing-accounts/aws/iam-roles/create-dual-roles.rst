.. _create-dual-roles:

===============================
Creating Dual IAM Roles for QDS
===============================
Qubole supports creating two IAM roles as part of IAM role authentication for a single Qubole account. With this approach,
you can restrict access to data to only those commands and queries that have to operate on this data and deny ``all``
action access to all Qubole users in this account.

.. note:: :ref:`iam-role-dual-iam-role` highlights the difference between the cross-account IAM Role-based authentication and the
          dual IAM Roles-based authentication.

The function of two IAM roles are described below.

Cross Account IAM Role
----------------------
The cross-account IAM role is a cross-AWS-account role created at an account level as described in
:ref:`create-iam-roles`. :ref:`aws-quick-start-guide` provides a detailed step-by-step procedure on how to go about
creating IAM roles on the **AWS end** as well as the **Qubole end**.

The cross-account IAM role gets the following permissions:

- EC2 permissions
- S3 permissions over the default location
- No S3 permissions over Input S3 buckets
- Qubole keys assume the cross-account role
- ``iam:GetInstanceProfile`` and ``iam:PassRole`` permissions

Dual IAM Role
-------------
The Dual IAM Role is configured at the QDS cluster level to interact with data sources. Qubole cannot assume the second
role to access S3 data and hence, it provides much more data security. This within-an-account role has different cross-
account policy and S3 policy for accessing data. It must have the following permissions:

- EC2 permissions
- S3 permissions over the default location
- S3 permissions over Input S3 buckets.

- Cross-account IAM role and Qubole cannot assume this role but only start EC2 instances with this role
- ``iam:GetInstanceProfile`` and ``iam:PassRole`` permissions.

Configuring the Cross Account IAM Role
--------------------------------------
:ref:`manage-roles` and :ref:`aws-quick-start-guide`/:ref:`create-iam-roles` to configure a cross-account IAM role
that is a first IAM role.

.. note:: In a Dual IAM Role setup, you must allow the cross account IAM Role to only access the default location and not
          the AWS S3 data buckets (Bucket2). For simplicity, S3 data buckets are referred to as **Bucket2** and
          the default location is referred to as **Bucket1**.

.. important:: Ensure that the cross-account policy passes the Dual IAM Role and not the Cross Account IAM Role. For the
               policy, see the step 3 below.

Configuring the Dual IAM Role
-----------------------------
To configure the Dual IAM Role, perform the following steps:

1. Create an AWS EC2 policy by performing the following steps:

   a. Log into AWS Console through console.aws.amazon.com.
   b. Navigate to the **Identity and Access Management** interface.
   c. Navigate to the **Policies** interface within the **Identity and Access Management** interface.
   d. Click **Create Policy**.
   e. Click **Create Your Own Policy**.
   f. Enter a **Policy Name** for the EC2 policy.
   g. Provide a **Policy Description**.
   h. For the **Policy Document**, use one of the following samples and update the text as required.

   Here is a sample IAM template for EC2 settings.

   **Sample 1** - This sample is a simpler AWS policy for EC2 settings.

   .. sourcecode:: bash

    {
        "Version": "2012-10-17",
        "Statement": [
         {
             "Effect": "Allow",
             "Action": [
                 "ec2:AuthorizeSecurityGroupEgress",
                 "ec2:AuthorizeSecurityGroupIngress",
                 "ec2:AttachVolume",
                 "ec2:CancelSpotInstanceRequests",
                 "ec2:CreateSecurityGroup",
                 "ec2:CreateTags",
                 "ec2:CreateVolume",
                 "ec2:DeleteSecurityGroup",
                 "ec2:DeleteTags",
                 "ec2:DeleteVolume",
                 "ec2:Describe*",
                 "ec2:DescribeVolumes",
                 "ec2:DetachVolume",
                 "ec2:ImportKeyPair",
                 "ec2:DescribeKeyPairs",
                 "ec2:ModifyInstanceAttribute",
                 "ec2:RequestSpotInstances",
                 "ec2:RevokeSecurityGroupIngress",
                 "ec2:RunInstances",
                 "ec2:StartInstances",
                 "ec2:StopInstances",
                 "ec2:TerminateInstances"
             ],
             "Resource": ["*"]
             },
             {
             "Effect": "Allow",
             "Action": ["sts:DecodeAuthorizationMessage"],
             "Resource": ["*"]
         },
         {
             "Effect": "Allow",
              "Action": [
                   "iam:CreateServiceLinkedRole",
                   "iam:PutRolePolicy"
              ],
              "Resource": ["arn:aws:iam::*:role/aws-service-role/spot.amazonaws.com/AWSServiceRoleForEC2Spot", "arn:aws:iam::*:role/aws-service-role/spotfleet.amazonaws.com/AWSServiceRoleForEC2SpotFleet"],
              "Condition": {
                  "StringLike": {
                      "iam:AWSServiceName": ["spot.amazonaws.com","spotfleet.amazonaws.com"]
                  }
              }
         }
         ]
         }

2. You must create a second IAM Role on the AWS console with S3 policies and ensure to provide access to the S3 data buckets
   (Bucket2) and the default location (Bucket1).

   .. note:: Only the Dual IAM Role must have access to the S3 data buckets (Bucket2) and not the Cross-account IAM Role.

   Here is an example.

   .. sourcecode:: bash

        {
         "Version": "2012-10-17",
         "Statement": [
                        {
                          "Effect": "Allow",
                          "Action": [
                                      "s3:DeleteObject",
                                      "s3:GetObject",
                                      "s3:GetObjectAcl",
                                      "s3:PutObject",
                                      "s3:PutObjectAcl",
                                      "s3:GetBucketAcl",
                                      "s3:GetBucketLocation",
                                      "s3:ListBucket"
                                    ],
                          "Resource": [
                                        "arn:aws:s3:::<Bucket1path>/*",
                                        "arn:aws:s3:::<Bucket1path>",
                                        "arn:aws:s3:::<Bucket2path>/*",
                                        "arn:aws:s3:::<Bucket2path>"
                                      ]
                        }
         ]
        }


3. Go to the AWS account and perform the following steps to update trust relationships of the Dual IAM Role:

   a. Navigate to the **Identity and Access Management** interface.
   b. Navigate to the **Roles** interface within the **Identity and Access Management** interface.
   c. Click the new Dual IAM role.
   d. Click the **Trust Relationships** tab.
   e. Click **Edit Trust Relationships**.
   f. For the **Policy Document**, use the following code and update the text as required.

      .. note:: In the example below, the only the AWS service is allowed access and the allow-access is not specified for Qubole.

      .. sourcecode:: bash

           {
              "Version": "2012-10-17",
              "Statement": [
              {
                  "Effect": "Allow",
                  "Principal": {
                                "Service": "ec2.amazonaws.com"
                  },
                 "Action": "sts:AssumeRole"
              }
              ]
           }

4. On the AWS account, perform the following steps to create an IAM policy for the Dual IAM role:

   a. Navigate to the **Identity and Access Management** interface.
   b. Navigate to the **Policies** interface within the **Identity and Access Management** interface.
   c. Click **Create Policy**.
   d. Click **Create Your Own Policy**.
   e. Enter a **Policy Name** for the account policy.
   f. Provide a **Policy Description**.
   g. For the **Policy Document**, use the following code and update the text as required.

      .. sourcecode:: bash

          {   "Version": "2012-10-17",
              "Statement": [
              {
                  "Effect": "Allow",
                  "Action": "iam:GetInstanceProfile",
                  "Resource": "arn:aws:iam::<arn_number>:instance-profile/<Dual_Role>"
              },
              {
                  "Effect": "Allow",
                  "Action": "iam:PassRole",
                  "Resource": "arn:aws:iam::<arn_number>:role/<Dual_Role>"
              }
              ]
          }

      In the above policy example, ``<Dual_Role>`` is the placeholder of the Dual IAM Role name.

5. Navigate to the QDS UI's **Clusters** page. Edit the cluster that must be accessible to the Dual IAM Role.
   Configure the *Dual IAM Role*'s name in the **Advanced Configuration** > **EC2 SETTINGS** of the cluster.

   Here is an example of the **Instance Profile** in a cluster's configuration UI.

   .. image:: ../ma-images/RoleInstanceProfile.png

   You must add the Dual IAM Role name that is displayed for an instance profile in the policy. For example, in the
   policy element, ``"Resource": "arn:aws:iam::<arn_number>:instance-profile/<Dual_Role>"``,
   ``<Dual_Role>`` is the Dual IAM Role name. You must add it in the **Instance Profile** text box.

   For more information on configuring ``role-instance-profile`` using cluster APIs, see :ref:`ec2-settings`.

Limitations of Using Dual IAM Roles
-----------------------------------
The limitations of using dual IAM roles are mentioned in the following list:

* Hive queries must only run on the cluster's master node and not the QDS server. Use :ref:`Hive on the master node <hive-master-node>`
  only if Hive queries must access data in S3 buckets that are only accessible through the Dual IAM Role.
* S3 Explorer in the **Explore** tab as well as **Notebooks** do not work for S3 buckets, which are only accessible through
  the Dual IAM Role.
* The :ref:`S3 Files data dependency <wait-s3-dependency>` on the Qubole Scheduler might not work. The dependencies only
  work over the default location or S3 path to which the Cross-account IAM Role has access to.
* Data import and data export jobs must have the **Use Hadoop Cluster** option enabled for any data which is not accessible
  to the Cross-account IAM Role. The **Use Hadoop Cluster** option is at the end of the query composer for a data
  import/export command in the **Analyze** UI and it looks as illustrated here.

  .. image:: ../ma-images/UseHadoopClusterOption.png

  For more information, see :ref:`compose-dataexport` and :ref:`compose-data-import`.
* An Amazon S3 path in commands works only when the S3 path is accessible to the Cross-account IAM Role.
* Data in results and logs is accessible to other users in the same QDS account given that the default S3 location, which
  is storing them is accessible to the Cross-account IAM Role. However, you can hide commands from other QDS users in
  the same account as required and the steps to hide commands are described in :ref:`data-rbac`.