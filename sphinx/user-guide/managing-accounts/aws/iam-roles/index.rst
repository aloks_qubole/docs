.. _roles-index:

################################################
Configuring your Access Settings using IAM Roles
################################################



.. toctree::
   :maxdepth: 1
   :titlesonly:

   account-settings.rst
   iam-role-dual-iam-role.rst
   create-iam-roles.rst
   override-iam-roles.rst
   create-dual-roles.rst



