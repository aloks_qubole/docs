.. _override-iam-roles:

===================================
Overriding a Cross-Account IAM Role
===================================
A cross-account authorized by IAM-based-role has its own disadvantage of using by many users. Since all users of that
account use the same role and are granted with same S3 permissions. This scenario may not be suitable in a large
enterprise. To overcome this issue, Qubole provides a feature to override the cross-account's IAM Role settings at
user-level in an account.

Overriding an IAM role requires to do three steps related to IAM policies as mentioned below:

* `Obtaining the External ID`_
* `Creating a User-level Role S3 Policy`_
* `Updating a User-level Role Trust Policy`_

Qubole-generated External ID can avoid the misuse of the Role-ARN from assuming IAM Role-based authorization by multiple
users.

Obtaining the External ID
-------------------------
.. note:: It is an optional but a recommended step to avoid the misuse of the Role-ARN from assuming IAM Role-based
          authorization by multiple users.

The following figure illustrates a default IAM-role-based account that contains the clone and overriding options in the
drop-down list.

.. image:: ../../../ug-images/DefaultRoleMyAccount.png

Click **Override Access Mode (IAM Roles)** and the dialog to override the IAM Roles account. The dialog shows a text field
to enter **Role ARN** and a checkbox to enable **External ID**. Qubole-generated External ID can avoid the misuse of the
Role-ARN from assuming IAM Role-based authorization by multiple users. If you select the checkbox to enable **External ID**,
the external ID is displayed as shown in the following figure.

.. image:: ../../../ug-images/OverrideRoleMode.png

Click **Save** after entering the Role ARN to override the account-level IAM Role settings. Click **Cancel** to revert
to the previous **My Accounts** tab.

For more information on:

* How to override the IAM-Role-based account, see :ref:`override-access-mode-role`
* Users and accounts, see :ref:`users-and-accounts`

Creating a User-level Role S3 Policy
------------------------------------
Perform the following steps to create an AWS S3 Policy:

1. Navigate to the **Identity and Access Management** interface.
2. Navigate to the **Policies** interface within the **Identity and Access Management** interface.
3. Click **Create Policy**.
4. Click **Create Your Own Policy**.
5. Enter a **Policy Name** for the S3 policy.
6. Provide a **Policy Description**.
7. For the **Policy Document**, use the following code and update the text as required.

.. sourcecode:: bash

    {
     "Version": "2012-10-17",
     "Statement": [
                    {
                      "Effect": "Allow",
                      "Action": [
                                  "s3:DeleteObject",
                                  "s3:GetObject",
                                  "s3:GetObjectAcl",
                                  "s3:PutObject",
                                  "s3:PutObjectAcl",
                                  "s3:GetBucketAcl",
                                  "s3:GetBucketLocation",
                                  "s3:ListBucket"
                                ],
                      "Resource": [
                                    "arn:aws:s3:::<user-bucketpath/*>",
                                    "arn:aws:s3:::<user-bucketpath>",
                                    "arn:aws:s3:::<default-location>",
                                    "arn:aws:s3:::<default-location/*>"
                                  ]
                    },
                    {
                      "Effect": "Allow",
                      "Action": [
                                 "s3:GetObject",
                                 "s3:ListBucket"
                                ],
                      "Resource": [
                                    "arn:aws:s3:::paid-qubole/*",
                                    "arn:aws:s3:::paid-qubole"
                                  ]
                      }
                      ]
     }

.. note:: In the above policy example, replace the <user-bucketpath>.

8. Click **Create Policy**.

.. note:: The asterisk (*) after the slash (/) in the first line under Resource indicates all sub directories stored in
          the location provided to the left of the slash (/).

          The EC2 policy of the cross-account IAM Role is used for overridden roles created from that account.

Updating a User-level Role Trust Policy
---------------------------------------
Perform the following steps to update trust relationships of the newly create AWS IAM service role:

1. Navigate to the **Identity and Access Management** interface.
2. Navigate to the **Roles** interface within the **Identity and Access Management** interface.
3. Click the new cross-account IAM role.
4. Click the **Trust Relationships** tab.
5. Click **Edit Trust Relationships**.
6. For the **Policy Document**, use the following code and update the text as required.

.. sourcecode:: bash

     {
      "Version": "2012-10-17",
      "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
               "AWS": "arn:aws:iam::<customerawsaccountid>:role/<cross-account-role>"
                     },
        "Action": "sts:AssumeRole",
        "Condition": {
           "StringEquals": {
               "sts:ExternalId": "<externalid>"
                }
           }
      }
      ]
    }

.. note:: In the above policy example, replace the customerawsaccountid and externalid. external id check is optional.

7. After editing, click **Update Trust Policy** to save changes.
8. Return to Qubole UI. Enter the Role ARN and option of ``externalid`` under my accounts section. For more information, see
   :ref:`manage-roles`. See :ref:`override-access-mode-role` for more information on override Role ARN.

Supported Commands by an Overridden IAM Role
---------------------------------------------
The following commands are supported by an overridden IAM role:

* Shell command
* Hive command
* Hadoop command
* Spark command except DDL commands
* Pig
* Presto. The support for Presto command is not enabled by default. To enable it, create a ticket with
  `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.

Limitations of an Overridden IAM Role
-------------------------------------
The **My Amazon S3** option in the **Explore** page and **S3 dependency** in a Qubole Scheduler do not work in a
user-level IAM Role.