.. _whitelist-ip:

=========================
Whitelisting IP Addresses
=========================
Whitelisting IP addresses allows users of an account to login only from certain IP addresses.

You can add the IP addresses to whitelist them in the `Control Panel <h ttps://api.qubole.com/v2/control-panel>`__.

To whitelist an IP address, perform the following steps:

1. On the QDS user interface, navigate to the `Control Panel <https://api.qubole.com/v2/control-panel>`__. Click the
   `Whitelist IP <https://api.qubole.com/v2/control-panel#whitelist-ip>`__ tab. If there is no whitelist IP address
   added, it is as shown in the following figure.

   .. image:: ../ug-images/EmptyWhitelist.png

2. Click the add icon |AddIcon| to add an IP address to the whitelist.

   .. |AddIcon| image:: ../ug-images/AddIcon.png

   A dialog with **OK** and **Cancel** buttons are displayed. Click **OK** to add a new IP address. The dialog to add a
   new IP is displayed. Enter the IP address in the IP CIDR text field as illustrated in the following figure.

   .. image:: ../ug-images/AddNewWhitelistIP.png

   .. caution:: Once you add IP addresses to whitelist, logging in to QDS is possible only from the *whitelisted* IP addresses.

   Click **Add Entry** to save the IP address to the whitelist. After clicking **Add Entry**, the **Whitelist IP** tab
   displays the newly-added IP address as shown in the following figure.

   .. image:: ../ug-images/WhitelistIPAdded.png

   Click the refresh icon |RefreshIcon| to refresh the whitelist. Repeat step 2 to add another IP address to whitelist.

.. |RefreshIcon| image:: ../ug-images/RefreshIcon.png

In the **Action** column, click **Delete** to remove an IP address from the whitelist. Select the checkbox that is against
a whitelisted IP addresses to delete more than one whitelisted IP at once. After you select a
whitelisted IP address, a **Delete Selected** button appears as shown in the following figure.

.. image:: ../ug-images/BulkDeleteWhitelistIP.png

Click **Delete Selected**. A dialog with **OK** and **Cancel** buttons are displayed. Click **OK** to delete whitelisted
IP addresses. After you click the **OK** button, the selected whitelisted IP addresses are not seen in the
**Whitelist IP** tab as shown in the following figure.

   .. image:: ../ug-images/WhitelistIPDeleted.png

