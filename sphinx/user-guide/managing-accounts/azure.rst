.. _azure:

Configuring Advanced Settings for Azure
.......................................

* **Ganglia monitoring**: see :ref:`performance-monitoring-ganglia`.

* **Default resource group**. When you :ref:`configured your account <QDS-steps>`, you specified the Azure resource group in which
  QDS brings up your clusters. This allowed QDS to create a default resource group. If you want your clusters to use a predefined
  resource group instead, shut down any running clusters and proceed as follows:

  1. Define the resource group in the `Azure Portal <https://portal.azure.com/>`__.
  2. Navigate to **Account Settings** in the QDS UI.
  3. Set **Default Resource Group** to **Use Existing**.
  4. Choose your resource group from the drop-down list.
  5. Check the box to **Push Compute Settings to all clusters**.
  6. Click **Save**.

  Clusters will use this resource group by default from now on. You can override the default at the cluster level; see
  :ref:`azure_RG`.

* **Idle Session Timeout** - Optionally specify how long (in minutes) QDS should wait to terminate an idle QDS UI session. The
  default is 1440 minutes (24 hours). To change it, enter a number from 1 to 10080 (10080 minutes is a week).
* **Idle Cluster Timeout** - Optionally specify how long (in hours) QDS should wait to terminate an idle cluster.
  The default is 2 hours. To change it, enter a number between 0 and 6. You can override this timeout at the
  :ref:`cluster <modify-Azure-cluster>` level. See also :ref:`Shutting Down an Idle Cluster <auto-works-downscaling-shutdown>`.

  .. note::  See :ref:`aggressive-downscaling-azure` for information about an additional set of capabilities that currently
             require a `Qubole Support ticket <https://qubole.zendesk.com/hc/en-us>`__.

