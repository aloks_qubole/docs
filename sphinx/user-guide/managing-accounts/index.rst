.. _managing-accounts-index:

################
Account Settings
################


How you configure Qubole Account Settings depends on which Cloud provider you use. Choose one of the following options:

.. toctree::
   :maxdepth: 1
   :titlesonly:

   aws/index.rst
   azuredet.rst
   azure.rst
   oracle.rst
   brandinglogo.rst
   whitelist-ip.rst

For more information, see :ref:`iam-index`.