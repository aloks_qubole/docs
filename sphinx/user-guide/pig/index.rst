.. _pig-qubole-index:

#########
Pig (AWS)
#########


.. note:: Pig is currently supported on AWS only.

This section explains how to use Pig on a Qubole cluster. It covers the following topics:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    pig-qubole
    running-pig-job.rst
    pig-hcatalog-integration


See :ref:`compose-pig-query`.