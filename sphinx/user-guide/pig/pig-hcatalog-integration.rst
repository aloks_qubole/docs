.. _pig-hcatalog-integration:

========================
Pig HCatalog Integration
========================
Pig scripts can access Hive metastore using HCatalog libraries. Only Pig 0.11 (Pig11) or later version supports the
HCatalog integration. Pig scripts can access Hive metastore using HCatalog libraries. The Pig HCatalog integration is
supported on Hadoop 2 clusters. It is enabled by default on Hadoop 2 clusters.

.. note:: You can configure the Pig version on an Hadoop 2 (Hive) cluster. Pig 0.11 is the default version. Pig 0.15 and
          Pig 0.17 (beta) are the other supported versions. You can also choose between MapReduce and Tez as the execution
          engine when you set the Pig 0.17 (beta) version. Pig 0.17 (beta) is only supported with Hive 1.2.0.

A Pig code for loading data from a Hive table and storing data into a Hive table is shown in the following example.

.. sourcecode:: bash

    A = LOAD 'default_qubole_memetracker' USING org.apache.hcatalog.pig.HCatLoader();
    B = FILTER A BY month == '2009-04';
    STORE B INTO 'default_qubole_memetracker_snipped' USING org.apache.hcatalog.pig.HCatStorer();

**All data definition language (DDL) statements must be run using the Hive command interface**.