.. _pkgmgmt-preinstalled-packages:

=======================================================
Viewing the List of Pre-installed Python and R Packages
=======================================================
A newly created environment contains Anaconda distribution of R and Python packages by default and a list of pre-installed
Python and R packages. Click **See list of pre-installed packages**. For more information, see :ref:`package-management`.
By default, the list displays **Python Packages**. Click the **R Packages** tab to see that list. The pre-installed packages
are separately listed in:

* `List of Pre-installed Python Packages`_
* `List of Pre-installed R Packages`_

List of Pre-installed Python Packages
-------------------------------------
Packages are available in the environment at ``/usr/lib/envs/<env-ID-and-version-details>``.

.. sourcecode:: python

     _license                  1.1                      py27_1
     _nb_ext_conf              0.3.0                    py27_0
     alabaster                 0.7.9                    py27_0
     anaconda                  4.2.0               np111py27_0
     anaconda-clean            1.0.0                    py27_0
     anaconda-client           1.5.1                    py27_0
     anaconda-navigator        1.3.1                    py27_0
     argcomplete               1.0.0                    py27_1
     astroid                   1.4.7                    py27_0
     astropy                   1.2.1               np111py27_0
     babel                     2.3.4                    py27_0
     backports                 1.0                      py27_0
     backports_abc             0.4                      py27_0
     beautifulsoup4            4.5.1                    py27_0
     bitarray                  0.8.1                    py27_0
     blaze                     0.10.1                   py27_0
     bokeh                     0.12.2                   py27_0
     boto                      2.42.0                   py27_0
     boto3                     1.4.4                    py27_0
     botocore                  1.5.67                   py27_0
     bottleneck                1.1.0               np111py27_0
     bz2file                   0.98                     py27_0
     cairo                     1.12.18                       6
     cdecimal                  2.3                      py27_2
     cffi                      1.7.0                    py27_0
     chest                     0.2.3                    py27_0
     click                     6.6                      py27_0
     cloudpickle               0.2.1                    py27_0
     clyent                    1.2.2                    py27_0
     colorama                  0.3.7                    py27_0
     configobj                 5.0.6                    py27_0
     configparser              3.5.0                    py27_0
     contextlib2               0.5.5                    py27_0
     cryptography              1.5                      py27_0
     curl                      7.49.0                        1
     cycler                    0.10.0                   py27_0
     cython                    0.24.1                   py27_0
     cytoolz                   0.8.0                    py27_0
     dask                      0.11.0                   py27_0
     datashape                 0.5.2                    py27_0
     dbus                      1.10.10                       0
     decorator                 4.0.10                   py27_0
     dill                      0.2.5                    py27_0
     docutils                  0.12                     py27_2
     dynd-python               0.7.2                    py27_0
     entrypoints               0.2.2                    py27_0
     enum34                    1.1.6                    py27_0
     et_xmlfile                1.0.1                    py27_0
     expat                     2.1.0                         0
     fastcache                 1.0.2                    py27_1
     filelock                  2.0.6                    py27_0
     flask                     0.11.1                   py27_0
     flask-cors                2.1.2                    py27_0
     fontconfig                2.11.1                        6
     freetype                  2.5.5                         1
     funcsigs                  1.0.2                    py27_0
     functools32               3.2.3.2                  py27_0
     futures                   3.0.5                    py27_0
     gensim                    1.0.1               np111py27_0
     get_terminal_size         1.0.0                    py27_0
     gevent                    1.1.2                    py27_0
     glib                      2.43.0                        1
     greenlet                  0.4.10                   py27_0
     grin                      1.2.1                    py27_3
     gst-plugins-base          1.8.0                         0
     gstreamer                 1.8.0                         0
     h5py                      2.6.0               np111py27_2
     harfbuzz                  0.9.39                        1
     hdf5                      1.8.17                        1
     heapdict                  1.0.0                    py27_1
     icu                       54.1                          0
     idna                      2.1                      py27_0
     imagesize                 0.7.1                    py27_0
     ipaddress                 1.0.16                   py27_0
     ipykernel                 4.5.0                    py27_0
     ipython                   5.1.0                    py27_0
     ipython_genutils          0.1.0                    py27_0
     ipywidgets                5.2.2                    py27_0
     itsdangerous              0.24                     py27_0
     jbig                      2.1                           0
     jdcal                     1.2                      py27_1
     jedi                      0.9.0                    py27_1
     jinja2                    2.8                      py27_1
     jmespath                  0.9.0                    py27_0
     jpeg                      8d                            2
     jsonschema                2.5.1                    py27_0
     jupyter                   1.0.0                    py27_3
     jupyter_client            4.4.0                    py27_0
     jupyter_console           5.0.0                    py27_0
     jupyter_core              4.2.0                    py27_0
     keras                     2.0.2                    py27_0
     lazy-object-proxy         1.2.1                    py27_0
     libdynd                   0.7.2                         0
     libffi                    3.2.1                         0
     libgcc                    4.8.5                         2
     libgfortran               3.0.0                         1
     libgpuarray               0.6.8                         0
     libpng                    1.6.27                        0
     libprotobuf               3.2.0                         0
     libsodium                 1.0.10                        0
     libtiff                   4.0.6                         2
     libxcb                    1.12                          0
     libxml2                   2.9.2                         0
     libxslt                   1.1.28                        0
     llvmlite                  0.13.0                   py27_0
     locket                    0.2.0                    py27_1
     lxml                      3.6.4                    py27_0
     mako                      1.0.6                    py27_0
     markupsafe                0.23                     py27_2
     matplotlib                2.0.2               np111py27_0
     mistune                   0.7.3                    py27_0
     mkl                       2017.0.3                      0
     mkl-service               1.1.2                    py27_2
     mock                      2.0.0                    py27_0
     mpld3                     0.2                      py27_0
     mpmath                    0.19                     py27_1
     multipledispatch          0.4.8                    py27_0
     nb_anacondacloud          1.2.0                    py27_0
     nb_conda                  2.0.0                    py27_0
     nb_conda_kernels          2.0.0                    py27_0
     nbconvert                 4.2.0                    py27_0
     nbformat                  4.1.0                    py27_0
     nbpresent                 3.0.2                    py27_0
     networkx                  1.11                     py27_0
     nltk                      3.2.4                    py27_0
     nose                      1.3.7                    py27_1
     notebook                  4.2.3                    py27_0
     numba                     0.28.1              np111py27_0
     numexpr                   2.6.2               np111py27_0
     numpy                     1.11.3                   py27_0
     odo                       0.5.0                    py27_1
     openpyxl                  2.3.2                    py27_0
     openssl                   1.0.2l                        0
     pandas                    0.20.1              np111py27_0
     partd                     0.3.6                    py27_0
     patchelf                  0.9                           0
     path.py                   8.2.1                    py27_0
     pathlib2                  2.1.0                    py27_0
     patsy                     0.4.1                    py27_0
     pbr                       1.10.0                   py27_0
     pep8                      1.7.0                    py27_0
     pexpect                   4.0.1                    py27_0
     pickleshare               0.7.4                    py27_0
     pillow                    3.3.1                    py27_0
     pip                       9.0.1                    py27_1
     pixman                    0.32.6                        0
     pkginfo                   1.3.2                    py27_0
     ply                       3.9                      py27_0
     prompt_toolkit            1.0.3                    py27_0
     protobuf                  3.2.0                    py27_0
     psutil                    4.3.1                    py27_0
     ptyprocess                0.5.1                    py27_0
     py                        1.4.31                   py27_0
     pyasn1                    0.1.9                    py27_0
     pycairo                   1.10.0                   py27_0
     pycosat                   0.6.1                    py27_1
     pycparser                 2.14                     py27_1
     pycrypto                  2.6.1                    py27_4
     pycurl                    7.43.0                   py27_0
     pyflakes                  1.3.0                    py27_0
     pygments                  2.1.3                    py27_0
     pygpu                     0.6.8                    py27_0
     pylint                    1.5.4                    py27_1
     pymc                      2.3.6               np111py27_2
     pyopenssl                 16.0.0                   py27_0
     pyparsing                 2.1.4                    py27_0
     pyqt                      5.6.0                    py27_0
     pytables                  3.2.3.1             np111py27_0
     pytest                    2.9.2                    py27_0
     python                    2.7.13                        0
     python-dateutil           2.5.3                    py27_0
     pytz                      2016.6.1                 py27_0
     pyyaml                    3.12                     py27_0
     pyzmq                     15.4.0                   py27_0
     qt                        5.6.0                         0
     qtawesome                 0.3.3                    py27_0
     qtconsole                 4.2.1                    py27_1
     qtpy                      1.1.2                    py27_0
     readline                  6.2                           2
     redis                     3.2.0                         0
     redis-py                  2.10.5                   py27_0
     requests                  2.11.1                   py27_0
     rope                      0.9.4                    py27_1
     ruamel_yaml               0.11.14                  py27_0
     s3transfer                0.1.10                   py27_0
     scikit-image              0.12.3              np111py27_1
     scikit-learn              0.18.1              np111py27_1
     scipy                     0.19.0              np111py27_0
     seaborn                   0.7.1                    py27_0
     setuptools                27.2.0                   py27_0
     simplegeneric             0.8.1                    py27_1
     singledispatch            3.4.0.3                  py27_0
     sip                       4.18                     py27_0
     six                       1.10.0                   py27_0
     smart_open                1.5.3                    py27_0
     snowballstemmer           1.2.1                    py27_0
     sockjs-tornado            1.0.3                    py27_0
     sphinx                    1.4.6                    py27_0
     spyder                    3.0.0                    py27_0
     sqlalchemy                1.0.13                   py27_0
     sqlite                    3.13.0                        0
     ssl_match_hostname        3.4.0.2                  py27_1
     statsmodels               0.6.1               np111py27_1
     subprocess32              3.2.7                    py27_0
     sympy                     1.0                      py27_0
     tensorflow                1.1.0               np111py27_0
     terminado                 0.6                      py27_0
     theano                    0.9.0                    py27_0
     tk                        8.5.18                        0
     toolz                     0.8.0                    py27_0
     tornado                   4.4.1                    py27_0
     traitlets                 4.3.0                    py27_0
     unicodecsv                0.14.1                   py27_0
     wcwidth                   0.1.7                    py27_0
     werkzeug                  0.11.11                  py27_0
     wheel                     0.29.0                   py27_0
     widgetsnbextension        1.2.6                    py27_0
     wrapt                     1.10.6                   py27_0
     xlrd                      1.0.0                    py27_0
     xlsxwriter                0.9.3                    py27_0
     xlwt                      1.1.2                    py27_0
     xz                        5.2.2                         0
     yaml                      0.1.6                         0
     zeromq                    4.1.4                         0
     zlib                      1.2.8                         3

List of Pre-installed R Packages
--------------------------------
Packages are available in the environment at ``/usr/lib/envs/<env-ID-and-version-details>``.

.. sourcecode:: r

     _r-mutex                  1.0.0               anacondar_1    r
     r                         3.3.2                  r3.3.2_0    r
     r-assertthat              0.1                    r3.3.2_4    r
     r-backports               1.0.4                  r3.3.2_0    r
     r-base                    3.3.2                         0    r
     r-base64enc               0.1_3                  r3.3.2_0    r
     r-bh                      1.62.0_1               r3.3.2_0    r
     r-bitops                  1.0_6                  r3.3.2_2    r
     r-boot                    1.3_18                 r3.3.2_0    r
     r-broom                   0.4.1                  r3.3.2_0    r
     r-car                     2.1_4                  r3.3.2_0    r
     r-caret                   6.0_73                 r3.3.2_0    r
     r-catools                 1.17.1                 r3.3.2_2    r
     r-class                   7.3_14                 r3.3.2_0    r
     r-cluster                 2.0.5                  r3.3.2_0    r
     r-codetools               0.2_15                 r3.3.2_0    r
     r-colorspace              1.3_1                  r3.3.2_0    r
     r-crayon                  1.3.2                  r3.3.2_0    r
     r-curl                    2.3                    r3.3.2_0    r
     r-data.table              1.10.0                 r3.3.2_0    r
     r-dbi                     0.5_1                  r3.3.2_0    r
     r-devtools                1.12.0                 r3.3.2_0    r
     r-dichromat               2.0_0                  r3.3.2_2    r
     r-digest                  0.6.10                 r3.3.2_0    r
     r-dplyr                   0.5.0                  r3.3.2_0    r
     r-essentials              1.5.2                  r3.3.2_0    r
     r-evaluate                0.10                   r3.3.2_0    r
     r-forcats                 0.1.1                  r3.3.2_0    r
     r-foreach                 1.4.3                  r3.3.2_0    r
     r-foreign                 0.8_67                 r3.3.2_0    r
     r-formatr                 1.4                    r3.3.2_0    r
     r-ggplot2                 2.2.0                  r3.3.2_0    r
     r-gistr                   0.3.6                  r3.3.2_0    r
     r-git2r                   0.16.0                 r3.3.2_0    r
     r-glmnet                  2.0_5                  r3.3.2_0    r
     r-gtable                  0.2.0                  r3.3.2_0    r
     r-haven                   1.0.0                  r3.3.2_0    r
     r-hexbin                  1.27.1                 r3.3.2_0    r
     r-highr                   0.6                    r3.3.2_0    r
     r-hms                     0.3                    r3.3.2_0    r
     r-htmltools               0.3.5                  r3.3.2_0    r
     r-htmlwidgets             0.8                    r3.3.2_0    r
     r-httpuv                  1.3.3                  r3.3.2_0    r
     r-httr                    1.2.1                  r3.3.2_0    r
     r-irdisplay               0.4.4                  r3.3.2_0    r
     r-irkernel                0.7.1                  r3.3.2_0    r
     r-iterators               1.0.8                  r3.3.2_0    r
     r-jsonlite                1.1                    r3.3.2_0    r
     r-kernsmooth              2.23_15                r3.3.2_0    r
     r-knitr                   1.15.1                 r3.3.2_0    r
     r-labeling                0.3                    r3.3.2_2    r
     r-lattice                 0.20_34                r3.3.2_0    r
     r-lazyeval                0.2.0                  r3.3.2_0    r
     r-lme4                    1.1_12                 r3.3.2_0    r
     r-lubridate               1.6.0                  r3.3.2_0    r
     r-magrittr                1.5                    r3.3.2_2    r
     r-maps                    3.1.1                  r3.3.2_0    r
     r-markdown                0.7.7                  r3.3.2_2    r
     r-mass                    7.3_45                 r3.3.2_0    r
     r-matrix                  1.2_7.1                r3.3.2_0    r
     r-matrixmodels            0.4_1                  r3.3.2_0    r
     r-memoise                 1.0.0                  r3.3.2_0    r
     r-mgcv                    1.8_16                 r3.3.2_0    r
     r-mime                    0.5                    r3.3.2_0    r
     r-minqa                   1.2.4                  r3.3.2_2    r
     r-mnormt                  1.5_5                  r3.3.2_0    r
     r-modelmetrics            1.1.0                  r3.3.2_0    r
     r-modelr                  0.1.0                  r3.3.2_0    r
     r-munsell                 0.4.3                  r3.3.2_0    r
     r-nlme                    3.1_128                r3.3.2_0    r
     r-nloptr                  1.0.4                  r3.3.2_2    r
     r-nnet                    7.3_12                 r3.3.2_0    r
     r-openssl                 0.9.5                  r3.3.2_0    r
     r-pbdzmq                  0.2_4                  r3.3.2_0    r
     r-pbkrtest                0.4_6                  r3.3.2_0    r
     r-plyr                    1.8.4                  r3.3.2_0    r
     r-pryr                    0.1.2                  r3.3.2_0    r
     r-psych                   1.6.9                  r3.3.2_0    r
     r-purrr                   0.2.2                  r3.3.2_0    r
     r-quantmod                0.4_7                  r3.3.2_0    r
     r-quantreg                5.29                   r3.3.2_0    r
     r-r6                      2.2.0                  r3.3.2_0    r
     r-randomforest            4.6_12                 r3.3.2_0    r
     r-rbokeh                  0.5.0                  r3.3.2_0    r
     r-rcolorbrewer            1.1_2                  r3.3.2_3    r
     r-rcpp                    0.12.8                 r3.3.2_0    r
     r-rcppeigen               0.3.2.9.0              r3.3.2_0    r
     r-readr                   1.0.0                  r3.3.2_0    r
     r-readxl                  0.1.1                  r3.3.2_0    r
     r-recommended             3.3.2                  r3.3.2_0    r
     r-repr                    0.10                   r3.3.2_0    r
     r-reshape2                1.4.2                  r3.3.2_0    r
     r-rmarkdown               1.3                    r3.3.2_0    r
     r-rpart                   4.1_10                 r3.3.2_0    r
     r-rprojroot               1.1                    r3.3.2_0    r
     r-rstudioapi              0.6                    r3.3.2_0    r
     r-rvest                   0.3.2                  r3.3.2_0    r
     r-scales                  0.4.1                  r3.3.2_0    r
     r-selectr                 0.3_0                  r3.3.2_0    r
     r-shiny                   0.14.2                 r3.3.2_0    r
     r-sourcetools             0.1.5                  r3.3.2_0    r
     r-sparsem                 1.74                   r3.3.2_0    r
     r-spatial                 7.3_11                 r3.3.2_0    r
     r-stringi                 1.1.2                  r3.3.2_0    r
     r-stringr                 1.1.0                  r3.3.2_0    r
     r-survival                2.40_1                 r3.3.2_0    r
     r-tibble                  1.2                    r3.3.2_0    r
     r-tidyr                   0.6.0                  r3.3.2_0    r
     r-tidyverse               1.0.0                  r3.3.2_0    r
     r-ttr                     0.23_1                 r3.3.2_0    r
     r-uuid                    0.1_2                  r3.3.2_0    r
     r-whisker                 0.3_2                  r3.3.2_2    r
     r-withr                   1.0.2                  r3.3.2_0    r
     r-xml2                    1.0.0                  r3.3.2_0    r
     r-xtable                  1.8_2                  r3.3.2_0    r
     r-xts                     0.9_7                  r3.3.2_2    r
     r-yaml                    2.1.14                 r3.3.2_0    r
     r-zoo                     1.7_13                 r3.3.2_0    r

