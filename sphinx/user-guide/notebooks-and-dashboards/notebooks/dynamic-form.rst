.. _dynamic-form:

======================================
Using Dynamic Input Forms in Notebooks
======================================
Qubole supports Apache Zeppelin's `dynamic input forms <https://zeppelin.apache.org/docs/0.8.0/usage/dynamic_form/intro.html>`__
and also based on the language backend. You can run the paragraphs several time after you change the input values. You
can create forms by:

* `Using Form Templates`_
* `Using Programming Language`_

Dynamic forms are supported on Spark notebooks. For more information about parameterized notebook API that uses these
dynamic forms to populate parameter values, see :ref:`run-notebook-api`.

Using Form Templates
--------------------
You can create a text-input form, select form, and a checkbox form using form templates. You can change the values in the
input fields and rerun the paragraph as many times as required.

A simple form can be created as shown in this example.

.. image:: notebook-images/FormTemplate.png

A simple checkbox form can be created as shown in this example.

.. image:: notebook-images/CheckboxForm.png

For more information, see `using form templates <https://zeppelin.apache.org/docs/0.8.0/usage/dynamic_form/intro.html#using-form-templates-scope-paragraph>`__.


Using Programming Language
--------------------------
You can create a text input form, checkbox form using the scala (``%spark``) and Python (``%pyspark``) interpreters. As in
using form templates, by using the programming language, you can also change the value of inputs and rerun the paragraphs
as many times as required.

A simple form can be created as shown in this example.

.. image:: notebook-images/ProgramFormTemplate.png

A simple checkbox form can be created as shown in this example.

.. image:: notebook-images/ProgramCheckboxForm.png

For more information, see `creating forms programmatically <https://zeppelin.apache.org/docs/0.8.0/usage/dynamic_form/intro.html#creates-programmatically-scope-paragraph>`__.

Example
-------

The following example shows how to use ``z.bind()`` function to create universal variable with dynamic forms. You can run this sample code as a paragraph
on the **Notebooks** page.

First Paragraph

.. sourcecode:: bash

   %spark

   z.angularBind("year",z.input("Year"))

Second Paragraph

.. sourcecode:: bash

    %spark
    z.show(sqlContext.sql("""
    SELECT avg(depth) as avg_depth, max(depth) as max_depth, min(depth) as min_depth
    FROM eq
    WHERE year = """ + z.angular("year") + """
    """
    ))

Third Paragraph

.. sourcecode:: bash

   %spark
   z.show(sqlContext.sql("""
   SELECT avg(lat) as avg_lat, avg(lon) as avg_lon
   FROM eq
   WHERE year = """ + z.angular("year") + """
   """
   ))

The following figure shows the sample paragraphs that use universal variables using Angular Bind.

.. image:: notebook-images/angular-bind.png