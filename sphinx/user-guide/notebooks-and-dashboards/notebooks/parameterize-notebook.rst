.. _parameterize-notebook:

##########################
Parameterizing Notebooks
##########################

If you want to run notebooks paragraphs with different values, you can parameterize the notebook and then pass the required parameters
from the **Analyze** UI, **Scheduler** UI, or by using the REST API.

1. :ref:`define-parameters`
2. :ref:`run-notebook-interface`

.. _define-parameters:

Defining Parameters
-------------------

You should define the parameters that have to be passed to the cell when running the notebook through various interfaces.

The following examples show how to define parameters.

Python, Scala, and SQL
......................

The following example shows how to define Python read parameters.

.. sourcecode:: python

   %pyspark
   param1 = z.input("param_1")
   param2 = z.input("param_2")
   print(param1)
   print(param2)

The following example shows how to define Scala read parameters.

.. sourcecode:: scala

   val param1 = z.input("param_1")
   val param2 = z.input("param_2")
   println(param1)
   println(param2)

The following example shows how to define SQL read parameters.

.. sourcecode:: bash

   %sql
   select * from employees where emp_id='${param_1}'

Angular Variables
.................

The following example shows how to set Python angular variables.

.. sourcecode:: python

   %pyspark
   z.put("AngularVar1",z.input("param_1"))
   z.put("AngularVar2",z.input("param_2"))


The following example shows how to set Scala angular variables.

.. sourcecode:: scala


   %spark
   z.put("AngularVar1", z.input("param_1"))
   z.put("AngularVar2", z.input("param_2"))

   // below two to pass to %sql
   z.angularBind("AngularVar1",z.input("param_1"))
   z.angularBind("AngularVar2",z.input("param_2"))


The following example shows how to get Python angular variables.

.. sourcecode:: python

  %pyspark
  var_1 = z.get("AngularVar1")
  var_2 = z.get("AngularVar2")
  print(var_1)
  print(var_2)


The following example shows how to get Scala angular variables.

.. sourcecode:: scala


   %spark
   val var_1 = z.get("AngularVar1")
   val var_2 = z.get("AngularVar2")

   println(var_1)
   println(var_2)

The following example shows how to get SQL angular variables.

.. sourcecode:: bash

   %sql
   select * from employees where emp_id= '${AngularVar1}'

.. _run-notebook-interface:

Running Parameterized Notebooks
-------------------------------

You can run the parameterized notebooks from the **Analyze** UI, **Scheduler** UI, or by using the REST API.

* :ref:`Run a notebook from the Analyze UI<run-notebook-in-analyze>`
* :ref:`Run a notebook from the Scheduler UI <run-notebooks-on-schedule>`
* :ref:`Run a notebook by using the REST API <run-notebook-api>`


Examples
--------

The following illustrations show the parameterized notebook after the execution.

.. image:: notebook-images/parameterizednotebook1.png

.. image:: notebook-images/parameterizednotebook2.png

.. image:: notebook-images/parameterizednotebook3.png

.. image:: notebook-images/parameterizednotebook4.png





