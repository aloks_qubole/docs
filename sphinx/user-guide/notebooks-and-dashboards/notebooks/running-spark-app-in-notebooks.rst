.. _running-spark-app-in-notebooks:

Running Spark Applications in Notebooks
=======================================

You can run Spark Applications from the **Notebooks** page of the QDS UI. When running Spark applications in notebooks, you should understand notebooks, how to associate interpreters with notebooks, how to run concurrent commands, and how to set the context.

* :ref:`understand-notebooks`
* :ref:`associate-interpreters`
* :ref:`concurrent-commands`
* :ref:`diff_context`

Log in to QDS with your username and password. Navigate to the **Notebooks** page.

.. _understand-notebooks:

Understanding Notebooks
-----------------------
You can create any number of new notebooks. Notebooks data is synced and persisted in cloud storage (for example S3) regularly. Notebooks are
associated with a cluster, so notebooks from cluster A can not be accessed by cluster B.

See :ref:`notebook-index` for more information on Qubole's Notebooks. For more information, see:

* :ref:`use-spark-notebook` describes Spark interpreters.
* :ref:`configure-spark-notebook` describes the configuration associated with the Spark notebook.
* :ref:`run-notebooks-on-schedule` describes how to schedule notebooks using the cron scheduler.
* :ref:`run-notebook-api` describes how to run a notebook through a REST API call.

.. _associate-interpreters:

Associating Interpreters with Notebooks
---------------------------------------

Spark Interpreters are associated with notebooks by default. However, if you want to use any user interpreter then you must associate the interpreter with the notebook.
Interpreters are started on demand when required by notebooks.


1. On the **Notebooks** page, click on the **Gear** icon.

   .. image:: notebook-images/ExpActionIcon.png

   On the **Settings** page, list of Interpreters are displayed as shown in the following figure.

   .. image:: notebook-images/notebook-interpreter-association.png

   The first interpreter on the list is the default interpreter.

2. Click on the required interpreter to associate with the notebook.

3. Click **Save**.


.. _concurrent-commands:

Running Concurrent Spark Commands
---------------------------------

You can run multiple Spark SQL commands in parallel.


  1. On the **Notebooks** page, click **Interpreters**.
  2. For the required interpreter, click on the corresponding **edit** button.
  3. Set the following properties:

        * ``zeppelin.spark.concurrentSQL`` = ``true``
        * ``zepplin.spark.sql.maxConcurrency`` = ``number of concurrent commands to be run``

  4. Create multiple paragraphs with Spark SQL commands and click the **Run** icon on the left side to run all the paragraphs.

     All the paragraphs run concurrently.


.. _diff_context:

Using SparkContext, HiveContext and SQLContext Objects
------------------------------------------------------
Similar to a Spark shell, a SparkContext (SC) object is available in the Notebook. In addition, HiveContext is also
available in a Notebook if you have to set the following configuration to **true** in interpreter settings:

``zeppelin.spark.useHiveContext``

If this configuration is not set to true, then SQLContext is available in a Notebook instead of HiveContext.



