.. _running-shell-command-notebook:

====================================
Running Shell Commands in Notebooks
====================================

You can run shell commands either sequentially or concurrently from the **Notebooks** page of the QDS UI. Shell Interpreters are associated with notebooks by default. However, if you want to use any user interpreter then you must associate the interpreter with the notebook.
Interpreters are started on demand when required by notebooks.

Steps
-----

1. Navigate to the **Notebooks** page.
2. On the **Notebooks** page, click **Interpreters**.
3. For the shell interpreter, click on the corresponding **edit** button.
4. Set ``zepplin.shell.concurrentCommands`` = ``true``.

   You can run up to five shell commands concurrently.

5. Create multiple paragraphs with Spark SQL commands and click the **Run** icon on the left side to run all the paragraphs.

   All the paragraphs run concurrently.

   The following figure shows sample shell commands run from the **Notebooks** page.

   .. image:: notebook-images/shell-cmd.png



