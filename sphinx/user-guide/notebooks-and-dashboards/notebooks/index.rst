.. _notebook-index:

==========
Notebooks
==========

This section explains how to use Notebooks. It covers the following topics:


.. toctree::
    :maxdepth: 1

    notebook.rst
    manage-notebook.rst
    using-notebook.rst
    running-spark-app-in-notebooks
    running-shell-command-notebook.rst
    link-notebook-github.rst
    zeppelin-metrics.rst
    datadog-zeppelin.rst
    config-interpreters.rst
    notebook-interpreter-modes.rst
    dynamic-form.rst
    parameterize-notebook.rst
    data-visualization/index

For information on using Notebooks with Spark, see:

   * :ref:`configure-spark-notebook`
   * :ref:`use-spark-notebook`
   * :ref:`run-notebooks-on-schedule`

To create, configure, run, clone, delete or bind a notebook through a REST API call, see :ref:`notebook-api`.