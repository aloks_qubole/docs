.. _charting-support:

===============================
Data Visualization in Notebooks
===============================

Notebooks in the QDS UI support data visualization. You can use packages, such as matplotlib and plotly that are part of Python libraries to represent the datasets and dataframes in a visual format.
Python libraries are available as part of QDS package management.

.. note:: Package management is available as an open beta feature. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
   to enable this feature on the QDS account.


.. toctree::
    :maxdepth: 1

    setup-dv
    matplotlib
    plotly






