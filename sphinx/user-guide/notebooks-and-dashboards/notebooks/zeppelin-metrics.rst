.. _zeppelin-metrics:

=======================================================
Understanding the Zeppelin Metrics for Monitoring (AWS)
=======================================================

Notebooks support Datadog monitoring when the Datadog monitoring is enabled at the QDS account level.
You can configure Datadog settings at the cluster level for Notebooks as described in :ref:`cluster-monitoring`.

For more information on enabling Datadog in **Control Panel** > **Account Settings**, see :ref:`iam-keys` or :ref:`manage-roles`.

The following table lists the different Zeppelin metrics that are displayed in the Datadog account.

+--------------------------------------------------+---------------------------------------------------------------------------------+
| Zeppelin Metrics                                 | Metric Definition                                                               |
+==================================================+=================================================================================+
| zeppelin.pendingJobs.total.number                | Total number of jobs in pending state on zeppelin side.                         |
+--------------------------------------------------+---------------------------------------------------------------------------------+
|  zeppelin.driver.count.master                    | Total number of drivers running on the master.                                  |
+--------------------------------------------------+---------------------------------------------------------------------------------+
| zeppelin.driver.memoryAllocatedMB.master         | Total memory allocated (in MB) to all the drivers running on the master.        |
+--------------------------------------------------+---------------------------------------------------------------------------------+
| zeppelin.driver.memoryAllocatedPercentage.master | Percentage of master memory allocated to all the drivers running on the master. |
+--------------------------------------------------+---------------------------------------------------------------------------------+
| zeppelin.notebook.total.diskUsageMB              | Total disk space (in MB) consumed by all notebooks and dashboards on a cluster. |
+--------------------------------------------------+---------------------------------------------------------------------------------+
| zeppelin.notebook.total.number                   | Total number of notebooks loaded in the memory.                                 |
+--------------------------------------------------+---------------------------------------------------------------------------------+

In addition to these, the standard JVM metrics such as zeppelin.heap.usage are also displayed.

