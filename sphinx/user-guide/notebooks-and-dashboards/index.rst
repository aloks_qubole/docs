.. _notebook-dashboard-index:

========================
Notebooks and Dashboards
========================

This section explains how to use Notebooks and Dashboards.


.. toctree::
    :maxdepth: 1

    notebooks/index.rst
    dashboards/index.rst