.. _dashboards-intro:

==========================
Introduction to Dashboards
==========================
Dashboards provide a means for a QDS account user (a dashboard *publisher*) to share a
:ref:`notebook <first-class-notebooks>` with other users of the QDS account, and also with users
(*dashboard-only consumers*) who are not otherwise members of the QDS account.

To add a dashboard-only user, follow :ref:`these instructions <add-dashboard-user>`.


* :ref:`Publishers or associated notebook owners <dashboards-publisher>` can create, edit, and share dashboards. Ensure
  that you have permissions to the **Folder** resource as described in :ref:`manage-roles-user-resources-actions`.
* Dashboard-only :ref:`Consumers <dashboards-consumer>` can run the dashboard and change the value of parameters (for example dates
  or customer sets) but can't change the underlying code (the notebook itself).

A dashboard is a report view (read-and-execute-only) of a notebook, and has the following characteristics:

* Requires a running cluster, as with notebooks.
* Must be created by the associated notebook owner, who must be a member of a QDS account.
* Currently available from the QDS UI only (no API at present).
* Includes the entire notebook (can't be just a portion of it).
* Includes only one notebook (can't combine multiple notebooks or portions).
* Is separate from the underlying notebook (the associated notebook owner and other users of the notebook can continue to develop the
  notebook over time without affecting the dashboard).
* Can be used by any number of consumers concurrently.

Dashboards are organized in the QDS UI in the same way as notebooks, using the following main folders:

* **My Home**
* **Common**
* **Users**

You can create any number of sub-folders under any of these folders. Associated notebook owners can make dashboards
available to consumers by placing them in the **Common** folder.

