.. _dashboards-consumer:

==============================
Using Dashboards as a Consumer
==============================
A *dashboard-only consumer* is someone who is not a full member of a QDS account, but is allowed to
use dashboards. Full QDS-account members can also use dashboards without restrictions.

As a dashboard-only consumer you can do the following with the dashboards in your folder:

* Run a dashboard as :ref:`published <dashboards-publisher>` (the *base view*); *OR*
* Change the values of the dashboard parameters, if any  (for example, change the date or customer set) and run the dashboard with
  those values.

You can also see and run dashboards in other users' folders (under **Users**), or in the **Common** folder, if you have
been granted permission by the associated notebook owner.

As a dashboard-only user you *cannot*:

* Create a dashboard.
* Publish a dashboard.

Using a Dashboard
-----------------
Proceed as follows:

1. From the main menu of the QDS UI, navigate to **Dashboards**.
2. Choose a dashboard.
3. If necessary, click **Enable Interactive Mode**. This starts the cluster that runs the underlying notebook.

   * In *interactive mode*, you are insulated from changes by other users of the dashboard, or by the associated notebook owner.
     The dashboard will reflect only the changes that you make.
   * In *non-interactive mode*, the dashboard will reflect any changes made by other users (to the dashboard or to the underlying notebook)
     while you are working.

4. Run the dashboard. Either:

   * Run it as is (the base view); *OR*
   * Change the value of some or all parameters (if any) and run the dashboard.

   You can export the output as a PDF.

Understanding the Different Dashboard Modes
...........................................
By default the dashboard is in a **Read-only** (compute is DOWN) or in an **Editing** (compute is UP) mode.

The associated notebook owner can see the dashboard in the **Editing** mode if the compute is **UP**. Pull down the
**Editing** mode drop-down list and you can see **Enable Interactive Mode** which enables you to go into personalized view.
The **Interactive Mode** lets you create a personalized view where you (or any user) can change the paragraph parameters (if any)
and see a different result than the actual dashboard.

You (or any user) must have an **Execute** permission to enter into the **Interactive Mode**.

Changing the Dashboard Color Theme
----------------------------------
A QDS user with allowed ``Update`` permission can change the theme of the dashboard. The half black-and-white circle icon
in the top-left of the dashboard lets you do that. The icon is as illustrated here.

.. image:: db-images/DBThemeChange.png

Click the theme icon and you can see different themes. Choose the one that you want to use and the new theme immediately
gets applied on to the dashboard.