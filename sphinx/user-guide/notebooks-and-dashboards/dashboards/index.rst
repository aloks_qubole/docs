.. _dashboards-index:

==========
Dashboards
==========
This section explains how to use Dashboards, which allow QDS users to share notebooks with other users who do not have
to be signed up for a full QDS account. The following topics provide more information:


.. toctree::
    :maxdepth: 1

    dashboard-intro.rst
    dashboard-publisher.rst
    add-dashboard-user.rst
    dashboard-consumer.rst
    viewing-source-notebook.rst
    view-all-dashboards.rst
    email-dashboard.rst
    download-dashboard.rst



* For information on using Notebooks, see :ref:`notebook-index`.

* For information on using Notebooks with Spark, see:

   * :ref:`configure-spark-notebook`
   * :ref:`use-spark-notebook`
   * :ref:`run-notebooks-on-schedule`
   * :ref:`debug-notebooks`

