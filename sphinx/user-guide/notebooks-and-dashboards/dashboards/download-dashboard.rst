.. _download-dashboard:

=======================
Downloading a Dashboard
=======================

You can download a dashboard in PDF, PNG, and HTML formats.

1. In the **Dashboards** page, click on the Settings icon as shown in the following figure.

   .. image:: db-images/dash-settings1.png

2. Select **Download As**.
3. In the **Download Dashboard As** dialog box, select the required format from the drop-down list. By default, **PDF** is selected.
4. Click **Download**.

The following figure shows a sample **Download Dashboard As** dialog box.

.. image:: db-images/download-dashboard.png

.. note:: If a dashboard fails to render within 3 minutes, then the download option fails.

You can also download dashboards by using the command API. See :ref:`convert-notebook-api`
