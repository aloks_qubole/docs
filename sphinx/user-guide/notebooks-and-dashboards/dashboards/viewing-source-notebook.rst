.. _view-source-notebook:

==========================================
Viewing the Source Notebook from Dashboard
==========================================

As a notebook owner or a consumer, you can view the source notebook that is associated with the dashboard.

Steps
-----

1. From the **Home** menu, click **Dashboards**. The **Dashboards** page is displayed.
2. On the left navigation pane, identify the dashboard for which you want to view the source notebook.
3. Right-click on the required dashboard or click on the **Settings** icon next to the dashboard, and select **View Notebook** from the menu as shown below.

   .. image:: db-images/view-notebook.png



The source notebook is opened in the **Notebooks** page, in a separate tab.