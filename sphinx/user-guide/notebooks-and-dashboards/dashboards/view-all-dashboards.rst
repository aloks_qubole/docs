.. _view-all-dashboards:

=======================
Viewing All Dashboards
=======================

If you have the **Update** permission, you can view all the dashboards associated with the notebook.

Steps
-----

1. From the **Home** menu, click **Notebooks**. The **Notebooks** page is displayed.
2. On the left navigation pane, select the required notebook.
3. On the top right corner, click on the **Manage notebook dashboard(s)** icon as shown below.

   .. image:: db-images/manage-dashboard.png

The associated dashboards are displayed on the **Dashboards** pane on the right side as shown below.

.. image:: db-images/dashboard-window.png



