.. _add-dashboard-user:

Adding a Dashboard-Only User
============================

To add a dashboard-only user-- a user who is limited to using dashboards as a :ref:`consumer <dashboards-consumer>`--
proceed as follows:

#. In the QDS UI, navigate to the **Control Panel** and choose **Manage Users**.
#. On the resulting screen, click on the person icon near the top right.
#. On the resulting screen, enter the user's email address in the **User Email(s)** field.
#. Click in the **Groups** field and choose **dashboard-user** from the dropdown.

You will see a message that an invitation has been sent to that user. Once the user has received the invitation and
followed the instructions in it, he or she can use dashboards as described :ref:`here <dashboards-consumer>`.