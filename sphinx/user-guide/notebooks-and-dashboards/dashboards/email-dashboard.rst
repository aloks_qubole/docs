.. _email-dashboard:

====================
Emailing a Dashboard
====================

You can email a dashboard as an attachment in PDF, PNG, and HTML formats.

1. In the **Dashboard** page, click on the Settings icon as shown in the following figure.

   .. image:: db-images/dash-settings.png

2. Select **Email as attachment**.
3. In the **Email Dashboard** dialog box, select the required format from the drop-down list. By default, **PDF** is selected.
4. Enter the email address. If you want to send the attachment to multiple recipients, then add comma separated email addresses.
5. Click **Send**.

The following figure shows a sample **Email Dashboard** dialog box.

.. image:: db-images/email-dashboard.png

.. note:: If a dashboard fails to render within 3 minutes, then the email option fails.

You can also email dashboards as attachments by using the command API. See :ref:`convert-notebook-api`.

