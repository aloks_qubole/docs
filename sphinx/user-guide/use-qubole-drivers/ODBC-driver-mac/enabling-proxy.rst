.. _enabling-proxy-mac:

=============================
Enabling the Proxy Connection
=============================
You can enable proxy if you want to use a proxy server for your connections on **Qubole ODBC Driver DSN Setup**. To enable:

1. Open iODBC Administrator64 with **Administrator Privileges**. (``sudo /Applications/iODBC/iODBC\ Administrator64.app/Contents/MacOS/iODBC\ Administrator64``)
2. Go to **System DSN** and select **Qubole ODBC Driver DSN**.
3. Click **Configure** and the dialog is illustrated here.

   .. image:: ../../../admin-guide/ag-images/configureODBCMac3.png

4. Enter the valid API Token.
5. Change the **DSI** to the required one - (Hive, Presto or Spark).
6. Create a keyword entry by clicking the **+** button. Enter ``PROXY_ENABLED = true``.

   .. note:: The keywords associated with the proxy connection are ``PROXY_HOST``, ``PROXY_PORT``, ``PROXY_USERNAME``,
             ``PROXY_PASSWORD``, ``PROXY_ENABLED``, and ``PROXY_TYPE``.

7. Similarly, set the ``PROXY_HOST`` and ``PROXY_PORT`` keyword values, which are mandatory. Example: ``PROXY_HOST = 192.168.56.1``
   and ``PROXY_PORT = 8081``.
8. Enter a value each for the ``PROXY_USERNAME`` and ``PROXY_PASSWORD`` (after adding them as keywords) if authentication
   is required.
9. Add the ``PROXY_TYPE`` keyword and add a different value if you do not want the default ``HTTP`` value. These are
   the other supported proxy types:

   * ``HTTP_1_0``
   * ``SOCKS4``
   * ``SOCKS4A``
   * ``SOCKS5``
   * ``SOCKS5_HOSTNAME``

   Here is an example of the DSN Setup with the Proxy connection enabled.

   .. image:: ../../../admin-guide/ag-images/Proxy-ODBCMac.png