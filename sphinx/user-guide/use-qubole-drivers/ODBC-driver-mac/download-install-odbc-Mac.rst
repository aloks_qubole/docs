.. _download-install-odbc-driver-Mac:

==================================
Downloading the ODBC Driver on Mac
==================================
Proceed as follows to prepare your system and download the driver:

.. note:: The Qubole driver is stored in the publicly accessible Cloud storage, and works on all Cloud platforms supported
          in QDS.

1. Download and install the iODBC Driver Manager for Mac from: http://www.iodbc.org.
2. Download the Qubole ODBC driver from: https://s3.amazonaws.com/paid-qubole/odbc/1.1.11/qds-odbc-1.1.11.dmg.
3. :ref:`download-install-odbc-driver-Mac-install` and configure the BI tool (if required) on the Mac OS. For more
   information on the configuration of the BI tools, see :ref:`Partner Integration Guides <partner-integration-guides-index>`.
4. Read the Release Notes: :ref:`release-notes-odbcv1.1.11-index`.