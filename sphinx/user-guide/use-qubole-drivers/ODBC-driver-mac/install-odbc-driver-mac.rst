.. _download-install-odbc-driver-Mac-install:

=================================
Installing the ODBC Driver on Mac
=================================
Before installing the driver, you can install the iODBC Driver Manager that you have downloaded it as mentioned in
:ref:`download-install-odbc-driver-Mac`.

Installing the iODBC Driver Manager
-----------------------------------
If you want to use the UI to configure the ODBC driver, use the iODBC Driver Manager, which you can download from `iODBC Downloads <http://www.iodbc.org/dataspace/iodbc/wiki/iODBC/Downloads>`__.
After downloading the iODBC, install it by:

1. Double-clicking the downloaded iODBC Driver Manager (.dmg) file.
2. Double-clicking the installer file, ``iODBC-SDK.pkg`` and following the instructions in the prompts.

Installing the ODBC Driver
--------------------------
Perform these steps to install the ODBC driver on Mac:

1. Control-click the QuboleMacODBCInstaller.dmg in your **Downloads** folder (or wherever you have chosen to download it)
   and choose **Open** in the resulting dialog box.

   .. note:: You need to control-click the package, and click **Open** to affirm that you really do want to
             open it, because Qubole is not currently an *identified developer* for the Mac. Qubole expects to
             rectify this shortly.

2. In the folder that opens, click the icon labelled **Qubole Mac ODBC Driver Installer.pkg**.

   .. image:: ../../../admin-guide/ag-images/InstallODBCMac2.png

3. Click **Agree** to accept the software license agreement and the dialog is illustrated here.

   .. image:: ../../../admin-guide/ag-images/InstallODBCMac3.png

4. Click **Continue** and on the resulting screen, click **Install** and the dialog is illustrated here.

   .. image:: ../../../admin-guide/ag-images/InstallODBCMac4.png

5. To launch the installation, provide an Administrator password for your Mac and the dialog is illustrated here.

   .. image:: ../../../admin-guide/ag-images/InstallODBCMac5a.png

   You should see a confirmation that the software was installed successfully as illustrated here.

   .. image:: ../../../admin-guide/ag-images/InstallODBCMacFinal.png

.. note:: In JDBC and ODBC driver configurations, https://api.qubole.com is the default endpoint.

.. _download-install-odbc-driver-Mac-test:

Testing the Installation
------------------------

.. To compile and run the sample program provided in the installation package:

.. .. sourcecode:: bash

.. ..   $ gcc QuboleODBCSample.c -L /usr/local/iODBC/bin/ -liodbc -I /usr/local/iODBC/include/ -o QuboleODBCSample
.. ..   $ ./QuboleODBCSample

.. You should see the tables in your account.

To run a simple test:

1. Run the command:
   ``'/Library/Application Support/iODBC/bin/iodbctest'``

   (Make sure you include the single quotes, or the space between ``Application`` and ``Support`` will cause the command
   to fail.)
2. In the command-line utility, enter ``?`` to see a list of available data sources.
3. At the prompt, enter ``dsn=Qubole ODBC Driver DSN``
4. At the ``SQL>`` prompt, enter the ``SHOW TABLES`` command.

To test the driver with Microsoft Excel:

1. Open a new Excel Worksheet.
2. In **Data** tab, choose **Select New Database Query** from **Database option**.
3. Go to the **System DSN** tab and choose the appropriate driver, then click **OK**.
4. Leave the username and password blank if prompted, and click **OK**.