.. _odbc-driver-mac-index:

===================
ODBC Driver for Mac
===================
This section explains how to download the ODBC driver from Amazon AWS storage and install and test it on a Mac.  Here is
a figure that illustrates the interaction with Qubole through ODBC drivers.

.. image:: ../driver-images/ODBC-Qubole.png

The following sub-sections provide details:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    download-install-odbc-Mac
    install-odbc-driver-mac
    check-driver-version
    config-mac-odbc-driver
    enable-logging
    enabling-proxy
    advanced-config-mac