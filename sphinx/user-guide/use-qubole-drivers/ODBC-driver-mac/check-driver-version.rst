.. _download-install-odbc-driver-Mac-check-version:

Verifying the Driver Version
============================
The :ref:`download-install-odbc-driver-Mac` always lists the most-recently released version of the ODBC driver.

To check the version of a driver already on your Mac, enter the following command:

``otool -L /Library/qubole/odbc/lib/universal/libquboleodbc.dylib``