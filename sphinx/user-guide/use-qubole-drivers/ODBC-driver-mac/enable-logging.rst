.. _download-install-odbc-driver-Mac-enable logging:

Enabling Logging
================
Proceed as follows to enable logging in the driver:

1. Open the driver configuration file (``~/.qubole.odbc.ini``) in a text editor.
2. Change the ``LogLevel`` to the required level. The different levels imply as given below:

   * ``0`` implies ``Off``
   * ``1`` implies ``Fatal``
   * ``2`` implies ``Error``
   * ``3`` implies ``Warning``
   * ``4`` implies ``Info``
   * ``5`` implies ``Debug``
   * ``6`` implies ``Trace``

3. Set the LogPath attribute to the full path of the folder where you want to save log files. This directory must exist
   and it must have write access to you and other users if the application using the driver runs as a specific user.
4. Save the driver configuration file.
5. Restart the application you are using the driver with. Configuration changes will not be picked up by the application
   until it reloads the driver.

.. note:: The name of the Driver log file is ``qubole_driver.log``.