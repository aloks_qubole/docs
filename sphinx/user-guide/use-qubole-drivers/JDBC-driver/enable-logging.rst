.. _debug-jdbc-applications:

================
Enabling Logging
================
Use the logs to debug a JDBC application issue. By default, JDBC logging is disabled. Enable it to get the logs.

For enabling JDBC logging, add the following properties to the JDBC connection string:

* ``LogLevel``: It can have one of these values: 1, 2, 3, 4, 5, 6 where

  - 1 implies ``LOG_OFF`` that disables all logging.
  - 2 implies ``LOG_FATAL`` that logs very severe error events that might lead the driver to abort.
  - 3 implies ``LOG_ERROR`` that logs error events that might still allow the driver to continue running.
  - 4 implies ``LOG_WARNING`` that logs potentially harmful situations.
  - 5 implies ``LOG_INFO`` that logs general information that describes the driver's progress.
  - 6 implies ``LOG_DEBUG`` that logs a detailed information that is useful for debugging the driver.
  - 7 implies ``LOG_TRACE`` that logs a more detailed information than the ``LOG_DEBUG`` level.

* ``LogPath``: It is the log file location to which you must have write access.

  For example, this is a log path: ``jdbc:qubole://hive/default?LogLevel=6;LogPath=C:\Users\User\Desktop``

Logs are printed in a file named ``QuboleJDBC_Driver.log``.

