.. _verify-jdbc-driver-version:

Verifying the JDBC Driver Version
=================================
The :ref:`download-jdbc-driver` always lists the most-recently released version of the JDBC driver.
You can get the version from the driver INFO logs. You can also get the version number by using the
``qds-jdbc-1.1.12.jar`` file.

Verifying the JDBC Driver Version using the qds-jdbc-1.1.12.jar
---------------------------------------------------------------
To verify on Windows:

1. Launch the **Command Prompt**.
2. Go to the folder that contains ``qds-jdbc-1.1.12.jar``.
3. Run the following commands:

   a. ``jar -xvf qds-jdbc-1.1.12.jar META-INF/MANIFEST.MF``
   b. ``more "META-INF/MANIFEST.MF" | findstr "Implementation-Version"``

To verify on Linux and Mac:

1. Launch the **Terminal**.
2. Go to the folder that contains ``qds-jdbc-1.1.12.jar``.
3. Run the following command:

   ``jar -xvf qds-jdbc-1.1.12.jar META-INF/MANIFEST.MF | grep Implementation-Version META-INF/MANIFEST.MF``