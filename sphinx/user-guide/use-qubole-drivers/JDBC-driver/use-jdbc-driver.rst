.. _use-jdbc-driver:

============================================
Connecting to Qubole through the JDBC Driver
============================================
To connect to Qubole through JDBC, perform the following steps:

.. note:: In JDBC and ODBC driver configurations, https://api.qubole.com is the default endpoint.

* Register the driver by specifying the name of the driver URL class. **com.qubole.jdbc.jdbc41.core.QDriver** is
  the driver class name.
* URL is the connection string as per the query. See :ref:`JDBC-connection-string` for more information.
* You can leave the username empty.
* Use the account API token as the password. See :ref:`manage-my-account` on how to get the API token from the
  **Control Panel** UI of the account.

Here is a `Java code sample <https://s3.amazonaws.com/paid-qubole/jdbc/JDBC+Qubole+Example.txt>`__ for connecting to Qubole.