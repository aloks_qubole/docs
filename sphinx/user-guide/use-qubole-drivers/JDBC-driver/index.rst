.. _jdbc-driver-index:

===========
JDBC Driver
===========
Qubole provides its own JDBC driver for Hive, Presto, and Spark. The Qubole JDBC jar can also be added as a Maven
dependency. Here is an example `POM.xml <https://s3.amazonaws.com/paid-qubole/jdbc/Example_pom.xml>`__
with dependency on the JDBC jar. Add the repositories, group, and artifact ID as mentioned in the above POM file.
Change the version as required but Qubole recommends to add the latest version.

Benefits of the Qubole JDBC Driver
----------------------------------
The Qubole driver provides several advantages over the open-source JDBC drivers that are mentioned below:

* The cluster does not have to be running all the time.
* Queries are displayed in the QDS **Analyze** page.
* Users are authenticated through the Qubole account API Token.
* The JDBC supports cluster auto-start.
* There is no need to disable a cluster's auto-termination.
* You do not need to edit CNAME (Canonical name) every time you use Hive.

The topics below describe how to install and configure the JDBC driver before using it:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    download-jdbc-driver
    check-driver-version
    additional-properties
    use-jdbc-driver
    enable-logging
    enabling-proxy
