.. _enabling-proxy-jdbc:

=============================
Enabling the Proxy Connection
=============================
You can enable the proxy connection if you want to use a proxy server for connections. To enable it, add the following
properties to the connection string.

.. sourcecode:: bash

    proxy=true
    proxyhost=<proxy server ip>
    proxyport=<proxy server port value> (has to be a integer value)
    proxyusername= <username for proxy authentication> (optional)
    proxypassword= <password for proxy authentication> (optional)
    proxytype=<http or socks> (optional - default value is http)

An example proxy connection would be:

``jdbc:qubole://hive/hadoop2?proxy=true;proxyhost=10.0.0.1;proxyport=8080;proxyusername=qubole;proxypassword=qubole"``