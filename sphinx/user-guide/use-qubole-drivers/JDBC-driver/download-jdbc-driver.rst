.. _download-jdbc-driver:

===========================
Downloading the JDBC Driver
===========================
Proceed as follows to download the latest Qubole JDBC driver JAR:

.. note:: The Qubole driver is stored in publicly accessible Cloud storage, and works on all Cloud platforms supported
          in QDS.

1. Download the Qubole JDBC driver: https://s3.amazonaws.com/paid-qubole/jdbc/qds-jdbc-1.1.12.jar.

   This is the JAR for JDBC version 4.1, which QDS currently uses.

2. Read the :ref:`JDBC Driver Release Notes <jdbc-relnotes-v1.1.12-index>` for this version of the driver.

For instructions on using the driver to connect to a QDS cluster, see :ref:`use-jdbc-driver`.