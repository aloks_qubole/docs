.. _use-qubole-drivers:

############################
Qubole JDBC and ODBC Drivers
############################
Qubole provides custom JDBC and ODBC drivers. Qubole recommends you to use these drivers, rather than the open-source
versions, on QDS.

The following topics provide download and installation information:

.. toctree::
    :maxdepth: 2
    :titlesonly:

    ODBC-driver-windows/index.rst
    ODBC-driver-mac/index.rst
    ODBC-driver-linux/index.rst
    JDBC-driver/index.rst




