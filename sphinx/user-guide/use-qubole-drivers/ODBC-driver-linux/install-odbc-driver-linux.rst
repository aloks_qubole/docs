.. _install-odbc-driver-linux:

===================================
Installing the ODBC Driver on Linux
===================================
A driver manager is required for managing the communication between Qubole and the ODBC driver.

Prerequisites
-------------
Qubole ODBC driver is supported on the following Linux distributions:

* CentOS
* Red Hat Linux Enterprise 7.x

You can use iODBC or unixODBC as the driver manager as they are both supported by the ODBC driver.

iODBC as the Driver Manager
...........................
If iODBC is not installed on the centOS/RHEL, run the following command as a sudo user.

``# yum install libiodbc``

It provides the ``iodbctest`` command utility to verify the connection.

unixODBC as the Driver Manager
..............................
unixODBC offers command line utilities (``isql`` and ``odbcinst``) to install, configure, and test the driver. To verify
if this package is installed along with distribution, run the following commands:

1. ``$ which odbcinst``
2. ``$ which isql``

If unixODBC is not installed, run the following command to install it.

``$ sudo yum install unixODBC.x86_64``

Verify the paths of ODBC configuration files (``odbcinst.ini`` and ``odbc.ini``) using ``odbcinst`` command as mentioned
here:

1. ``$ odbcinst -j``
2. ``$ yum install libiodbc``

Installing the ODBC Driver
--------------------------
Perform these steps to install the ODBC driver on Linux:

1. This is an optional step if you want to verify the integrity of the downloaded package.

   Run ``$ rpm -checksig qds-odbc-1.1.11-1.x86_64.rpm``. The expected command output is:

   ``qds-odbc-1.1.11-1.x86_64.rpm: sha1 md5 OK``

   Additionally, the package is signed by SHA-256. Download the key and signature from these locations:

   * `Key location <https://s3.amazonaws.com/paid-qubole/odbc/1.1.11/odbc-linux-verification/pubkey.pem>`__
   * `Signature location <https://s3.amazonaws.com/paid-qubole/odbc/1.1.11/odbc-linux-verification/qds-odbc-1.1.11-1.x86_64.sig>`__

   Add the key and signature to verify the package as shown below:

   ``$ openssl dgst -sha256 -verify pubkey.pem -signature qds-odbc-1.1.11-1.x86_64.sig qds-odbc-1.1.11-1.x86_64.rpm``

   The expected response is ``Verified OK``.

2. Install Qubole  driver package as a sudo user (super user) by running the command below.

   ``$ sudo yum install qds-odbc-1.1.10-1.el7.centos.x86_64.rpm``

3. After installing the driver. the LD_PRELOAD environment variable is set with the required library. Check the ``ENVIRONMENT``
   variable (``LD_PRELOAD``) by logging out of the current shell terminal and logging back through a new shell terminal.
   Here is a sample illustration of the command.

   .. sourcecode:: bash

       [ec2-user@aws-instance ~]$ echo $LD_PRELOAD
       :/lib64/libiodbcinst.so.2:::

4. Check the Qubole driver's entry in ``/etc/odbcinst.ini``. This file has an entry for drivers installed on the system.

   ``$ cat /etc/odbcinst.ini``

   The expected output is as below.

   .. sourcecode:: bash

       [ODBC Drivers]
       QuboleODBC=Installed
       [QuboleODBC]
       Description=Qubole ODBC Driver DSN
       Driver=/usr/local/qubole/libquboleodbc.so

5. After the package is installed, the driver gets added as a default system DSN entry into ``/etc/odbc.ini``.

.. _test-Linux-driver:

Testing the Installation
------------------------
You can verify the Linux driver installation by searching the Qubole driver entry in the ``/etc/odbcinst.ini``. Run
the command below to search the Linux driver.

``$ cat /etc/odbcinst.ini``