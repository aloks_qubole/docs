.. _download-odbc-driver-linux:

====================================
Downloading the ODBC Driver on Linux
====================================
Proceed as follows to prepare your system and download the driver:

.. note:: The Qubole driver is stored in the publicly accessible Cloud storage, and works on all Cloud platforms supported
          in QDS.

1. Download the Qubole ODBC driver: https://s3.amazonaws.com/paid-qubole/odbc/1.1.11/qds-odbc-1.1.11-1.x86_64.rpm.
2. Read the Release Notes: :ref:`release-notes-odbcv1.1.11-index`.

:ref:`install-odbc-driver-linux` describes the steps to install the ODBC driver.