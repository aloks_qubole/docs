.. _config-odbc-on-linux:

===========================
Configuring the ODBC Driver
===========================
A Data Source Name (DSN) is a logical name that identifies a database and the set of connection attributes used to
connect to the database. Configure the DSN in the ODBC driver.

Prerequisites
-------------
These are the prerequisites:

* Ensure that you have the access to the object/resource that you refer to in the driver configuration. If you see
  an access denied error, check with the Qubole account administrator. For more information, see :ref:`manage-roles-user-resources-actions`.

Types of DSN
------------
There are two types of DSNs that you can configure:

* System DSN
* User DSN

The ``/etc/odbcinst.ini`` file contains information about ODBC drivers available to users and the
``/etc/odbc.ini`` file contains information about DSNs available to all users. These System DSNs are useful for
applications such as web servers that may not be running as a real user and so there is no home directory to contain
an ``.odbc.ini`` file. It is a good practice to have DSN defined for each user as it does not affect the system configuration.

Configuring a User-level DSN
----------------------------
To configure a user DSN, set the ODBC environment variables in the user profile. Add an entry
into ``.bash_profile`` or ``.profile`` to initialize the user environment during a bash login.

If you are using ``iODBC- Insert ODBCINI and ODBCINSTINI``, configure the corresponding files as illustrated here.

.. sourcecode:: bash

       [ec2-user@aws-instance ~]$ cat .bash_profile
       # .bash_profile

       # Get the aliases and functions
       if [ -f ~/.bashrc ]; then
       . ~/.bashrc
       fi

       # User specific environment and startup programs

       PATH=$PATH:$HOME/.local/bin:$HOME/bin
       ODBCINI=$HOME/.odbc.ini
       ODBCINSTINI=$HOME/.odbcinst.ini
       export PATH
       export ODBCINI
       export ODBCINSTINI

If you are using ``unixODBC- Insert ODBCSYSINI and ODBCINSTINI``, configure the corresponding files as illustrated here.

   .. sourcecode:: bash

       [ec2-user@aws-instance ~]$ cat .bash_profile
       # .bash_profile

       # Get the aliases and functions
       if [ -f ~/.bashrc ]; then
        . ~/.bashrc
       fi

       # User specific environment and startup programs

       PATH=$PATH:$HOME/.local/bin:$HOME/bin
       ODBCSYSINI=$HOME/.odbc.ini
       ODBCINSTINI=$HOME/.odbcinst.ini
       export PATH
       export ODBCSYSINI
       export ODBCINSTINI

Configuring the System DSN
--------------------------
The Qubole ODBC driver comes with a default DSN that you can see in the the ``odbc.ini`` file. After you install the
driver, you can trace the ``.odbc.ini`` file in the ``/usr/local/qubole/`` location. You can add another DSN or modify the
existing DSN property values. For the Qubole ODBC driver, these are the defaults in the ``.odbc.ini`` file.

.. sourcecode:: bash

       [ODBC]
       # Specify any global ODBC configuration here such as ODBC tracing.

       [ODBC Data Sources]
       Qubole ODBC Driver DSN=QuboleODBC

       [Qubole ODBC Driver DSN]
       # This key is not necessary and it is to just describe the data source.
       Description=Qubole ODBC Driver DSN

       # Driver: The location where the ODBC driver is installed to.
       Driver=/usr/local/qubole/libquboleodbc.so
       # APITOKEN: The API Token for the account as provided by Qubole.
       APITOKEN=""
       # CLUSTER_LABEL: The label of the cluster on which queries are to be run.
       CLUSTER_LABEL=default
       # ENDPOINT: The Qubole endpoint for the account as provided by Qubole.
       ENDPOINT=https://api.qubole.com
       #DSI: The data source name : Hive, Spark, Presto, or SQLCommand
       DSI=hive
       APPID=""

In the ``.odbc.ini`` file, you can edit any DSN property's value. You can change these DSN properties as per the requirements:

* ``APITOKEN``: Enter The API token of the QDS account.

  .. note:: To find the API token, navigate to **Control Panel** on the Qubole UI and click the **My Accounts** tab. Click
            **Show** for the account and copy the API token that is displayed. For more information, see :ref:`manage-my-account`.

* ``CLUSTER_LABEL``: The cluster label is default, which implies that it is default cluster of the account. You can change
  it by adding the cluster’s label that you want to use.
* ``ENDPOINT``: It is the QDS environment. By default, it is https://api.qubole.com. To change it to a different QDS
  environment on which you have the QDS account, add that QDS environment as the endpoint. For more information on the
  endpoints, see Supported Qubole Endpoints on Different Cloud Providers.
* ``DSI``: It is the Data Source Interface. By default, it is Hive. You can change it to Presto or Spark as required.
* ``APPID``: When you set the Data Source Name as Spark, enter the APP ID. It is not applicable to the Presto or Hive data source.

Configuring a Multi-user DSN
----------------------------
Configure a multi-user DSN by editing a ``.odbc.ini`` file in the home directory of the current user (``$HOME``). Create a
DSN into the section ``ODBC Data Sources`` as mentioned below:

.. sourcecode:: bash

       [ODBC Data Sources]
       Qubole ODBC Driver DSN=QuboleODBC
       Test=QuboleODBC

       [Test]
       Description=Qubole ODBC Driver DSN
       # Driver: The location where the ODBC driver is installed.
       Driver=/usr/local/qubole/libquboleodbc.so
       # APITOKEN: The API Token for the account as provided by Qubole.
       APITOKEN="<API Token>"
       # CLUSTER_LABEL: The label of the cluster on which queries are to be run.
       CLUSTER_LABEL=presto
       # ENDPOINT: The Qubole endpoint for the account as provided by Qubole.
       ENDPOINT=https://us.qubole.com
       #DSI: The data source name: Hive, Spark, Presto, or SQLCommand
       DSI=hive
       APPID=""