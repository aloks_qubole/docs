.. _enabling-proxy-linux:

=============================
Enabling the Proxy Connection
=============================
If you are connecting to the data source through a proxy server, you must provide connection details of the proxy server
in addition to the ``END_POINT``, ``CLUSTER_LABEL``, ``API_TOKEN``, and ``DSI``. The keywords associated with the proxy
connection are ``PROXY_HOST``, ``PROXY_PORT``, ``PROXY_USERNAME``, ``PROXY_PASSWORD``, ``PROXY_ENABLED``, and ``PROXY_TYPE``.

To configure a proxy server, append the DSN section in the ``/etc/odbc.ini`` configuration file with connection parameters
as described in these steps:

1. Create a keyword entry, **PROXY_ENABLED**. Enter ``PROXY_ENABLED = true``.
2. Add the **PROXY_HOST** keyword, add the hostname or IP address of the proxy server as the keyword value. For example,
   ``PROXY_HOST = 192.168.56.1``. **It is a mandatory property**.
3. Add the **PROXY_PORT** keyword, add the number of the TCP port that the proxy server uses to listen to client connections.
   For example, ``PROXY_PORT = 8081``. **It is a mandatory property**.
4. Add the **PROXY_USERNAME** keyword, add the username to access the proxy server as the keyword value.
5. add the **PROXY_PASSWORD** keyword, add the password for the username as the keyword value.
6. Add the ``PROXY_TYPE`` keyword and add a different value if you do not want the default ``HTTP`` value. These are
   the other supported proxy types:

   * ``HTTP_1_0``
   * ``SOCKS4``
   * ``SOCKS4A``
   * ``SOCKS5``
   * ``SOCKS5_HOSTNAME``

Here is a sample illustration of the DSN Setup with the proxy connection enabled.

.. sourcecode:: bash

    $ cat /etc/odbc.ini
    [ODBC]
    # Specify any global ODBC configuration here such as ODBC tracing.
    [ODBC Data Sources]
    Qubole ODBC Driver DSN=QuboleODBC

    [Qubole ODBC Driver DSN]
    # This key is not necessary and it just describes the data source.
    Description=Qubole ODBC Driver DSN
    # Driver: The location where the ODBC driver is installed.
    Driver=/usr/local/qubole/libquboleodbc.so
    # APITOKEN: The API Token for the account as provided by Qubole.
    APITOKEN=<APITOKEN>
    # CLUSTER_LABEL: The label of the cluster on which queries are to be run
    CLUSTER_LABEL=default
    # ENDPOINT: The endpoint for the account as provided by Qubole.
    ENDPOINT=https://api.qubole.com
    #DSI: The data source name : Hive, Spark, Presto, or SQLCommand
    DSI=hive
    APPID=""

    PROXY_ENABLED=true
    PROXY_HOST=192.168.56.1
    PROXY_PORT=8081
    PROXY_USERNAME=username
    PROXY_PASSWORD=passwd
    PROXY_TYPE=HTTP
