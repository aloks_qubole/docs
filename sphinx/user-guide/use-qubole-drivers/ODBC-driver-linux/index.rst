.. _odbc-driver-linux-index:

=====================
ODBC Driver for Linux
=====================
Qubole ODBC Driver in Linux enables Business Intelligence (BI) tool servers to access the Qubole Data services. Here is
a figure that illustrates the interaction with Qubole through ODBC drivers.

.. image:: ../driver-images/ODBC-Qubole.png

The following sub-sections provide details:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    download-install-odbc-linux
    install-odbc-driver-linux
    check-linux-driver-version
    config-linux-odbc-driver
    enabling-proxy
    advanced-config-linux
    test-connection-linux