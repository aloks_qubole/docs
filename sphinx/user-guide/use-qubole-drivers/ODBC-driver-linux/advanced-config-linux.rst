.. _odbc-advanced-config-linux:

================================
Setting Additional Configuration
================================
The advanced driver configuration properties are described in the following section:

* ``MAX_CHAR_SIZE``: It specifies the maximum size of columns of ``SQL_CHAR`` that the driver describes through result
  set descriptions and catalog functions. Add the ``MAX_CHAR_SIZE`` property and add a value if you do not want to use
  the default value, which is ``100000``. However, some tools require the maximum value to be set below a particular limit.

  Example: For Microsoft SQL Server, you must set this value  equal to or less than 8000.

* ``SCHEMA_NAME``: Add this property and enter the schema name as its value, to filter to that schema while fetching metadata
  so that only the related ``TABL_SCHEM`` configured is exposed.

  Here is an example of the ODBC driver's advanced configuration.

  .. sourcecode:: bash

       [ec2-user@aws-instance ~]$ cat /etc/odbc.ini
       [ODBC]
       # Specify any global ODBC configuration here such as ODBC tracing.

       [ODBC Data Sources]
       Qubole ODBC Driver DSN=QuboleODBC

       [Qubole ODBC Driver DSN]
       # This key is not necessary and is only to give a description of the data source.
       Description=Qubole ODBC Driver DSN
       # Driver: The location where the ODBC driver is installed to.
       Driver=/usr/local/qubole/libquboleodbc.so
       # APITOKEN: The API Token for the Qubole account as provided by Qubole.
       APITOKEN=<API Token>
       # CLUSTER_LABEL: The label of the cluster on which queries are to be run.
       CLUSTER_LABEL=presto
       # ENDPOINT: The Qubole endpoint for the account as provided by Qubole.
       ENDPOINT=https://api.qubole.com
       #DSI: The data source name: Hive, Spark, Presto, or SQLCommand
       DSI=Presto
       # Maximum SQL_CHAR
       MAX_SQL_CHAR=10000
       # Schema name
       SCHEMA_NAME=default