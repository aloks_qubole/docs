.. _odbc-driver-linux-test:

Testing the ODBC Connection
===========================
You can test the connection using **iODBC** or **unixODBC**. For more information, see:

* `Testing the Connection using iODBC`_
* `Testing the Connection using unixODBC`_

Testing the Connection using iODBC
----------------------------------
Verify the connection with Qubole using the ``iodbctest`` tool. The tool lists all DSNs if multiple DSNs configured.
Check the driver version and enter the query to be processed on Qubole UI.

1. Run the ``iodbctest`` command.
2. In the command-line utility, type ``?`` to see a list of available data sources.
3. At the prompt, type a DSN from the list . For example, ``dsn=Qubole ODBC Driver DSN`` or ``dsn=Test`` (verify the
   version of driver after this step.) :ref:`odbc-driver-linux-check-version` describes how to verify the driver version.
4. At the SQL> prompt, enter the ``SHOW TABLES`` command.

Here is a sample illustration.

.. sourcecode:: bash

   [ec2-user@aws-instance ~]$ iodbctest
   iODBC Demonstration program
   This program shows an interactive SQL processor
   Driver Manager: 03.52.0709.0909

   Enter ODBC connect string (? shows list): ?

   DSN                              | Driver
   ------------------------------------------------------------
   Qubole ODBC Driver DSN | QuboleODBC
   Test                   | QuboleODBC
   Enter ODBC connect string (? shows list): dsn=Test
   Driver: 10.01.1031 (QuboleODBC)
   SQL>[Show tables;]

Testing the Connection using unixODBC
-------------------------------------
Verify the connection with Qubole using the ``isql`` utility as mentioned in this command:

``isql -v <DSN Name>``

Here is a sample illustration.

.. sourcecode:: bash

   [ec2-user@aws-instance ~]$ isql -v "Test"
   +----------------------------------------+
   | Connected!               	        	|
   | sql-statement                      	|
   | help [tablename]                    	|
   | quit                                  	|
   +----------------------------------------+
   sql>[show tables;]

To verify the triggered query, navigate to the **Analyze** page on the Qubole UI.