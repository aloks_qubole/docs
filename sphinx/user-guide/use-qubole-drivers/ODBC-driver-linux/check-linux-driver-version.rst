.. _odbc-driver-linux-check-version:

Verifying the Driver Version
============================
The :ref:`download-odbc-driver-linux` always lists the most-recently released version of the ODBC driver.

Run ``rpm -qi qds-odbc`` to check the driver details. The command returns the version as illustrated below.

.. sourcecode:: bash

    $ rpm -qi qds-odbc
    Name        : qds-odbc
    Version     : 1.1.11
    Release     : 1
    Architecture: x86_64
    Install Date: Thu 14 Mar 2019 05:07:57 AM UTC
    Group       : Converted/System Env
    Size        : 145213569
    License     : Copyright (c) Qubole Inc.
    Signature   : (none)
    Source RPM  : qds-odbc-1.1.11-1.src.rpm
    Build Date  : Wed 27 Feb 2019 02:03:03 AM UTC
    Build Host  : localhost
    Relocations : /usr
    Packager    : Qubole
    URL         : www.qubole.com
    Summary     : Qubole ODBC Driver
    Description : An ODBC driver to provide access to Qubole Data Services (Presto/ Spark/ Hive).
