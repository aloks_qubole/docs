.. _install-odbc-driver:

=====================================
Installing the ODBC Driver on Windows
=====================================
The Qubole ODBC driver provides access to data in QDS from Business Intelligence applications such as
Microsoft Excel and Tableau.

Qubole currently provides the Microsoft |copy| Windows ODBC driver.

.. |copy|   unicode:: U+000A9 .. COPYRIGHT SIGN

.. note:: The Qubole ODBC driver does not support the Windows 32-bit edition.

The following sections provide more information:

* :ref:`install-odbc-driver-prereq`
* :ref:`install-odbc-driver-install`
* :ref:`install-odbc-driver-switch`

.. _install-odbc-driver-prereq:

Prerequisites
-------------
The Qubole ODBC Driver only supports the 64-bit edition of the following Microsoft Windows versions:

* Microsoft Windows 7
* Microsoft Windows 8

:ref:`Download <download-odbc-driver>` the latest ODBC driver package before proceeding.

.. _install-odbc-driver-install:

Installing the ODBC Driver
--------------------------
Let us try to install the ODBC driver on a 64-bit (x64) edition of Microsoft Windows.

.. note:: The Qubole ODBC driver does not support the Windows 32-bit edition.

Perform the following steps to install the ODBC driver on Microsoft Windows:

1. Click QuboleUL_x64.msi for 64-bit to run it.
2. A welcome screen is displayed as shown in the following figure.

   .. image:: ../../../admin-guide/ag-images/ODBCWelcome.png

   Click **Next** to continue. Click **Cancel** if you want to cancel the installation.
3. Clicking **Next** displays the end-user licence agreement as shown in the following figure.

   .. image:: ../../../admin-guide/ag-images/ODBCTerms.png

   Read the terms and select **I agree to the terms in the Licence Agreement**. Click **Next** to continue only after
   accepting the terms. Click **Back** to go to the previous dialog and click **Cancel** if you want to cancel the
   installation.
4. Clicking **Next** displays the dialog to set the destination folder as shown in the following figure.

   .. image:: ../../../admin-guide/ag-images/ODBCDestinationFolder1.png

   A default location is as shown in the above figure. If you want a different location, click **Change** and browse to
   select a required location. Click **Next** to continue. Click **Back** to go to the previous dialog and click **Cancel**
   if you want to cancel the installation.
5. Clicking **Next** displays the **Ready to Install** dialog as shown in the following figure.

   .. image:: ../../../admin-guide/ag-images/ODBCReadytoInstall.png

   Click **Install** to begin the installation. Click **Back** to go to the previous dialog and click **Cancel** if you
   want to cancel the installation.
6. The installation process begins and the progress is as shown in the following figure.

   .. image:: ../../../admin-guide/ag-images/InstallODBC.png

   After the installation is complete, the message is as shown in the following figure.

   .. image:: ../../../admin-guide/ag-images/ODBCInstallComplete.png

   Click **Finish** to complete the installation process.

.. _install-odbc-driver-switch:

Configuring the ODBC API Token for QDS Account
----------------------------------------------
You must add your API token in the Windows ODBC administrator. Because each account has a different API token, you must
change the ODBC API token in the Windows ODBC administrator if you switch to a different QDS account. Change the **Data**
**Source Type** in the Windows ODBC administrator according to your requirements. Do this from the Windows Control Panel:

1. Navigate to the ODBC administrator from the Windows Control Panel (the path varies depending on your Windows
   version, as described :ref:`here <download-odbc-driver-check-version>`).
2. Enter the token for the current QDS account, as shown in the QDS UI (**Control Panel** > **My Accounts**).
3. Choose the required Data Source Type.