.. _download-odbc-driver:

======================================
Downloading the ODBC Driver to Windows
======================================
Proceed as follows to download the latest Qubole ODBC driver package to a Microsoft Windows server:

.. note:: The Qubole driver is stored in publicly accessible Cloud storage, and works on all Cloud platforms supported
          in QDS.

1. Download the Qubole ODBC driver from: https://s3.amazonaws.com/paid-qubole/odbc/1.1.11/qds-odbc-1.1.11.msi (binary for x64 systems).
2. :ref:`Install the driver <install-odbc-driver>` and configure the BI tool (if required) on the Windows server. For more
   information on the configuration of the BI tools, see :ref:`Partner Integration Guides <partner-integration-guides-index>`.
3. Read the Release Notes: :ref:`release-notes-odbcv1.1.11-index`.