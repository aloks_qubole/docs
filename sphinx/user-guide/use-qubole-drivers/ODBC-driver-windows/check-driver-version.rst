.. _download-odbc-driver-check-version:

Verifying the Driver Version
============================
The :ref:`download-odbc-driver` always lists the most-recently released version of the ODBC driver.

You can see the version number by looking at the file properties of the driver binary.

If you want to check whether your system is already running the latest version, download
the latest driver and compare that version with the version you have. Use the ODBC Data Source Administrator
to check the version of the driver you are currently using:

   * On Windows 8 and above, open the Control Panel and go to **Search ODBC**, and then choose **Set ODBC Data Source (64bits)**.

     .. note:: The Qubole ODBC driver does not support the Windows 32-bit edition.

   * On Windows 7/Vista, open the Control Panel and go to **Administrative Tools > Data Source (ODBC)**
     Then go to the **System DSN** tab, choose the DSN and click **configure**.
     The version number appears at the bottom left of the window that pops up.

:ref:`Install <install-odbc-driver>` the new version if necessary.


