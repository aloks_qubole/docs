.. _odbc-driver-windows-index:

=======================
ODBC Driver for Windows
=======================
Qubole provides an ODBC driver for Hive, Presto, and Spark.

For download instructions, installation steps and configuration details, see:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    download-odbc-driver
    install-odbc-driver
    check-driver-version
    use-odbc-driver
    enable-logging
    enabling-proxy
    advanced-config.rst