.. _enable-logging-windows:

Enabling Logging
================
For debugging ODBC applications, configure the **Logging Options** as described here:

1. On **Qubole ODBC Driver DSN Setup** , click **Logging Options**. The following dialog box is displayed.

   .. image:: ../../../admin-guide/ag-images/QuboleODBCLoggingOptions.png

2. Choose the **Log Level**, **Log Path** (the log file location, to which you or a user must have write access), and
   **Log Rotation** as required. The name of the log file is ``qubole_driver.log``.

