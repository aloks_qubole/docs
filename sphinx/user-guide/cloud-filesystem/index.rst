.. _cloud-filesystem:

==================
Cloud File Systems
==================
Qubole supports the following file system client connectors to access the corresponding cloud storage:

* Amazon native-S3 and S3a file systems
* Native Azure file system
* Oracle OCI file system

The file systems are explained in these topics:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    aws-filesystem
    defloc-folder-qdsaccess.rst
    azure-filesystem
    oracle-filesystem