.. _use-oracle-filesystem:

Using the Oracle OCI Object Store
=================================

Qubole supports the Oracle OCI `object store <https://docs.cloud.oracle.com/iaas/Content/Object/Concepts/objectstorageoverview.htm>`__. The URI scheme is ``oci://<bucket>@<namespace>/<path>``.
