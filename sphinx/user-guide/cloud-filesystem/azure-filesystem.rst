.. _use-azure-filesystem:

Using the Azure File System
===========================
Qubole supports the Native Azure file system for Azure Cloud storage. To see which cluster types are currently supported on Azure,
see :ref:`os-version-support`. The URI scheme is ``wasb://``.

`Azure file system <https://hadoop.apache.org/docs/stable2/hadoop-azure/index.html>`__ provides more details.