.. _schedule-ui:

==================================
Using the Scheduler User Interface
==================================

The Qubole Scheduler provides a way to run commands at specific intervals without manual intervention. Navigate to the
**Scheduler** tab to see and modify scheduled jobs and create new ones. See :ref:`scheduler-qubole-index` for more
information.

.. _scheduler-keyboard-shortcuts:

Using Keyboard Shortcuts
------------------------

Press **Shift** + **/** (implies **?**) anywhere on the **Scheduler** page to see the list of keyboard shortcuts.

You can disable/enable the keyboard shortcuts in **Control Panel** > **My Profile**. By default the shortcuts are enabled.
For more information, see :ref:`manage-profile`.


