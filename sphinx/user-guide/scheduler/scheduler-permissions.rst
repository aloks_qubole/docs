.. _scheduler-permissions:

=============================================
Managing Scheduler Permissions through the UI
=============================================
QDS supports setting permissions for a specific scheduler in the **Scheduler** UI page in addition to the Object Policy
REST API. For more information on the API, see :ref:`set-object-policy-scheduler`. You can allow/deny the schedule's access
to a user/group.

To allow/deny permissions to a specific schedule through the UI, perform these steps:

1. Navigate to the **Scheduler** page and click the gear icon in the schedule to which you want to restrict permissions.

   .. image:: sc-v2-images/SchedulerOperations.png

   You must be a system-admin, owner of the schedule, or a user who has the **Manage** permission to set permissions and
   even see **Manage Permissions**.

2. Click **Manage Permissions**. The **Manage Permissions** dialog is displayed as shown here.

   .. image:: sc-v2-images/SchedulerPermissions.png

3. Select a user/group that you want to allow or deny access to. Some permissions are set by default and the current
   permissions are displayed for the selected user/group. To allow select the checkbox below the schedule policy action,
   and to deny, uncheck the checkbox below the scheule policy action or do not select the checkbox. The different
   scheduler permissions are:

   * **Read** - This permission allows/denies a user to view the specific schedule. The UI will not display a schedule for
     which a user does not have read permission. This implies that all other actions even if granted are ineffective on the UI.
   * **Update** - This permission allows/denies a user to edit the schedule configuration.
   * **Delete** - This permission allows/denies a user to delete the schedule.
   * **Clone** - This permission allows/denies a user to clone the schedule.
   * **Manage** - This permission allows/denies a user to manage this schedule's permissions.

   .. note:: If you a allow a user with a permission who is part of the group that has restricted access, then that user
             is allowed access and vice versa. For more information, see `Understanding the Precedence of Scheduler Permissions`_.

4. Click **Add New Permission** to assign permissions to another user/group. Allow/deny schedule permissions as described
   in the step 3. Specific schedule permissions for a user and a group are illustrated in this sample.

   .. image:: sc-v2-images/SchedulerPermissions1.png

   If you want a user/group to not view the schedule, then you can deny the read access group as illustrated above.

5. Click **Save** after assigning scheduler permissions to the user(s)/group(s).

.. _scheduler-permission-precedence:

Understanding the Precedence of Scheduler Permissions
-----------------------------------------------------
The precedence of scheduler permissions are mentioned below:

* The schedule owner and system-admin have all permissions that cannot be revoked.
* Users take precedence over groups.
* A user who is not assigned with any specific permissions inherits them from the group that he is part of.
* If the schedule ACL permissions are defined by a user, who is the current owner, then that user has all access by
  default. Even if there is a access control set for deny. Basically QDS honors ownership over object ACLs.
* If the schedule ACL permissions are not defined by a user, who is the current owner, QDS allows that user to do schedule
  operations if there is no explicit deny permission set for that user. But if a READ permission is denied to the user,
  then the user cannot see that specific schedule in the **Schedule** list.

   .. image:: sc-v2-images/SchedulerPermissions1.png