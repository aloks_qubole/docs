.. _scheduler-qubole-index:

################
Qubole Scheduler
################

The QDS scheduler allows you to configure jobs and specify the intervals at which they run, and provides additional
capabilities that make it a powerful tool for automating your workflow. Job characteristics you can
:ref:`specify <setting-schedule-parameters>` include:

* The job type (Hive Query, Hadoop Job, etc.) and query parameters
* The job start and end date and time, and its time zone and frequency
* :ref:`Javascript macros<macros_in_scheduler>` to be included
* The Hadoop Fair Scheduler `pool <https://hadoop.apache.org/docs/r1.2.1/fair_scheduler.html>`__
  (Hadoop 1) or `queue <https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/FairScheduler.html>`__
  (Hadoop 2) to be used
* The number of concurrent jobs to allow
* :ref:`Hive <wait-hive-table-dependency>` and :ref:`AWS S3<wait-s3-dependency>` dependencies, to ensure that the job runs
  only when the data it needs is available

For more information, see the following topics:

.. toctree::
   :maxdepth: 1
   :titlesonly:

   schedule-ui.rst
   scheduler-concepts.rst
   viewing-schedule.rst
   creating-new-schedule.rst
   wait-s3-dependency.rst
   wait-hive-table-dependency.rst
   scheduler-permissions.rst
   editing-schedule.rst
   hive-datasets-as-schedule-dependency
   macros-in-scheduler

