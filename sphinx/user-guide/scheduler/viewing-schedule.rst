.. _viewing-schedule:

==================
Viewing a Schedule
==================
In the `Schedule <https://api.qubole.com/v2/Scheduler>`__ tab, select the listed schedule that you want to see. Alternatively,
if you know the ID/name of the schedule that you want to see, you can use Filter. See :ref:`filter-job` more information.

.. note:: Press **Ctrl** + **/** to see the list of available keyboard shortcuts. See :ref:`scheduler-keyboard-shortcuts`
          for more information.

          There is a rerun limit for schedule reruns to be processed concurrently at a given point of time. :ref:`qds-scheduler-concepts` provides more information.

After you select a schedule from the list, the schedule details are visible in the **Schedule** tab as shown in the following figure.

.. image:: sc-v2-images/Scheduler1.png

Click the **permalink icon** |PermalinkIcon| to see the schedule's permalink.

.. |PermalinkIcon| image:: sc-images/PermalinkIcon1.png

In the schedule details, **Scheduled schedule summary** and **Runs** showing instances' details.

On the top of the schedule summary, you can:

* Click the **Clone** button for cloning a schedule.
* Click the **Edit** button for editing a schedule.
* Click the **Stop** button for killing/stopping a schedule.
* Click the **Suspend** button for suspending a schedule.

.. note:: After you stop a schedule, you cannot resume it. However, you can suspend a schedule and resume it later.

If you click the **Suspend** button, a dialog to suspend with **OK** and **Cancel** buttons is displayed.

Click **OK** to suspend a schedule. In the schedule details page, the suspended state is shown and the suspend icon replaced with
a **Resume** button.

.. note:: The default filter shows only active schedules. When a schedule is suspended, it disappears from the list of active
          schedules. To see the list of suspended schedules, select the status as suspended. See :ref:`filter-job` for more
          information.

You can resume a suspended schedule any time by clicking **Resume**.

A dialog to resume with **OK** and **Cancel** buttons is displayed as shown below.

.. image:: sc-v2-images/ResumeJob.png

Select **Skip Missed Instances** if you want to skip instances that were supposed to have run in the past. This setting
would be disabled/enabled and it is as set when the schedule was created previously. Based on the need for skipping
instances, you can select it if it remains unselected.

Click **OK** to resume a schedule.

.. note:: Use the tooltip |Help_Tooltip| to know more information on each field.

.. |Help_Tooltip| image:: ../ug-images/Help_Tooltip.png

Viewing Schedule Actions
------------------------
The **Runs** tab displays the schedule actions of the schedule as illustrated in the following figure.

.. image:: sc-v2-images/ViewInstances1.png

Click **Rerun** to run the instance again. Click **Show More** to see the earlier schedule actions.

.. note:: A ``_SUCCESS`` file is created in the output folder for successful schedule. You can set
          ``mapreduce.fileoutputcommitter.marksuccessfuljobs`` to ``false`` to disable creation of _SUCCESS file
          or to ``true`` to enable creation of the ``_SUCCESS`` file.

.. _filter-job:

Filtering a Schedule
--------------------
In the `Schedule <https://api.qubole.com/v2/Scheduler>`__ tab, click the filter icon |FilterIcon| to limit the search
result.

.. |FilterIcon| image:: sc-v2-images/FilterIcon.png

The **Filter** dialog is displayed. Enter the schedule name (or ID) and select the status, user (active and disabled users),
and command-type from the corresponding drop-down lists as illustrated in the following figure.

.. image:: sc-v2-images/SchedulerFilter1.png