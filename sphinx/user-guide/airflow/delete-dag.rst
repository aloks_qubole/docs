.. _delete-airflow-dag:

====================================
Deleting a DAG on an Airflow Cluster
====================================

You can delete a DAG on an Airflow Cluster from the Airflow Web Server.

Before you delete a DAG, you must ensure that the DAG must be either in the **Off** state or does not have any active DAG runs. If the DAG has any active runs pending, then you should mark all tasks under those DAG runs as completed.

1. From the **Clusters** page, click on the **Resources** drop-down list against the airflow cluster, and select **Airflow Web Server**.
   The **Airflow Web Server** is displayed as shown in the illustration.

.. image:: airflow-images/delete-dag.png

2. Click **DAGs** tab to view the list of DAGs.
3. Click on the delete button under the **Links** column against the required DAG.
4. Click **OK** to confirm.

By default, it takes 5 minutes for the deleted DAG to disappear from the UI. You can modify this time limit by configuring the ``scheduler.dag_dir_list_interval`` setting in the ``airflow.cfg`` file.
This time limit does not work on sub-DAG operators.

.. note:: It is recommended not to decrease the time limit value substantially because it might lead to high CPU usage.



