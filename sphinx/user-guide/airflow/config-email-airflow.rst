.. _congifure-email-alerts-airflow:

=====================================
Enabling notifications for Airflow
=====================================

You can receive alerts through email about Airflow processes by enabling the notifications in the Airflow configuration.

1. Navigate to the **Clusters** page.
2. Click **Edit** against the required Airflow cluster to edit the configuration.
3. Click **Advanced Configuration** on the **Edit Cluster Settings** page.
4. Under **AIRFLOW CLUSTER SETTINGS** section, add the following variables in the **Overrride Airflow Configuration Variables** field:

   * ``core.alert_via_email=True``
   * ``core.alert_emails=email1, email2, email3...``

..

   A sample configuration is shown in the following illustration.

   .. image:: airflow-images/notification-settings.png

5. Click **Update and Push** for the changes to take effect immediately or click **Update only** for the changes to take effect with the cluster restart.

