.. _airflow-index:

========
Airflow
========


.. note:: See :ref:`os-version-support` for up-to-date information on Airflow support in QDS. To enable Airflow on
          Oracle OCI, create a `Qubole Support ticket <https://qubole.zendesk.com/hc/en-us>`__.


This section explains how to deploy and use Airflow. It covers the following topics:


.. toctree::
    :maxdepth: 1
    :titlesonly:

    introduction-airflow.rst
    set-up-datastore.rst
    config-airflow-cluster.rst
    upload-dag.rst
    register-dag.rst
    delete-dag.rst
    monitor-airflow-cluster.rst
    config-email-airflow.rst
    interoperability.rst
    qubole-operator-api.rst
    airflow-examples.rst
    node-bootstrap.rst
    experimental_apis/index.rst
