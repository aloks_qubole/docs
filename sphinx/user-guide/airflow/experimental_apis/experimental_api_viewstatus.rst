.. _experimental_api_viewstatus:

==============================================
View Task Status Through Experimental API
==============================================

..  http:get:: /airflow-webserver-<cluster-id>/api/experimental/dags/<DAG_ID>/tasks/<TASK_ID>/

You can now view a task status by running the experimental API.

.. note:: To run the experimental APIs, you require to have Airflow version 1.8.2 or above.

Required Role
-------------
A user with read permission to the cluster can make this API call.

Syntax
''''''

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" "https://
   <ENV>.qubole.com/airflow-webserver-<cluster-id>/api/experimental/dags/<DAG_ID>/tasks/<TASK_ID>"

.. note:: You need to specify the QDS environment in the above syntax. It works with all the environments described in
          :ref:`qubole-endpoints`.

Sample Query
''''''''''''

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" "https://
   api.qubole.com/airflow-webserver-70136/api/experimental/dags/Demo_Dag/tasks/Demo_Task"

Sample Response
'''''''''''''''

.. sourcecode:: bash

   {
     "adhoc": "False",
     "args": "()",
     "depends_on_past": "False",
     "email": "None",
     "email_on_failure": "True",
     "email_on_retry": "True",
     "end_date": "None",
     "execution_timeout": "None",
     "kwargs": "{'dag': <DAG: Demo_Dag>, 'task_id': 'qubole_task', 'tags': 'aiflow_example_run', 'fetch_logs': True, 'qubole_conn_id': 'qubole_default', 'params': {}, 'cluster_label': 'default', 'query': 'show tables', 'command_type': 'hivecmd'}",
     "max_retry_delay": "None",
     "on_failure_callback": "<function handle_failure_retry at 0x7efcc5d07d70>",
     "on_retry_callback": "<function handle_failure_retry at 0x7efcc5d07d70>",
     "on_success_callback": "None",
     "owner": "qubole",
     "params": "{}",
     "pool": "None",
     "priority_weight": "1",
     "queue": "default",
     "resources": "{'disk': {'_qty': 512, '_units_str': 'MB', '_name': 'Disk'}, 'gpus': {'_qty': 0, '_units_str': 'gpu(s)', '_name': 'GPU'}, 'ram': {'_qty': 512, '_units_str': 'MB', '_name': 'RAM'}, 'cpus': {'_qty': 1, '_units_str': 'core(s)', '_name': 'CPU'}}",
     "retries": "0",
     "retry_delay": "0:05:00",
     "retry_exponential_backoff": "False",
     "run_as_user": "None",
     "sla": "None",
     "start_date": "2018-12-02 00:00:00",
     "task_id": "qubole_task",
     "trigger_rule": "all_success",
     "wait_for_downstream": "False"
    }

