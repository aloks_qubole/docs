.. _experimental_api_dagrun:

==========================================
Trigger DAG Run Through Experimental API
==========================================

..  http:post:: /airflow-webserver-<cluster-id>/api/experimental/dags/<DAG_ID>/dag_runs/

You can now trigger a DAG run by running the experimental API.

.. note:: To run the experimental APIs, you require to have Airflow version 1.8.2 or above.

Required Role
-------------
A user with read permission to the cluster can make this API call.

Syntax
''''''

.. sourcecode:: bash

   curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" -d '{}'
   "https://<ENV>.qubole.com/airflow-webserver-<cluster-id>/api/experimental/dags/<DAG_ID>/dag_runs"

.. note:: You need to specify the QDS environment in the above syntax. It works with all the environments described in
          :ref:`qubole-endpoints`.

Sample Query
''''''''''''

.. sourcecode:: bash

   curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" -d '{}'
   "https://api.qubole.com/airflow-webserver-70136/api/experimental/dags/Demo_Dag/dag_runs"

Sample Response
'''''''''''''''

.. sourcecode:: bash

   {
     "message": "Created <DagRun Demo_Dag @ 2018-12-04 08:44:23: manual__2018-12-04T08:44:23, externally triggered: True>"
   }

