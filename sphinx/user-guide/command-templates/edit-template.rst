.. _edit-template:

==========================
Editing a Command Template
==========================

From the list of command templates available in `Templates <https://api.qubole.com/command_templates>`__, you can modify
an existing command template.

Perform the following steps to edit a command template:

1. Select a template that you want to edit from the list of command templates. The following figure shows a template
   that is selected from the templates' list.

.. image:: template-images/EditTemplate.png

2. Click the **edit icon** |Edit_Icon| for editing it.

.. |Edit_Icon| image:: ../../images/Edit_Cluster_button.PNG

**Edit Command Template** is displayed as shown in the following figure.

.. image:: template-images/EditTemplate1.png

3. Edit the template name if you want to change it.

4. Edit the query statement if it is required.

5. Change the **Form Field** values and add a new parameter by clicking **+** sign if it is required. See
   :ref:`create-command-template` for more information.

6. Adding **Macros** is optional. To use macros in the query, click the **+Add Macro** button available in
   the **Macros** field. See :ref:`create-command-template` for more information.

7. Click **Submit** to save the changes.
