.. _command-templates:

#################
Command Templates
#################

The Command Template is a QDS feature that provides you a template to compose a command/query once and modify a parameter
value multiple times or add another parameter along with its value.

If you want to run the same command multiple times with a different set of parameters, instead of rewriting the commands,
you can use command templates. A command template contains two variable fields known as **Form Fields** and **Macros**.
**Form Fields** are the inputs (parameter/value) that are provided when you run a query. **Macros** are dynamically
determined but are not required to be provided as inputs when you run a query.

For example, you can create a command template for the following command.

::

   SELECT CouponType FROM default_qubole_airline_origin_destination WHERE DestStateName = 'New York';

The same command can be run for other destination states by changing the value for **DestStateName**.

The following topics explain about how to view, create, and edit command templates:

.. toctree::
    :maxdepth: 1

    view-template.rst
    create-template.rst
    edit-template.rst

.. note:: Clicking the Qubole logo on the QDS UI displays the Qubole homepage.

For more information, see :ref:`analyze-index`.