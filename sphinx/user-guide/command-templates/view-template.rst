.. _command-templates-view:

==========================
Viewing a Command Template
==========================

To see a command template, click `Templates <https://api.qubole.com/command_templates>`__ from the QDS user
interface sidebar.

The command templates are listed if the current account contains templates created by all users of the current
account. **Templates** appears blank when no template is created in that account.

The following figure shows a list of command templates for a specific account.

.. image:: template-images/Template.png

Click a command template to see its details. The right-side of the page displays the command template run details
by default. Click **Details** to see the command template details such as **Input Variables** (Form Fields) and
**Macros** if any of them are set. The following figure illustrates a template's details.

.. image:: template-images/TemplateDetails.png

If you want to see a specific command template and you know its name/ID, then you can view it by using the filter. See
:ref:`filter-command-template` for more information.

Click the **edit icon** |Edit_Icon| for editing command template.

.. |Edit_Icon| image:: ../../images/Edit_Cluster_button.PNG

Click the **cross-mark icon** |StopIcon| for removing a command template.

.. |StopIcon| image:: ../scheduler/sc-images/KillIcon.png

Click the **clone icon** |CloneIcon| for cloning a command template.

.. |CloneIcon| image:: ../scheduler/sc-images/CloneIcon.png

While cloning a command template, you can retain other details but you must rename the template.

Click the **permalink icon** |PermalinkIcon| to see the command template's permalink.

.. |PermalinkIcon| image:: template-images/template-permalink.png

.. _filter-command-template:

Filtering a Command Template
----------------------------
In `Templates <https://api.qubole.com/command_templates>`__, click **Filter** to look for a specific command template
if you know its name/ID. The **Filter Templates** dialog is displayed as shown in the following figure.

.. image:: template-images/FilterTemplates.png

Enter the name in the **Template Name** text field and template ID in the **Template ID** text field. Select a specific
user from the **User** drop-down list if you do not want the default (All) users option. Select a required command type
from the **Command Type** drop-down list if you do not want to see all command types.

Click **Apply** to view the details of the specified command template. Click **Clear** to reenter the values in any
**Filter Templates** text field.
