.. _encrypt-communication-presto:

================================================
Encrypting Communication within a Presto Cluster
================================================
The nodes in a Presto cluster communicate over HTTP, and you can enable communication
over HTTPS. QDS supports using SSL to encrypt communication within a Presto cluster; create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable this.
:ref:`connect-to-when-presto-when-ssl-enabled` describes how to connect to the Presto server from outside QDS and
:ref:`authenticating-direct-connections` describes how to authenticate direct connections to Presto from outside QDS.


.. important:: Even when SSL is enabled, HTTP is still enabled on the master node which is only open to the QDS Control Plane.
               Qubole uses HTTP to provide you access to the :ref:`Presto UI <using-clusters-ui>`, :ref:`Query Tracker <compose-presto-query>`,
               and Presto notebooks.

You can disable HTTP on an SSL-enabled cluster by adding the following configuration to the cluster's **Presto Overrides**:

.. sourcecode:: bash

   config.properties:
   http-server.http.enabled=false

.. caution:: Disabling HTTP comes at a cost of features that are using it in QDS. The Presto-UI, Live QueryTracker and Presto
             notebooks do not work after HTTP is disabled.