.. _access-datastore-in-presto:

=============================================
Accessing Data Stores through Presto Clusters
=============================================
Qubole now supports accessing data stores through Presto clusters by adding a ``catalog`` parameter while
creating a data store using a REST API request. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
to enable this feature.

:ref:`create-a-dbtap` and :ref:`edit-a-dbtap` describe the ``catalog`` parameter. This parameter is supported for MySQL,
Postgres, and Redshift.

.. note:: Presto is not currently supported on all Cloud platforms; see :ref:`os-version-support`.