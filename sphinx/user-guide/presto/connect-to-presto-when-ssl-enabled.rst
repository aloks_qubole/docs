.. _connect-to-when-presto-when-ssl-enabled:

===============================================================
Connecting to an SSL-enabled Presto Server without using Qubole
===============================================================
For connecting to an SSL-enabled Presto server without using Qubole, perform these steps:

.. warning:: There would be no query history saved and Qubole Support might not always address issues that you may face
             when you connect to a Presto server externally.

1. Allow inbound traffic on port 8443 of the cluster master from the node where you want to run the client on.
2. Create a file called ``ca.cer`` containing the below public CA certificate.

    .. sourcecode:: bash

        -----BEGIN CERTIFICATE-----
        MIID+jCCAuKgAwIBAgIJAOKPZF/HWPl6MA0GCSqGSIb3DQEBBQUAMFsxCzAJBgNV
        BAYTAlhYMQswCQYDVQQIEwJLQTELMAkGA1UEBxMCQkExDzANBgNVBAoTBlF1Ym9s
        ZTEMMAoGA1UECxMDT3BzMRMwEQYDVQQDEwpxdWJvbGUuY29tMB4XDTE5MDEwODA0
        NDUwMVoXDTIyMDEwNzA0NDUwMVowWzELMAkGA1UEBhMCWFgxCzAJBgNVBAgTAktB
        MQswCQYDVQQHEwJCQTEPMA0GA1UEChMGUXVib2xlMQwwCgYDVQQLEwNPcHMxEzAR
        BgNVBAMTCnF1Ym9sZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
        AQCs0NN5upYPkiPN2zXBoSbJnSi8oPSqdAHUsIauQrkhhS/P1bA3qLncjj+XzjPW
        N+IZkCRWm6j4V/ZjHngnnilsCsPh0SnvKqybmZ+Fs2O6CJFoP9bsillNZ4sdFcvO
        Do2foGyyuJYFrE3WWvepQE0Qx+mmLfJNaPt+cvgtQ2v/dd/lE5dbXddwn3ZsJTBp
        FUHD8voooHdQwFkEhb6eLHg2tk3XQpncBfwM9GN+v+6kIeDk5UefDbboDPx6s3fq
        BKoPWx86cGYVeDPqZScN/evKGwa5IjJ2uoh3JNtc1kNDvUdm05eyX2Gpsa5IPGnq
        JcaLPI0KNeVfncyB9qLwpcftAgMBAAGjgcAwgb0wHQYDVR0OBBYEFEmKFMyoPUcX
        U5grd1eBgfatvo3XMIGNBgNVHSMEgYUwgYKAFEmKFMyoPUcXU5grd1eBgfatvo3X
        oV+kXTBbMQswCQYDVQQGEwJYWDELMAkGA1UECBMCS0ExCzAJBgNVBAcTAkJBMQ8w
        DQYDVQQKEwZRdWJvbGUxDDAKBgNVBAsTA09wczETMBEGA1UEAxMKcXVib2xlLmNv
        bYIJAOKPZF/HWPl6MAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAFP4
        f4qQlDXJbx9FYHR64aalV4AM3TE5/ZOtrqblAA8SB3GZYz98GDG/UVq+irrOhpS+
        G7qM8Ucv+TNyLOn08t9dNYmn+CK55QN2BPPLuG2bp9U5EQsigvFt4mboCtn3/BHV
        QMfABm3Ci/vnWPp4wih/HgbMMon0VjkZ8Tywh0I9j8Gdp7kXNp/88YGgc3zY+geH
        ho8Mi9Yq8hA1Z3bdRmYCLiJc/Evn+dAFvpGM74tIDMBy09Hfaa+JCSpaQrKmcr1Z
        ovRo0HBfvz0OFeOdYl76J8Dv6iqk1YbefNvht53vWQIPCR/FbYiwkDxsr4CHnrst
        VN7hs3xEfhHV7JcFf+8=
        -----END CERTIFICATE-----

3.  Add ``ca.cer`` to the Java keystore by using the below command.

   .. sourcecode:: bash

       keytool -noprompt -import -alias QuboleCA -file ca.cer -keystore ${JAVA_HOME}/jre/lib/security/cacerts -storepass changeit

4. Use the below sample command to run queries (ensure to use https://).

   .. sourcecode:: bash

       <Path/location of presto-cli-executable-*.jar> --server https://<HOST>:8443 --catalog hive --schema default
