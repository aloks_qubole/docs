.. _running-first-presto-query:

Running a First Presto Query
============================
By default, your account has a cluster named *presto* on which Presto queries run. You can modify this cluster and create others;
see :ref:`configuring-your-presto-cluster` for instructions.

.. note:: Presto is not currently supported on all Cloud platforms; see :ref:`os-version-support`.

Navigate to the **Analyze** page and click the **Compose** button. Select **Presto Query** from the drop-down list.
You can run a query against a pre-populated table that records the number of flight itineraries in every quarter of 2007.

For example, run the following command:

.. sourcecode:: bash

  select quarter, count(*) from default_qubole_airline_origin_destination where year='2007' group by quarter;

If the Presto cluster is not active, the query automatically starts it, and that may take a few minutes. You can watch the progress of the
job under the **Logs** tab; when it completes, you can see the query results under the **Results** tab.

Congratulations! You have just run your first Presto query on QDS!

For more information, see :ref:`compose-presto-query`, :ref:`differences-presto-hive`, and the other topics in :ref:`this Presto section <presto-qubole-index>`.