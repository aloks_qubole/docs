.. _explore-redshift-postgres:

===================================================
Exploring Data in Postgres and Redshift Data Stores
===================================================

This section explains how to explore Postgres and Redshift data stores. See:

* :ref:`explore-postgres`
* :ref:`explore-redshift`

.. _explore-postgres:

Exploring Data in a Postgres Data Store
---------------------------------------
After adding data to a Postgres data store, navigate to **Explore** page. By default,
you see the Qubole Hive database with the expanded default database as shown in the following figure.

.. image:: ../ug-images//ExploreDefaultView.png

Pull down the drop-down list and select a Postgres data store.

Click the *gear* icon on the right and choose the **Edit** button; the data store's fields become editable.

Modify the data store fields as needed and click **Update** to save your changes. Click **Reset to Default** to
restore the default settings. Click **Cancel** to retain the previous settings.

Choose a table from a non-default database to view its data. By default, **Properties** are displayed.
Click the icon |ExpActionIcon1| to see the available actions.

.. |ExpActionIcon1| image:: ../ug-images/ExpActionIcon.png

Choosing **Import table to Hive** produces a result similar to this:

.. image:: images/PostgresTable.png

Click the **full view icon** |ExpFullViewIcon| to see the table in the full window.

.. |ExpFullViewIcon| image:: ../ug-images/ExpFullviewIcon.png

Click the **refresh icon** |ExpRefreshIcon| to refresh the table.

.. |ExpRefreshIcon| image:: ../ug-images/ExpRefreshIcon.png

Clicking **Import table to Hive** brings up the  **Analyze** query composer in another tab. The query composer contains
a data import query with the Postgres data store selected in the **Data Store** drop-down list. See :ref:`compose-data-import`
for more information.

Click **Rows** to see the sample data in the table.


.. _explore-redshift:

Exploring Data in a Redshift Data Store
---------------------------------------
After adding data into a Redshift data store, navigate to the **Explore** page. By default,
you see the Qubole Hive database with the expanded default database as shown in the following figure.

.. image:: ../ug-images//ExploreDefaultView.png

Pull down the drop-down list and select a Redshift data store.

Click the *gear* icon on the right and choose the **Edit** button; the data store's fields become editable.

Modify the data store fields as needed and click **Update** to save your changes. Click **Reset to Default** to
restore the default settings. Click **Cancel** to retain the previous settings.

Choose a table from a non-default database to view its data. By default, **Properties** are displayed.
Click the icon |ExpActionIcon1| to see the available actions.


Choosing **Import table to Hive** produces a result similar to this:

.. image:: images/PostgresTable.png

Click the **full view icon** |ExpFullViewIcon| to see the table in the full window.

Click the **refresh icon** |ExpRefreshIcon| to refresh the table.

Clicking **Import table to Hive** brings up the **Analyze** query composer of in another tab. The query composer contains
a data import query with the Redshift data store selected in the **Data Store** drop-down list. See :ref:`compose-data-import`
for more information.

Click **Rows** to see the sample data in the table.
