.. _explore:

=============================
Using QDS in Data Exploration
=============================

Use the QDS **Explore** page to search data from various sources.
By default, you see the Qubole Hive database with the expanded default database.

.. note:: Clicking the Qubole logo on the QDS UI displays the Qubole homepage.

For access to your Cloud storage, and to adding an external data store, pull down the drop-down list next to **Qubole Hive**.

.. note::  Only a system administrator can add a data store; other users do not see the **Add Data Store** option.

Use **QDS Explore** to:

* View data in Hive tables

* Connect to any supported database and view data from its tables

* Connect to data buckets in Cloud storage and viewing the  data

The following sections explain the functions of the Explore feature:

* :ref:`hive-analyze-data`
* :ref:`hive-data-export`
* :ref:`adddatastore`
* :ref:`exploreS3data`
* :ref:`S3-dataexport`
* :ref:`createschema-S3`
