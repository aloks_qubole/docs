.. _adddatastore:

Adding a Data Store
===================

You need a data store if you want to import or export data from or to an external relational database management system
(RDBMS). You must be a system administrator to add a data store.

See :ref:`Data Store <db-tap>` for supported data store types, and instructions for adding a data store.
