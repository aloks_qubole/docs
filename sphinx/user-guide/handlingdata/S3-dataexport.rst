.. _S3-dataexport:

Exporting Data from Cloud Tables
================================
After analyzing data, you can export the data back to an external data store.

.. note:: You can pin/upin the custom Cloud path by clicking the pin icon available next to the Cloud location.


Follow these steps to export data from a file in Cloud storage to a data store:

          The role assigned to you must allow **upload** and **download** for the **Object Storage** resource. For more information, see :ref:`manage-roles-user-resources-actions`.

#. In the QDS UI, navigate to **Explore**.
#. Select **My Amazon S3** or **My Blob** from the drop-down list. See
   :ref:`exploreS3data` for more information.
#. Select the file from which you want to export data.
#. From the **Sample Data** tab, select the format and delimiter or accept the default.
   Click the **Export Data** button. Alternatively, click the icon to get a drop-down list.
#. When you click **Export Data**, the command composer of **Analyze** opens in another tab with
   **Command Type** selected as **Data Export** and **Mode** as **Directory Export** selected.

   The source directory is shown in the **Export Directory** text field.
#. Type a field separator in the **Field Terminated by** text field or accept the default field separator,
   ``\\0x01``.
#. Select a data store from the **Data Store** drop-down list.
#. Select a table from the **DbTable** drop-down list.
#. Select an update mode from the **Db Update Mode** drop-down list. **Append** mode is the default selection.
#. Click **Run**. The result appears in the **Results** tab and the command logs in the **Logs** tab.