.. _exploreS3data:

Exploring Data in Cloud
=======================
You can export data and create Hive tables from existing data in the Cloud. The data files are stored in
folders in Cloud storage. You can upload and download files to and from these folders in Cloud storage.

You configure the storage location from the
**Account Settings** page under **Control Panel** in the QDS UI.
See :ref:`Managing Account Settings <manage-accounts>` for more information.

Select a file to see sample data. By default, **Properties** of the file are displayed. Click the **Sample Data** tab
to see the data in text/JSON format.

.. note:: You can pin/upin the custom Cloud path by clicking the pin icon available at the end of the S3 bucket location.

The following figure provides an example of **Sample Data** and **Properties** of a file in AWS S3. For Azure Blob, the
corresponding URI would be ``wasb://default-datasets@paidqubole.blob.core.windows.net/``.

.. image:: ../ug-images/S3SampleData.png

By default, the data formatted in **Text** is displayed in the **Sample Data** tab. Select **JSON** from the drop-down list
to see data in the JSON format. Click **Raw Data** to see the unformatted text. Select
**Skip Header Row and Use As Column Names** to set the first row as the column names. It is useful if the parent table
contained first row as the header row and the new table contains the system-defined column names such as
**Col 1** and **Col 2**.

The options available in the **Delimiter** drop-down list are:

* Comma (default)
* Tab
* Space
* Semicolon
* Ctrl-A
* Ctrl-B
* Ctrl-C

See :ref:`S3-dataexport` and :ref:`createschema-S3` for more information.
