.. _user-guide-index:

###########
User Guide
###########

Table of contents.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    qds.rst
    features/index.rst
    managing-accounts/index.rst
    analyze/index.rst
    command-templates/index.rst
    handlingdata/index.rst
    clusters/index.rst
    airflow/index.rst
    hive/index.rst
    hadoop/index.rst
    pig/index.rst
    presto/index.rst
    spark/index.rst
    data-science/index.rst
    notebooks-and-dashboards/index.rst
    scheduler/index.rst
    cloud-filesystem/index.rst
    package-management/index.rst
    using-macros.rst
    use-qubole-drivers/index.rst
    file-partition-sensors.rst
    billing/index.rst
    qds-helpcenter.rst
    qds-feedback.rst