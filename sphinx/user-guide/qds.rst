.. _qds:

Introduction
============

Qubole Data Service (QDS) provides a highly integrated set of tools to help your organization analyze data and build reliable data-driven
applications in the Cloud. QDS takes care of orchestrating and managing Cloud resources, allowing you to focus on analyzing and using
the data.

Use QDS to explore data from various sources, analyze your data, launch and manage compute clusters, run recurring commands, and use notebooks to save, share,
and re-run a set of queries on a data source.

.. tabs::

   .. tab:: **Setup your QDS account**

      :ref:`Configure your Account Settings <managing-accounts-index>`

      :ref:`Manage your clusters <cluster-admin-index>`

      :ref:`Manage your roles and permissions <managing-access-permissions>`

   .. tab:: **Setup your data**

      :ref:`Setup and connect to the Hive metastore <custom-hive-metastore>`

      :ref:`Connect to data stores <adddatastore>`

      Bring in other data

         :ref:`Set up a data store <db-tap>`

         :ref:`Import your data <compose-data-import>`

         :ref:`Spark streaming <spark_streaming>`



      Prepare your data

        :ref:`Airflow <airflow-index>`

        :ref:`Scheduler <scheduler-qubole-index>`


   .. tab:: **Get insights**

     Explore

      :ref:`handling-data-index`

     Analyze

      :ref:`analyze-index`

      :ref:`notebook-index`

      :ref:`smart-query-index`

     Collaborate

      :ref:`command-templates`

      :ref:`dashboards-index`

   .. tab:: **Administer**

      :ref:`qds-clusters-index`

      Generate Reports

        :ref:`Generate the Cluster Usage report <cluster-usage-report>`

      :ref:`View Usage dashboard <usage>`

      :ref:`View QCUH and Monthly Usage <view-qcuh-monthly-usage>`

   .. tab:: **Engines**

      :ref:`hadoop-qubole-index`

      :ref:`hive-qubole-index`

      :ref:`pig-qubole-index`

      :ref:`presto-qubole-index`

      :ref:`spark-qubole-index`

   .. tab:: **APIs**

      :ref:`account-api`

      :ref:`apps-api`

      :ref:`cluster-api`

      :ref:`command-api`

      :ref:`command-template-api`

      :ref:`custom-metastore-api`

      :ref:`Dbtap-api`

      :ref:`folder-api-index`

      :ref:`groups-api-index`

      :ref:`hive-metadata-api`

      :ref:`notebook-api`

      :ref:`object_policy-api`

      :ref:`package-management-environment-api-index`

      :ref:`reports-api`

      :ref:`roles-api`

      :ref:`Scheduler-api`

      :ref:`sensor-api-index`

      :ref:`Users-api`

      :ref:`qubole-azure`

      :ref:`qubole-oracle-bmc`