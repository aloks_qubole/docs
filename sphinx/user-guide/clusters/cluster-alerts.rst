.. _cluster-alerts:

##########################
Health Checks for Clusters
##########################

This section explains the various health checks configured for the clusters.


Cluster HDFS Disk Utilization
=============================

This alert checks the free space allotted to HDFS and sends an alert if the free space is lower than a configurable limit.


Node Disk Utilization
=====================

This alert checks the free space allotted to HDFS on each node of the cluster and sends an alert if the free space is lower than a configurable limit.


Simple Hadoop Job Probe
=======================

This alert probes a simple end-to-end hadoop job in the cluster to check the overall health of the cluster.



