.. _deeplearning-index:

=============================
Deep Learning
=============================

Qubole Supports Deep Learning libraries on QDS-on-AWS, which can be used to build artificial neural networks.
Deep Learning clusters come with anaconda environments with all popular Deep Learning packages pre-installed.

.. note:: Deep Learning clusters are available for a beta access. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
          to enable this feature on a QDS account.

Qubole supports Python 2.7.13 and Python 3.5.3 on Deep Learning clusters and the default version is Python 3.5.3. In addition to
the standard packages included in Anaconda release 4.2.0, the Deep Learning environments come with these pre-installed packages:

* Chainer
* CuPy
* Keras2
* NCCL 2
* NVIDIA Driver (384.125)
* NVIDIA CUDA (9.0)
* NVIDIA cuDNN (7.0)
* Pytorch
* Sonnet
* TensorFlow (1.7)
* TensorFlowOnSpark (1.0.0)
* TFLearn
* Theano

.. note:: The version number indicates that only the mentioned versions are supported.

This section covers:

.. toctree::
    :maxdepth: 1

    config-deeplearning-cluster
    using-deeplearning-notebook
    monitoring-dl-cluster