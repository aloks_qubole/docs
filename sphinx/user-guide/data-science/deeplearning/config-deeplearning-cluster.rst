.. _config-deeplearning-cluster:

===================================
Configuring a Deep Learning Cluster
===================================
QDS supports Deep Learning libraries on QDS-on-AWS.

.. note:: Deep Learning clusters are available for a beta access. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
          to enable this feature on a QDS account.

Currently, Deep Learning clusters support all instance types of ``g2``, ``g3``, ``p2``, and ``p3`` instance families.

:ref:`manage-clusters` describes the cluster settings configured in the UI and :ref:`using-clusters-ui` describes
the cluster UI.

Here is a sample cluster UI for creating a cluster that shows the Deep Learning cluster type.

.. image:: dl-images/DeepLearningCluster.png