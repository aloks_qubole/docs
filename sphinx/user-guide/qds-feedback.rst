.. _qds-feedback.rst:

==================
Providing Feedback
==================
The top-right corner of the QDS user interface (UI) contains a feedback icon |Icon|.

.. |Icon| image:: ug-images/Icon.png

Clicking the Feedback icon displays the QDS UI feedback dialog as shown below.

.. image:: ug-images/DialogueBox.png

You can click the **Feedback** button from any screen. Enter your feedback in the textbox provided and click **Send Feedback**.

You can also directly access the Community posts or submit a support ticket using the **Ask the Community** or **Submit Support Ticket**
icons below the textbox.