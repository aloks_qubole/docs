.. _hive-data-export:

Exporting Data from the Hive Metastore
======================================
Navigate to **Explore**. By default, **Explore** displays the Qubole Hive metastore. See
:ref:`configure-thrift-metastore-custom` for more information.

You can export data from the Hive metastore into an existing data store.

Select the Hive table, and click the |ExpActionIcon| icon.

.. |ExpActionIcon| image:: hive-images/ExpActionIcon.png

A drop-down list is displayed as shown in the following figure.

.. image:: hive-images/HiveTableIcons.png

Choose **Data Export** from the list.

The command composer of **Analyze** opens in another tab with **Command Type** set to **Data Export** and
**Mode** to **HiveTable Export**. The table name appears under **Hive Table**. You need to provide further information
before running a data export command, as follows:

1. (Optional). If you need partitions for the table, identify them partition in the **Hive Table Partition Spec**
   text field using the same format as the following example: **dt=20130101/country=US**

2. Select a **Data Store** from the drop-down list.

3. Selecting a data store, populates the **DBTable** drop-down list. Select the database table
   to which you want to export data.

4. Select the **DB Update Mode** from the drop-down list; **AppendMode** is the default. **Insert and Update Mode** is currently
   available only for MySQL data stores.

   A sample Hive table data export command is shown in the following AWS example.

   .. image:: hive-images/DataExportHiveTable.png

5. Click **Run** to execute a command. The command result appears under the **Results** tab, and the command logs under the
   **Logs** tab.
