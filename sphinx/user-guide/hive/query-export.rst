.. _query-export:

Query Export
============

A common operation is to export the results of a query to a database. This operation can be modelled as a workflow of
Hive Query followed by a Data Export command. Query Export is a template with fields to fill in the Hive Query and the
Data Export Command. The Hive Query should write its results out to a pre-determined location in an S3 bucket. The Data
Export command must export the Hive table data from the pre-determined location to a Data Store.

See :ref:`compose-query-export` to use the `Analyze <https://api.qubole.com/v2/analyze>`__  user interface for composing
a Hive query export.

See :ref:`submit-a-hive-command` and :ref:`submit-a-db-export-command` for more information on REST API commands.

Example
-------
The following example shows Hive query and data export queries.

*(only a subset of fields are shown)*

Hive Query
..........

.. sourcecode:: sql

    INSERT OVERWRITE directory 's3://..../daily_tick_data/temp'
    SELECT
      stock_symbol,
      max(high),
      min(low),
      sum(volume)
    FROM
      daily_tick_data
    WHERE
      date1='2011-07-01'
    GROUP BY
      stock_symbol

Data Export
...........

.. sourcecode:: sql

    Export Dir: s3://..../daily_tick_data/temp
