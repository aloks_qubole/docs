.. _hive-analyze-data:

Analyzing Data in Hive Tables
=============================
Use the **Explore** page to analyze Hive data. By default, the page displays the Qubole Hive metastore. See
:ref:`configure-thrift-metastore-custom` for more information.

From the Hive metastore, select the Hive table that requires data analysis. Click the icon |ExpActionIcon|.

.. |ExpActionIcon| image:: hive-images/ExpActionIcon.png

A drop-down list is displayed as shown in the following figure.

.. image:: hive-images/HiveTableIcons.png

Choose **Analyze Data** from the list.

The query composer in opens in a new tab where you can compose your query.

Click **Run** to execute a query. The query result is displayed in the **Results** tab.

A sample Hive query with a successful result is shown in the following figure.

.. image:: hive-images/AnalyzeHiveTableQuery.png

