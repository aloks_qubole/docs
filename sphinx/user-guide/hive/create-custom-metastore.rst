.. _create-custom-metastore:

================================
Creating a Custom Hive Metastore
================================
Qubole supports using a custom Hive metastore on the Qubole account. By default, each account comes with the Qubole Hive
metastore but if you want to use a custom metastore, then you create a new metastore (if you do not have a metastore).

.. note:: :ref:`migrating-hive-metastore` described how to migrate the data from the Qubole-managed Hive metastore to
          the custom-managed Hive metastore. :ref:`custom-hive-metastore` describes how to connect to a custom metastore.

Create a custom Hive metastore by performing these steps:

1. Log into MySQL and create the metastore database and tables as shown in the example below.

   .. sourcecode:: bash

      > mysql -uroot
     ...
     mysql> CREATE DATABASE <database-name>;
     mysql> USE <database-name>;
     mysql> SOURCE <metastore-schema-script>

2. Create a MySQL user and grant access to the metastore database as illustrated in the example below.

   .. sourcecode:: bash

      mysql> CREATE USER 'hiveUser'@'localhost' IDENTIFIED BY 'hivePassword';
      mysql> REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'hiveUser'@'localhost';
      mysql> GRANT ALL ON hive_metastore.* TO 'hiveUser'@'localhost';
      mysql> FLUSH PRIVILEGES;

3. Configure the ``hive-site.xml`` file with the above metastore properties, the database and user.

4. Run the SQL scripts to create default tables for different Hive versions listed below:

   * For Hive 1.2.0, the SQL scripts are available in this file: `hive-schema-1.2.0.mysql.sql <https://github.com/apache/hive/blob/master/metastore/scripts/upgrade/mysql/hive-schema-1.2.0.mysql.sql>`__.
   * For Hive 2.1.0, the SQL scripts are available in this file: `hive-schema-2.1.0.mysql.sql <https://github.com/apache/hive/blob/master/metastore/scripts/upgrade/mysql/hive-schema-2.1.0.mysql.sql>`__.
   * For Hive 2.3.0, the SQL scripts are available in this file: `hive-schema-2.3.0.mysql.sql <https://github.com/apache/hive/blob/master/metastore/scripts/upgrade/mysql/hive-schema-2.3.0.mysql.sql>`__.

5. After creating the default tables in the custom Hive metastore, create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
   to enable the metastore.
6. When the custom Hive metastore is enabled on the QDS account, you can connect to the custom Hive metastore as
   described in :ref:`custom-hive-metastore`.