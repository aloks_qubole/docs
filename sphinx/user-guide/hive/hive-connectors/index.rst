.. _custom_hive_connectors:

###############
Hive Connectors
###############

.. toctree::
    :maxdepth: 1
    :titlesonly:

    dynamoDB-connector
    JDBC-connector
    mongoDB-tables



