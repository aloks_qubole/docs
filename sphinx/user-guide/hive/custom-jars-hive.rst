.. _custom-jar-hive:

==========================
Adding Custom Jars in Hive
==========================
When adding custom jars in Hive, it is **strongly recommended to avoid** overriding the ``hive.aux.jars.path``
property in **HIVE SETTINGS** and **HADOOP CLUSTER SETTINGS** in the **Hadoop 2 (Hive)** cluster. Instead, you can
add jars using any of these ways:

* Use the `add jar statement <https://cwiki.apache.org/confluence/display/Hive/LanguageManual+Commands>`__ to add custom
  jars at a query level or through the :ref:`Hive bootstrap <bootstrap-script>`.
* Add the custom jar by copying it into the ``/usr/lib/hive1.2/auxlib`` directory through the cluster's :ref:`node bootstrap <nodebootstrapscript>`.
  Qubole recommends using this approach when you run Hive on the master node or with HiveServer2.