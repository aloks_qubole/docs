.. _using-hive-on-tez:

=================
Using Hive on Tez
=================
This section explains how to configure and use Hive on Tez in a Qubole cluster. It covers the following topics:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    hive-on-tez
    hive-tez-tuning

