.. _hive-table-formats:

==================
Hive Table Formats
==================
This section describes the table formats available in Hive on a Qubole cluster.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    delimited.rst
    json.rst
    rcfile.rst
    orc.rst
    avro.rst






