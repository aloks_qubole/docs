.. _delimited-tables:

Delimited Tables
================
These are the delimited tables.

DDL Statement (AWS)
-------------------

::

  create external table nation_s3_txt 
  (N_NATIONKEY INT, N_NAME STRING, N_REGIONKEY INT, N_COMMENT STRING) 
  ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '|' 
  STORED AS TEXTFILE 
  LOCATION  's3://qtest-qubole-com/datasets/presto/functional/nation_s3_txt';

Features
--------
The features of delimited table are:

- Entire record must be one line in text file
- Field delimiter configurable through DDL
- Delimited files may be compressed using gzip, bzip2 or lzo
