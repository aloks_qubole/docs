.. _orc-tables:

ORC Tables
==========
The Optimized Row Columnar (ORC) file format offers an efficient way for storing Hive data.

DDL Statement (AWS Example)
---------------------------

::

  create external table nation_s3_orc 
  (N_NATIONKEY INT, N_NAME STRING, N_REGIONKEY INT, N_COMMENT STRING)  
  STORED AS ORC
  LOCATION  's3://qtest-qubole-com/datasets/presto/functional/nation_s3_orc'
  TBLPROPERTIES ("orc.compress"="SNAPPY")
  ;

