.. _sessions:

======================
Understanding Sessions
======================

Hive allows you to embed code (Python scripts, shell scripts, Java functions) into SQL queries. This is a
way to add functionality that is not natively present in HiveQL.
See :ref:`manage-sessions` for more information.
