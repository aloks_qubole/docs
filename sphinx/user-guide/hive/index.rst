.. _hive-qubole-index:

####
Hive
####

This section explains how to use Hive on a Qubole cluster. It covers the following topics:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    apache-hive
    data-model
    hive-connectors/index.rst
    hive-table-formats/index.rst
    use-hive-versions
    bootstrap-script
    hive-analyze-data
    S3-create-schema
    hive-data-export
    custom-hive-metastore
    create-custom-metastore
    migrating-hive-metastore
    custom-jars-hive
    using-hive-on-tez/index.rst
    hive-different-architecture
    automatic-statistics
    hive-metrics
    hive-mapjoin-options
    sessions
    running-hive-query
    optimize-hive-queries
    example-datasets
    query-export
    refresh-table


For more information, see :ref:`hive-administration` and :ref:`analyze-index`.

