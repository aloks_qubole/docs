.. _authenticating-direct-connections:

============================================
Authenticating Direct Connections to Engines
============================================
QDS users can connect to the query engines running in QDS from outside QDS as well. Use the authentication mechanisms that
are mentioned below for the security purpose when you connect to the query engines from outside QDS.

Authenticating Direct Connections to Presto
-------------------------------------------
Apart from `existing authentication schemes <https://prestodb.io/docs/0.180/security.html>`__, Presto
supports a file-based authentication.

.. important::  The file-based authentication is only available in Presto 0.180 and later versions. Currently, it does
                not work when SSL is used for the inter-node communication within the cluster. For more information,
                see `Points to Remember on the File-based Authentication`_.

You must create password digests to add the username:password pair before `Enabling the File-based Authentication`_.
These ``username``:``password`` pairs are used to authenticate users, who try to access Presto directly (that is from
outside of QDS). The file-based authentication feature has been enhanced to accept only hashed passwords. Earlier, plain
passwords were accepted and stored as-is on the cluster which posed a security threat. MD5, SHA1, Unix Crypt, and BCrypt
hashed passwords are supported. Qubole recommends using MD5 and BCrypt as SHA1 and Unix Crypt are less secure. It accepts
the input file as a collection of the ``username``:``password`` lines.

Creating Password Digests
-------------------------
You must create password digests and add them in the authentication file. Create a password digest using the ``htpasswd``
command. For more info on the command options, see `htpasswd <https://httpd.apache.org/docs/2.4/programs/htpasswd.html>`__.
You can create a password digest using any of these methods:

* ``htpasswd -n <UserName>``
* ``htpasswd /location/existing_file <UserName>``
* ``htpasswd -c /localtion/new_file_to_create <UserName>``

In the following examples, password digests (passwords) are created by running ````htpasswd -n <UserName>```` for the
different encrypted passwords that Qubole supports are as described below:

* **MD5**: Generate the MD5 encrypted password using this command syntax: ``htpasswd  -n <UserName>``.

  For example, run this command: ``htpasswd -n admin``.

  The command output is: ``admin:$apr1$EnUyEvmk$SWUlSWtcdTIsgLihPNW09``

* **SHA1**: Generate the SHA1 encrypted password using this command syntax: ``htpasswd -n -s <UserName>``.

  For example, run this command: ``htpasswd -n -s user1``.

  The command output is:  ``user1:{SHA}bWdU2ejuS1lWa0mv11TR2YAoOzs=``

* **Unix Crypt**: Generate the Unix Crypt-based password using this command syntax: ``htpasswd -n -d <UserName>``.

  For example, run this command: ``htpasswd -n -d user2``.

  The command output is: ``user2:EY8ULaXsqebTI``

* **BCrypt**: Generate the BCrypt-based password using this command syntax: ``htpasswd -n -B <UserName>``.

  For example, run this command: ``htpasswd -n -B user3``.

  The command output is: ``user3:$2a$05$/.QHkz9ytxOWwvcE5zMt7O0fabySUNcdKMqVmJpqTOGIRdFfjlSey``

Here is the example of the passwords for users, which you can add as part of the ``authentication`` file.

.. sourcecode:: bash

    authentication:
    admin:$apr1$EnUyEvmk$SWUlSWtcdTIsgLihPNW09
    user1:{SHA}bWdU2ejuS1lWa0mv11TR2YAoOzs=
    user2:EY8ULaXsqebTI
    user3:$2a$05$/.QHkz9ytxOWwvcE5zMt7O0fabySUNcdKMqVmJpqTOGIRdFfjlSey

In the above example:

* The ``admin`` uses **MD5** encryption.
* The ``user1`` uses the **SHA1** encryption.
* The ``user2`` uses the **Unix Crypt** encryption.
* The ``user3`` uses the **BCrypt** encryption.

Enabling the File-based Authentication
--------------------------------------
You can enable the file-based authentication by adding this configuration as a Presto override in the cluster configuration.

.. sourcecode:: bash

     config.properties:
     http-server.authentication.type=FILE
     authentication.filebased.config-file=etc/authentication

     authentication:
     admin:$apr1$EnUyEvmk$SWUlSWtcdTIsgLihPNW09
     user1:{SHA}bWdU2ejuS1lWa0mv11TR2YAoOzs=
     user2:EY8ULaXsqebTI
     user3:$2a$05$/.QHkz9ytxOWwvcE5zMt7O0fabySUNcdKMqVmJpqTOGIRdFfjlSey

Adding this override configures Presto to use ``etc/authentication`` as input for ``username``:``password`` pairs.
The content of this file is defined in the ``authentication:`` section as mentioned above. `Creating Password Digests`_
describes how to create them for different encrypted passwords.

.. note:: Ensure that the content of the ``etc/authentication`` is in the valid JSON format or else, the Presto server fails to start.

In direct connections, you must provide username and password correctly and use ``https://MASTER_HOST_NAME:8443`` as the URL.
You cannot change the port and it must be always ``8443`` for an SSL connection.

To authenticate direct connections to Presto Server where username is stored in the source name header, use these configurations:

* ``http-server.authentication.post.source-has-username: <true/fasle>``. When it is enabled, the source name is used as
  the username for authentication and only POST API calls (submitted queries) are authenticated.
* ``http-server.authentication.source-has-username: <true/fasle>``. When it is enabled, the source name is used as the
  username for authentication but all API calls to the Presto server are authenticated.

  .. warning:: Enable ``http-server.authentication.source-has-username`` only if you have a custom Presto client as the
               standard Presto clients do not populate the source field in all requests except the POST request, which
               causes query failures.

If you are using open source, JDBC drivers must enable SSL apart from these configurations as described in the
`Presto JDBC topic <https://prestodb.io/docs/0.180/installation/jdbc.html>`__.

Points to Remember on the File-based Authentication
---------------------------------------------------
Here are some conditions that hold good to the basic authentication for direct connections in Presto:

* You must use the Qubole Presto Ruby client for uninterrupted access to clusters through QDS
  after enabling this feature. This feature does not support the old QDS Java client.
* You must have an encrypted channel while submitting password and you must get SSL enabled in the master
  node by creating a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.