.. _security-audit.rst:

============================
Standard Security Compliance
============================
QDS deploys baselines in its production environments that are compliant with SOC2, HIPAA, and ISO-27001.
For more information, see `Security <https://www.qubole.com/products/trust/>`__.

The Qubole endpoint (QDS on the AWS cloud) https://us.qubole.com is SOC2 compliant. For more information, see
:ref:`qubole-endpoints`.