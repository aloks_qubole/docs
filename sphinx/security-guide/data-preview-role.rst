.. _data-preview-role:

Using the Data Preview Role to Restrict Access to Data
======================================================

By default, users can see the data previews and results that appear on the **Explore**, **Analyze**, and **Notebooks** UI pages. The **Data Preview** role enables you to restrict the visibility of sensitive data on these pages. By configuring this role to deny permission to view the data to certain users, you ensure that the data is not exposed to unauthorized users.

To create a **Data Preview** role, go to the **Manage Roles** screen in the QDS Control Panel. Click the **add** icon |AddIcon4| to create a new role. Choose the resource **Data Preview**, as shown here:

.. image:: sg-images/create_data_preview_role.png

For detailed information and instructions for creating roles and assigning them to groups and users, see :ref:`manage-roles`.

.. |ADDIcon4| image:: sg-images/AddIcon.png
