.. _data-rbac:

Using Role-based Access Control for Commands
============================================

QDS enables commands to be private or public to support both privacy and collaboration. When working with sensitive data, you can prevent other users in your Qubole account from viewing your commands by setting command access to private. Alternatively, you can set command access to public so that all users in your account can see them, allowing collaboration with other members of your organization. You can change the command access setting before, during, or after running queries. By default, command access is set to public, but you can change the default value from the **Control Panel > My Preferences** page.

.. note:: A command that is marked private cannot be viewed even by the account administrator. Only the user who created the command and those with super-administrator access can view private commands.

Setting command access for a query
----------------------------------

You can change the access setting for any of the following:

* an existing command
* a command that is currently running
* a command you are composing but haven't yet submitted

To change the access setting for an existing command, select the command in the **History** tab in the left pane of the **Analyze** page. For an existing command, select the **Manage Permissions** icon either in the left pane or in the command editor and choose the desired setting. To change the access setting for a command that is currently running, or one that you are still composing, you can select the **Manage Permissions** icon in the command editor.

The **Manage Permissions** icon resembles a person and a key:

.. image:: sg-images/manage_permissions_icon.png

To change the access setting for an existing command, select the command in the **History** tab, and click the **Manage Permissions** icon:

.. image:: sg-images/manage_permissions_in_UI.png

Changing the default value for command access
---------------------------------------------

The initial default value for command access is public. You can change the default value to private in the **My Preferences** screen. There are two ways to open **My Preferences**:

* From anywhere in the QDS UI, you can click the person icon in the upper right of the blue bar at the top of the screen...

.. image:: sg-images/OpenMyPreferences.png

* ...or you can click the dropdown arrow for the page you are on (Explore, Analyze, etc.):

.. image:: sg-images/access_my_preferences.png

In the dropdown list for **Command permissions**, click **Anyone in this account** to allow public access to your commands, or **Only me (make it private)** to restrict access to you alone:

.. image:: sg-images/MyPreferencesUI.png
