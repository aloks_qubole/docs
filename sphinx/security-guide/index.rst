.. _security-guide-index:

======================
Product Security Guide
======================
Qubole offers a complete suite of security features across its product stack, allowing organizations to achieve a high
level of security and compliance while implementing their Big Data Platform. Qubole security features include
encryption, identity management, role-based access control, data governance, and adherence to compliance
standards, as described in the following topics:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    access-control-security
    authenticating-direct-connections
    data-security
    data-management
    data-ranger
    data-ranger-for-presto
    data-rbac
    data-preview-role
    infra-security
    hive-hadoop-security-azure
    security-audit
