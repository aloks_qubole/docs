.. _access-control-security.rst:

Securing through Identity and Access Management
-----------------------------------------------
Qubole provides each account with granular access control over resources such as clusters, notebooks, and
users/groups. For a complete list of resources, see **Control Panel** > **Manage Roles**. For more information, see:

* :ref:`manage-users`
* :ref:`manage-groups`
* :ref:`manage-roles`

Accessing through API Tokens
----------------------------
API tokens are used to authenticate with the API. An API token is for a user per account. This implies that a user, who is part of 10 accounts has 10 API tokens; while a user with a single account has one API token. *Only the API token of the user's default account can be reset*. For more information on multiple accounts, default accounts, and using API tokens, see :ref:`manage-my-account`.

An API token can be used to schedule jobs using external schedulers such as cron, but an API token is not required when jobs are
scheduled using the Qubole scheduler. The jobs are shown by the user whose API is being used. If it is required to use
a single user for all scheduled jobs, create a common user to run them.

Accessing QDS through OAuth and SAML
------------------------------------
Qubole supports the following open standards for accessing QDS:

* **OAuth**: An open standard for authorization. As part of OAuth, you can sign in to QDS using Google credentials.

* **SAML**: A single-sign-on authorization service that can be used to sign in to QDS.

:ref:`using-saml-sso` describes how to access QDS using Google authorization and SAML.


