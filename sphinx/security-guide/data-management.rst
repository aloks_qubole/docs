.. _data-management:

====================================
Understanding Data Encryption in QDS
====================================
Command metadata and results are stored in your Cloud storage. To speed up access - by default -
these are also cached on QDS servers.

Qubole caches:

* Metadata for one day that is 24 hours
* Command results for 7 days
* Notebook paragraph-results for 30 days

.. _data-management-azure:

Encryption for Data at Rest on Azure
------------------------------------

* Data at rest is `encrypted automatically <https://docs.microsoft.com/en-us/azure/storage/common/storage-service-encryption>`__ in Azure Blob storage, and Azure does not provide a way to disable encryption.

* Data at rest is `encrypted by default <https://docs.microsoft.com/en-us/azure/data-lake-store/data-lake-store-encryption>`__ in Azure Data Lake storage. If you accept the default, you can't change the setting after setting up the account.

* QDS supports the Azure encryption mechanisms; there is nothing you need to do in QDS.

Encryption on AWS
-----------------

QDS provides encryption mechanisms to protect the data, as follows.

Encrypting AWS Cached Data
..........................

Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable encryption of results while
fetching them from the object storage, though it might slow down the data retrieving process as QDS would not be caching
the results onto cache.

There is no option to disable Metastore caching and read-only Notebooks are always cached with encryption on.

.. _encrypting-S3data:

Encrypting Data on Amazon S3
............................

Qubole supports protecting data on Amazon S3 through encryption mechanisms. It supports the server-side and client-side
encryption as described in :ref:`enabling-encryption-data-index`.

Encrypting Ephemeral Data on AWS Clusters
.........................................

On the QDS clusters, you can encrypt data on Ephemeral HDFS as described in :ref:`encrypt-HDFS-data`.

To enable encryption on the ephemeral drives through a Cluster REST API, see :ref:`cluster-security-settings`.