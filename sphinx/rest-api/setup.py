import os
import sys
from setuptools import setup

INSTALL_REQUIRES = ['pytest==2.7.0', 'qds-sdk >= 1.1.0', 'MySQL-python', 'pytest-xdist', 'pytest-rerunfailures==0.05', 'pytest-timeout==0.3']
if sys.version_info < (2, 7, 0):
    INSTALL_REQUIRES.append('argparse>=1.1')

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="doctest",
    version="0.0.1",
    author="Qubole",
    author_email="dev@qubole.com",
    description=("Tests for Qubole docs"),
    keywords="qubole doc test ",
    url="https://bitbucket.org/qubole/docs",
    packages=['lib'],
    install_requires=INSTALL_REQUIRES
)
