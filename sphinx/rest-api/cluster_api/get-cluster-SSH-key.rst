.. _cluster-ssh-key-api:

============================================
View the Cluster Instance-level SSH Key Pair
============================================

..  http:get:: /api/v1.3/clusters/<Cluster ID>/ssh_key

Use this API to view the cluster instance-level public SSH key. Qubole automatically generates the public and private
SSH key pair and puts the public SSH key on each node on the cluster. This happens only if the Unique SSH
Key feature is enabled on the QDS account. If it is not enabled, then the default Qubole public key is on the cluster nodes.

Parameters
----------
None

Request API Syntax
------------------

.. sourcecode:: bash

       curl -X GET -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
       "https://<QDS environment>/api/v1.3/clusters/<Cluster ID>/ssh_key"

.. note:: The above syntax uses the QDS environment as the placeholder. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................

Here is a sample request to see the instance-level public SSH key of a QDS cluster that has 120 as its cluster ID in the
https://api.qubole.com environment.

.. sourcecode:: bash

       curl -X GET -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
       "https://api.qubole.com/api/v1.3/clusters/120/ssh_key"