.. _add-node:

Add a Node to a Cluster
=======================

..  http:post:: /api/v1.3/clusters/(string:id_or_label)/nodes

This API adds a new worker node to the cluster. The instance type is picked up from the cluster configuration. This
action starts the operation asynchronously. The operation can be monitored using the command ID in the response through
the :ref:`command status <view-command-status>` API.

.. note:: A cluster must be running to add a new node. An Add Node API does not check for maximum size of the cluster.
          Currently, this function is supported for Hadoop 1 and Hadoop 2 clusters only.

          Qubole has deprecated Hadoop 1 as-a-service. For more information, see :ref:`hadoop1-dep`.

See :ref:`manual-scale-a-cluster` for more information.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows adding nodes to a cluster. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" -d
    '{"node_count": "$value"}' "https://api.qubole.com/api/latest/clusters/${cluster_id}/nodes"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **cluster_id**            | Hadoop Cluster ID                                                                        |
+---------------------------+------------------------------------------------------------------------------------------+
| node_count                | The total number of nodes. The default value of **node count** is 1.                     |
|                           | (Hadoop-1 and Hadoop-2)                                                                  |
+---------------------------+------------------------------------------------------------------------------------------+

Response
--------

The response contains a JSON object representing the command ID of the add node operation. All attributes mentioned
here are returned (except when otherwise specified or redundant).

Example
-------

**Request**

.. sourcecode:: bash

   curl -X POST -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   https://api.qubole.com//api/v1.3/clusters/353181/nodes

.. note:: The response is the same as a query_hist response.

**Response**

.. sourcecode:: bash

   {
        "submit_time": 1431419457,
        "status": "waiting",
        "saved_query_mutable_id": null,
        "command_source": "",
        "progress": 0,
        "resolved_macros": null,
        "end_time": null,
        "start_time": null,
        "command": {
            "cluster_inst_id": 17991,
            "parameters": null,
            "action": "add",
            "private_dns": null
        },
        "pid": null,
        "qbol_session_id": 80475,
        "command_type": "ClusterManageCommand",
        "created_at": "2015-05-12T08:30:57Z",
        "account_id": 14,
        "name": null,
        "timeout": null,
        "template": "generic",
        "label": "Hadoop",
        "user_id": 1432,
        "pool": null,
        "num_result_dir": -1,
        "qlog": null,
        "path": "/tmp/2015-05-12/14/353181",
        "id": 353181,
        "meta_data": {
            "results_resource": "commands/353181/results",
            "logs_resource": "commands/353181/logs"
        },
        "can_notify": false
    }