.. _remove-node:

Remove a Node from a Cluster
============================

..  http:delete:: /api/v1.3/clusters/(string:id_or_label)/nodes

This API removes a worker node from a cluster. This action starts the operation asynchronously.

In **Hadoop 2** clusters, nodes are removed from the cluster gracefully, which means that a node is only
removed after tasks/jobs running on that particular node get completed. A node is only removed only if the current cluster
size is greater than the configured minimum cluster size in **Hadoop 2** clusters.

The operation can be monitored using the command ID in the response through the
:ref:`command status <view-command-status>` API.

.. note:: A cluster must be running to remove a node. A Remove Node API does not check for maximum size of the cluster.
          Currently, this function is supported on Hadoop 2 clusters.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows removing nodes from an existing
  cluster. See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Response
--------

The response contains a JSON object representing the command ID of the remove node operation. All the attributes mentioned
here are returned (except when otherwise specified or redundant).

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

.. csv-table::
    :header: "Parameter", "Description"
    :widths: 15, 50
    :quote: '

    **private_dns**, Private DNS of the worker node.

Examples
--------
`Example 1`_ shows an example to remove nodes and `Example 2`_ shows an example to remove nodes by passing the private DNS
of the worker node.

Example 1
.........
**To remove nodes from a cluster with 277344 as its ID.**

.. sourcecode:: bash

    curl -X DELETE
    -H "X-AUTH-TOKEN:$AUTH_TOKEN"
    -H "Content-Type:application/json" \
    https://api.qubole.com/api/v1.3/clusters/277344/nodes

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: bash

    {
        "command_source": "",
        "pid": null,
        "qlog": null,
        "command_type": "ClusterManageCommand",
        "id": 277344,
        "saved_query_mutable_id": null,
        "user_id": 3635,
        "label": "opentsdb2",
        "template": "generic",
        "qbol_session_id": 40375,
        "progress": 0,
        "can_notify": false,
        "status": "waiting",
        "account_id": 1612,
        "start_time": null,
        "meta_data": {
            "results_resource": "commands/277344/results",
            "logs_resource": "commands/277344/logs"
        },
        "num_result_dir": -1,
        "submit_time": 1432291974,
        "path": "/tmp/2015-05-22/1612/277344",
        "pool": null,
        "resolved_macros": null,
        "name": null,
        "created_at": "2015-05-22T10:52:54Z",
        "command": {
            "action": "remove",
            "private_dns": "ip-10-234-18-139.ec2.internal",
            "cluster_inst_id": 23528,
            "parameters": null
        },
        "end_time": null,
        "timeout": null
    }

Example 2
.........
**To remove nodes from a cluster by specifying the private DNS of the worker node.**

.. sourcecode:: bash

    curl -i -X DELETE -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:application/json" \
    https://api.qubole.com/api/v1.3/clusters/<cluster-id>/nodes?private_dns=<private_dns>

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.