.. _start-terminate-cluster:

Start or Terminate a Cluster
============================

..  http:put:: /api/v1.3/clusters/<cluster-id/cluster-label>/state

Use this API to start or terminate a cluster.

.. caution:: You must not manually launch instances in the security groups created by Qubole when a cluster is active.
             Launching the instances in the Qubole security groups would result in the incorrect display of the number
             of nodes in the **Clusters** UI page.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows start/stop operations on a cluster.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter             | Description                                                                                                                                                                        |
+=======================+====================================================================================================================================================================================+
| **state**             | It is used to state the action to perform. Its valid values are ``start`` to start a cluster and ``terminate`` to terminate it. Starting a running cluster or stopping a           |
|                       | terminated cluster will have no effect.                                                                                                                                            |
+-----------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Examples
--------

Start a Cluster
...............

.. caution:: You must not manually launch instances in the security groups created by Qubole when a cluster is active.
             Launching the instances in the Qubole security groups would result in the incorrect display of the number
             of nodes in the **Clusters** UI page.

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"state":"start"}' \ https://api.qubole.com/api/v1.3/clusters/1710/state

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Response
^^^^^^^^

.. sourcecode:: json

   {"message":"Starting cluster with id 1710."}


Terminate a Running Cluster
...........................

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"state":"terminate"}' \ https://api.qubole.com/api/v1.3/clusters/1710/state

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Response
^^^^^^^^

.. sourcecode:: json

   {"message":"Terminating cluster with id 1710."}