.. _get-cluster-information:

View a Cluster Configuration
============================

..  http:get:: /api/v1.3/clusters/(string:id_or_label)

This API is used to get the configuration details of a single cluster.
Invoke this API with the cluster ID or a label that is assigned to it.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a cluster configuration. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
-------

Get the configuration information of a single cluster
.....................................................

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   https://api.qubole.com/api/v1.3/clusters/1710

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Response
........

.. sourcecode:: json

   HTTP/1.1 200 OK
   Content-Type: application/json; charset=utf-8

       "security_settings":{
        "encrypted_ephemerals":false
      },
      "enable_ganglia_monitoring":false,
      "label":[
      "my_cluster"
      ],
      "ec2_settings":{
        "compute_validated":false,
        "compute_secret_key":"<your_ec2_compute_secret_key>",
        "aws_region":"us-west-2",
        "vpc_id":null,
        "aws_preferred_availability_zone":"Any",
        "compute_access_key":"<your_ec2_compute_access_key>",
        "subnet_id":null
      },
      "node_bootstrap_file":"node_bootstrap.sh",
      "hadoop_settings":{
      "use_hadoop2":false,
       "custom_config":null,
       "fairscheduler_settings":{
         "default_pool":null
       }
      },
      "disallow_cluster_termination":false,
      "presto_settings":{
      "enable_presto":false,
       "custom_config":null
      },
     "id":116,
     "state":"DOWN",
     "node_configuration":{
      "max_nodes":10,
      "master_instance_type":"m1.large",
      "slave_instance_type":"m1.xlarge",
      "use_stable_spot_nodes":false,
      "slave_request_type":"spot",
      "initial_nodes":1,
      "spot_instance_settings":{
        "maximum_bid_price_percentage":"100.0",
        "timeout_for_request":10,
        "maximum_spot_instance_percentage":60
       }
    }
   }