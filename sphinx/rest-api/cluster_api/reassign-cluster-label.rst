.. _reassign-cluster-label:

Reassign a Cluster Label
========================

..  http:put:: /api/v1.3/clusters/reassign-label

This API is used to reassign a label from one cluster to another.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows editing a cluster configuration. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

+---------------------------------+----------------------------------------------------------------+
| Parameter                       | Description                                                    |
+=================================+================================================================+
| **label**                       | Label to be moved *from* the source cluster.                   |
+---------------------------------+----------------------------------------------------------------+
| **destination\_cluster**        | ID or label of the cluster to move the label *to*.             |
+---------------------------------+----------------------------------------------------------------+

Examples
--------

Reassign a label from one cluster to another
--------------------------------------------

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"label":"pig_workflow", "destination_cluster": 1710}' \ https://api.qubole.com/api/v1.3/clusters/reassign-label

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Response
--------

.. sourcecode:: json

   {"message":"Reassigned cluster label pig_workflow from cluster with id 1711 to cluster with id 1710."}