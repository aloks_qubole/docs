.. _get-cluster-state:

Check Cluster Status
====================

..  http:get:: /api/v1.3/clusters/(string:id or label)/state

Use this API to get the current state of a cluster.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a cluster configuration. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Various Types of Cluster States
-------------------------------
+---------------------------+------------------------------------------------------------------------------------------+
| States                    | Description                                                                              |
+===========================+==========================================================================================+
| ``UP``                    | This state indicates that the cluster is active.                                         |
+---------------------------+------------------------------------------------------------------------------------------+
| ``DOWN``                  | This state indicates that the cluster is inactive/not running.                           |
+---------------------------+------------------------------------------------------------------------------------------+
| ``PENDING``               | This state indicates that the cluster is in the process of booting up.                   |
+---------------------------+------------------------------------------------------------------------------------------+
| ``TERMINATING``           | This state indicates that the cluster is in the process of shutting down.                |
+---------------------------+------------------------------------------------------------------------------------------+

Examples
--------

Get the state of a running cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   https://api.qubole.com/api/v1.3/clusters/1710/state

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: json

    {
        "state": "UP",
        "last_health_check_action_at": null,
        "start_at": "2014-04-25T14:08:28Z",
        "down_at": null,
        "cluster_id": 1710,
        "terminate_reason": null,
        "nodes": [
            {
                "instance_type": "m1.large",
                "ec2_instance_id": "i-abcdefdd",
                "is_spot_instance": false,
                "up_time": "2014-04-25T14:06:02Z",
                "private_ip": "domU-11-22-33-44-55-02.compute-1.internal",
                "last_seen_time": "2014-04-25T14:06:02Z",
                "role": "master",
                "down_time": null,
                "hostname": "ec2-55-66-33-120.compute-1.amazonaws.com"
            },
            {
                "instance_type": "m1.xlarge",
                "ec2_instance_id": "i-abcdef54",
                "is_spot_instance": false,
                "up_time": "2014-04-25T14:06:02Z",
                "private_ip": "ip-11-22-33-44.ec2.internal",
                "last_seen_time": "2014-04-25T14:06:02Z",
                "role": "node0001",
                "down_time": null,
                "hostname": "ec2-55-88-44-120.compute-1.amazonaws.com"
            },
            {
                "instance_type": "m1.xlarge",
                "ec2_instance_id": "i-abcdef55",
                "is_spot_instance": false,
                "up_time": "2014-04-25T14:06:02Z",
                "private_ip": "ip-11-66-111-222.ec2.internal",
                "last_seen_time": "2014-04-25T14:06:02Z",
                "role": "node0002",
                "down_time": null,
                "hostname": "ec2-55-222-111-100.compute-1.amazonaws.com"
            }
        ],
        "last_health_check_action": null,
        "config_id": 2196
    }


Get the state of a terminated cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json"
    https://api.qubole.com/api/v1.3/clusters/1711/state

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: json

    {"state":"DOWN"}