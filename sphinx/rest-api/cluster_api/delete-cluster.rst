.. _delete-cluster:

Delete a Cluster
================

..  http:delete:: /api/v1.3/clusters/<cluster-id/cluster-label>

Use this API to delete a specific cluster by calling its ID or label.

.. note:: The cluster that has the label as ‘default’ cannot be deleted. Hence, there is always at least one cluster in an account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows deleting a cluster. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Examples
--------

**Goal**

Delete a cluster.


.. sourcecode:: bash

    curl -X DELETE
    -H "X-AUTH-TOKEN:$AUTH_TOKEN"
    -H "Content-Type: application/json" -H "Accept: application/json" \
    https://api.qubole.com/api/v1.3/clusters/1710

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Response
--------

Returns a message with the status of the delete operation.

    {"message":"Successfully deleted cluster with id 1710."}