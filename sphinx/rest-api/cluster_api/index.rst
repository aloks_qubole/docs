.. _cluster-api:

###########
Cluster API
###########

.. toctree::
    :maxdepth: 1
    :titlesonly:

    list-clusters
    create-new-cluster
    get-cluster-information
    edit-cluster
    clone-cluster
    start-terminate-cluster
    validate-compute-credentials
    get-cluster-state
    get-cluster-SSH-key
    delete-cluster
    reassign-cluster-label
    run-adhoc-scripts-cluster
    metrics-cluster
    resize-cluster
    add-node
    remove-node