.. _delete-role-api:

=============
Delete a Role
=============

.. http:delete:: /api/v1.2/roles/<role-id/name>

This API is used to delete a role that is not required.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows deleting roles. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **<role-id/name>**        | ID or name of the role that is to be deleted                                             |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the Request API syntax.

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/roles/<role-id>"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/105/roles/19"

Sample Response
...............

.. sourcecode:: bash

    Success
    {"status":"done"}