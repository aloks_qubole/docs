.. _create-roles-api:

=============
Create a Role
=============

.. http:post:: api/v1.2/roles

This API is used to create a role such as system user and system administrator in a QDS account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating roles. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **name**                  | Name of the role that must be unique in the Qubole account.                              |
+---------------------------+------------------------------------------------------------------------------------------+
| **policies**              | Array of policies to be assigned to role. Each policy include following parameters:      |
|                           |                                                                                          |
|                           | * **resource**: Name of the resource such as `all` and `cluster`. The resources are:     |
|                           |                                                                                          |
|                           |   .. note:: :ref:`manage-roles-user-resources-actions` describes the policy resources    |
|                           |             that are listed in the **Control Panel** > **Manage Roles** UI.              |
|                           |                                                                                          |
|                           |   - ``all`` - It denotes **All** resources.                                              |
|                           |   - ``account`` - It denotes the **Account** resource.                                   |
|                           |   - ``app`` -  It denotes the **App** resource.                                          |
|                           |   - ``cluster`` - It denotes the **Clusters** resource.                                  |
|                           |   - ``command`` - It denotes the **Commands** resource.                                  |
|                           |   - ``datastore`` - It denotes the **Data Source** resource.                             |
|                           |   - ``data_preview`` - It denotes **Read Access** to data.                               |
|                           |   - ``environment`` - It denotes the **Environments and Packages** resource.             |
|                           |   - ``folder`` - It denotes the **Folder** resource.                                     |
|                           |   - ``group`` - It denotes the **Groups** resource.                                      |
|                           |   - ``note`` - It denotes the **Notes** (notebooks) resource.                            |
|                           |   - ``notebook_dashboard`` - It denotes the **Notebook Dashboards** resource.            |
|                           |   - ``object_storage`` - It denotes the **Object Storage** resource.                     |
|                           |   - ``role`` - It denotes the **Roles** resource.                                        |
|                           |   - ``saved_query`` - It denotes the **Workspace** resource.                             |
|                           |   - ``scheduler`` - It denotes the **Scheduler** resource.                               |
|                           |   - ``scheduler_instance`` - It denotes the **Scheduler Instance** resource.             |
|                           |   - ``template`` - It denotes the **Template** resource.                                 |
|                           |   - ``qbol_user``  - It denotes the **Users** resource.                                  |
|                           |                                                                                          |
|                           | * **action**: Name of the action with particular resource.                               |
|                           | * **access**: Giving or denying permissions. It can be either `allow` or `deny`.         |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"policies":"[{\"resource\":\"all\",\"action\":[\"all\"],\"access\":\"allow\"}]",
         "name":"my_role"}' \ "https://api.qubole.com/api/v1.2/roles"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"policies":"[{\"resource\":\"all\",\"action\":[\"all\"],\"access\":\"allow\"}]",
         "name":"my_role"}' \ "https://api.qubole.com/api/v1.2/roles"


Sample Response
...............

.. sourcecode:: bash

    {"status":"success","roles":{"id":6,"name":"my_role","source":"user-defined",
     "policy":"[{\"resource\":\"all\",\"action\":[\"all\"],\"access\":\"allow\"}]","account_id":64}}
