.. _list-groups-for-role-api:

================================
List Groups with a Specific Role
================================
.. http:get:: /api/v1.2/roles/<role-id/name>/groups

This API is used to list groups assigned with a particular role.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing groups assigned with a
  specific role. See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **<role-id/name>**        | ID or name of the role to list associated groups of the current account                  |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the syntax of the Request API.

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/roles/<role-id/name>/groups"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/roles/system-admin/groups"