.. _roles-api:

=========
Roles API
=========

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-role-api
    list-groups-for-role-api
    delete-role-api
