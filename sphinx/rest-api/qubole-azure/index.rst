.. _qubole-azure:

==================================
APIs for Qubole on Microsoft Azure
==================================
The following topics describe Qubole REST APIs. You can also use these functions interactively via a GUI; see the
:ref:`user-guide-index` and the :ref:`admin-guide-index`.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    azure-cluster-api/index.rst




