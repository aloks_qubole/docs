.. _clone-azure-cluster-api:

==================================
Clone a Cluster on Microsoft Azure
==================================

..  http:post:: /api/v2/clusters/<cluster ID>/clone

Use this API to clone a cluster.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a cluster. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
:ref:`azure-cluster-api-parameters` describes the list of parameters of a cluster on Microsoft Azure. You can change
the name of the cluster.


Request API Syntax
------------------
:ref:`azure-cluster-request-api-syntax` explains the entire syntax for creating an Azure cluster.
You can add the configuration that needs modification of an existing cluster in the Clone API payload.

Sample API Request
------------------
Here is a sample API request to clone a cluster with 1223 as its ID.

.. sourcecode:: bash

   curl -X POST -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   -d '{
    "cluster_info": {
             "label": ["azure1-clone"],
             "min_nodes": 1,
             "max_nodes": 4,
             "cluster_name": "Azure1-clone",
             "node_bootstrap": "node_bootstrap.sh",
             }
    }' \ "https://azure.qubole.com/api/v2/clusters/1223/clone"