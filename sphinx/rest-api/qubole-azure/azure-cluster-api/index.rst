.. _azure-cluster-api:

====================================
APIs for Clusters on Microsoft Azure
====================================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-azure-cluster-api.rst
    edit-azure-cluster-api.rst
    clone-azure-cluster-api.rst
    list-azure-clusters.rst
    get-azure-cluster-info-api.rst
    start-terminate-azure-cluster-api.rst
    check-azure-cluster-status-api.rst
    delete-azure-cluster-api.rst


