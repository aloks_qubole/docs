.. _check-azure-cluster-status:

=======================================
Check Cluster Status on Microsoft Azure
=======================================

..  http:get:: /api/v2/clusters/<cluster ID/cluster-label>

Use this API to get the current state of a cluster.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a cluster configuration. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Request API Syntax
------------------
Here is the API request syntax to know the current status of a cluster.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://azure.qubole.com/api/v2/clusters/<cluster ID/cluster label>/state"

Sample API Request
..................
Here is the sample API request to know the current status of a cluster that has 250 as its ID.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://azure.qubole.com/api/v2/clusters/250/state"