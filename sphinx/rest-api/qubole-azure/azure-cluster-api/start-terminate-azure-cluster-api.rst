.. _start-terminate-azure-cluster:

===============================================
Start or Terminate a Cluster on Microsoft Azure
===============================================

..  http:put:: /api/v2.0/clusters/<cluster-id/cluster-label>/state

Use this API to start or terminate a cluster.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows start/stop operations on a cluster.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter             | Description                                                                                                                                                                        |
+=======================+====================================================================================================================================================================================+
| **state**             | It is used to state the action to perform. Its valid values are ``start`` to start a cluster and ``terminate`` to terminate it. Starting a running cluster or stopping a           |
|                       | terminated cluster will have no effect.                                                                                                                                            |
+-----------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the request API syntax to start/terminate a cluster.

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"state":"<start/terminate>"}' \ "https://azure.qubole.com/api/v2/clusters/<cluster ID/cluster label>/state"

Sample API Requests
-------------------
Here are sample requests to start and terminate a cluster with 200 as its ID.

Start a Cluster
...............

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"state":"start"}' \ "https://azure.qubole.com/api/v2/clusters/200/state"

Terminate a Running Cluster
...........................

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"state":"terminate"}' \ "https://azure.qubole.com/api/v2/clusters/200/state"