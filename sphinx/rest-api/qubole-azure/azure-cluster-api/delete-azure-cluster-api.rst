.. _delete-azure-cluster-api:

===================================
Delete a Cluster on Microsoft Azure
===================================

..  http:delete:: /api/v2/clusters/<cluster ID>

Use this API to delete a specific cluster.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a cluster. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
None

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X DELETE -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   "https://azure.qubole.com/api/v2/clusters/<cluster-ID>"

Sample API Request
..................
Here is a sample request to delete the cluster that has 301 as its ID.

.. sourcecode:: bash

   curl -X DELETE -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   "https://azure.qubole.com/api/v2/clusters/301"