.. _remove-packages-api:

=====================================================
Remove Packages from a Package Management Environment
=====================================================

.. http:delete:: /api/v1.2/package/<env ID>/remove_packages

Use this API to remove Python and R packages from a Package Management environment.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows updating an environment. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------+------------------------------------------------------------------------------------------------+
| Parameter           | Description                                                                                    |
+=====================+================================================================================================+
| **source_type**     | Specify ``python_package`` or ``r_package`` and it is mandatory while removing packages        |
|                     | from an environment.                                                                           |
+---------------------+------------------------------------------------------------------------------------------------+
| **package_names**   | Specify the name of the package. To remove a specific version of the package, specify it in    |
|                     | this format: ``<packagename>==<version-number>``. For example, ``biopython==0.1``. You can     |
|                     | remove any  number of packages as a comma-separated list.                                      |
+---------------------+------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json"
    -d ' {"source_type": "<package type>", "package_names": "<packagename1>,<packagename2>,....,<packagenameN>"}
          "https://api.qubole.com/api/v1.2/package/<envID>/remove_packages"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is a sample API call for removing Python packages from an environment that has 120 as its ID.

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"source_type": "python_package", "package_names": "numpy,bipython==0.1,tensorflow"}' \
         "https://api.qubole.com/api/v1.2/package/120/remove_packages"

Here is a sample API call for removing R packages from an environment that has 120 as its ID.

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"source_type": "r_package", "package_names": "r-rserve,r-brew==1.0"}' \
         "https://api.qubole.com/api/v1.2/package/120/remove_packages"
