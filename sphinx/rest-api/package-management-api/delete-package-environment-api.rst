.. _delete-package-management:

=======================================
Delete a Package Management Environment
=======================================

.. http:delete:: /api/v1.2/package/<env ID>/delete_environment

Use this API to delete a Package Management environment. You can only delete an environment when its attached cluster is
**down**/**inactive**. After the environment is deleted, its attached cluster gets detached automatically.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows deleting an environment. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
None

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/package/<env ID>/delete_environment"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is a sample to delete the environment with 140 as its ID.

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/package/140/delete_environment" 