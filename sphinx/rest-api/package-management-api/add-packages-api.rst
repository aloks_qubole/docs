.. _add-packages-api:

==================================================
Add Packages to the Package Management Environment
==================================================

.. http:post:: /api/v1.2/package/<env ID>/add_packages

Use this API to add Python and R packages to a Package Management environment. A newly created environment supports the
conda-forge channel which supports more packages. :ref:`pkgmgmt-preinstalled-packages` provides the list of pre-installed
packages. :ref:`package-management-faq-index` provides answers to questions related to adding packages.

.. note:: In addition to the default Conda package repo, you can also add only R packages from the CRAN package repo.
          For more information, see :ref:`package-management`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows updating an environment. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------+------------------------------------------------------------------------------------------------+
| Parameter           | Description                                                                                    |
+=====================+================================================================================================+
| **source_type**     | Only Python and R packages can be added. Specify ``python_package`` or ``r_package`` and it is |
|                     | mandatory while adding packages through an environment.                                        |
+---------------------+------------------------------------------------------------------------------------------------+
| **package_names**   | Specify the name of the package. If you just specify the name, the latest version of the       |
|                     | package gets installed. To install a specific version of the package, specify it in this       |
|                     | format: ``<packagename>==<version-number>``. For example, ``biopython==0.1``. You can add any  |
|                     | number of packages as a comma-separated list.                                                  |
+---------------------+------------------------------------------------------------------------------------------------+
| repo_url            | You must set this parameter to ``https://cran.r-project.org\`` only when you want to add the R |
|                     | package from the CRAN package repo.                                                            |
+---------------------+------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d ' {"source_type": "<package type>", "package_names": "<packagename1>,<packagename2>,....,<packagenameN>"}' \
          "https://api.qubole.com/api/v1.2/package/<envID>/add_packages"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Requests
...................

.. note:: If you upgrade or downgrade a **Python** package, the changed version is reflected only after you restart the
          Spark interpreter. :ref:`interpreter-operations` lists the restart and other Spark interpreter operations.

Here is a sample API call for adding Python packages to an environment that has 100 as its ID.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \

    -d '{"source_type": "python_package", "package_names": "numpy,bipython==0.1,tensorflow"}' \
         "https://api.qubole.com/api/v1.2/package/100/add_packages"

Here is a sample API call for adding R packages to an environment that has 100 as its ID.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"source_type": "r_package", "package_names": "r-rserve,r-brew==1.0"}' \
         "https://api.qubole.com/api/v1.2/package/100/add_packages"


Here is a sample API call for adding a R package from the CRAN package repo to an environment that has 100 as its ID.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"source_type": "r_package", "repo_packages": "[ {\"name\": \"RMySQL\", \"repo_url\": \"https://cran.r-project.org\"}]" }' \
    "https://api.qubole.com/api/v1.2/package/100/add_packages"