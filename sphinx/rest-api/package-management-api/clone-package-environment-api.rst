.. _clone-package-environment-api:

======================================
Clone a Package Management Environment
======================================

.. http:put:: /api/v1.2/package/<env ID>/clone_environment

Use this API to clone a package management environment. You cannot change the Python and R versions.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating an environment. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------+------------------------------------------------------------------------------------------------+
| Parameter           | Description                                                                                    |
+=====================+================================================================================================+
| name                | Provide a name to the environment. By default, the cloned environment inherits the parent      |
|                     | environment's name with `-clone` as it suffix that is `<environmentname-clone>`.               |
+---------------------+------------------------------------------------------------------------------------------------+
| description         | You can add a brief description about the environment.                                         |
+---------------------+------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name": "<EnvironmentName>", "description": "<Description>"}' \
    "https://api.qubole.com/api/v1.2/package/<env ID>/clone_environment"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is a sample request to clone a package environment that has 100 as its ID.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name": "PackageEnv2", "description": "Environment for adding packages"}' \
    "https://api.qubole.com/api/v1.2/package/100/clone_environment"