.. _package-management-environment-api-index:

==================================
Package Management Environment API
==================================
QDS provides a package management environment to add and manage Python and R packages in Spark applications. For more
information, see :ref:`package-management`.

.. note:: This feature is not available by default. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
          to enable this feature on the QDS account.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-package-environment-api
    attach-cluster-to-environment-api
    detach-cluster-from-environment-api
    edit-package-environment-api
    clone-package-environment-api
    list-package-environments-api
    delete-package-environment-api
    add-packages-api
    list-packages-api
    remove-packages-api


