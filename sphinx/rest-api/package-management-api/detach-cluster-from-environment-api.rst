.. _detach-cluster-from-environment-api:

==========================================================
Detach the Cluster from the Package Management Environment
==========================================================

.. http:put:: /api/v1.2/package/<env ID>/detach_cluster

Use this API to detach the cluster from the Package Management environment. In case, if you detach an environment from
the Airflow 1.8.2 cluster with Python 3.5, then you must attach another Python 3.5 environment to get the Airflow cluster
running. Otherwise, the cluster does not start.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows updating an environment. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
None

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/package/<env ID>/detach_cluster"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is detaching a cluster from the environment with 120 as its ID.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/package/120/detach_cluster"