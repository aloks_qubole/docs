.. _create-package-environment-api:

=======================================
Create a Package Management Environment
=======================================

.. http:post:: /api/v1.2/package/create_environment

Use this API to create a package management environment.

.. note:: This feature is not available by default. Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
          to enable this feature on the QDS account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating an environment. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------+------------------------------------------------------------------------------------------------+
| Parameter           | Description                                                                                    |
+=====================+================================================================================================+
| **name**            | Provide a name to the environment and it is mandatory.                                         |
+---------------------+------------------------------------------------------------------------------------------------+
| description         | You can add a brief description about the environment.                                         |
+---------------------+------------------------------------------------------------------------------------------------+
| python_version      | The default Python version is 2.7. The other supported version is 3.5.                         |
+---------------------+------------------------------------------------------------------------------------------------+
| r_version           | It is the version of R and its supported and default version is 3.3.                           |
+---------------------+------------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the syntax to create an environment.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name": "<EnvironmentName>", "description": "<Description>", "python_version":"<Supported Python Version>",
          "r_version":"<Supported R version>"}' \ "https://api.qubole.com/api/v1.2/package/create_environment"


.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is a sample API call to create an environment.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name": "PackageEnv", "description": "Environment for adding packages", "python_version":"3.5",
        "r_version":"3.3"}' \ "https://api.qubole.com/api/v1.2/package/create_environment"


If you do not specify the Python and R versions, the default Python 2.7 and R 3.3 versions would be installed. A newly
created environments gets a list of Python and R packages installed by default. For more information,
see :ref:`package-management`.

**Response**

.. sourcecode:: bash

    { "environment":
        {
            "account_id": 1,
            "cluster_id": null,
            "created_at": "2017-11-22T12:19:40Z",
            "description": "Environment for adding packages",
            "env_predefine_python_id": 1,
            "env_predefine_r_id": 3,
            "env_python_detail_id": 200,
            "env_r_detail_id": null,
            "id": 81,
            "name": "PackageEnv",
            "python_version_ui": "3.5",
            "qbol_user_id": 88,
            "r_version_ui": "3.3",
            "status": true,
            "updated_at": "2017-12-06T12:05:48Z"
        }
    }