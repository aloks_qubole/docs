.. _list-package-environments-api:

====================================
List Package Management Environments
====================================

.. http:get:: /api/v1.2/package/list_environments

Use this API to list all environments. On the QDS UI, navigate to **Control Panel** > **Environments** to see the list of
environments. For more information, see :ref:`package-management`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows reading/updating an environment. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
None

Request API Syntax
------------------
As there are no parameters, this syntax does not require a sample.

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/package/list_environments"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

