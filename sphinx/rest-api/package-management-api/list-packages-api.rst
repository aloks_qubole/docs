.. _list-packages-api-in-env:

=================================================
List Packages of a Package Management Environment
=================================================

.. http:get:: /api/v1.2/package/<env ID>/list_packages

Use this API to see the packages added in a specific Package Management environment. ``<env ID>`` denotes the environment's ID.
:ref:`package-management-faq-index` provides answers to questions related to adding packages.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows reading/updating an environment. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
None

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
     "https://api.qubole.com/api/v1.2/package/<env ID>/list_packages"

Sample API Request
..................
Here is the sample API request to list packages of a Package Management environment that has 516 as its ID.

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
     "https://api.qubole.com/api/v1.2/package/516/list_packages"

Response
--------
Here is the sample response that provides the list of packages and installed packages' history.

.. note:: ``installed_pkgs_history`` provides a list of successfully installed packages per request and ``packages``
           provides a list of last 5 unsuccessful add-packages' requests and the last successful add-packages' request.

.. sourcecode:: bash

   {
    "env_id": 516,
    "packages": {
        "python": [
            {
                "cluster_status": null,
                "cluster_status_msg": null,
                "created_at": "2018-01-09T14:50:53Z",
                "env_id": 516,
                "error": null,
                "id": 548,
                "qbol_user_id": <user ID>,
                "requirements": "tensorflow,ipython,numpy",
                "status": "success",
                "updated_at": "2018-01-09T14:53:03Z"
            },
       {
                "cluster_status": null,
                "cluster_status_msg": null,
                "created_at": "2018-01-09T10:32:18Z",
                "env_id": 516,
                "error": "  Could not find a version that satisfies the requirement biopython==0.1 (from versions: 1.0a3, 1.0a4, 1.10, 1.20, 1.21, 1.22, 1.23, 1.24, 1.30, 1.40b0, 1.41, 1.42, 1.43, 1.44, 1.45, 1.46, 1.47, 1.48, 1.49b0, 1.49, 1.50b0, 1.50, 1.51b0, 1.51, 1.52, 1.53, 1.54b0, 1.54, 1.55b0, 1.55, 1.56, 1.57, 1.58, 1.59, 1.60, 1.61, 1.62b0, 1.62, 1.63b0, 1.63, 1.64, 1.65, 1.66, 1.67, 1.68, 1.69, 1.70)\nNo matching distribution found for biopython==0.1\nAddition of Python package biopython==0.1 creation failed for the last job",
                "id": 547,
                "qbol_user_id": 12609,
                "requirements": "tensorflow,ipython,biopython==0.1",
                "status": "error",
                "updated_at": "2018-01-09T10:33:54Z"
       }
       ],
      "r": [
            {
                "cluster_status": null,
                "cluster_status_msg": null,
                "created_at": "2018-01-09T14:59:55Z",
                "env_id": 516,
                "error": null,
                "id": 129,
                "qbol_user_id": <user ID>,
                "repo_packages": null,
                "requirements": "r-rserve",
                "status": "success",
                "updated_at": "2018-01-09T15:03:03Z"
            },
            {
                "cluster_status": null,
                "cluster_status_msg": null,
                "created_at": "2018-01-09T15:18:41Z",
                "env_id": 516,
                "error": "PackageNotFoundError: Package not found: '' Package missing in current linux-64 channels: \n  - r-rserve ==0.1\n\nYou can search for packages on anaconda.org with\n\n    anaconda search -t conda r-rserve\n\n\nAddition of R package r-rserve==0.1,r-brew creation failed for the last job",
                "id": 130,
                "qbol_user_id": 12609,
                "repo_packages": null,
                "requirements": "r-rserve,r-rserve==0.1,r-brew",
                "status": "error",
                "updated_at": "2018-01-09T15:21:03Z"
            }
      ]
      },
      "installed_pkgs_history": {
          "python": [
            {
                "cluster_status": null,
                "cluster_status_msg": null,
                "created_at": "2018-01-04T05:09:20Z",
                "env_id": 516,
                "error": null,
                "id": 527,
                "qbol_user_id": <user ID>,
                "requirements": "tensorflow",
                "status": "success",
                "updated_at": "2018-01-04T05:10:08Z"
            },
            {
                "cluster_status": null,
                "cluster_status_msg": null,
                "created_at": "2018-01-09T10:27:32Z",
                "env_id": 516,
                "error": null,
                "id": 546,
                "qbol_user_id": <user ID>,
                "requirements": "tensorflow,ipython",
                "status": "success",
                "updated_at": "2018-01-09T10:28:34Z"
            },
            {
                "cluster_status": null,
                "cluster_status_msg": null,
                "created_at": "2018-01-09T14:50:53Z",
                "env_id": 516,
                "error": null,
                "id": 548,
                "qbol_user_id": <user ID>,
                "requirements": "tensorflow,ipython,numpy",
                "status": "success",
                "updated_at": "2018-01-09T14:53:03Z"
            }
      ],
      "r":  [
            {
                "cluster_status": null,
                "cluster_status_msg": null,
                "created_at": "2018-01-09T14:59:55Z",
                "env_id": 516,
                "error": null,
                "id": 129,
                "qbol_user_id": <user ID>,
                "repo_packages": null,
                "requirements": "r-rserve",
                "status": "success",
                "updated_at": "2018-01-09T15:03:03Z"
            }
      ]
      }
    }