.. _cluster-usage-report:

====================
Cluster Usage Report
====================

.. http:get:: api/v1.2/reports/cluster_usage_report

This API provides you the cluster usage report in JSON format.

.. note:: The following points are related to a report API:

          * If the difference between start date and end date is more than 60 days, then the system defaults to 1 month
            window from the current day's date.
          * If either start date or end date is not provided, then the system defaults to 1 month window from the current
            day's date.
          * If you want to get data for a window more than 2 months, then create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing cluster usage reports. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter               | Description                                                                                                                                                                                    |
+=========================+================================================================================================================================================================================================+
| **start_date**          | The date from which you want the report (inclusive). This parameter supports the timestamp in the UTC timezone (YYYY-MM-DDTHH:MM:SSZ) format. The date cannot be earlier than 90 days.         |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **end_date**            | The date until which you want the report (inclusive). The API default is *today*. This parameter also supports timestamp in the UTC timezone (YYYY-MM-DDTHH:MM:SSZ) format.                    |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| clusters_filter[]       | Denotes the cluster ID of the cluster for which the usage report must be filtered. For example, ``clusters_filter[]=1`` indicates that the usage report must be filtered to the                |
|                         | cluster assigned with an ID 1.                                                                                                                                                                 |
+-------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Response Parameters
-------------------

+----------------+----------------------------------------------------------------------------+
| Parameter      | Description                                                                |
+================+============================================================================+
| start_date     | The starting date of the report.                                           |
+----------------+----------------------------------------------------------------------------+
| end_date       | The ending date of the report.                                             |
+----------------+----------------------------------------------------------------------------+
| cluster_usage  | An array of properties as mentioned in :ref:`cluster-usage-properties`.    |
+----------------+----------------------------------------------------------------------------+

.. _cluster-usage-properties:

Cluster Usage Properties
........................

+----------------------+-------------------------------------------------------------------------------------+
| qcuh                 | Displays the QCUH information for AWS                                               |
+----------------------+-------------------------------------------------------------------------------------+
| start_time           | The time at which the cluster node was started.                                     |
+----------------------+-------------------------------------------------------------------------------------+
| end_time             | The time at which the cluster node was terminated.                                  |
+----------------------+-------------------------------------------------------------------------------------+
| cluster_instance_id  | The cluster instance ID of the cluster node.                                        |
+----------------------+-------------------------------------------------------------------------------------+
| cluster_id           | The ID of the cluster                                                               |
+----------------------+-------------------------------------------------------------------------------------+
| terminate_reason     | The reason for the cluster to be terminated.                                        |
+----------------------+-------------------------------------------------------------------------------------+
| tags                 | Displays cluster labels                                                             |
+----------------------+-------------------------------------------------------------------------------------+
| spot_nodes           | Number of spot nodes spawned by the cluster                                         |
+----------------------+-------------------------------------------------------------------------------------+
| on_demand_nodes      | Number of on-demand nodes spawned by the cluster                                    |
+----------------------+-------------------------------------------------------------------------------------+
| last_computed_time   | It is the time at which the given usage (``qcuh``) was calculated.                  |
+----------------------+-------------------------------------------------------------------------------------+

Examples
--------
You must URL encode the parameter name and its value in the REST API call.

Request without a Cluster Filter
................................

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/reports/cluster_usage_report?start_date=2015-08-10+18%3A30%3A00&end_date=2015-08-11+18%3A29%3A59"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Request with a Cluster Filter
.............................
You must URL encode the parameter name and its value in the REST API call.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/reports/cluster_usage_report?start_date=2015-08-10+18%3A30%3A00&end_date=2015-08-11+18%3A29%3A59&clusters_filter[]=1&clusters_filter[]=2"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Response
........

.. sourcecode:: bash

    {
      "cluster_usage": [{
          "qcuh": 3.0,
          "start_time": "2015-08-11 04:11:19 +0000",
          "end_time": "2015-08-11 05:01:15 +0000",
          "cluster_inst_id": 69759,
          "cluster_id": 1,
          "terminate_reason": "INACTIVITY",
          "tags": "default",
          "spot_nodes": 0.0,
          "on_demand_nodes": 2.0,
          "last_computed_time": "2018-07-17T00:00:00Z"
      }, {
          "qcuh": 3.0,
          "start_time": "2015-08-11 00:20:38 +0000",
          "end_time": "2015-08-11 01:15:18 +0000",
          "cluster_inst_id": 69700,
          "cluster_id": 1,
          "terminate_reason": "INACTIVITY",
          "tags": "default",
          "spot_nodes": 0.0,
          "on_demand_nodes": 2.0,
          "last_computed_time": "2018-07-17T00:00:00Z"
      }],
      "end_date": "2015-08-11T18:29:59Z",
      "start_date": "2015-08-10T18:30:00Z"
    }