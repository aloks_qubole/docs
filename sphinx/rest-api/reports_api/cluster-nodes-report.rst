.. _cluster-nodes-report:

Cluster Nodes Report
====================

.. http:get:: /api/v1.2/reports/cluster_nodes

This API provides the cluster nodes report in JSON format.

.. note:: The following points are related to a report API:

          * If the difference between start date and end date is more than 60 days, then the system defaults to 1 month
            window from the current day's date.
          * If either start date or end date is not provided, then the system defaults to 1 month window from the current
            day's date.
          * If you want to get data for a window more than 2 months, then write an email to help@quoble.com.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing cluster nodes reports. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

+----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter      | Description                                                                                                                                                                                    |
+================+================================================================================================================================================================================================+
| start\_date    | The date from which you want the report (inclusive). This parameter supports the timestamp in the UTC timezone (YYYY-MM-DDTHH:MM:SSZ) format. The date cannot be earlier than 90 days.         |
+----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| end\_date      | The date until which you want the report (inclusive). The API default is *today*. This parameter also supports timestamp in the UTC timezone (YYYY-MM-DDTHH:MM:SSZ) format.                    |
+----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

 
Response Parameters
-------------------

+----------------+------------------------------------+
| Parameter      | Description                        |
+================+====================================+
| start\_date    | The starting date of the report.   |
+----------------+------------------------------------+
| end\_date      | The ending date of the report.     |
+----------------+------------------------------------+

An array of:

+----------------------+-------------------------------------------------------------------------------------+
| role                 | The role of the instance (master or worker).                                        |
+----------------------+-------------------------------------------------------------------------------------+
| cluster\_id          | The id of the cluster                                                               |
+----------------------+-------------------------------------------------------------------------------------+
| public\_ip           | The public hostname of the cluster node.                                            |
+----------------------+-------------------------------------------------------------------------------------+
| ec2\_instance\_id    | The ec2 instance ID of the cluster node.                                            |
+----------------------+-------------------------------------------------------------------------------------+
| private\_ip          | The private hostname of the cluster node.                                           |
+----------------------+-------------------------------------------------------------------------------------+
| start\_time          | The time at which the cluster node was started.                                     |
+----------------------+-------------------------------------------------------------------------------------+
| end\_time            | The time at which the cluster node was terminated.                                  |
+----------------------+-------------------------------------------------------------------------------------+

 
Examples
--------

**Goal**

To get the default report.

.. sourcecode:: bash

   curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
   -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/reports/cluster_nodes"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Goal**

To get the report for clusters online during a specific time period.

.. sourcecode:: bash

   curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
   -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/reports/cluster_nodes?start_date=2014-04-01&end_date=2014-04-21"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

.. sourcecode:: json

    {
      "end_date": "2014-04-21T10:00:00Z",
      "cluster_nodes": [
        {  
          "ec2_instance_id":"i-437ad9ac",
          "private_ip":"ip-10-40-7-209.ec2.internal",
          "start_time":"2015-02-12T06:59:42Z",
          "role":"master",
          "public_ip":"ec2-23-20-255-83.compute-1.amazonaws.com",
          "end_time":"2015-02-12T08:13:52Z",
          "cluster_id":10268
        },
        {  
          "ec2_instance_id":"i-887bd867",
          "private_ip":"ip-10-165-32-171.ec2.internal",
          "start_time":"2015-02-12T06:59:42Z",
          "role":"node0001",
          "public_ip":"ec2-54-144-51-140.compute-1.amazonaws.com",
          "end_time":"2015-02-12T08:13:52Z",
          "cluster_id":10268
        }
      ],
      "start_date": "2014-04-01T05:00:00Z"
    }