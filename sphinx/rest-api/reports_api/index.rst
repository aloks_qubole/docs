.. _reports-api:

###########
Reports API
###########

.. toctree::
    :maxdepth: 1
    :titlesonly:

    all-commands-report
    canonical-hive-commands-report
    cluster-nodes-report
    cluster-usage-report

