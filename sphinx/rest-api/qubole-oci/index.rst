.. _qubole-oracle-bmc:

==============================================
APIs for Qubole on Oracle Cloud Infrastructure
==============================================
The following topics describe Qubole REST APIs. You can also use these functions interactively via a GUI; see the
:ref:`user-guide-index` and the :ref:`admin-guide-index`.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    oci-cluster-api/index.rst