.. _list-oracle-oci-cluster-api:

============================
List a Cluster on Oracle OCI
============================

..  http:get:: /api/v2/clusters/

Use this API to list all clusters.

Parameters
----------
None

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a cluster. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   "https://oraclecloud.qubole.com/api/v2/clusters/"

Sample API Request
..................

It is the same as the syntax as there is no variable in the API.