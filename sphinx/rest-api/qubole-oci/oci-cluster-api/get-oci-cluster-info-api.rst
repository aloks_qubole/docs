.. _get-oracle-oci-cluster-info-api:

============================
View a Cluster on Oracle OCI
============================

..  http:get:: /api/v2/clusters/<cluster ID>

Use this API to view a specific cluster's information/configuration.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a cluster. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
None

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   "https://oraclecloud.qubole.com/api/v2/clusters/<cluster-ID>"

Sample API Request
..................
Here is a sample request to view the cluster configuration that has 100 as its ID.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   "https://oraclecloud.qubole.com/api/v2/clusters/100"