.. _edit-oracle-oci-cluster-api:

============================
Edit a Cluster on Oracle OCI
============================


..  http:put:: /api/v2/clusters/<cluster-id>/<cluster label>

Use this API to edit a cluster that is on Oracle Cloud Infrastructure.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a cluster. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
:ref:`oracle-oci-cluster-api-parameters` describes the list of parameters of a cluster. You can change the name of the
cluster.

**While editing a cluster, all parameters are optional**.

Request API Syntax
------------------
:ref:`oracle-oci-cluster-request-api-syntax` explains the entire syntax for creating a cluster.
You can add the configuration that needs modification of an existing cluster in the Edit API payload.

Sample API Request
..................
Here is a sample API request to edit a cluster with 1223 as its ID.

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:$X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   -d '{
    "cluster_info": {
             "min_nodes": 1,
             "max_nodes": 8,
             }
    }' \ "https://oraclecloud.qubole.com/api/v2/clusters/1223"