.. _oracle-bmc-cluster-api:

================================================
APIs for Clusters on Oracle Cloud Infrastructure
================================================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-oci-cluster-api.rst
    edit-oci-cluster-api.rst
    clone-oci-cluster-api.rst
    get-oci-cluster-info-api.rst
    list-oci-clusters-api.rst
    start-terminate-oci-cluster.rst
    check-oci-cluster-status-api.rst
    delete-oci-cluster-api.rst