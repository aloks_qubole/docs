from ConfigParser import SafeConfigParser
from qds_sdk.qubole import Qubole
from lib.utils import *
from qds_sdk.cluster import *
import pytest
import logging
import sys
import json
from os import environ
import requests


def pytest_addoption(parser):
    """Add option --suite if you want to run specific suites.

    E.g. --suite=storage or use all
    Add option --env to specify environment found in setup.cfg
    """
    parser.addoption('--env', dest='environment', default='qa',
                     help='Specify environment: "qa", "staging", "multicluster"')
    parser.addoption('--env_version', dest='env_version', default='v1.2',
                     help='Api version')


def load_settings():
    """Settings read from the setup.cfg file

    TODO: Module level/suite level setup.cfg
    """
    sparse = SafeConfigParser()
    sparse.read('../../../tests/doc_tests/setup.cfg')
    return sparse


def pytest_configure(config):

    cfg = load_settings()
    Qubole.configure(
        api_token=cfg.get(config.option.environment, 'auth_token'),
        api_url=cfg.get(config.option.environment, 'api_url'),
        skip_ssl_cert_check=True
    )

@pytest.fixture(scope="function")
def config(request):
    """ Read all the configuration from setup.cfg and return a dict

    Provided to tests that mark the fixture for use
    """
      # turn off logging from requests module
    requests_log = logging.getLogger("requests")
    requests_log.setLevel(logging.ERROR)
    cfg = load_settings()
    settings =dict(cfg._sections[request.config.option.environment])
    settings['version'] = str(request.config.option.env_version)
    return settings

@pytest.fixture(scope="function")
def logger(request):
    LOG = logging.getLogger(request.function.__name__)
    LOG.setLevel(logging.DEBUG)
    stream = logging.StreamHandler(sys.stdout)
    stream.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream.setFormatter(formatter)
    LOG.addHandler(stream)
    return LOG

@pytest.fixture
def cluster_info(config):
    """ set basic cluster ClusterInfo
    """
    clabel = random_sequence()+"_doc_test"
    cluster_settings = ClusterInfo(
        label=clabel,
        aws_access_key_id=environ.get('TES_ACCESS_KEY_ID'),
        aws_secret_access_key=environ.get('TES_SECRET_ACCESS_KEY')
        )
    cluster_settings.set_ec2_settings(
        aws_region='us-east-1'
    )

    master_instance_type = 'c3.large'
    slave_instance_type = 'c3.large'

    cluster_settings.set_hadoop_settings(
        master_instance_type=master_instance_type,
        slave_instance_type=slave_instance_type,
        initial_nodes=1,
        max_nodes=2,
        slave_request_type='spot',

    )
    cluster_settings.set_spot_instance_settings(
        timeout_for_request=10,
        maximum_bid_price_percentage=100)
    cluster_settings.set_stable_spot_instance_settings(
        timeout_for_request=10,
        maximum_bid_price_percentage=150,
        allow_fallback=False)

    return cluster_settings
