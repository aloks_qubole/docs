.. _rest-api-index:

##################
REST API Reference
##################

The following topics describe Qubole REST APIs. You can also use these functions interactively via a GUI; see the
:ref:`user-guide-index` and the :ref:`admin-guide-index`.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    api_overview.rst
    qubole-endpoints.rst
    account_api/index.rst
{% if cloud.lower() == "none" %}
    account_api_v2/index.rst
{% endif %}
    apps_api/index.rst
    cluster_api/index.rst
    command_api/index.rst
    command_template_api/index.rst
    custom_metastore_api/index.rst
    dbtap_api/index.rst
    folder_api/index.rst
    groups-api/index.rst
    hive_metadata_api/index.rst
    notebook_api/index.rst
    dashboard_api/index.rst
    object_policy-api/index.rst
    package-management-api/index.rst
    reports_api/index.rst
    roles-api/index.rst
    scheduler_api/index.rst
    sensor_api/index.rst
    users-api/index.rst
    qubole-azure/index.rst
    qubole-oci/index.rst



