.. _invite-user-api:

=================================
Invite a User to a Qubole Account
=================================
.. http:post:: /api/v1.2/users/invite_new

This API is used to invite new users to a Qubole account. New users are invited by an existing Qubole user of a Qubole
account. After new users are added to the Qubole account, they become part of the *system-users* group by default.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows inviting users to an account.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.


Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **invitee_email**         | Email address of the new user, who should be added to the Qubole account.                |
+---------------------------+------------------------------------------------------------------------------------------+
| **account**               | Account ID of the current user                                                           |
+---------------------------+------------------------------------------------------------------------------------------+
| groups                    | This parameter is used to add groups to the new user. By default, a new user is added to |
|                           | to the *system-user* group.                                                              |
+---------------------------+------------------------------------------------------------------------------------------+
| user_type                 | This parameter defines the type of user to be created. Applicable values are *service* or|
|                           | *regular*. By default, a *regular* user is created.                                      |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the API request syntax.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"invitee_email":"<qubole email address>","account":"<account-ID>","groups":"<groups>", "user_type":"service" }' \
    "https://api.qubole.com/api/v1.2/users/invite_new"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"invitee_email":"user@qubole.com","account":"4332","groups":"system-admin" }' \
    "https://api.qubole.com/api/v1.2/users/invite_new"
