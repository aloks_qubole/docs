.. _enable-user-api:

=================================
Enable a User in a Qubole Account
=================================
.. http:post:: /api/v1.2/accounts/enable_qbol_user

This API is used to enable a user in a particular Qubole account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows enabling users.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **qbol_user_id**          | ID of the user, who should be enabled. Or, it can also be the email address of the user. |
|                           | :ref:`view-users-api` describes how to get the                                           |
|                           | users. Send the `get_users` API request. From the successful                             |
|                           | response, trace the ``user-id`` and ``email``, and use its ``id`` value.                 |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the Request API syntax.

.. sourcecode:: bash

    curl  -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"qbol_user_id" : "<id>/<user-email-address>"}' \ "https://api.qubole.com/api/v1.2/accounts/enable_qbol_user"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

To get the ``qbol_user_id``, perform the following steps:

1. Send the GET API request, GET /api/v1.2/accounts/get_users as described in :ref:`view-users-api`.
2. A successful API response contains multiple values. For example, the values may look like
   ``{ id: xyz, ... user_id: abcd, email:abc@.. }``. Use the value that is with the ``id`` parameter.

Sample Request
..............
Here is a sample request.

.. sourcecode:: bash

    curl  -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"qbol_user_id" : "4"}' \ "https://api.qubole.com/api/v1.2/accounts/enable_qbol_user"

Here is a sample request with the user's email address.

.. sourcecode:: bash

    curl  -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"qbol_user_id" : "user@qubole.com"}' \ "https://api.qubole.com/api/v1.2/accounts/enable_qbol_user"
