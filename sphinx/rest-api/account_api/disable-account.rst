.. _disable-account-api:

=====================
Disable a QDS Account
=====================
Use this API to disable a specific Qubole account. A user having access to multiple Qubole accounts can still access
other Qubole accounts except the disabled account. For enabling the disabled-account, create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.
This feature is currently supported only through the API.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.

Parameters
----------
.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| **id**                    | Account ID of the account that you want to disable.                                                         |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"id":"<Account-Id>"}' \ "https://api.qubole.com/api/v1.2/accounts/disable"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request API to delete a Qubole account with ID 23.

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json"
   -d '{"id":"23"}' \ "https://api.qubole.com/api/v1.2/accounts/disable"