{% if cloud.lower() == "notgcp" %}
.. _account-ssh-key-api:

=====================================
View the Account-level Public SSH Key
=====================================
{% else %}
.. _account-ssh-key-api:

=====================================
View the Account-level Public SSH Key {{ cloud }}-{{ cloud }}
=====================================
{% endif %}

..  http:get:: /api/v1.2/accounts/ssh_key

Use this API to view the account-level public SSH key set in a QDS account. Qubole expects this key to be present on the
bastion node if the Unique SSH key feature is enabled otherwise QDS cannot connect to the bastion host.

The public SSH key is used while configuring the bastion host in a Virtual Private Cloud (VPC). Qubole communicates with
the cluster that is on a private subnet in the VPC through the bastion host. This happens only if the Unique SSH
Key feature is enabled on the QDS account. If it is not enabled, then the default Qubole public key is used to SSH into
the bastion host. For more information, see :ref:`clusters-in-vpc`.

You must add this public SSH key to the bastion host by appending ``~/.ssh/authorized_keys`` for an ``ec2-user``. For
more information, see :ref:`clusters-in-vpc`.

.. important:: The account-level unique SSH key feature is enabled by default on https://us.qubole.com  and https://in.qubole.com.
               If you have the QDS account on any other QDS environment, then create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
               to get it enabled.

Parameters
----------
None

Request API Syntax
------------------

.. sourcecode:: bash

       curl -X GET -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
       "https://<QDS environment>/api/v1.2/accounts/ssh_key"

.. note:: The above syntax uses the QDS environment as the placeholder. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................

.. only:: aws

   Hello i am aws-account-ssh

.. only:: gcp

   Hello i am gcp-account-ssh

.. only:: oracle

   Hello i am oracle-account-ssh

.. only:: azure

  Hello i am azure-account-ssh

.. only:: notgcp
   
  Hello i am notgcp-account-ssh

{% if cloud.lower() == "notgcp" %}
Hello i am notgcp-account-ssh by jinja
{% elif cloud.lower() == "gcp" %}
Hello i am gcp-account-ssh by jinja
{% endif %}

Here is a sample request to see the account-level public SSH key of a QDS account in the https://us.qubole.com environment.

.. sourcecode:: bash

       curl -X GET -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
       "https://us.qubole.com/api/v1.2/accounts/ssh_key"