.. _view-qcuh-monthly-usage:

=================================
View QCUH and Monthly Usage (AWS)
=================================

The APIs to view Qubole Computer Unit Hour (QCUH) and monthly usage details of a specific AWS account are described in the
following section.

View Qubole Compute Unit Hour
-----------------------------

.. http:get::  /api/v1.2/accounts/qcuh_customer

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing an account's details. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Use this API to see the QCUH per account.

Parameters
----------
.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------+------------------------------------------------------------------------------------------------------------------+
| Parameter             | Description                                                                                                      |
+=======================+==================================================================================================================+
| **acc_id**            | Account ID is optional. If account ID is not specified, the current account's QCUH is displayed.                 |
+-----------------------+------------------------------------------------------------------------------------------------------------------+
| **month**             | Denotes the month in ``MM`` format, for which you want to see the QCUH information.                              |
+-----------------------+------------------------------------------------------------------------------------------------------------------+
| **year**              | Denotes the year in ``YYYY`` format associated with the month for which you want to see the QCUH information.    |
+-----------------------+------------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"acc_id":"$ACC_ID","month":"$MONTH","year":"$YEAR"}' \ "https://api.qubole.com/api/v1.2/accounts/qcuh_customer"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............

Here is a sample request to view QCUH details of account with ID 234.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"acc_id":"234","month":"03","year":"2016"}' \ "https://api.qubole.com/api/v1.2/accounts/qcuh_customer"

View Monthly Usage Information
------------------------------

.. http:get::  /api/v1.2/accounts/monthly_usage

Use this API to see the montly usage details of a QDS account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing an account's details. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| acc_id                    | Account ID is optional. If account ID is not specified, the current account's monthly usage information     |
|                           | is displayed.                                                                                               |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"acc_id":"<account-ID>"}' \ "https://api.qubole.com/api/v1.2/accounts/monthly_usage/"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request to view monthly usage details of account with ID 234.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d {"acc_id":"234"}' \ "https://api.qubole.com/api/v1.2/accounts/monthly_usage/"





