.. _clone-account-api:

===================
Clone a QDS Account
===================

..  http:post:: /api/v1.2/accounts/clone

Use this API to clone a QDS account. You can clone the QDS account users and decide to clone/not clone the clusters from
a parent account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows cloning an account. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| **id**                    | Account ID of the account that is to be cloned                                                              |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **name**                  | Name of the new cloned account. Provide a name to the account.                                              |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| clone_qbol_users          | Set this parameter to ``true`` when you want to associate parent account users with the cloned account as   |
|                           | well. If it is not set, only the user, who clones the account would get associated with the cloned account. |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| generate_external_id      | Generates the new external ID for the cloned account. The available values are ``true`` and ``false``.      |
|                           | This feature is disabled by default. Contact Qubole Support to enable the same for your account.            |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

.. note:: By default, clusters are cloned.


Request API Syntax
------------------

.. sourcecode:: bash

   curl -X POST -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"id":"<Account-Id>", "name":"<account-name>", "clone_qbol_users":"false"}' \ "https://api.qubole.com/api/v1.2/accounts/clone"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request API to clone a QDS account with ID 23.

.. sourcecode:: bash

   curl -X POST -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json"
   -d '{
        "id":"$ACCOUNT_ID",
        "name":"clone acc",
        "clone_qbol_users":false,}' \ "https://api.qubole.com/api/v1.2/accounts/clone/"

Sample Response
...............

.. sourcecode:: bash

    {
         "name": "clone acc",
         "state": "success_create",
         "groups": [
             "system-admin",
             "everyone"
                   ],
         "authentication_token": "$AUTH_TOKEN",
         "account_id": "$ACCOUNT_ID",
         "is_aws_keys_enabled": true,
         "status": "success"
    }
