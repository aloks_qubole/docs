.. _suspend-resume-account:

================================
Suspend and Resume a QDS Account
================================
You can suspend and resume a QDS account through a REST API call.

Suspend a QDS Account
---------------------

.. http:put:: /api/v1.2/accounts/suspend

You can now temporarily suspend an account and block access to non-system-admin users. Only system-admins can work on
the suspended account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows suspending an account. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Request API Syntax
------------------
Here is the syntax for an API request.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"id":"<account_id>"}' \ "https://api.qubole.com/api/v1.2/accounts/suspend"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request to suspend the account with ID 3002.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"id":"3002"}' \ "https://api.qubole.com/api/v1.2/accounts/suspend"


Resume a QDS Account
--------------------

.. http:put:: /api/v1.2/accounts/resume

A system-admin can resume the suspended account and thus restore access to other users.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows resuming a suspended account. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Request API Syntax
------------------
Here is the syntax for an API request.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"id":"<account_id>"}' \ "https://api.qubole.com/api/v1.2/accounts/resume"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request to resume the suspended account with ID 3002.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"id":"3002"}' \ "https://api.qubole.com/api/v1.2/accounts/resume"