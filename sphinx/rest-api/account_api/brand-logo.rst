.. _brand-logo-docs:

============================
Brand Logo and Documentation
============================

.. http:put:: /api/v1.2/accounts/branding

Use this API to configure the logo and documentation links for resellers, who are within the account.

.. note:: This feature is not enabled by default. To enable it on the QDS account, create a ticket with
          `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.

          :ref:`brandinglogo` describes how to add the logo through the UI.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows cloning an account. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| **account_id**            | It is the account ID of the Qubole account for which you need to do branding. Specify the following         |
|                           | branding sub-options:                                                                                       |
|                           |                                                                                                             |
|                           | * **logo**:                                                                                                 |
|                           |                                                                                                             |
|                           |   - **logo_uri**: It is a publicly accessible logo URI image in a **png**/**gif**/**svg**/**jpg** format.   |
|                           |     The image size must be less than 100 KB. It is mandatory for branding the logo.                         |
|                           |     It must have the pixel dimension of (**120px x 48px**).                                                 |
|                           |                                                                                                             |
|                           | * link: You can add a documentation link. It has these two sub options:                                     |
|                           |                                                                                                             |
|                           |   - **link_url**: Specify the documentation URL. It is mandatory for adding a documentation link.           |
|                           |   - link_label: Add a label to describe the documentation URL.                                              |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the syntax for an API request.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json"\
    -d '{"account_id":"<account ID>","logo":{"logo_uri":"<image URI>"}, "link":{"link_url":"<doc-link>",
    "link_label":"<documentation-label>"} }'  \ "https://api.qubole.com/api/v1.2/accounts/branding"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Requests
...............
Here is an example to brand logo and add documentation links.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"account_id":"24","logo":{"logo_uri":"https://www.xyz.com/images/logo.jpg"},
    "link":{"link_url":"https://www.xyz.com/documentation", "link_label":"Documentation"} }' \
    "https://api.qubole.com/api/v1.2/accounts/branding"

Here is an example to only brand the logo.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"account_id":"24","logo":{"logo_uri":"https://www.xyz.com/images/logo.jpg"}}' \
    "https://api.qubole.com/api/v1.2/accounts/branding"

Here is an example to only add documentation. You must brand the logo before adding the documentation.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"account_id":"24", "link":{"link_url":"https://www.xyz.com/documentation", "link_label":"Documentation"} }' \
    "https://api.qubole.com/api/v1.2/accounts/branding"