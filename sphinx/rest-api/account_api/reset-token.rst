.. _reset-token-api:

Reset Authentication Token
==========================

.. http:put:: api/v1.2/accounts/token

Use this API to reset your authentication token. Users with relevant permissions can also use this API to reset the authentication
tokens of other users in the account.


Required Roles
--------------

Users with the following permissions can use this API to reset the authentication tokens of other users:

* A user who is part of the system-admin group.
* A user who has the *manage users* permission. See :ref:`manage-roles` for more information.

.. note:: A user can reset his authentication token if he has the *authentication token* permission on accounts.


Parameters
----------

You must pass at least one of the parameters (*emails* or *groups*) in the API body. If you pass both parameters, a union of the users present in the
*emails* parameter and the users present in the *groups* parameter is considered.

+---------------------+---------------------------------------------------------------------------------------------------------------+
| **Parameter**       | **Description**                                                                                               |
+=====================+===============================================================================================================+
| emails              | Provide the email ID of the user whose authentication token you want to reset. Use comma-separated            |
|                     | values to provide multiple email IDs.                                                                         |
|                     |                                                                                                               |
|                     | .. note:: When resetting multiple tokens using the *emails* parameter, no user token is reset if any of the   |
|                     |           email IDs passed are invalid or not part of the account.                                            |
+---------------------+---------------------------------------------------------------------------------------------------------------+
| groups              | Provide the name of the group whose authentication token you want to reset. Use comma-separated               |
|                     | values to provide multiple groups.                                                                            |
|                     |                                                                                                               |
|                     | .. note:: When used for a group, this resets the authentication tokens of **all** the users present in that   |
|                     |          group. In case of any errors, no user token is reset.                                                |
+---------------------+---------------------------------------------------------------------------------------------------------------+


If your authentication token is successfully reset (by you or by another user), you will receive a confirmation email.


Request API Syntax
------------------

.. sourcecode:: bash

     curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
     -d {"emails": "<emails>", "groups": "<groups>"}' \ "https://api.qubole.com/api/v1.2/accounts/token"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Other endpoints to access QDS are described in :ref:`qubole-endpoints`.

Sample Requests
---------------

Sample Request Using both the Parameters
........................................

.. sourcecode:: bash

    curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -H 'cache-control: no-cache' -d '{"emails": "test_1@domain.com,test_2@domain.com", "groups": "group_1,group_2"}' \
    "https://api.qubole.com/api/v1.2/accounts/token"

Sample Request Using only the emails Parameter
..............................................

.. sourcecode:: bash

    curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -H 'cache-control: no-cache' -d '{"emails": "test_1@domain.com"}' \ "https://api.qubole.com/api/v1.2/accounts/token"


Sample Request Using only the groups Parameter
..............................................

.. sourcecode:: bash

    curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -H 'cache-control: no-cache' -d '{"groups": "group_1}' \ "https://api.qubole.com/api/v1.2/accounts/token"



Sample Response
------------------
.. sourcecode:: bash

    {
       "message": "Tokens updated successfully"
    }

