.. _sse-account-api:

===================================
Enable SSE on the QDS Control Plane
===================================

.. http:put:: /api/v1.2/account

Use this API to enable SSE-KMS on the QDS Control Plane.

The QDS Control Plane denotes all the components except the clusters.

For more details, see :ref:`enabling-sse-kms`. Refer to :ref:`data-encryption-matrix` to see the mapping of SSE
with different engines and the QDS Control Plane.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| **sse_algo**              | It is the algorithm used for encryption. Currently, Qubole supports ``AES256`` and ``KMS`` on the QDS       |
|                           | Control Plane. Qubole only supports configuring ``SSE-KMS`` for this parameter (that is through this API).  |
|                           | To set SSE=AES256 on the QDS Control Plane, you must create a ticket with                                   |
|                           | `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.                                                   |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **sse_key**               | Use this parameter to specify the key for encrypting data. If you do not specify the key, then              |
|                           | the default S3 KMS key is used. You must set this parameter to the specific KMS key ID if you do not want   |
|                           | the default S3 KMS key.                                                                                     |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{ "sse_algo":"SSE-KMS", "sse_key":"<sse-kms-key>" }'  \ "https://api.qubole.com/api/v1.2/account/"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json"
   -d '{ "sse_algo":"SSE-KMS", "sse_key":"<sse-kms-key>" }'  \ "https://api.qubole.com/api/v1.2/account/"