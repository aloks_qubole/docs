.. _view-users-api:

===========================
View Users of a QDS Account
===========================

..  http:get:: /api/v1.2/accounts/get_users

Use this API to view users of a QDS account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing an account's details. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/accounts/get_users"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

View Pending Users of a QDS account
-----------------------------------
..  http:get:: /api/v1.2/accounts/get_pending_users

Use this API to view pending users of a QDS account, who are awaiting an approval. You must have administration
privileges to see the list of pending users for a QDS account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing an account's details. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.


.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/accounts/get_pending_users"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

View User Emails
----------------
..  http:get:: /api/v1.2/accounts/get_user_emails

Use this API to get the users' emails associated with the current QDS account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing an account's details. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/accounts/get_user_emails"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.