..  _convert-notebook-api:

Submit a Notebook Convert Command
=================================

..  http:post::  /api/v1.2/commands/

Use this API to submit a Notebook convert command to download or email notebooks and dashboards in PDF, PNG, and HTML formats.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-------------------+----------------------------------------------------------------------------------------------------+
| Parameters        | Description                                                                                        |
+===================+====================================================================================================+
| **command_type**  | NotebookConvertCommand                                                                             |
+-------------------+----------------------------------------------------------------------------------------------------+
| **resourceId**    | Id of the notebook or dashboard to be converted.                                                   |
+-------------------+----------------------------------------------------------------------------------------------------+
| **resourceType**  | ``Note`` or ``NotebookDashboard``                                                                  |
+-------------------+----------------------------------------------------------------------------------------------------+
| **fileFormat**    | Format to which the notebook or dashboard has to be converted.                                     |
|                   | The supported values are ``pdf``, ``html``, and ``png``.                                           |
+-------------------+----------------------------------------------------------------------------------------------------+
| **mode**          | mail or attachment                                                                                 |
+-------------------+----------------------------------------------------------------------------------------------------+
| **mailIds**       | List of comma separated of email addresses to which converted notebook or dashboard has to be sent.|
+-------------------+----------------------------------------------------------------------------------------------------+

Examples
~~~~~~~~

Example 1: Python API Framework
................................

**Notebook**

The following sample code shows how to email a notebook as a PNG file.

.. sourcecode:: python

   import sys
   import pycurl
   import json
   c= pycurl.Curl()
   url="https://api.qubole.com/api/v1.2/commands"
   auth_token = <provide auth token here>
   c.setopt(pycurl.URL, url)
   c.setopt(pycurl.HTTPHEADER, ["X-AUTH-TOKEN: "+ auth_token, "Content-Type:application/json", "Accept: application/json"])
   c.setopt(pycurl.POST,1)

   data=json.dumps({"resourceId":15, "resourceType":"Note", "fileFormat": "png", "mode":"mail","mailIds":"test@xyz.com,random@xyz.com", "command_type":"NotebookConvertCommand"})

   c.setopt(pycurl.POSTFIELDS, data)
   c.perform()


The following sample code shows how to download a notebook as a PNG file.

.. sourcecode:: python

   import sys
   import pycurl
   import json
   c= pycurl.Curl()
   url="https://api.qubole.com/api/v1.2/commands"
   auth_token = <provide auth token here>
   c.setopt(pycurl.URL, url)
   c.setopt(pycurl.HTTPHEADER, ["X-AUTH-TOKEN: "+ auth_token, "Content-Type:application/json", "Accept: application/json"])
   c.setopt(pycurl.POST,1)

   data=json.dumps({"resourceId":15, "resourceType": "Note","fileFormat": "png", "command_type":"NotebookConvertCommand"})

   c.setopt(pycurl.POSTFIELDS, data)
   c.perform()


**Dashboard**

The following sample code shows how to download a dashboard as a PNG file.

.. sourcecode:: python

   import sys
   import pycurl
   import json
   c= pycurl.Curl()
   url="https://api.qubole.com/api/v1.2/commands"
   auth_token = <provide auth token here>
   c.setopt(pycurl.URL, url)
   c.setopt(pycurl.HTTPHEADER, ["X-AUTH-TOKEN: "+ auth_token, "Content-Type:application/json", "Accept: application/json"])
   c.setopt(pycurl.POST,1)

   data=json.dumps({"resourceId":15, "resourceType": "NotebookDashboard","fileFormat": "png", "command_type":"NotebookConvertCommand"})

   c.setopt(pycurl.POSTFIELDS, data)
   c.perform()


Example 2: Using Command Line Parameter
.......................................

**Notebook**

The following cURL command shows how to email a notebook as a PNG file.

.. sourcecode:: bash

   curl  -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"resourceId":"15", "resourceType":"Note", "fileFormat":"png", "command_type":"NotebookConvertCommand",
   "mode":"mail", "mailIds": "test@xyz.com,random@xyz.com"}' \
   "https://api.qubole.com/api/v1.2/commands"


The following cURL command shows how to download a notebook as a PNG file.

.. sourcecode:: bash

   curl  -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"resourceId":"15", "resourceType":"Note", "fileFormat":"png", "command_type":"NotebookConvertCommand"}' \
    "https://api.qubole.com/api/v1.2/commands"


**Dashboard**

The following cURL command shows how to download a dashboard as a PNG file.

.. sourcecode:: bash

   curl  -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"resourceId":"15", "resourceType": "NotebookDashboard", "fileFormat":"png", "command_type":"NotebookConvertCommand"}' \
    "https://api.qubole.com/api/v1.2/commands"

See :ref:`view-command-status` to check the status of the ``NotebookConvertCommand`` command and :ref:`view-command-results` to download the converted files.