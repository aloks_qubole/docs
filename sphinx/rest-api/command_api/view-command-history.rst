.. _view-command-history:

View the Command History
========================

..  http:get:: /api/v1.2/commands/

Use this API to view the command history.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing the command history. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.


View Queries by User ID
^^^^^^^^^^^^^^^^^^^^^^^

+-----------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Resource URI          | commands/                                                                                                                                                                                                                  |
+-----------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Request Type          | GET                                                                                                                                                                                                                        |
+-----------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Supporting Versions   | v1, v1.2                                                                                                                                                                                                                   |
+-----------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Return Value          | This curl request will return a JSON object containing the Command Objects with all its attributes as described above. Additionally returned JSON object contained the next\_page, previous page, and per page parameter.  |
+-----------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

 
Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+--------------------------+---------------------------------------------------------------------------------------------------+
| Parameter                | Description                                                                                       |
+==========================+===================================================================================================+
| page                     | It is the number of pages that contain the commands' history. Its value is an integer.            |
+--------------------------+---------------------------------------------------------------------------------------------------+
| per_page                 | Its value is an integer and it is the number of commands to be retrieved per page. Its **maximum**|
|                          | value can be ``100``. Retrieve the next 100 commands based on the last command ID for a given QDS |
|                          | account.                                                                                          |
+--------------------------+---------------------------------------------------------------------------------------------------+
| all_users                | By default it is set to `0`. Set it to `1` to get the command history of all users.               |
+--------------------------+---------------------------------------------------------------------------------------------------+
| include_query_properties | This parameter is set to ``false`` by default. Setting it to ``true`` displays query              |
|                          | properties such as **tags** and **query history comments**.                                       |
+--------------------------+---------------------------------------------------------------------------------------------------+
| start_date               | The date from which you want the command history (inclusive). The API default is 30 days before   |
|                          | the end date. This parameter also supports timestamp in the UTC timezone (YYYY-MM-DDTHH:MM:SSZ)   |
|                          | format.                                                                                           |
+--------------------------+---------------------------------------------------------------------------------------------------+
| end_date                 | The date until which you want the command history (inclusive). The API default is *today*. This   |
|                          | parameter also supports timestamp in the UTC timezone (YYYY-MM-DDTHH:MM:SSZ) format.              |
+--------------------------+---------------------------------------------------------------------------------------------------+

Sample API Requests
-------------------
Examples:

To get last 10 commands for current user:

::

    curl -i -X GET -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:application/json" -H "Accepts:application/json" \
    "https://api.qubole.com/api/v1.2/commands"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

(Pagination) To get results 10-12 for current user (4th page with 3 results per page):

::

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"page":"4", "per_page":"3"}' "https://api.qubole.com/api/v1.2/commands"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

or

::

    curl -i -H "X-AUTH-TOKEN: $AUTH_TOKEN" "https://api.qubole.com/api/v1.2/commands?page=4&per_page=3"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

The sample response for the paginated call will be:

::

    {
       “paging_info":{"previous_page":3,"next_page":5,"per_page":3},
       “commands”: [{<standard command object as described in create a command>}, ..]
    }