.. _cancel-a-command:

Cancel a Command
================

..  http:put:: /api/v1.2/commands/(int:id)
 
This API is used to cancel a command.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.


Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+--------------+---------------+
| Parameter    | Description   |
+==============+===============+
| **status**   | kill          |
+--------------+---------------+

Example
-------

.. sourcecode:: bash


    curl  -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"status":"kill"}' \ "https://api.qubole.com/api/v1.2/commands/${QUERYID}"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.