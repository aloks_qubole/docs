.. _view-command-logs:

View Command Logs
=================

.. http:get:: /api/v1.2/commands/(int:command_id)/logs


Retrieves the log (i.e. stderr) of the command (`command_id`).

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing commands' logs. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Response
--------

The response is raw text containing the log of the command.

For **Workflow** commands, the ``sequence_number`` parameter enables downloading of the logs of a workflow subcommand.

Example
-------

**Goal**

To view the logs of command, example QUERYID=1234


.. sourcecode:: bash

     curl  -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" 
     -H "Content-Type: application/json"
     -H "Accept: text/plain"
     "https://api.qubole.com/api/v1.2/commands/${QUERYID}/logs"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: bash

    Total MapReduce jobs = 1
    Getting Hadoop cluster information ...
    Cluster not found - provisioning cluster machines ...
    Waiting for Hadoop to come up ...
    Launching Job 1 out of 1
    Number of reduce tasks determined at compile time: 1
    In order to change the average load for a reducer (in bytes):
      set hive.exec.reducers.bytes.per.reducer=<number>
    In order to limit the maximum number of reducers:
      set hive.exec.reducers.max=<number>
    In order to set a constant number of reducers:
      set mapred.reduce.tasks=<number>
    ......
    ......
    ......
    Hadoop job information for Stage-1: number of mappers: 2; number of reducers: 1
    2012-10-11 16:57:30,346 Stage-1 map = 0%,  reduce = 0%
    2012-10-11 16:57:59,725 Stage-1 map = 1%,  reduce = 0%
    2012-10-11 16:58:02,784 Stage-1 map = 8%,  reduce = 0%
    ......
    ......
    ......
    2012-10-11 16:59:46,147 Stage-1 map = 99%,  reduce = 0%, Cumulative CPU 73.01 sec
    2012-10-11 16:59:47,159 Stage-1 map = 99%,  reduce = 0%, Cumulative CPU 73.01 sec
    2012-10-11 16:59:48,172 Stage-1 map = 99%,  reduce = 0%, Cumulative CPU 73.01 sec
    2012-10-11 16:59:49,183 Stage-1 map = 100%,  reduce = 0%, Cumulative CPU 73.01 sec
    2012-10-11 16:59:50,195 Stage-1 map = 100%,  reduce = 0%, Cumulative CPU 73.01 sec
    2012-10-11 16:59:51,207 Stage-1 map = 100%,  reduce = 0%, Cumulative CPU 73.01 sec
    2012-10-11 16:59:52,218 Stage-1 map = 100%,  reduce = 0%, Cumulative CPU 73.01 sec
    .....
    .....
    .....
    2012-10-11 17:00:06,655 Stage-1 map = 100%,  reduce = 17%, Cumulative CPU 115.15 sec
    2012-10-11 17:00:07,668 Stage-1 map = 100%,  reduce = 100%, Cumulative CPU 115.15 sec
    2012-10-11 17:00:08,684 Stage-1 map = 100%,  reduce = 100%, Cumulative CPU 119.52 sec
    MapReduce Total cumulative CPU time: 1 minutes 59 seconds 520 msec
    Ended Job = job_14.201210111655_0001
    1 Rows loaded to s3n://paid-qubole/......
    MapReduce Jobs Launched: 
    Job 0: Map: 2  Reduce: 1   Accumulative CPU: 119.52 sec   HDFS Read: 0 HDFS Write: 0 SUCESS
    Total MapReduce CPU Time Spent: 1 minutes 59 seconds 520 msec
    OK
    Time taken: 303.329 seconds