.. _command-api:

###########
Command API
###########

.. toctree::
    :maxdepth: 1
    :titlesonly:

    command-object

    cancel-a-command
    submit-a-db-export-command
    submit-a-db-import-command
    submit-a-db-tap-query-command
    submit-a-hive-command
    submit-a-hadoop-jar-command
    submit-a-hadoop-s3distcp-command
    submit-a-pig-command
    submit-a-presto-command
    submit-a-refresh-table-command
    submit-a-shell-command
    submit-a-spark-command
    submit-a-composite-command
    convert-notebook
    view-command-history
    view-command-jobs
    view-command-logs
    view-command-results
    view-command-status
    view-command-error-logs
    view-status-with-results
