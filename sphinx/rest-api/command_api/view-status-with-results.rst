.. _view-command-status-with-results-api:

================================
View Command Status with Results
================================

.. http:get:: /api/v1.2/commands/<command-ID>/status_with_results

Use this API to view the command status and its result.

For a running command, the response returns status and progress fields. For a completed command, the response returns
results and path fields.

The response contains either the results inline (TAB separated) or contains an S3 directory location that contains the
actual results. The response returns the packaged and inline results when the command output is less than 20 MB
and contains less than 700 files.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing command status. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values. Presto is not currently supported on all Cloud platforms; see :ref:`os-version-support`.

+-------------+-----------------------------------------------------------------------------------------------------------------+
| Parameter   | Description                                                                                                     |
+=============+=================================================================================================================+
| raw         | By default, it is set to ``false``. Set it to ``true`` to see the result as is without converting delimiters    |
|             | (^A) into tabs. This works well for Presto query results. However, for a Hive command with number of result     |
|             | rows less than 1000, then delimiters are still tabs.                                                            |
+-------------+-----------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the request API syntax.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/commands/${QUERYID}/status_with_results"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Requests
...................
Here is a sample request to view results of a command with 546935 as its ID.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/commands/546935/status_with_results"

Response
........

.. sourcecode:: bash

    {
      "id": "546935",
      "status": "done",
      "progress": 100,
      "error": null,
      "inline": true,
      "results": "default_qubole_airline_origin_destination\r\ndefault_qubole_memetracker\r\n",
      "qlog": "{\"QBOL-QUERY-SCHEMA\":{\"-1\":[{\"ColumnType\":\"string\",\"ColumnName\":\"tab_name\"}]}}",
      "path": "/tmp/2016-04-21/4483/546935"
    }


Here is a sample request to view results of a command with 573178 as its ID.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/commands/573178/status_with_results"

Response
........

.. sourcecode:: bash

    {
      "id": "573178",
      "status": "waiting",
      "progress": 0
    }