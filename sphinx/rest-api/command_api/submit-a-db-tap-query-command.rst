.. _submit-a-db-tap-query-command:

Submit a DB Tap Query Command
=============================

..  http:post:: /api/v1.2/commands/

This API is to submit a DB Tap query.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter                | Description                                                                                                                                                                                  |
+==========================+==============================================================================================================================================================================================+
| **query**                | Specify DB Tap query to run.                                                                                                                                                                 |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **db\_tap\_id**          | Specify the DB Tap id of the target database to run the query on.                                                                                                                            |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **command\_type**        | DbTapQueryCommand                                                                                                                                                                            |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| macros                   | Expressions to evaluate macros used in the DB Tap Query command. Refer to :ref:`macros_in_scheduler` for more details.                                                                       |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| name                     | Add a name to the command that is useful while filtering commands from the command history. It does not accept **& (ampersand)**, **< (lesser than)**, **> (greater than)**,                 |
|                          | **" (double quotes),** and **' (single quote)** special characters, and HTML tags as well. It can contain a maximum of 255 characters.                                                       |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| tags                     | Add a tag to a command so that it is easily identifiable and searchable from the commands list in the **Commands History**. Add a tag as a filter value while searching commands.            |
|                          | It can contain a maximum of 255 characters. A comma-separated list of tags can be associated with a single command.                                                                          |
|                          | While adding a tag value, enclose it in square brackets. For example, ``{"tags":["<tag-value>"]}``.                                                                                          |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| macros                   | Denotes the macros that are valid assignment statements containing the variables and its expression as: ``macros: [{"<variable>":<variable-expression>}, {..}]``. You can add more than one  |
|                          | variable. For more information, see :ref:`using-macros`.                                                                                                                                     |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| timeout                  | It is a timeout for command execution that you can set in seconds. Its default value is 129600 seconds (36 hours). QDS checks the timeout for a command every 60 seconds. If the timeout is  |
|                          | set for 80 seconds, the command gets killed in the next minute that is after 120 seconds. By setting this parameter, you can avoid the command from running for 36 hours.                    |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| script_location          | Denotes the AWS S3 location of the query.                                                                                                                                                    |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Example
'''''''

**Goal:** Show tables

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"command_type": "DbTapQueryCommand", "db_tap_id":"1",  "query":"show tables"}' \ "https://api.qubole.com/api/v1.2/commands"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

   HTTP/1.1 200 OK
   Content-Type: application/json; charset=utf-8

    {
     "timeout":null,
     "qlog":null,
     "status":"waiting",
     "meta_data":{
      "results_resource":"commands/000000/results",
      "logs_resource":"commands/000000/logs"
     },
     "account_id":00,
     "user_id":00,
     "command_source":"API",
     "command":{
      "db_tap_id":1,
      "query":"show tables",
      "md_cmd":null
     },
     "pool":null,
     "can_notify":false,
     "end_time":null,
     "command_type":"DbTapQueryCommand",
     "label":"default",
     "pid":null,
     "progress":0,
     "num_result_dir":-1,
     "created_at":"2014-12-24T09:01:27Z",
     "submit_time":1419411687,
     "name":null,
     "start_time":null,
     "template":"generic",
     "resolved_macros":null,
     "path":"/tmp/2014-12-24/234/000000",
     "id":000000,
     "qbol_session_id":null
    }