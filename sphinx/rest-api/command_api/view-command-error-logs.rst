.. _view-command-error-logs:

=======================
View Command Error Logs
=======================

.. http:get:: /api/v1.2/commands/<Command-ID>/error_logs

Use this API to see the failed command's error logs. However, this API is currently supported only on **Spark Scala**
and **Spark Command-Line** commands.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing commands' logs. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/commands/<Command-ID>/error_logs"

Sample API Request
..................
Here is a sample request of a failed Spark Scala command with 1200 as its ID.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/commands/1200/error_logs"

Here is the sample response.

.. sourcecode:: bash

    {
      "error_log": "org.apache.spark.sql.catalyst.analysis.NoSuchTableException: Table or view 'products_orc_202' not found in database 'default';"
    }