.. _submit-a-presto-command:

Submit a Presto Command
=======================

.. http:post:: /api/v1.2/commands/

This API is used to submit a Presto command.

.. note:: Presto is not currently supported on all Cloud platforms; see :ref:`os-version-support`. Presto supports querying
          tables backed by the storage handlers.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter              | Description                                                                                                                                                                                              |
+========================+==========================================================================================================================================================================================================+
| **query**              | Specify Presto query to run. Either the query or the script_location is required.                                                                                                                        |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **script\_location**   | Specify a S3 path where the presto query to run is stored. Either the query or the script_location is required. The AWS storage credentials stored in the account are used to retrieve the script file.  |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **command\_type**      | Presto command                                                                                                                                                                                           |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|  label                 | Specify the cluster label on which this command is to be run.                                                                                                                                            |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|  retry                 | Denotes the number of retries for a job. Valid values of ``retry`` are 1, 2, and 3.                                                                                                                      |
|                        |                                                                                                                                                                                                          |
|                        | .. caution:: Configuring retries will just do a blind retry of a Presto query. This may lead to data corruption for non-Insert Overwrite Directory (IOD) queries.                                        |
|                        |                                                                                                                                                                                                          |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| name                   | Add a name to the command that is useful while filtering commands from the command history. It does not accept **& (ampersand)**, **< (lesser than)**, **> (greater than)**,                             |
|                        | **" (double quotes),** and **' (single quote)** special characters, and HTML tags as well. It can contain a maximum of 255 characters.                                                                   |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| tags                   | Add a tag to a command so that it is easily identifiable and searchable from the commands list in the **Commands History**. Add a tag as a filter value while searching commands.                        |
|                        | It can contain a maximum of 255 characters. A comma-separated list of tags can be associated with a single command. While adding a tag value, enclose it in square brackets. For example,                |
|                        | ``{"tags":["<tag-value>"]}``.                                                                                                                                                                            |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| macros                 | Denotes the macros that are valid assignment statements containing the variables and its expression as: ``macros: [{"<variable>":<variable-expression>}, {..}]``. You can add more than one variable.    |
|                        | For more information, see :ref:`using-macros`.                                                                                                                                                           |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| timeout                | It is a timeout for command execution that you can set in seconds. Its default value is 129600 seconds (36 hours). QDS checks the timeout for a command every 60 seconds. If the timeout is              |
|                        | set for 80 seconds, the command gets killed in the next minute that is after 120 seconds. By setting this parameter, you can avoid the command from running for 36 hours.                                |
+------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

:ref:`presto-system-metrics` describes the list of metrics that can be seen on the Datadog monitoring service. It
also describes the abnormalities and actions that you can perform to handle abnormalities.

Examples
~~~~~~~~

Show tables
^^^^^^^^^^^

.. sourcecode:: bash

   curl  -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"query":"show tables;", "command_type": "PrestoCommand"}' \ "https://api.qubole.com/api/v1.2/commands"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

.. note:: For a given Presto query, a new Presto Query Tracker is displayed in the **Logs** tab when:

          * A cluster instance that ran the query is still up.
          * The query info is still present in the Presto server in that cluster instance. The query information is
            periodically purged from the server.

          If any of the above 2 conditions is not met, the older Presto Query Tracker is displayed in the **Logs** tab.

**Sample Response**

.. sourcecode:: json

    { "command": 
      { "query": "show tables", }, 
      "qbol_session_id": 0000, 
      "created_at": "2014-01-21T16:01:09Z", 
      "user_id": 00, 
      "status": "waiting", 
      "command_type": "PrestoCommand", 
      "id": 4850, 
      "progress": 0, 
      "meta_data": { 
        "results_resource": "commands/4850/results", 
        "logs_resource": "commands/4850/logs" 
      } 
    }


Count Number of Rows
^^^^^^^^^^^^^^^^^^^^

.. sourcecode:: bash

   export QUERY="select count(*) as num_rows from miniwikistats;" curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{\"query\":\"$QUERY\", \"command_type\" : \"PrestoCommand\" }' \ "https://api.qubole.com/api/v1.2/commands/"

**Sample Response**

.. sourcecode:: json

    { 
      "command": { "query": "select count(*) as num_rows from miniwikistats;", }, 
      "qbol_session_id": 0000, 
      "created_at": "2014-10-11T16:54:57Z", 
      "user_id": 00, 
      "status": "waiting", 
      "command_type": "PrestoCommand", 
      "id": 4852, 
      "progress": 0, 
      "meta_data": { 
        "results_resource": "commands/4852/results", 
        "logs_resource": "commands/4852/logs" 
      } 
    }