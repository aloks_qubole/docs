.. _view-command-results:

View Command Results
====================

.. http:get:: /api/v1.2/commands/(int:command_id)/results

This retrieves results for a completed command (`command_id`).

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing command results. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Presto is not currently supported on all Cloud platforms; see :ref:`os-version-support`.

+----------------+-----------------------------------------------------------------------------------------------------------------+
| Parameter      | Description                                                                                                     |
+================+=================================================================================================================+
| raw            | By default, it is set to ``false``. Set it to ``true`` to see the result as is without converting delimiters    |
|                | (^A) into tabs. This works well for Presto query results. However, for a Hive command with number of result     |
|                | rows less than 1000, then delimiters are still tabs.                                                            |
+----------------+-----------------------------------------------------------------------------------------------------------------+
|include_headers | By default, it is set to ``false``. This is an option to view headers in the results.                           |
+----------------+-----------------------------------------------------------------------------------------------------------------+

Response
--------

When the command results in the Amazon S3 directory location are less than 20MB and contain less than 700 files, the result is returned inline
in the JSON response.
When the results are greater 20MB or the number of files is more than 700, the Amazon S3 directory location that contains the
result files is returned.

  ``Status Code 422: Command is not done. Results are unavailable.``

For **Workflow** commands, the ``sequence_number`` parameter enables downloading of the results of a workflow subcommand.


Example
-------

**Goal**

To view the results of command, example QUERYID=1234

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
   -H "Content-Type: application/json" \
   -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/commands/${QUERYID}/results"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

The following is the response, if the result is inlined:

.. sourcecode:: http

   HTTP/1.1 200 OK
   Content-Type: application/json; charset=utf-8

   {"inline":true, "results":"1\t240\r\n2\t300"}


The following is the response, if the result is NOT inlined:

.. sourcecode:: http

   HTTP/1.1 200 OK
   Content-Type: application/json; charset=utf-8

   {"inline":false, "result_location":[ "An array of S3 paths. Directories end with '/' in end" ]}

The following is the response when result is inline and is very large set of files, and shows the complete
path of the results file to download from.

.. sourcecode:: bash

   HTTP/1.1 200 OK
   Content-Type: application/json; charset=utf-8

   { "inline": true, "results": "Too many files to process - download from ... ", "result_location": ["s3://mybucket/results/results..."] }


Example to include headers in the response
------------------------------------------

**Goal**

To view headers in the response, example QUERYID=183560526

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
   -H "Content-Type: application/json" \
   -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/commands/183560526/results?include_headers=true"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

The following is the response:

.. sourcecode:: http

   HTTP/1.1 200 OK
   Content-Type: application/json; charset=utf-8

   {
    "inline": true,
    "results": "itinid\tmktid\tseqnum\tcoupons\tyear\tquarter\torigin\toriginaptind\torigincitynum\torigincountry\
    toriginstatefips\toriginstate\toriginstatename\toriginwac\tdest\tdestaptind\tdestcitynum\tdestcountry\tdeststatefips\
    tdeststate\tdeststatename\tdestwac\tbreak\tcoupontype\ttkcarrier\topcarrier\trpcarrier\tpassengers\tfareclass\tdistance\
    tdistancegroup\tgateway\titingeotype\tcoupongeotype\r\n\"ItinID\"\t\"MktID\"\t\"SeqNum\"\t\"Coupons\"\t\"Year\"\
    t\"Quarter\"\t\"Origin\"\t\"OriginAptInd\"\t\"OriginCityNum\"\t\"OriginCountry\"\t\"OriginStateFips\"\t\"OriginState\
    "\t\"OriginStateName\"\t\"OriginWac\"\t\"Dest\"\t\"DestAptInd\"\t\"DestCityNum\"\t\"DestCountry\"\t\"DestStateFips\"\t\
    "DestState\"\t\"DestStateName\"\t\"DestWac\"\t\"Break\"\t\"CouponType\"\t\"TkCarrier\"\t\"OpCarrier\"\t\"RPCarrier\"\t\
    "Passengers\"\t\"FareClass\"\t\"Distance\"\t\"DistanceGroup\"\t\"Gateway\"\t\"ItinGeoType\"\t\"CouponGeoType\"\r\n\
    "200734005923\"\t\"200737154697\"\t2\t4\t2007\t3\t\"LGA\"\t2\t63760\t\"US\"\t\"36\"\t\"NY\"\t\"New York\"\t22\t\"BOS\"\t0\
    t12200\t\"US\"\t\"25\"\t\"MA\"\t\"Massachusetts\"\t13\t\"X\"\t\"A\"\t\"US\"\t\"US\"\t\"ZW\"\t1.00\t\"X\"\t185.00\t1\t0.00\t2\t2\r\n"
   }