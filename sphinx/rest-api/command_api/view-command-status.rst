.. _view-command-status:

View Command Status
===================

.. http:get:: /api/v1.2/commands/(int:command_id)


Use this API to check the status of any command. A user can check any query for the whole account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing command status. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
-------

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/commands/${QUERYID}"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample response:

.. sourcecode:: json

    {
      "command": {
        "approx_mode": false,
        "approx_aggregations": false,
        "query": "select count(*) as num_rows from miniwikistats;",
        "sample": false
      },
      "qbol_session_id": 0000,
      "created_at": "2012-10-11T16:54:57Z",
      "user_id": 00,
      "status": "done",
      "command_type": "HiveCommand",
      "id": 3852,
      "progress": 100,
      "meta_data": {
        "results_resource": "commands\/3852\/results",
        "logs_resource": "commands\/3852\/logs"
      }
      "throttled": true
    }

.. note:: If the maximum concurrency of the account has been reached, the API contains an additional ``throttled`` field
          set to true. The status of the command will still be in the ``waiting`` state.

When checking the status of a Hadoop command, the JSON response object contains an additional field (``job_url``), the
value of which is URL to JobTracker page specific to this job. A detailed information related to the job such as the
number of mappers and reducers, current status, counters, and so on can be retrieved using this URL.

A command's ``status`` can have one of the following values:

* ``cancelled``
* ``done``
* ``error``
* ``running``
* ``waiting``
