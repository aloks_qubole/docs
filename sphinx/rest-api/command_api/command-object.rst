.. _command-object:

Command Object
==============

Many Command APIs like submitting, canceling or viewing the status,
return a (json encoded) command object. The fields of this object are
detailed below:

+--------------------------------------+--------------------------------------+
| Field                                | Description                          |
+======================================+======================================+
| **id**                               | The ID of the command.               |
+--------------------------------------+--------------------------------------+
| **status**                           | The status of the command can be one |
|                                      | of the following:                    |
|                                      |                                      |
|                                      | -  *waiting*: queued in QDS but      |
|                                      |    not started processing yet        |
|                                      | -  *running*: being processed        |
|                                      | -  *cancelling*: the command is      |
|                                      |    being cancelled in response to a  |
|                                      |    user request                      |
|                                      | -  *cancelled*: command is complete  |
|                                      |    and was cancelled by the user     |
|                                      | -  *error*: command is complete and  |
|                                      |    failed                            |
|                                      | -  *done*: command is complete and   |
|                                      |    was successful                    |
+--------------------------------------+--------------------------------------+
| **command\_type**                    | The type of the command.             |
+--------------------------------------+--------------------------------------+

