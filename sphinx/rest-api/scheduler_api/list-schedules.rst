.. _list-schedules:

List Schedules
==============

.. http:get:: /api/v1.2/scheduler/

This API is used to list all existing schedules created to run commands automatically at certain frequency in a
specified interval.

+-----------------------+---------------------------------------------------------------------+
| Resource URI          | scheduler/                                                          |
+-----------------------+---------------------------------------------------------------------+
| Request Type          | GET                                                                 |
+-----------------------+---------------------------------------------------------------------+
| Supporting Versions   | v1.2                                                                |
+-----------------------+---------------------------------------------------------------------+
| Return Value          | A JSON array of schedules. It displays all schedules in all states. |
+-----------------------+---------------------------------------------------------------------+

.. note:: A ``_SUCCESS`` file is created in the output folder for successful schedules. You can set
          ``mapreduce.fileoutputcommitter.marksuccessfuljobs`` to ``false`` to disable creation of _SUCCESS file
          or to ``true`` to enable creation of the ``_SUCCESS`` file.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing all schedules.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

.. note:: You can use the ``name`` parameter to fetch scheduled jobs by name. The search pattern must contain at least
          3 characters. QDS displays partial and complete matches.

Example
-------

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \ "https://api.qubole.com/api/v1.2/scheduler"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

.. sourcecode:: json

    {
        "paging_info": {
            "next_page": 2,
            "previous_page": null,
            "per_page": 10
        },
        "schedules": [
            {
                "id": 8,
                "name": "8",
                "status": "KILLED",
                "concurrency": 1,
                "frequency": 1,
                "time_unit": "days",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 108,
                "start_time": "2012-07-01 02:00",
                "end_time": "2022-07-01 02:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "UTC",
                "next_materialized_time": null,
                "command": {
                    "query": "select stock_symbol, max(high), min(low), sum(volume) from daily_tick_data where date1='$formatted_date$'",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": null,
                    "loader_stable": null,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {
                    "hive_tables": [
                        {
                            "window_end": "0",
                            "time_zone": "UTC",
                            "window_start": "-1",
                            "interval": {
                                "days": "1"
                            },
                            "name": "daily_tick_data",
                            "initial_instance": "2012-07-01T00:00Z",
                            "columns": {
                                "stock_exchange": [
                                    "nasdaq",
                                    "nyse"
                                ],
                                "stock_symbol": [
                                    "ibm",
                                    "orcl"
                                ]
                            }
                        }
                    ]
                },
                "incremental": {},
                "time_out": 10,
                "command_type": "HiveCommand",
                "macros": [
                    {
                        "formatted_date": "Qubole_nominal_time.format('YYYY-MM-DD')"
                    }
                ],
                "template": "generic",
                "pool": null,
                "label": "default",
                "is_digest": false,
                "can_notify": false,
                "digest_time_hour": 0,
                "digest_time_minute": 0,
                "email_list": "qubole@qubole.com",
                "bitmap": 0
            },
            {
                "id": 51,
                "name": "51",
                "status": "KILLED",
                "concurrency": 1,
                "frequency": 1,
                "time_unit": "days",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 108,
                "start_time": "2013-03-30 07:30",
                "end_time": "2015-01-01 00:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "Amsterdam",
                "next_materialized_time": null,
                "command": {
                    "query": "alter table recover partitions demo_data3",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": null,
                    "loader_stable": null,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {
                    "hive_tables": null
                },
                "incremental": {},
                "time_out": 10,
                "command_type": "HiveCommand",
                "macros": [],
                "template": "generic",
                "pool": null,
                "label": "default"
            },
            {
                "id": 52,
                "name": "52",
                "status": "SUSPENDED",
                "concurrency": 1,
                "frequency": 1,
                "time_unit": "days",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 108,
                "start_time": "2013-03-30 07:30",
                "end_time": "2015-01-01 00:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "Amsterdam",
                "next_materialized_time": "2013-04-02 07:30",
                "command": {
                    "query": "alter table demo_data3 recover partitions",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": null,
                    "loader_stable": null,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {
                    "hive_tables": null
                },
                "incremental": {},
                "time_out": 10,
                "command_type": "HiveCommand",
                "macros": [],
                "template": "generic",
                "pool": null,
                "label": "default",
                "is_digest": false,
                "can_notify": false,
                "digest_time_hour": 0,
                "digest_time_minute": 0,
                "email_list": "qubole@qubole.com",
                "bitmap": 0
            },
            {
                "id": 53,
                "name": "53",
                "status": "DONE",
                "concurrency": 1,
                "frequency": 1,
                "time_unit": "days",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 108,
                "start_time": "2013-04-01 07:00",
                "end_time": "2015-01-01 00:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "Amsterdam",
                "next_materialized_time": "2015-01-01 07:00",
                "command": {
                    "query": "alter table daily_tick_data recover partitions",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": null,
                    "loader_stable": null,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {
                    "hive_tables": null
                },
                "incremental": {},
                "time_out": 10,
                "command_type": "HiveCommand",
                "macros": [],
                "template": "generic",
                "pool": null,
                "label": "default",
                "is_digest": false,
                "can_notify": false,
                "digest_time_hour": 0,
                "digest_time_minute": 0,
                "email_list": "qubole@qubole.com",
                "bitmap": 0
            },
            {
                "id": 71,
                "name": "71",
                "status": "KILLED",
                "concurrency": 1,
                "frequency": 1440000,
                "time_unit": "minutes",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 12,
                "start_time": "2013-04-10 00:00",
                "end_time": "2037-04-10 00:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "UTC",
                "next_materialized_time": null,
                "command": {
                    "mode": 2,
                    "dbtap_id": 15,
                    "hive_table": "xxx",
                    "part_spec": null,
                    "hive_serde": null,
                    "db_where": null,
                    "db_columns": null,
                    "schema": null,
                    "md_cmd": true,
                    "db_parallelism": 1,
                    "db_extract_query": "select a,b,c from 3int_100M where $CONDITIONS",
                    "retry": 0
                },
                "dependency_info": {},
                "incremental": {},
                "time_out": 0,
                "command_type": "DbImportCommand",
                "macros": {},
                "template": "generic",
                "pool": null,
                "label": "default"
            },
            {
                "id": 108,
                "name": "108",
                "status": "KILLED",
                "concurrency": 1,
                "frequency": 1449000,
                "time_unit": "minutes",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 12,
                "start_time": "2013-05-01 00:00",
                "end_time": "2037-05-01 00:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "UTC",
                "next_materialized_time": null,
                "command": {
                    "query": "alter table 3int_100m_sqooped recover partitions",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": "3int_100m_sqooped",
                    "loader_stable": 60,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {},
                "incremental": {},
                "time_out": 0,
                "command_type": "HiveCommand",
                "macros": {},
                "template": "s3import",
                "pool": null,
                "label": "default"
            },
            {
                "id": 128,
                "name": "128",
                "status": "KILLED",
                "concurrency": 1,
                "frequency": 1440,
                "time_unit": "minutes",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 108,
                "start_time": "2013-05-13 00:00",
                "end_time": "2037-05-13 00:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "UTC",
                "next_materialized_time": "2014-04-18 00:00",
                "command": {
                    "query": "alter table demo_memetracker recover partitions",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": "demo_memetracker",
                    "loader_stable": 60,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {},
                "incremental": {},
                "time_out": 0,
                "command_type": "HiveCommand",
                "macros": {},
                "template": "s3import",
                "pool": null,
                "label": "default",
                "is_digest": false,
                "can_notify": false,
                "digest_time_hour": 0,
                "digest_time_minute": 0,
                "email_list": "qubole@qubole.com",
                "bitmap": 0
            },
            {
                "id": 200,
                "name": "200",
                "status": "RUNNING",
                "concurrency": 1,
                "frequency": 14,
                "time_unit": "days",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 12,
                "start_time": "2013-05-15 00:00",
                "end_time": "2037-05-15 00:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "UTC",
                "next_materialized_time": "2016-04-27 00:00",
                "command": {
                    "query": "show tables;",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": null,
                    "loader_stable": null,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {
                    "hive_tables": null
                },
                "incremental": {},
                "time_out": 10,
                "command_type": "HiveCommand",
                "macros": [],
                "template": "generic",
                "pool": null,
                "label": "default",
                "is_digest": false,
                "can_notify": false,
                "digest_time_hour": 0,
                "digest_time_minute": 0,
                "email_list": "qubole1@qubole.com",
                "bitmap": 0
            },
            {
                "id": 201,
                 "name": "201",
                 "status": "KILLED",
                "concurrency": 1,
                "frequency": 1449000,
                "time_unit": "minutes",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 12,
                "start_time": "2013-05-28 00:00",
                "end_time": "2037-05-28 00:00",
                "created_at": "2012-07-01 02:00",
                "time_zone": "UTC",
                "next_materialized_time": null,
                "command": {
                    "query": "alter table 3int_100m_sqooped recover partitions",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": "3int_100m_sqooped",
                    "loader_stable": 60,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {
                    "hive_tables": null
                },
                "incremental": {},
                "time_out": 10,
                "command_type": "HiveCommand",
                "macros": [],
                "template": "s3import",
                "pool": null,
                "label": "default"
            },
            {
                "id": 203,
                "name": "203",
                "status": "SUSPENDED",
                "concurrency": 1,
                "frequency": 40,
                "time_unit": "minutes",
                "no_catch_up": false,
                "cron_expression": null,
                "user_id": 108,
                "start_time": "2013-05-13 00:00",
                "created_at": "2012-07-01 02:00",
                "end_time": "2037-05-13 00:00",
                "time_zone": "UTC",
                "next_materialized_time": "2014-01-08 21:20",
                "command": {
                    "query": "alter table demo_memetracker recover partitions",
                    "sample": false,
                    "approx_mode": false,
                    "approx_aggregations": false,
                    "loader_table_name": "demo_memetracker",
                    "loader_stable": 60,
                    "md_cmd": null,
                    "script_location": null,
                    "retry": 0
                },
                "dependency_info": {
                    "hive_tables": null
                },
                "incremental": {},
                "time_out": 10,
                "command_type": "HiveCommand",
                "macros": [],
                "template": "s3import",
                "pool": null,
                "label": "default",
                "is_digest": false,
                "can_notify": false,
                "digest_time_hour": 0,
                "digest_time_minute": 0,
                "email_list": "qubole@qubole.com",
                "bitmap": 0
            }
        ]
    }