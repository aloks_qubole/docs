.. _suspendresume-or-kill-a-schedule:

Suspend, Resume or Kill a Schedule
==================================

.. http:put:: /api/v1.2/scheduler/(int:id)

Use this API to suspend, resume, or kill a schedule.

.. note:: After you stop a schedule, you cannot resume it. However, you can suspend a schedule and resume it later.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows suspending, resuming, or killing
  a schedule. See :ref:`manage-groups` and :ref:`manage-roles` for more information.

This API is used to suspend, resume, or kill an existing schedule created to run commands automatically at certain
frequency in a specified interval.

+-----------------------+-------------------------------------------------+
| Resource URI          | scheduler/\ *id*                                |
+-----------------------+-------------------------------------------------+
| Request Type          | PUT                                             |
+-----------------------+-------------------------------------------------+
| Supporting Versions   | v1.2                                            |
+-----------------------+-------------------------------------------------+
| Return Value          | JSON object with the status of the operation.   |
+-----------------------+-------------------------------------------------+

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter                | Description                                                                                                                                                                            |
+==========================+========================================================================================================================================================================================+
| **status**               | It indicates the status and its valid values are ``suspend``, ``resume`` or ``kill``.                                                                                                  |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| no_catch_up              | .. note:: You can use this parameter while resuming a suspended schedule.                                                                                                              |
|                          |                                                                                                                                                                                        |
|                          | Set this parameter to ``true`` if you want to skip schedule actions that were supposed to have run in the past and run only the api/v1.2 schedule actions. By default, this            |
|                          | parameter is set to ``false``. When a new schedule is created, the scheduler runs schedule actions from start time to the current time. For example, if a daily schedule is            |
|                          | created from Jun 1, 2015 on Dec 1, 2015, schedules are run for Jun 1, 2015, Jun 2, 2015, and so on. If you do not want the scheduler to run the missed schedule actions for            |
|                          | months earlier to Dec, set  ``no_catch_up`` to ``true``. The main use of skipping a schedule action is if when you suspend a schedule and resume it later, in which case,              |
|                          | there will be more than one schedule action and you might want to skip the earlier schedule actions. For more information, see :ref:`qds-scheduler-concepts`.                          |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Examples
--------

Example to Suspend a Schedule
.............................

.. sourcecode:: bash

     curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
     -d '{"status":"suspend"}' \ "https://api.qubole.com/api/v1.2}/scheduler/${SCHEDID}/"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

    {"succeeded":"true","status":"SUSPENDED"}

.. note:: There is a rerun limit for a scheduled job. `qds-scheduler-concepts` provides more information.

Example to Resume a Suspended Schedule
......................................

.. note:: A ``_SUCCESS`` file is created in the output folder for successful schedules. You can set
          ``mapreduce.fileoutputcommitter.marksuccessfuljobs`` to ``false`` to disable creation of _SUCCESS file
          or to ``true`` to enable creation of the _SUCCESS file.

.. sourcecode:: bash

     curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
     -d '{"status":"resume"}' \ "https://api.qubole.com/api/v1.2/scheduler/${SCHEDID}/"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

    {"succeeded":"true","status":"RUNNING"}

.. note:: After you stop a schedule, you cannot resume it. However, you can suspend a schedule and resume it later.

Example to Kill a Schedule
..........................

.. sourcecode:: bash

     curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
     -d '{"status":"kill"}' \ "https://api.qubole.com/api/v1.2/scheduler/${SCHEDID}/"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

    {"succeeded":"true","status":"KILLED"}