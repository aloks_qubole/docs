.. _kill-a-schedule-action:

Kill a Schedule Action
======================

.. http:put:: /api/v1.2/actions/(int:id)/kill

Cancels a running scheduled action. The *ID* can be obtained by listing the
actions using any one of the :doc:`list-schedule-actions` or the
:doc:`list-all-actions`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows killing a schedule action. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Response
--------

A JSON hash that encodes if the operation is a success or not.

.. sourcecode:: http

    {"kill_succeeded":"true"}

Example
-------

**Goal:** Rerun an action with ID ${ACTIONID}

.. sourcecode:: bash

     curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
     -H "Accept: application/json" \
     -H "Content-type: application/json" \
     {"status":"kill"} \
     "https://api.qubole.com/api/v1.2/actions/${ACTIONID}/kill"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

    {"kill_succeeded":"true"}