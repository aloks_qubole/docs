.. _view-a-schedule-action:

View a Schedule's Action
========================

.. http:get:: /api/v1.2/scheduler/<Schedule ID>/actions/<sequence_id>

Use this API to view an existing schedule's action.

Schedule ID  is the Schedule's ID and it is a mandatory parameter.

*sequence_id* is the nth action of the schedule and it is optional.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a schedule's actions.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Response
--------
If you provide only <Schedule ID> in the API call, for example, ``/api/v1.2/scheduler/123/actions``, then the API response
contains the list of Scheduler instances in the descending order based on instance ID, that is the most recent run is
the first action object in the actions array. The most recent Scheduler instance run might be the re-run instances of an
old instance. This is illustrated in `Example 1`_.

If you provide both <Schedule ID> and sequence_id in the API call, for example, ``/api/v1.2/scheduler/123/actions/5``,
then the API response contains the list of Scheduler run instances that belong to only that sequence ID and the results
are always in the descending order based on rerun_number, that is the most recent rerun is the first action object in
the actions array. This is illustrated in `Example 2`_.

The two types of API calls and the corresponding responses are illustrated in the below examples.

Examples
--------
Here are the two different examples.

Example 1
.........
**Goal:** View details of the schedule's action by providing only the schedule's ID.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    -H "Accept: application/json" \
    -H "Content-type: application/json" \
    "https://api.qubole.com/api/v1.2/scheduler/16537/actions"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

*For brevity, the JSON is truncated.*

.. sourcecode:: http

    {
    "paging_info": {
        "next_page": null,
        "previous_page": null,
        "per_page": 10
    },
    "actions": [
        {
            "id": 6587693,
            "sequence_id": 2,
            "nominal_time": "2017-03-16T00:00:00Z",
            "is_rerun_of": 3460235,
            "rerun_number": 2,
            "created_at": "2018-02-13T07:27:42Z",
            "query_hist_id": 128128536,
            "dependencies": {
                "found": [],
                "not_found": []
            },
            "done": true,
            "status": "done",
            "periodic_job_id": 16537,
            "perms": {
                "kill": true,
                "rerun": true
            }
            ,
            "command": {
                "id": 128128536,
                "path": "/tmp/2018-02-13/2056/128128536",
                "status": "done",
                "created_at": "2018-02-13T07:27:43Z",
                "updated_at": "2018-02-13T07:27:58Z",
                "command_type": "HiveCommand",
                "progress": 100,
                .....
                "command": {
                    "query": "show tables",
                    "sample": false,
                    ....
                }
            }
        },
        {
            "id": 6587685,
            "sequence_id": 3,
            "nominal_time": "2017-03-16T00:00:00Z",
            "is_rerun_of": null,
            "rerun_number": 1,
            ....
            ,
            "command": {
              ....
            }
        },
        {
            "id": 3460235,
            "sequence_id": 2,
            "nominal_time": "2017-03-16T00:00:00Z",
            "is_rerun_of": null,
            "rerun_number": 1,
            .....
            ,
            "command": {
               .....
            }
        },
        {
            "id": 3460233,
            "sequence_id": 1,
            "nominal_time": "2017-03-15T18:30:00Z",
            "is_rerun_of": null,
            "rerun_number": 1,
            ....
            ,
            "command": {
              ....
            }
        }
    ]
    }

Example 2
.........
**Goal:** View a schedule's action to retrieve its reruns by providing Scheduler ID and Sequence ID.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    -H "Accept: application/json" \
    -H "Content-type: application/json" \
    "https://api.qubole.com/api/v1.2/scheduler/16537/actions/2"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

*For brevity, the JSON is truncated.*

.. sourcecode:: http

    {
    "paging_info": {
        "next_page": null,
        "previous_page": null,
        "per_page": 10
    },
    "actions": [
        {
            "id": 6587693,
            "sequence_id": 2,
            "nominal_time": "2017-03-16T00:00:00Z",
            "is_rerun_of": 6587682,
            "rerun_number": 5,
            "created_at": "2018-02-13T07:27:42Z",
            "query_hist_id": 128128536,
            "dependencies": {
                "found": [],
                "not_found": []
            },
            "done": true,
            "status": "done",
            "periodic_job_id": 16537,
            "perms": {
                "kill": true,
                "rerun": true
            },
            "command": {
              ....
            }
        },
        {
            "id": 6587685,
            "sequence_id": 2,
            "nominal_time": "2017-03-16T00:00:00Z",
            "is_rerun_of": 3460235,
            "rerun_number": 4,
            "created_at": "2018-02-13T07:22:49Z",
            "query_hist_id": 128127532,
            ....
            ,
            "command": {
              ....
            }
        },
        {
            "id": 6587682,
            "sequence_id": 2,
            "nominal_time": "2017-03-16T00:00:00Z",
            "is_rerun_of": 3460235,
            "rerun_number": 3,
            "created_at": "2018-02-13T07:20:27Z",
            "query_hist_id": 128127085,
            .....
            ,
            "command": {
              ....
            }
        },
        {
            "id": 6587667,
            "sequence_id": 2,
            "nominal_time": "2017-03-16T00:00:00Z",
            "is_rerun_of": 3460235,
            "rerun_number": 2,
            "created_at": "2018-02-13T07:18:15Z",
            "query_hist_id": 128126570,
            ....
            ,
            "command": {
              ....
            }
        },
        {
            "id": 3460235,
            "sequence_id": 2,
            "nominal_time": "2017-03-16T00:00:00Z",
            "is_rerun_of": null,
            "rerun_number": 1,
            "created_at": "2017-03-16T15:18:25Z",
            "query_hist_id": 61115891,
            ....
            ,
            "command": {
              ....
            }
        }
    ]
    }