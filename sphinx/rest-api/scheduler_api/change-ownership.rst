.. _change-ownership-api:

Change Ownership
================

.. http:put:: api/v1.2/scheduler/(int: id)/owner

Use this API to change the ownership of an existing schedule.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows managing a schedule. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.


Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory.

+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Parameter                | Description                                                                                                                                                                |
+==========================+============================================================================================================================================================================+
| **new_owner_email**      | Provide the email ID of the (new) owner to whom you want to transfer the ownership of the schedule.                                                                        |
+--------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------+


Response
--------
The response contains a JSON object representing the email ID of the schedule's new owner.


Example
-------

**Goal**: Modify the owner of the schedule

.. sourcecode:: bash

    curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
    -d '{"new_owner_email":"user@xyz.com"}' \
    "https://api.qubole.com/api/v1.2/scheduler/24/owner"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: json


    {
    "success": true,
    "new_owner_email":"user@xyz.com"
    }
