.. _scheduler-api:

#############
Scheduler API
#############

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-a-schedule
    clone-a-schedule
    view-a-schedule
    change-ownership
    list-schedules
    edit-a-schedule
    preview-command-api
    suspendresume-or-kill-a-schedule
    list-schedule-actions
    view-a-schedule-action
    kill-a-schedule-action
    rerun-a-schedule-action
    list-all-actions
    view-an-action


