.. _list-schedule-actions:

List Schedule Actions
=====================

.. http:get:: /api/v1.2/scheduler/(int:id)/actions


Retrieves a list of actions run for a scheduler. The list is paginated.

.. note:: A ``_SUCCESS`` file is created in the output folder for successful schedules. You can set
          ``mapreduce.fileoutputcommitter.marksuccessfuljobs`` to ``false`` to disable creation of _SUCCESS file
          or to ``true`` to enable creation of the ``_SUCCESS`` file.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing actions that a scheduler
  runs. See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Response
--------
The list contains information about commands that are run as part of the action.
The list is ordered with api/v1.2 sequence first.

Example
-------

**Goal:** Retrieve a list of actions for a schedule.
The list has 3 actions per page.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    -H "Accept: application/json" \
    -H "Content-type: application/json" \
    "https://api.qubole.com/api/v1.2/scheduler/${SCHEDID}/actions?per_page=3"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

    {
      "actions": [
        {
          "status": "done",
          "is_rerun_of": 47519,
          "nominal_time": "2014-06-26T11:00:00Z",
          "done": true,
          "sequence_id": 4226,
          "query_hist_id": 277791,
          "rerun_number": 2,
          "id": 47520,
          "dependencies": {
            "not_found": [

            ],
            "found": [

            ]
          },
          .
          .
          .
          "periodic_job_id": 30562,
        },
        {
          "done": true,
          "sequence_id": 4226,
          "query_hist_id": 277790,
          .
          .
          .
          "status": "done",
        },
        {
          "status": "done",
          "is_rerun_of": null,
          "periodic_job_id": 30562,
        }
      ],
      "paging_info": {
        "previous_page": null,
        "per_page": "3",
        "next_page": 2
      }
    }