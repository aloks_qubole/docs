.. _view-an-action:

View an Action
==============

.. http:get:: /api/v1.2/actions/(int:id)

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a schedule information.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Response
--------

Response is a scheduled action. The action may or may not have an empty command child object.
A command may be empty if dependencies were not satisfied.

This API differs from the :doc:`view-a-schedule-action`.
In that API, instead of the *sequence_id* of the action, it accepts
the unique ID of the action within the schedule.

Example
-------

**Goal:** View the details about the first action of a schedule.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    -H "Accept: application/json" \
    -H "Content-type: application/json" \
    "https://api.qubole.com/api/v1.2/actions/37129/"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

    {
    "status": "done",
    "done": true,
    "command": {
        "meta_data": {
            "logs_resource": "commands/165025/logs",
            "results_resource": "commands/165025/results"
        },
        "status": "done",
        "command_source": "SCHEDULED",
        "progress": 100,
        "qbol_session_id": 49007,
        "command": {
            "approx_mode": false,
            "md_cmd": false,
            "loader_stable": null,
            "script_location": null,
            "query": "select stock_symbol, max(high), min(low), sum(volume) from daily_tick_data where date1='2012-07-01' group by stock_symbol",
            "sample": false,
            "loader_table_name": null,
            "approx_aggregations": false
        },
        "created_at": "2015-04-15T10:05:11Z",
        "start_time": 1429092315,
        "end_time": 1429092346,
        "command_type": "HiveCommand",
        "pid": 12110,
        "account_id": 3,
        "label": "default",
        "template": "generic",
        "timeout": null,
        "can_notify": false,
        "pool": null,
        "user_id": 3,
        "submit_time": 1429092311,
        "name": "",
        "id": 165025,
        "path": "/tmp/2015-04-15/3/165025",
        "qlog": "{\"QBOL-QUERY-SCHEMA\":{\"/tmp/2015-04-15/3/165025.dir/000\":[{\"ColumnType\":\"string\",\"ColumnName\":\"stock_symbol\"},{\"ColumnType\":\"float\",\"ColumnName\":\"_c1\"},{\"ColumnType\":\"float\",\"ColumnName\":\"_c2\"},{\"ColumnType\":\"bigint\",\"ColumnName\":\"_c3\"}]}}",
        "num_result_dir": 1,
        "resolved_macros": "{\"Qubole_nominal_time\":\"Sun Jul 01 2012 02:00:00 GMT+0000\",\"formatted_date\":\"2012-07-01\",\"Qubole_nominal_time_iso\":\"2012-07-01 02:00:00+00:00\"}"
    },
    "created_at": "2015-04-15T10:05:11Z",
    "periodic_job_id": 3094,
    "dependencies": {
        "not_found": [],
        "found": []
    },
    "nominal_time": "2012-07-01T02:00:00Z",
    "is_rerun_of": null,
    "sequence_id": 1,
    "query_hist_id": 165025,
    "rerun_number": 1,
    "id": 46096
    }