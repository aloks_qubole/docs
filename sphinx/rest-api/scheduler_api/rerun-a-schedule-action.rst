.. _rerun-a-schedule-action:

Rerun a Scheduled Action
========================

.. http:post:: /api/v1.2/actions/(int:id)/rerun

Use this API to rerun a scheduled action. Get the **Action ID** by listing the actions using any one of the
:ref:`list-schedule-actions` or the :ref:`list-all-actions`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows rescheduling.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Response
--------

A JSON hash that encodes if the operation is successful or not.

.. sourcecode:: http

      {"status":"rescheduled"}

Example
-------

**Goal:** Rerun an action with id ${ACTIONID}

.. sourcecode:: bash

     curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
     -H "Accept: application/json" \
     -H "Content-type: application/json" \
     "https://api.qubole.com/api/v1.2/actions/${ACTIONID}/rerun"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

    {"status":"rescheduled"}