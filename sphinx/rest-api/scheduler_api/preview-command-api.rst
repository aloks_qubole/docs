.. _preview-command-api:

===================================
Preview a Command Before Running It
===================================

.. http:post:: /api/v1.2/scheduler/preview_macro

After adding one or more macro variables in a command/query, you can preview it before running it. To see the preview of
the command, use this API. It can be used for previewing a new, existing, or edited command.

Parameters
----------
None

REST API Syntax
---------------
Here is the API syntax for running a preview command API call.

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
    -d '{ "command": { "query": "<query <macro>>", "command_type": "HiveCommand"}, "macros": [{"<macro>":"\"<macrovalue>\""}]
        }' \ "https://api.qubole.com/api/v1.2/scheduler/preview_macro"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Requests
...................
Here are two sample API calls to preview the commands.

Hive Command Schedule Sample
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
    -d '{"command": { "query": "show table $table$", "command_type": "HiveCommand" }, "macros": [{"table":"\"hivetabledata\""}]
    }'\ "https://api.qubole.com/api/v1.2/scheduler/preview_macro"

Workflow Command Schedule Sample
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
    -d `{
	      "command": { "sub_commands" : [ {
			"command_type" :  "HiveCommand",
              "query" :  "Some $hive$ query"

            },
            {
              "command_type" :  "PrestoCommand",
              "query" : "Some $presto$"
            }
          ],
         "command_type": "CompositeCommand" },
	     "macros": [{"presto":"\"prestoCmd\""}, {"hive":"\"hiveCmd\""}]
         } \ "https://api.qubole.com/api/v1.2/scheduler/preview_macro"
