.. _view-a-schedule:

View a Schedule
===============

.. http:get:: /api/v1.2/scheduler/(int:id)

This API is used to view an existing schedule that is created to run commands automatically at certain frequency in a
specified interval.

+-----------------------+------------------------------------------+
| Resource URI          | scheduler/\ *id*                         |
+-----------------------+------------------------------------------+
| Request Type          | GET                                      |
+-----------------------+------------------------------------------+
| Supporting Versions   | v1.2                                     |
+-----------------------+------------------------------------------+
| Return Value          | Json object representing the schedule.   |
+-----------------------+------------------------------------------+

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a schedule information.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
~~~~~~~

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
    "https://api.qubole.com/api/v1.2/scheduler/${SCHEDID}"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

    {
      "concurrency": 1,
      "time_unit": "days",
      "command": {
        "approx_mode": false,
        "query": "select stock_symbol, max(high), min(low), sum(volume) from daily_tick_data where date1='$formatted_date$' group by stock_symbol",
        "approx_aggregations": false,
        "sample": false
      },
      "user_id": 39,
      "dependency_info": {
        "hive_tables": [
          {
            "window_end": "0",
            "initial_instance": "2012-07-01T00:00Z",
            "name": "daily_tick_data",
            "interval": {
              "days": "1"
            },
            "columns": {
              "stock_symbol": [
                "ibm",
                "orcl"
              ],
              "stock_exchange": [
                "nasdaq",
                "nyse"
              ]
            },
            "window_start": "-1",
            "time_zone": "UTC"
          }
        ]
      },
      "time_out": 10,
      "macros": [
        {
          "formatted_date": "Qubole_nominal_time.format('YYYY-MM-DD')"
        }
      ],
      "end_time": "2022-07-01 02:00",
      "start_time": "2012-07-01 02:00",
      "frequency": 1,
      "id": 2266,
      "time_zone": "UTC",
      "command_type": "HiveCommand",
      "status": "RUNNING"
    }