.. _set-view-bootstrap-api-new:

==================================================================
Set and View a Hive Bootstrap in a QDS Account using API version 2
==================================================================

Use these APIs to set and view a Hive bootstrap in a QDS account. Refer to :ref:`Managing Hive Bootstrap <manage-hive-bootstrap>`
for more information on how to set a bootstrap using the bootstrap editor.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin groups.
* A user invoking this API must be part of a group associated with a role that allows editing an account. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Set a Hive Bootstrap
--------------------

..  http:put:: /api/v2/accounts/bootstrap

Use this API to set a Hive bootstrap in a QDS account.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| **script**                | Add Hive bootstrap content using this parameter.                                                            |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| is_user_bootstrap         | Use this parameter to personalize the user-level Hive bootstrap. Set it to ``true`` to modify it at the     |
|                           | user level.                                                                                                 |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

.. note:: The user-level Hive bootstrap is loaded after the account-level Hive bootstrap. In case of duplicate
          entries in user-level and account-level bootstraps, only the user-level Hive bootstrap becomes valid.

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"script":"<new-bootstrap>", "is_user_bootstrap":"true"}' \ "https://api.qubole.com/api/v2/accounts/bootstrap"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................

Here is a sample API request to set a Hive bootstrap.

.. sourcecode:: bash

   curl -X PUT -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{ "script": "add jar s3://qubole/jars/loc1/stringutils-1.0-SNAPSHOT.jar;
                   add jar s3://qubole/jars/loc1/udftest-1.0-SNAPSHOT.jar;
                   create temporary function udftest as 'com.qubole.hive.udftest.UDFTest';"
       }' \ "https://api.qubole.com/api/v2/accounts/bootstrap"

View a Bootstrap
----------------

..  http:get:: /api/v2/accounts/bootstrap

Use this API to view a Hive bootstrap and a personalized Hive bootstrap (if any set at the user-level) set in a QDS account.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| is_user_bootstrap         | This parameter is set to ``true`` to fetch the Hive bootstrap set at user level. Use this parameter to      |
|                           | only see the personalized Hive bootstrap (if available).                                                    |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin groups.
* A user invoking this API must be part of a group associated with a role that allows viewing an account's details. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"is_user_bootstrap":"true"}' \ "https://api.qubole.com/api/v2/accounts/bootstrap"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.



