.. _view-an-account-new:

======================================================
View Information for a QDS Account using API version 2
======================================================

..  http:get:: /api/v2/accounts/

Use this API to view the details of the current account. **This API displays the Role ARN and External ID for an IAM**
**Roles-based Qubole account**.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin groups can call this API request.
* A user invoking this API must be part of a group associated with a role that allows viewing an account's details. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

None.

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v2/accounts/"

.. note:: Qubole provides other endpoints to access QDS that are described in :ref:`qubole-endpoints`.

Sample Request
..............

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json"
   "https://api.qubole.com/api/v2/accounts/"

Sample Response
...............

.. sourcecode:: bash

    {
      "id": 8,
      "name": “account-data-analyst”,
      "storage_type": "CUSTOMER_MANAGED",
      "compute_type": "CUSTOMER_MANAGED",
      "storage_validated": true,
      "compute_validated": true,
      "is_aws_role_enabled": false,
      "idle_session_timeout": null,
      "storage_access_key": "$STORAGE_ACCESS_KEY",
      "storage_secret_key": "$STORAGE_SECRET_KEY",
      "storage_location": "data.com/logs/production",
      "authorized_ssh_key": null,
      "compute_access_key": "$COMPUTE_ACCESS_KEY",
      "compute_secret_key": "$COMPUTE_SECRET_KEY"
    }