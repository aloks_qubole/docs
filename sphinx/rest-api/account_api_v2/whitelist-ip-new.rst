.. _whitelist-ip-api-new:

=================================================
Whitelist an IP Address using the New API version
=================================================

Whitelisting IP addresses allows users of an account to log in only from certain
(`IPv4 or IPv6 <http://www.omnisecu.com/tcpip/ipv6/differences-between-ipv4-and-ipv6.php>`_) addresses.

.. note:: Send an email request to help@qubole.com to enable whitelisting for an account. Keep in mind that once
   whitelisting is enabled, users of the account can log in *only* from a whitelisted address.

* :ref:`whitelist-ip-api-add1`
* :ref:`whitelist-ip-api-list1`
* :ref:`whitelist-ip-api-delete1`

Required Role
-------------
To make this API call you must:

* Belong to the system-user or system-admin group.
* Belong to a group associated with a role that allows editing an account. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

.. _whitelist-ip-api-add1:

Add a Whitelisted IP Address
----------------------------

.. http:post:: /api/v2/accounts/whitelist_ip

Parameter
.........

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| ip_cidr                   | IP address to be whitelisted, in IPv4 or IPv6 format                                                        |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Example
.......

*Request:*

.. sourcecode:: bash

   curl -X POST -H "X-AUTH-TOKEN: $X_AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   -d '{"ip_cidr" : "103.252.24.87"}' \ "https://api.qubole.com/api/v2/accounts/whitelist_ip"

.. note:: The above example uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

*Response:*

.. sourcecode:: bash

   {"status":{"status_code":200,"message":"IP whitelisted successfully."}}

.. _whitelist-ip-api-list1:

List Whitelisted IP Addresses
-----------------------------

.. http:get:: /api/v2/accounts/whitelist_ip

Example
.......

*Request:*

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v2/accounts/whitelist_ip"

.. note:: The above example uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

*Response:*

An array of hashes containing account ID and IP address info; for example:

.. sourcecode:: bash

   {"account_whitelisted_ips":[{"account_id":1,"created_at":"2017-01-17T19:06:56Z","id":1,"ip_cidr":"103.252.24.92","updated_at":"2017-01-17T19:06:56Z"},{"account_id":1,"created_at":"2017-01-17T19:07:20Z","id":2,"ip_cidr":"103.252.24.91","updated_at":"2017-01-17T19:07:20Z"}]}


.. _whitelist-ip-api-delete1:

Delete One or More Whitelisted IP Addresses
-------------------------------------------

.. http:delete:: /api/v2/accounts/whitelist_ip/<id>

where ``<id>`` is the :ref:`ID <whitelist-ip-api-list>` of the whitelisted IP address. To delete multiple addresses, use
a comma-separated list of IDs.

Example
.......

*Request:*

.. sourcecode:: bash

   curl -X DELETE -H "X-AUTH-TOKEN:  $X_AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v2/accounts/whitelist_ip/1,2"

.. note:: The above example uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

*Response*

.. sourcecode:: bash

   {"status":{"status_code":200,"message":"Deleted"}}
