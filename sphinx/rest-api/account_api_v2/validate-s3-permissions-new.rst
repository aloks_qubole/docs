.. _validate-s3-permissions-new:

============================================================
Validate S3 Permissions of a QDS Account using API version 2
============================================================

..  http:get:: /api/v2/accounts/storage_permissions

Use this API to validate S3 Permissions of a QDS account in a Qubole environment. Ensure to use the correct
environment URL where the QDS account is created. For more information on the S3 permissions that get validated, see
:ref:`iam-keys` or :ref:`manage-accounts`.

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://<Qubole Environment>/api/v2/accounts/storage_permissions"

Here ``<Qubole Environment>`` is the Qubole environment in which you have the QDS account. For example, ``https://api.qubole.com``
is a Qubole environment.

.. note:: Qubole provides other endpoints to access QDS that are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is the sample request to check the storage permissions of an account in the ``https://api.qubole.com`` environment.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v2/accounts/storage_permissions"

Understanding Different Permission Validation Status
----------------------------------------------------
+-----------+--------------------------------------------------------------------------------------------------------+
|Status     | Description                                                                                            |
+===========+========================================================================================================+
| Error     | It implies that the permission is missing and it is mandatory. The functionality does not work as      |
|           | explained in the **Description** field.                                                                |
+-----------+--------------------------------------------------------------------------------------------------------+
| Warning   | It implies that the permission is missing and it is optional. You can perform some workaround steps as |
|           | explained in the **Description** field.                                                                |
+-----------+--------------------------------------------------------------------------------------------------------+

Sample Response of a Successful Validation
..........................................

.. sourcecode:: bash

    {
      status: "Success",
      messsage: "",
      permission_list: {
           s3:GetBucketLocation: {
              status: "Success",
              message: "",
              description: "Some s3 client libraries has dependency on this permission for other required s3 operations as mentioned above.",
              UIOrder: 1
           },
           s3:PutObject: {
              status: "Success",
              message: "",
              description: "To store the data in s3 obtained after computation and / or storing logs",
              UIOrder: 2
           },
           s3:GetObject: {
              status: "Success",
              message: "",
              description: "To read s3 data for computation purposes",
              UIOrder: 3
           },
           s3:PutObjectAcl: {
              status: "Success",
              message: "",
              description: "Writing Object Acl",
              UIOrder: 4
              },
           s3:GetObjectAcl: {
              status: "Success",
              message: "",
              description: "Reading Object Acl",
              UIOrder: 5
           },
           s3:GetBucketAcl: {
              status: "Success",
              message: "",
              description: "Reading Bucket Acl",
              UIOrder: 6
           },
           s3:ListBucket: {
              status: "Success",
              message: "",
              description: "To browse the s3 bucket in Qubole UI",
              UIOrder: 7
              },
           s3:DeleteObject: {
              status: "Success",
              message: "",
              description: "To remove the s3 objects while doing computation",
              UIOrder: 8
           }
      }
    }




