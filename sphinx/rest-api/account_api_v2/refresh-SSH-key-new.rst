.. _refresh-ssh-key-new:

==========================================================
Refresh the Account-level SSH Key Pair using API version 2
==========================================================

.. http:put:: api/v2/accounts/ssh_key

Use this API to create a new SSH key pair and use the public key to configure it on the bastion host of the VPC.

You must configure the public key of the SSH key pair on the bastion host of a VPC. Qubole communicates with the cluster
that is configured on a private subnet of a VPC through the bastion host.

If the Unique SSH Key feature is not enabled, then the default Qubole public key is used to SSH into the bastion host.
For more information, see :ref:`clusters-in-vpc`.

.. important:: The account-level unique SSH key feature is enabled by default on https://us.qubole.com  and https://in.qubole.com.
               If you have the QDS account on any other QDS environment, then create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
               to get it enabled.

Whenever the public SSH key is rotated, ensure to replace the public SSH key on the bastion host with the rotated public
SSH key. For more information on adding the unique public SSH key, see :ref:`clusters-in-vpc`.

The rotation policy for each cluster instance that is every time a cluster starts, a new SSH key pair is generated so
Qubole automatically handles the rotation of keys every time the cluster starts. This happens only if the Unique SSH
Key feature is enabled on the QDS account. If it is not enabled, then the default Qubole public key is added on the
cluster nodes.

.. caution:: The previously generated SSH key pair gets overridden by the newly generated SSH key pair.

Parameters
----------
None

Request API Syntax
------------------

.. sourcecode:: bash

       curl -X PUT -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
       "https://<QDS environment>/api/v2/accounts/ssh_key"

.. note:: The above syntax uses the QDS environment as the placeholder. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................

Here is a sample request to see the account-level public SSH key of a QDS account in the https://us.qubole.com environment.

.. sourcecode:: bash

       curl -X PUT -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:application/json" -H "Accept: application/json" \
       "https://us.qubole.com/api/v2/accounts/ssh_key"