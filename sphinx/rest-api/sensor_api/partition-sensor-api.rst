.. _partition-sensor-api:

=============================
Create a Partition Sensor API
=============================

.. http:post:: /api/v1.2/partition_sensor

Use this API to create a partition sensor API. :ref:`Airflow <airflow-index>` uses it for programmatically monitoring
workflows.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a sensor. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| **schema**                | The database that contains the Hive table partition, which needs monitoring.                                |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **table**                 | The name of the Hive table that contains the partition, which needs monitoring.                             |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **columns**               | This contains the array of Hive-table-column names and the corresponding values.                            |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"schema": "<Database Name>", "table":"<Hive table name>",
         "columns":[{"column":"<column name>", "values":["<value>"]}]}' \
    "https://api.qubole.com/api/v1.2/sensors/partition_sensor"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is an example of creating an Hive table partition sensor.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"schema": "default", "table":"hivetable", "columns":[{"column":"dt", "values":"[2017-05-19]"}]}' \
    "https://api.qubole.com/api/v1.2/sensors/partition_sensor"

The requests with 200 response code will have just a status field which will either contain true or false. The requets
with 422 response code will contain the error message as well.

.. sourcecode:: bash

    {"status": "true"}

.. sourcecode:: bash

    {"error": {"error_code": 422, "error_message": "Table can't be found in metastore"}}