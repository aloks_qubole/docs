.. _sensor-api-index:

==========
Sensor API
==========
Qubole supports file and partition sensors to monitor the file and Hive partition's availability. For more information,
see :ref:`file-partition-sensors`.

:ref:`Airflow <airflow-index>` uses file and partition sensors for programmatically monitoring workflows.

The APIs to create a file sensor and partition sensors are described in these topics:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    file-sensor-api
    partition-sensor-api
