.. _file-sensor-api:

========================
Create a File Sensor API
========================

.. http:post:: /api/v1.2/file_sensor

Use this API to create a file sensor API. :ref:`Airflow <airflow-index>` uses it for programmatically monitoring workflows.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a sensor. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| **files**                 | An array of the file's location that needs monitoring.                                                      |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"files": "["<file1 location>", "<file2 location>]" }' \ "https://api.qubole.com/api/v1.2/sensors/file_sensor"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
The following example creates a file sensor on AWS:

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"files": "["s3://abc/xyz", "s3://abc/123"]" }' \ "https://api.qubole.com/api/v1.2/sensors/file_sensor"

The requests with 200 response code will have just a status field which will either contain true or false. The requests
with 422 response code will contain the error message as well.

.. sourcecode:: bash

    {"status": "true"}

.. sourcecode:: bash

    {"error": {"error_code": 422, "error_message": "File list is missing or not in array format"}}
