.. _command-template-api:

####################
Command Template API
####################

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-command-template-api
    edit-command-template-api
    run-command-template-api
    clone-command-template-api
    view-command-template-api
    list-command-template-api
    remove-command-template-api
