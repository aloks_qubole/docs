.. _run-command-template-api:

======================
Run a Command Template
======================
.. http:post:: /api/v1.2/command_templates/<id>/run

Use this API to run a command template by using its unique ID. Qubole assigns an unique identifier (ID)
to each new command template once it is created.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows running a command template. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+--------------------+-------------------------------------------------------------------------------------------+
| Parameter          | Description                                                                               |
+====================+===========================================================================================+
| **input_vars**     | Specify values for the variables while running the template. For example, if you compose  |
|                    | a query that uses two variables, ``$Month$`` and ``$Country$``, the input variables with  |
|                    | default values (escaped) can be as:                                                       |
|                    | ``input_vars: [{"Month":"\"March\""}, {"Country":"\"US\""}]``. If default values are set  |
|                    | in the command template that is run, then those values are taken if you do not explicitly |
|                    | mention any value for a parameter/variable as part of ``input_vars``. Specify new values  |
|                    | for the variables to override the default values (if any).                                |
|                    | For more information, see :ref:`using-macros`.                                            |
+--------------------+-------------------------------------------------------------------------------------------+
| tag                | It is used to specify the cluster label on which this command must be run. If you want to |
|                    | run it on the default cluster, then this parameter is not required. You can use a ``tag`` |
|                    | or a ``label`` to specify the cluster label.                                              |
+--------------------+-------------------------------------------------------------------------------------------+
| label              | It is used to specify the cluster label on which this command must be run. If you want to |
|                    | run it on the default cluster, then this parameter is not required. You can use a ``tag`` |
|                    | or a ``label`` to specify the cluster label.                                              |
+--------------------+-------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the syntax using ``label`` for adding a cluster label.

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-Type: application/json"
    -d '{"input_vars":[{"<variable1>":"\"<value1>\""}, {"<variable2>":"\"<value2>\""}, {"<variable3>":"\"<value3>\""}],
         "label":"<cluster-label>"}'
    "https://api.qubole.com/api/v1.2/command_templates/<id>/run

Here is the syntax using ``tag`` for adding a cluster label.

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-Type: application/json" \
    -d '{"input_vars":[{"<variable1>":"\"<value1>\""}, {"<variable2>":"\"<value2>\""}, {"<variable3>":"\"<value3>\""}],
         "tag":"<cluster-label>"}' \
    "https://api.qubole.com/api/v1.2/command_templates/<id>/run

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is the sample API request to run a command template with ID 2134 that must be run on the cluster (with a200 as its label)

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-Type: application/json" \
    -d '{"input_vars":[{"Month":"\"March\""}, {"Country":"\"US\""}, {"Year":"\"2016\""}] "label":"a200"}' \
    "https://api.qubole.com/api/v1.2/command_templates/2134/run