.. _create-command-template-api:

=========================
Create a Command Template
=========================
.. http:post:: /api/v1.2/command_templates

Use this API to create a command template.

The Command Template is a QDS feature that provides you a template to compose a command/query once and modify a parameter
value multiple times or add another parameter along with its value. See :ref:`command-templates` for more information on
using the UI to create a command template.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a template. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+--------------------+-------------------------------------------------------------------------------------------+
| Parameter          | Description                                                                               |
+====================+===========================================================================================+
| **name**           | Enter a name for the command template. It can contain alphanumeric characters.            |
+--------------------+-------------------------------------------------------------------------------------------+
| **command_type**   | Denotes the command type. Qubole supports templates for DbExportCommand, DbImportCommand, |
|                    | DbTapQueryCommand, HiveCommand, HadoopCommand, PigCommand, PrestoCommand, ShellCommand,   |
|                    | refresh table, SparkCommand, and CompositeCommand.                                        |
+--------------------+-------------------------------------------------------------------------------------------+
| **command**        | Depending on the command type, specify the required and                                   |
|                    | optional parameters for that command type. See :ref:`command-api` for more information.   |
+--------------------+-------------------------------------------------------------------------------------------+
| input_vars         | It is a placeholder for variables whose values get substituted in the command template    |
|                    | when running it. While creating/editing a command template, you can specify default values|
|                    | for each variable that get picked when that command template runs. ``default_value`` is   |
|                    | the parameter to set default values. For example, if you compose a query that uses two    |
|                    | variables, ``$Month$`` and ``$Country$``, the input variables with default values         |
|                    | (escaped) can be as:                                                                      |
|                    |                                                                                           |
|                    | .. sourcecode:: bash                                                                      |
|                    |                                                                                           |
|                    |    input_vars: [{"name":"Month", "default_value":"\"January\""},                          |
|                    |                 {"name":"Country", default_value":"\"India\""}]                           |
|                    |                                                                                           |
+--------------------+-------------------------------------------------------------------------------------------+
| macros             | Denotes the macros that are valid assignment statements containing the variables and      |
|                    | its expression as: ``macros: [{"<variable>":<variable-expression>}, {..}]``. You can add  |
|                    | more than one variable. For more information, see :ref:`using-macros`.                    |
+--------------------+-------------------------------------------------------------------------------------------+
| command_tags       | Add a tag to a command so that it is easily identifiable and searchable from the command  |
|                    | templates' list. It acts as a filter value while searching templates. It can contain a    |
|                    | maximum of 255 characters. A comma-separated list of tags can be associated with a        |
|                    | single command template.                                                                  |
+--------------------+-------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the syntax for the request API.

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name":"<template-name>", "command_type":"HiveCommand", "command": {"query":"<query>"}, "command_tags":"<tag>",
    "input_vars":
    [{"name": "<variable1>", "default_value":"\"<default-value1>\""}, {"name": "<variable2>", "default_value":"\"<default-value2>\""},
    {...}, {"name": "<variablen>", "default_value":"\"<default-valuen>\""}],
    "macros":[{"<variable>":"\"<variable_expression>\""}]
    }' \ "https://api.qubole.com/api/v1.2/command_templates"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is the sample API request.

.. sourcecode:: bash

    curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name":"hivetemplate", "command_type":"HiveCommand",
    "command": {"query":"select * from default_qubole_memetracker  where created_at > '$sevendaysago' and state_name = $State$ and country_name = $Country$"},
    "command_tags":"airflow-cluster", "input_vars":
    [{"name": "Country", "default_value":"\"US\""}, {"name":"State", "default_value":"\"California\""}],
    "macros":[{"sevendaysago":"\"Qubole_nominal_time.clone().subtract('days', 7).strftime('%Y-%m-%d')\""}]
    }' \ "https://api.qubole.com/api/v1.2/command_templates"