.. _remove-command-template-api:

=========================
Delete a Command Template
=========================

.. http:delete:: /api/v1.2/command_templates/<id>

Use this API to delete an existing command template.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a template. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
None

Request API Syntax
------------------
Here is the syntax for the request API.

.. sourcecode:: bash

    curl -i -X DELETE -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/command_templates/<id>"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is the sample API request to delete the command template with 450 as its ID.

.. sourcecode:: bash

    curl -i -X DELETE -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/command_templates/450"