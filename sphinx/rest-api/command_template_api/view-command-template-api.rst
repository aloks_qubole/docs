.. _view-command-template-api:

=======================
View a Command Template
=======================
.. http:get:: /api/v1.2/command_templates/<id> or <name>

Use this API to view a specific command template by using its unique ID or name. Qubole assigns an unique identifier (ID)
to each new command template once it is created.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a template. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Request API Syntax
------------------
Here is the syntax for the request API.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/command_templates/<id> or <name>"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is the sample API request to view a template with its name as ``hivetemplate``.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/command_templates/hivetemplate"