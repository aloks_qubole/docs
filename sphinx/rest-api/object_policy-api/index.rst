.. _object_policy-api:

#################
Object Policy API
#################
Qubole supports managing access control for each notebook and cluster by overriding the access granted to the object
at the account-level in the **Control Panel**. Qubole supports overriding the account-level access to individual cluster
or notebook of a specific account through REST API calls. For more information, see :ref:`manage-roles`.

Managing access control to each object is described in these topics:

.. toctree::
    :maxdepth: 1
    :titlesonly:


    set-object-policy-cluster
    set-object-policy-note
    set-object-policy-scheduler
    set-object-policy-environment
    get-object-policy
