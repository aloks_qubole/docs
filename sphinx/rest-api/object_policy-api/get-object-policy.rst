.. _get-object-policy:

======================
View the Object Policy
======================

..  http:get:: /api/v1.2/object_policy/policy

Use this API to view a policy/policies set for an individual object. Since Qubole supports object policy API on notebooks,
clusters, package management environments, and scheduler, you can see the policy set for them.

Parameters
----------

.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------+------------------------------------------------------------------------------------------------+
| Parameter           | Description                                                                                    |
+=====================+================================================================================================+
| **source_id**       | It specifies the ID of the object, a cluster, an environment, a notebook, or a scheduler based |
|                     | on the ``source_type``.                                                                        |
+---------------------+------------------------------------------------------------------------------------------------+
| **source_type**     | It specifies the object. The values are ``cluster``, ``environment``, ``note``, and            |
|                     | ``scheduler``.                                                                                 |
+---------------------+------------------------------------------------------------------------------------------------+

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing the object policy. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d `{"source_id":"<Object_ID>", "source_type": "<Object>"}` \ "https://api.qubole.com/api/v1.2/object_policy/policy"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Requests
-------------------
Here is an example to see the object policy for a cluster with its ID as 2001.

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d `{"source_id":"2001", "source_type": "cluster"}` \ "https://api.qubole.com/api/v1.2/object_policy/policy"


Here is an example to see the object policy for a notebook with its ID as 250.

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d `{"source_id":"2001", "source_type": "note"}` \ "https://api.qubole.com/api/v1.2/object_policy/policy"