.. _set-object-policy-cluster:

===============================
Set Object Policy for a Cluster
===============================
You can set a policy for an individual object and restrict users or groups from accessing the object.
This overrides the access granted to the object at the account-level in the **Control Panel**. For more information,
see :ref:`manage-roles`.

:ref:`cluster-permissions` describes how to set cluster permissions through the QDS UI.

.. note:: :ref:`soft-enforcement-cluster-permissions` provides the list of cluster permissions that would be enforced
          with one cluster permission.

..  http:put:: /api/v1.2/object_policy/policy

Use this API to set an object policy. Qubole supports object policy API on notebooks and clusters. This section describes
setting an object policy for a cluster.

.. note:: If you allow a user with a permission who is part of the group that has restricted access, then that user
          is allowed access and vice versa. For more information, see :ref:`cluster-permission-precedence`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group or owner of the object.
* A user invoking this API must be part of a group associated with a role that allows setting an object policy. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------+------------------------------------------------------------------------------------------------+
| Parameter           | Description                                                                                    |
+=====================+================================================================================================+
| **source_id**       | It specifies the ID of the object based on the ``source_type``.                                |
+---------------------+------------------------------------------------------------------------------------------------+
| **source_type**     | It specifies the object. It must be a ``cluster`` for a cluster.                               |
+---------------------+------------------------------------------------------------------------------------------------+
| **policy**          | Array of policies to be assigned to a cluster. Each policy include following parameters:       |
|                     |                                                                                                |
|                     | .. note:: Escape the values of policy elements and corresponding values except the             |
|                     |           **user ID value** and **group ID value**.                                            |
|                     |                                                                                                |
|                     | * **action**: Name of the action with particular resource. The actions can be ``read``,        |
|                     |   ``update``, ``delete``, ``manage``, or ``all``. The actions imply as given below:            |
|                     |                                                                                                |
|                     |   - ``read``: This action is set to allow/deny a user/group to view the object.                |
|                     |   - ``update``: This action is set to allow/deny a user/group to edit the object.              |
|                     |   - ``delete``: This action is set to allow/deny a user/group to delete the object.            |
|                     |   - ``manage``: This action allows the user/group to manage the cluster's permissions.         |
|                     |   - ``all``: This action is set to allow/deny a user/group to do read/edit/delete the object.  |
|                     |     It gets the lowest priority always. That is ``read``, ``update``, and ``delete`` actions   |
|                     |     get precedence over the ``all`` action.                                                    |
|                     |   - ``start``: This action is set to allow/deny a user/group to start the cluster.             |
|                     |   - ``terminate``: This action is set to allow/deny a user/group to terminate the cluster.     |
|                     |                                                                                                |
|                     | * **access**: It is set to allow or deny actions. Its value is either `allow` or `deny`.       |
|                     | * **condition**: Array of user IDs and group IDs that have to be assigned with this policy:    |
|                     |                                                                                                |
|                     |   - **qbol_users**: An array of IDs of the user for whom this policy needs to be set. But      |
|                     |     these IDs are not similar to user IDs and create a ticket with                             |
|                     |     `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to get the user IDs.              |
|                     |   - **qbol_groups**: An array of IDs of the groups for whom this policy needs to be set.       |
|                     |                                                                                                |
+---------------------+------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"source_id":"<Object_ID>", "source_type": "<Object>",
          "policy": "[{\"access\":\"<Access>\",\"condition\":{\"qbol_users\":[<User ID>]},\"action\":[\"<Actions>\"]},
                     {\"access\":\"<Access>\",\"condition\":{\"qbol_groups\":[<Group ID>]},\"action\":[\"<Actions>\"]},
                     {\"access\":\"<Access>\",\"condition\":{\"qbol_users\":[<User ID>],\"qbol_groups\":[<Group ID>]},\"action\":[\"<Actions>\"]}]"}' \
    "https://api.qubole.com/api/v1.2/object_policy/policy"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is a sample API call to set an object policy for a cluster with its ID as 2001.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"source_id":"2001", "source_type": "cluster",
          "policy": "[{\"access\":\"allow\",\"condition\":{\"qbol_users\":[1715]},\"action\":[\"read\"]},
                     {\"access\":\"allow\",\"condition\":{\"qbol_groups\":[2352]},\"action\":[\"read\",\"update\"]},
                     {\"access\":\"deny\",\"condition\":{\"qbol_users\":[1715],\"qbol_groups\":[2352]},\"action\":[\"all\"]}]"}' \
    "https://api.qubole.com/api/v1.2/object_policy/policy"