.. _create-dashboard-api:

===================
Create a Dashboard
===================

..  http:post:: /api/v1.2/notebook_dashboard

Use this API to create a dashboard.  To know how to create a dashboard using the **Notebooks** UI, see :ref:`dashboards-publisher`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **name**                    | Name of the dashboard.                                                                                         |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **note_id**                 | Id of the notebook that has to be associated with the dashboard.                                               |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| is_scheduled                | Specifies if the dashboard has to be scheduled. Possible values are ``true`` or ``false``.                     |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| frequency                   | Specifies how often the schedule should run. Input is an integer.                                              |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| time_unit                   | Denotes the time unit for the frequency. Possible values are months, weeks, days, hours or minutes.            |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **location**                | Location of the folder. **Users/current_user_email_id** is the default location.                               |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X POST  -H 'X-AUTH-TOKEN: <AUTH TOKEN>'  -H 'Content-Type: application/json'  -H 'Accept: application/json'
   -d  '{"note_id":<notebook-id>,  "is_scheduled":"<true or false>", "frequency": <number>, "time_unit":<months, weeks, days, hours or minutes>,
    "name":"Name", "location":"<folder-location>"}'
   "https://api.qubole.com/api/v1.2/notebook_dashboard"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.


Sample API Request
..................


.. sourcecode:: bash

   curl -X POST  -H 'X-AUTH-TOKEN: <AUTH TOKEN>'  -H 'Content-Type: application/json'  -H 'Accept: application/json'
   -d  '{"note_id":97480,  "is_scheduled":"true", "frequency": 1440, "time_unit":"minutes", "name":"Sample", "location":"Users/email"}'
   "https://api.qubole.com/api/v1.2/notebook_dashboard"
