.. _dashboard-api:


##############
Dashboard API
##############

This section explains how to create a dashboard and list dashboards by using the REST APIs. For more
information on **Dashboards** UI, see :ref:`dashboards-index`. This section covers:

.. toctree::
    :maxdepth: 1
    :titlesonly:


    create-dashboard
    list-dashboard
