.. _list-dashboard-api:

===================
List Dashboards
===================

..  http:get:: /api/v1.2/notebooks/<note_id>/list_dashboards

Use this API to list dashboards for a notebook.  To view all the dashboards associated with the notebook using the **Notebooks** UI, see :ref:`view-all-dashboards`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **note_id**                 | Id of the notebook for which the dashboards have to be listed.                                                 |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET  -H 'X-AUTH-TOKEN: <AUTH TOKEN>'  -H 'Content-Type: application/json'  -H 'Accept: application/json'
   "https://api.qubole.com/api/v1.2/notebooks/<note_id>/list_dashboards"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.


Sample API Request
..................


.. sourcecode:: bash

   curl -X GET  -H 'X-AUTH-TOKEN: <AUTH TOKEN>'  -H 'Content-Type: application/json'  -H 'Accept: application/json'
   "https://api.qubole.com/api/v1.2/notebooks/97480/list_dashboards"

