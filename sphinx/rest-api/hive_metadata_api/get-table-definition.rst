.. _get-table-definition:

View a Hive Table Definition
============================

.. http:get:: /api/v1.2/hive/<schema_name>/<table>

Use this API to get the Hive table definition. This API also fetches AVRO table schema.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows  viewing a Hive table definition.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
-------

.. sourcecode:: bash

    curl -i -X GET -H "Accept: application/json" -H "Content-type: application/json" -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    https://api.qubole.com/api/v1.2/hive/default/miniwikistats

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

.. sourcecode:: json

    [
        {
            "name": "projcode",
            "type": "string",
            "comment": null
        },
        {
            "name": "pagename",
            "type": "string",
            "comment": null
        },
        {
            "name": "pageviews",
            "type": "int",
            "comment": null
        },
        {
            "name": "bytes",
            "type": "int",
            "comment": null
        },
        {
            "name": "dt",
            "type": "string",
            "comment": null
        }
    ]
