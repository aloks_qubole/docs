.. _schema-or-database:

Schema or Database
==================

.. http:get:: /api/v1.2/hive/default/

Use this API to view the Hive tables in Qubole.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing Hive tables in Qubole.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
~~~~~~~~~~

+-------------+-------------------------------------------------------------------------+
| Parameter   | Description                                                             |
+=============+=========================================================================+
| filter      | A regular expression to filter the result.                              |
+-------------+-------------------------------------------------------------------------+
| describe    | Value has to be \ *true*. The result contains columns for each table.   |
+-------------+-------------------------------------------------------------------------+

Returns a json array of tables available in Qubole.


Example
-------

**Goal**

With filter.

.. sourcecode:: bash

    curl -i -X GET \
    -H "Accept: application/json" \
    -H "Content-type: application/json" \
    -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    "https://api.qubole.com/api/v1.2/hive/default/?filter=.*qubole.*"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

.. sourcecode:: http

    ["default_qubole_airline_origin_destination","default_qubole_memetracker"]


**Goal**

With filter and describe.

.. sourcecode:: bash

    curl -i -X GET \
    -H "Accept: application/json" \
    -H "Content-type: application/json" \
    -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    "https://api.qubole.com/api/v1.2/hive/default/?filter=.*qubole.*&describe=true"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

.. sourcecode:: json

    [
      {
        "default_qubole_airline_origin_destination": {
          "columns": [
            {
              "name": "break",
              "type": "string"
            },
            {
              "name": "coupongeotype",
              "type": "string"
            },
            {
              "name": "coupons",
              "type": "string"
            },
            {
              "name": "coupontype",
              "type": "string"
            },
            {
              "name": "dest",
              "type": "string"
            },
            {
              "name": "destaptind",
              "type": "string"
            },
            {
              "name": "destcitynum",
              "type": "string"
            },
            {
              "name": "destcountry",
              "type": "string"
            },
            {
              "name": "deststate",
              "type": "string"
            },
            {
              "name": "deststatefips",
              "type": "string"
            },
            {
              "name": "deststatename",
              "type": "string"
            },
            {
              "name": "destwac",
              "type": "string"
            },
            {
              "name": "distance",
              "type": "string"
            },
            {
              "name": "distancegroup",
              "type": "string"
            },
            {
              "name": "fareclass",
              "type": "string"
            },
            {
              "name": "gateway",
              "type": "string"
            },
            {
              "name": "itingeotype",
              "type": "string"
            },
            {
              "name": "itinid",
              "type": "string"
            },
            {
              "name": "mktid",
              "type": "string"
            },
            {
              "name": "opcarrier",
              "type": "string"
            },
            {
              "name": "origin",
              "type": "string"
            },
            {
              "name": "originaptind",
              "type": "string"
            },
            {
              "name": "origincitynum",
              "type": "string"
            },
            {
              "name": "origincountry",
              "type": "string"
            },
            {
              "name": "originstate",
              "type": "string"
            },
            {
              "name": "originstatefips",
              "type": "string"
            },
            {
              "name": "originstatename",
              "type": "string"
            },
            {
              "name": "originwac",
              "type": "string"
            },
            {
              "name": "passengers",
              "type": "string"
            },
            {
              "name": "quarter",
              "type": "string"
            },
            {
              "name": "rpcarrier",
              "type": "string"
            },
            {
              "name": "seqnum",
              "type": "string"
            },
            {
              "name": "tkcarrier",
              "type": "string"
            },
            {
              "name": "year",
              "type": "string"
            }
          ]
        }
      },
      {
        "default_qubole_memetracker": {
          "columns": [
            {
              "name": "lnks",
              "type": "string"
            },
            {
              "name": "phr",
              "type": "string"
            },
            {
              "name": "site",
              "type": "string"
            },
            {
              "name": "ts",
              "type": "string"
            },
            {
              "name": "month",
              "type": "string"
            }
          ]
        }
      }
    ]