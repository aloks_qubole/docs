.. _store-table-properties:

Store Table Properties
======================

..  http:post:: /api/v1.2/hive/schema/table

Modify metadata of tables in the given schema of the hive metastore.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows modifying Hive table metadata in
  Qubole. See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+----------------------+--------------------------------------------------------------------------------------------+
|Parameter             | Description                                                                                |
+======================+============================================================================================+
| **interval**         | Number representing the interval at which data is loaded.                                  |
+----------------------+--------------------------------------------------------------------------------------------+
|**interval_unit**     | Unit of the interval. Valid values are *minutes, hours, days, weeks and months*.           |
+----------------------+--------------------------------------------------------------------------------------------+
|columns               | JSON Hash with Date/Time Format of partition columns. Date format should be a valid input  |
|                      | to the *strftime* function. If there are no partition columns, then it should be an empty  |
|                      | hash. For partition columns that are not date/time, the value should be an empty string.   |
+----------------------+--------------------------------------------------------------------------------------------+


Example
-------

Imagine a table `daily_tick_data` in the `default` hive schema that has the following partitions

#. stock_exchange
#. stock_symbol
#. year
#. date

.. sourcecode:: bash

   $ cat payload.json
   {
    "interval": 1,
    "interval_unit": "days",
    "columns": {
        "stock_exchange": "",
        "stock_symbol": "",
        "year": "%Y",
        "date": "%Y-%m-%d"
    }
   }
   $ curl -i -X POST -H "Accept: application/json" \
        -H "Content-type: application/json" \
        -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
        --data @payload.json \
          https://api.qubole.com/api/v1.2/hive/default/daily_tick_data/properties

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: json

    {"status":"successful"}

