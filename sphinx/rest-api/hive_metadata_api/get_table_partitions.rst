.. _get_table_partitions:

View Table Partitions and Location
==================================

.. http:get:: /api/v1.2/hive/<schema_name>/<table>/partitions

Use this API to get the Hive table partitions and locations.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows  viewing a Hive table definition.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
-------

.. sourcecode:: bash

    curl -X GET -H "Accept: application/json" -H "Content-type: application/json" -H "X-AUTH-TOKEN: 02a9528b98ad4b0c8abe
    50fd4535960d7777s37b18514fffa41017f5dac16e2b" "https://api.qubole.com/api/v1.2/hive/default/cars/partitions"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

.. sourcecode:: json

    [
         {
            "PART_NAME":"year=2013/month=1",
            "LOCATION":"s3n://dev.canopydata.com/sqoop/qa/account_id/4646/warehouse/cars/year=2013/month=1"
         }
    ]