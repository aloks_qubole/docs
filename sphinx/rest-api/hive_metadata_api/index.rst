.. _hive-metadata-api:

#################
Hive Metadata API
#################

.. toctree::
    :maxdepth: 1
    :titlesonly:

    schema-or-database
    get-table-definition
    store-table-properties
    get-table-properties
    delete-table-properties
    get_table_partitions
