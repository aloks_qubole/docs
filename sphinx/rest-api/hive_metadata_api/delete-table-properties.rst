.. _delete-table-properties:

Delete Table Properties
=======================

.. http:delete:: /api/v1.2/hive/default/(string:table_name)/properties

Delete properties associated with a Hive table.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows deleting a Hive table's properties.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.


Example
-------

**Goal**

To delete the Hive table properties.

.. sourcecode:: bash

   curl -i -X DELETE -H "Accept: application/json" \
   -H "Content-type: application/json" \
   -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
   "https://api.qubole.com/api/v1.2/hive/default/daily_tick_data/properties"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Response
--------
The response will be a JSON object with success or failure information.

.. sourcecode:: http

   HTTP/1.1 200 OK
   Content-Type: application/json; charset=utf-8

   {"status":"successful"}