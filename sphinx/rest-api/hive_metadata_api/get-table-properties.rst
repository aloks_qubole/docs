.. _get-table-properties:

Get Table Properties
====================

.. http:get:: /api/v1.2/hive/<schema_name>/<table_name>/table_properties

Use this API to get the Hive table properties.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows  viewing a Hive table's properties.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
-------

.. sourcecode:: bash

    curl -i -X GET -H "Accept: application/json" -H "Content-type: application/json" -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    "https://api.qubole.com/api/v1.2/hive/default/daily_tick_data/table_properties"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

.. sourcecode:: json

    {
        "location": "s3n://paid-qubole/default-datasets/stock_ticker",
        "owner": "ec2-user",
        "create-time": 1362388416,
        "table-type": "EXTERNAL_TABLE",
        "field.delim": ",",
        "serialization.format": ","
    }