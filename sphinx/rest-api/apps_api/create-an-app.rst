.. _create-an-app:

=============
Create an App
=============
.. http:post::  /api/v1.2/apps

Use this API to create a Spark Job Server App. The Spark Job Server allows you to start a long-running Spark application
and fire many code snippets at that Spark application. The benefits of using a Job Server are:

* Code snippets do not incur startup cost making the application run faster.
* One code snippet can depend on the result of the previous code snippet and thus the application runs incremently.

For more information on Spark Job Server, see :ref:`spark-job-server`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating an account. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+---------------------------+-------------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                                 |
+===========================+=============================================================================================================+
| **name**                  | Name of the Spark Job Server App.                                                                           |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **config**                | This parameter denotes the Spark job configuration along with its value. It allows an array of Spark Job    |
|                           | configuration and its value.                                                                                |
+---------------------------+-------------------------------------------------------------------------------------------------------------+
| **kind**                  | This parameter denotes the type of application. Currently, Qubole supports only **Spark** application.      |
+---------------------------+-------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{ "name": "<App Name>", "config":[ {"<Job Config>": "<value>"}, {"<Job Config>": "<value>"}] "kind": "spark"}' \
       "https://api.qubole.com/api/v1.2/apps"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request API to create a new Spark Job Server App.

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{ "name": "sparkjob_app", "config": {"spark.executor.memory": "3g"}, "kind": "spark"}' \
       "https://api.qubole.com/api/v1.2/apps"