.. _apps-api:

########
Apps API
########

This API is currently supported only to create/list/delete Spark Job Server apps. Fore more information on Spark Job
Server, see :ref:`spark-job-server`. The APIs are described in these topics:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-an-app.rst
    list-apps.rst
    delete-apps.rst

