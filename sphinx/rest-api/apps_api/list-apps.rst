.. _list-apps:

=========
List Apps
=========
.. http:get:: /api/v1.2/apps/

Use this API to get the list of Spark Job Server Apps.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating an account. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
None

Request API Syntax
------------------
Here is the API Syntax.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Accept: application/json" -H "Content-type: application/json" \
    "https://api.qubole.com/api/v1.2/apps"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.