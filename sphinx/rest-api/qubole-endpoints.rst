.. _qubole-endpoints:

=======================================================
Supported Qubole Endpoints on Different Cloud Providers
=======================================================
Qubole provides QDS on AWS, Microsoft Azure and Oracle Cloud infrastructure clouds on different endpoints that are as
mentioned below.

.. note:: In JDBC and ODBC driver configurations, https://api.qubole.com is the default endpoint. The suffix /api/${V}/
          is not required in JDBC/ODBC driver configurations.

Supported Qubole Endpoints on QDS-on-AWS
----------------------------------------
Qubole provides QDS on the AWS cloud through different endpoints, which are entry points to access QDS. For more
information, see :ref:`api-overview`.

Qubole now supports only HTTPS. All HTTP requests are now redirected to HTTPS. This is aimed at better security for
Qubole users.

The endpoints are described in the following table. APIs for QDS-on-AWS are supported on API ``v1.2``, ``v1.3``, and
``v2`` versions even though only some QDS components are supported only on v1.3 and v2 versions. The supported list
is mentioned in :ref:`api-overview`.

+----------------------------------------------+------------------------------------------------------------------------------------------------------+------------------------------------+
| API Endpoints/Access URLs                    | QDS Environment/Description                                                                          | Security Certification             |
+==============================================+======================================================================================================+====================================+
| ``https://api.qubole.com/api/${V}/``         | https://api.qubole.com is the URL to access UI. It is the oldest and default environment to access   | NA                                 |
|                                              | QDS on the AWS cloud.                                                                                |                                    |
+----------------------------------------------+------------------------------------------------------------------------------------------------------+------------------------------------+
| ``https://eu-central-1.qubole.com/api/${V}/``| https://eu-central-1.qubole.com is the correct URL. It is the environment for QDS on the AWS         | SOC2 and ISO 27001 compliant       |
|                                              | cloud.                                                                                               |                                    |
+----------------------------------------------+------------------------------------------------------------------------------------------------------+------------------------------------+
| ``https://in.qubole.com/api/${V}/``          | https://in.qubole.com is the correct URL. It is the environment for QDS on the AWS cloud.            | SOC2 and ISO 27001 compliant       |
+----------------------------------------------+------------------------------------------------------------------------------------------------------+------------------------------------+
| ``https://us.qubole.com/api/${V}/``          | https://us.qubole.com is the correct URL. It is the environment for QDS on the AWS cloud.            | SOC2 and ISO 27001 compliant       |
+----------------------------------------------+------------------------------------------------------------------------------------------------------+------------------------------------+
| ``https://wellness.qubole.com/${V}``         | https://wellness.qubole.com/ is the correct URL. It is the environment for QDS on the AWS cloud.     | HIPAA, SOC2, and ISO 27001         |
|                                              | The use of https://wellness.qubole.com falls under the HIPAA compliance safeguards and requires a    | compliant                          |
|                                              | Business Associates Agreement (BAA) prior to use. For more information, contact the Account          |                                    |
|                                              | Executive or `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.                               |                                    |
+----------------------------------------------+------------------------------------------------------------------------------------------------------+------------------------------------+

Supported Qubole Endpoints on QDS-on-Azure
------------------------------------------
Qubole provides QDS on the Microsoft Azure cloud through the endpoint that is mentioned in this table. APIs for QDS-on-Azure
are supported on api/v2, so, ``${V}`` = ``v2``.

+------------------------------------------------+----------------------------------------------------------------------------------------------+------------------------------------+
| Endpoint                                       | QDS Environment/Description                                                                  | Security Certification             |
+================================================+==============================================================================================+====================================+
| ``https://azure.qubole.com/api/${V}/``         | https://azure.qubole.com/ is the correct URL. It is the environment to access QDS on the     | SOC2 and ISO 27001 compliant       |
|                                                | Microsoft Azure cloud.                                                                       |                                    |
+------------------------------------------------+----------------------------------------------------------------------------------------------+------------------------------------+

Supported Qubole Endpoints on QDS-on-OCI
----------------------------------------
Qubole provides QDS on the Oracle Cloud Infrastructure (OCI) cloud through the endpoint that is mentioned in this table.
APIs for QDS-on-Azure are supported on api/v2, so, ``${V}`` = ``v2``.

+-------------------------------------------------+----------------------------------------------------------------------------------------------+------------------------------------+
| Endpoint                                        | QDS Environment/Description                                                                  | Security Certification             |
+=================================================+==============================================================================================+====================================+
| ``https://oraclecloud.qubole.com/api/${V}/``    | https://oraclecloud.qubole.com/ is the correct URL. It is the environment to access QDS on   | SOC2 and ISO 27001 compliant       |
|                                                 | the OCI cloud.                                                                               |                                    |
+-------------------------------------------------+----------------------------------------------------------------------------------------------+------------------------------------+