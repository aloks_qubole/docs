.. _api-overview:

Overview
========
The Qubole Data Service (QDS) is accessible via REST APIs.

To write and test applications that use these APIs, you can use any HTTP client in any
programming language to interact with Qubole Data Service. The detailed syntax of the calls is
described in subsequent sections.

You can also use the `QDS Python SDK <https://github.com/qubole/qds-sdk-py>`_
or the `QDS Java SDK <https://github.com/qubole/qds-sdk-java>`_ to interact with
the Qubole Data Service. The Python SDK also provides an easy-to-use
command-line interface.

.. note:: Qubole now supports only HTTPS. All HTTP requests are now redirected to HTTPS. This is aimed at better
          security for Qubole users.

Access URL
==========
All URLs referenced in the documentation have the following base:

``https://api.qubole.com/api/${V}/``

where ``${V}`` refers to the version of the API. Valid values of ``${V}`` are currently ``v1.2``, ``v1.3``, and ``v2`` for
APIs for QDS-on-AWS. The value of ``${V}`` is only ``v2`` for APIs for QDS-on-Azure and QDS-on-OCI.

Qubole supports other access URLs or endpoints that you can use to access QDS that are described in
:ref:`qubole-endpoints`.

Here are how features are supported on different API versions and accordingly described in the corresponding topics.

API Versions Supported on QDS-on-AWS
------------------------------------
The APIs on QDS-on-AWS are supported as follows:

* Only :ref:`cluster-api` is supported on v1.3. The API syntax for v1.2 Cluster API is entirely different and the latest
  features are not supported on it. The documentation is based on v1.3.
* All other APIs are only supported on v1.2. The documentation only describes the syntax and sample API calls with respect
  to v1.2.

API Versions Supported on QDS-on-Azure and QDS-on-OCI
-----------------------------------------------------
:ref:`qubole-azure` and :ref:`qubole-oracle-bmc` are only supported on API version 2 that is v2.

cURL is a useful tool for testing out Qubole REST API calls from the command line.

The Qubole API is served over HTTPS and Qubole redirects all HTTP access to HTTPS. Qubole now supports only HTTPS access,
which is aimed at better security for Qubole users.  When using HTTPS, ensure that the certificates on the client
machine are up-to-date.

.. _authentication:

Authentication
==============
API calls must be authenticated with a **Qubole API Token**.

Navigate to **Control Panel** in the QDS UI and click the **My Accounts** tab on the
left pane. Click **Show** for the account and copy the API token that is displayed.

Set the value of this API token to the ``AUTH_TOKEN`` environment variable when running the API examples via ``curl``.

.. note:: Qubole supports access URLs or endpoints that you can use to access QDS that are described in :ref:`qubole-endpoints`.

API Types
=========
The Qubole REST APIs are broadly divided into the following categories:

* APIs for Qubole on Amazon Web Service:

  - `Account API`_
  - `Apps API`_
  - `Cluster API`_
  - `Command API`_
  - `Command Template API`_
  - `Custom Metastore API`_
  - `DbTap API`_
  - `Folder API`_
  - `Group API`_
  - `Hive Metadata API`_
  - `Notebook API`_
  - `Object Policy API`_
  - `Reports API`_
  - `Role API`_
  - `Scheduler API`_
  - `Sensor API`_
  - `Users API`_

* :ref:`qubole-azure`
* :ref:`qubole-oracle-bmc`

.. _account_api:

Account API
-----------
These APIs let you to create a new account, edit and delete an existing account, view users, and set Hive bootstrap. See
:ref:`account-api` for more information.

Apps API
--------
These APIs allow you to create, list, and delete a Spark Job Server App. For more information, see :ref:`apps-api`.

.. _cluster_api:

Cluster API
-----------
These APIs allow you to create a new cluster, edit, restart, and delete an existing clusters. These APIs also allow you to
view the list of clusters in a Qubole account, and add and remove nodes in some clusters. See
:ref:`cluster-api` for more information.

.. _command_api:

Command API
-----------
The Command APIs let you submit queries and commands, check the status of commands,
retrieve results and logs, or cancel commands. See :ref:`command-api` for more information. The Qubole Data Service
currently supports these command types:

* :ref:`DbImport commands<submit-a-db-import-command>`
* :ref:`DbExport commands<submit-a-db-export-command>`
* :ref:`DBTap commands <submit-a-db-tap-query-command>`
* :ref:`Hive commands<submit-a-hive-command>`
* :ref:`Hadoop jobs<submit-a-hadoop-jar-command>`
* :ref:`Pig commands<submit-a-pig-command>`
* :ref:`Presto commands<submit-a-presto-command>`
* :ref:`Refresh Table command <submit-a-refresh-table-command>`
* :ref:`Shell commands<submit-a-shell-command>`
* :ref:`Spark commands <submit-a-spark-command>`
* :ref:`Workflow commands <submit-a-composite-command>`

.. _command_template_api:

Command Template API
--------------------
Use a commmand template API to run a command more than one time and run the command with different parameter values.
For more information, see :ref:`command-template-api`.

Custom Metastore API
--------------------
These APIs allow you to connect to a custom metastore, edit and view the metastore details. For more information, see
:ref:`custom-metastore-api`.

.. _dbtap_api:

DbTap API
---------
A DbTap identifies an external end point for import/export of data from QDS, such as a MySQL instance.
The DbTap APIs let you create, view, edit or delete a DbTap. For more information, see :ref:`dbtap-api`.

Folder API
----------
This API is mainly used to create and manage a Notebook/Dashboard folders. For more information, see :ref:`folder-api-index`.

.. _group_api:

Group API
---------
These APIs allow you to create groups, add/delete users in a group, and assign/unassign roles to a group in a Qubole
account. For more information, see :ref:`groups-api-index`.

.. _hive_metadata_api:

Hive Metadata API
-----------------
These APIs provide a set of read-only views that describe your Hive tables and the metadata.
For more information, see :ref:`hive-metadata-api`.

Notebook API
------------
These APIs allow you to create a notebook, clone, configure, run, import, and delete a notebook. For more information,
see :ref:`notebook-api`.

Object Policy API
-----------------
These APIs allow you to create object-level access policies for notebooks and clusters. For more information, see
:ref:`object_policy-api`.

.. _reports_api:

Reports API
-----------
These APIs let you view aggregated statistical and operational data for your commands. For more information, see
:ref:`reports-api`.

.. _role_api:

Role API
--------
These APIs allow you to create and delete a role to perform a set of actions in a Qubole account. For more information,
see :ref:`roles-api`.

.. _scheduler_api:

Scheduler API
-------------
The Scheduler APIs let you schedule any command or workflow to run at regular intervals. For more information, see
:ref:`scheduler-api`.

Sensor API
----------
These APIs allow you to create file and partition sensors to monitor the file and Hive partition's availability.
For more information, see :ref:`sensor-api-index`.

.. _users_api:

Users API
---------
These APIs let you view invite a user to a Qubole account and enable/disable users in a Qubole account. For more
information, see :ref:`users-api`.