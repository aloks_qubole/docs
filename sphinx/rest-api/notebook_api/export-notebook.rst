..  _export-notebook-api:

=================
Export a Notebook
=================

.. http:get:: /v2/notes/note_id/export

Use this API to export a notebook in the JSON format.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.


Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **note_id**                 | Id of the notebook that has to be exported.                                                                    |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+


Request API Syntax
------------------

.. sourcecode:: bash

   curl -X GET  -H 'X-AUTH-TOKEN: <AUTH TOKEN>'  -H 'Content-Type: application/json'  -H 'Accept: application/json'
   "https://api.qubole.com/v2/notes/<note_id>/export"


.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................

Here is an example to export a Spark notebook.

.. sourcecode:: bash

  curl -X GET  -H 'X-AUTH-TOKEN: <AUTH TOKEN>'  -H 'Content-Type: application/json'  -H 'Accept: application/json'
  "https://api.qubole.com/v2/notes/26962/export"

