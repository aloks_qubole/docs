.. _configure-notebook-api:

====================
Configure a Notebook
====================

..  http:put:: /api/v1.2/notebooks/<notebook ID>

Use this API to configure/edit a notebook. You cannot change the notebook type while configuring or editing it.

You can change the cluster associated with the notebook only when the following conditions are met:

* Notebook does not have any active command.
* Notebook does not have any active schedules associated.
* Notebook does not have any scheduled dashboard.

To know how to configure a notebook using the **Notebooks** UI, see :ref:`modify-notebook`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| name                        | It is the name of the notebook. It is a string and can accept alpha-numerical characters.                      +
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| location                    | It is the location of the folder. By default, it goes to **Users/current_user_email_id** folders. For more     |
|                             | information on notebook folders, see :ref:`folders-notebooks`. The accepted folder locations are:              |
|                             | **Users/current_user_email_id**, **Common**, and **Users**. The default location is                            |
|                             | **Users/current_user_email_id** and it is equivalent to **My Home** on the **Notebooks** UI. You need          |
|                             | privileges to create/edit notebooks in **Common** and **Users**. For more information, see                     |
|                             | :ref:`manage-folder-permissions`.                                                                              |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| default_lang                | Default language of the notebook. Default value is ``spark``. Other possible values are ``pyspark``, ``sql``,  |
|                             | and ``r``.                                                                                                     |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| cluster_id                  | The ID of the cluster to which the notebook is assigned. You must assign the notebook to a cluster             |
|                             | to use it, though you can leave the notebook unassigned when you create it. Make sure you assign               |
|                             | the notebook to the appropriate type of cluster: for example, assign a Spark notebook to a Spark cluster.      |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"<Name>", "location":"<Location>", "cluster_id":"<Cluster ID>", "default_lang":"<language>"}' \
   "https://api.qubole.com/api/v1.2/notebooks/<notebook ID>"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is an example to configure a Spark notebook with its ID as 2000.

.. sourcecode:: bash

   curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"Spark", "location":"Users/user@abc.com/Notebooks", "cluster_id":"4001", "default_lang":"sql"}' \
   "https://api.qubole.com/api/v1.2/notebooks/2000"