.. _bind-notebook-api:

============================
Bind a Notebook to a Cluster
============================

..  http:put:: /api/v1.2/notebooks/<notebook ID>

Use this API to assign a cluster to a notebook. To know how to bind a notebook using the **Notebooks** UI, see
:ref:`view-notebook` or :ref:`modify-notebook`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **cluster_id**              | The cluster ID of the cluster that you want to get it assigned to a specific notebook.                         |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"cluster_id":"<Cluster ID"}' \
    "https://api.qubole.com/api/v1.2/notebooks/<notebook ID>"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is a sample request to assign a notebook with its ID as 2002 to a cluster with its ID as 4002

.. sourcecode:: bash

   curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"cluster_id":"4002"}' \
    "https://api.qubole.com/api/v1.2/notebooks/2002"