.. _notebook-api:

############
Notebook API
############
This section explains how to create, configure, clone, bind, run, and delete a notebook through REST API calls. For more
information on **Notebooks** UI, see :ref:`notebook-index`. This section covers:

.. toctree::
    :maxdepth: 1
    :titlesonly:


    create-notebook
    configure-notebook
    import-notebook
    export-notebook
    clone-notebook
    bind-notebook
    run-notebook
    delete-notebook


