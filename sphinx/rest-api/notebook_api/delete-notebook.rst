.. _delete-notebook-api:

=================
Delete a Notebook
=================

..  http:delete:: /api/v1.2/notebooks/<notebook ID>

Use this API to delete a notebook. To know how to clone a notebook using the **Notebooks** UI, see :ref:`delete-notebook`.

Before deleting a notebook, you must ensure that the notebook does not have any active command, active schedules associated with the notebook, or any scheduled dashboard.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: No additional parameters are required in the delete a notebook API call.

Request API Syntax
------------------

.. sourcecode:: bash

   curl -i -X DELETE -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/notebooks/<notebook ID>"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is an example to delete a Spark notebook with its ID as 2000.

.. sourcecode:: bash

   curl -i -X DELETE -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/notebooks/2000"