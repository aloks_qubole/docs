..  _import-notebook-api:

=================
Import a Notebook
=================

.. http:post:: /api/v1.2/notebooks/import

Use this API to import a Spark notebook from a location and add it to the notebooks list in the QDS account.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values. Presto is not
          currently supported on all Cloud platforms; see :ref:`os-version-support`.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **name**                    | It is the name of the notebook. It is a string and can accept alpha-numerical characters.                      +
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| location                    | It is the location of the folder. By default, it goes to **Users/current_user_email_id** folders. For more     |
|                             | information on notebook folders, see :ref:`folders-notebooks`. The accepted folder locations are:              |
|                             | **Users/current_user_email_id**, **Common**, and **Users**. The default location is                            |
|                             | **Users/current_user_email_id** and it is equivalent to **My Home** on the **Notebooks** UI. You need          |
|                             | privileges to create/edit notebooks in **Common** and **Users**. For more information, see                     |
|                             | :ref:`manage-folder-permissions`.                                                                              |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **note_type**               | It is the type of notebook. The values are ``hive``, ``deep_learning`` ``spark`` and ``presto``.               |
|                             | Hive and Deep Learning are beta features. The Hive notebook beta-feature is available by default. To enable    |
|                             | Deep Learning on a QDS account, create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__. |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **file**                    | It is a parameter that you can only specify to import a notebook from a location on the local hard disk.       |
|                             | For adding the complete location path, you must start it with ``@/``. For example, ``"file":"@/home/spark...``.|
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **nbaddmode**               | It must only be used with the **file** parameter. Its value is ``import-from-computer``.                       |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **url**                     | It is the AWS S3 location, a valid JSON URL, or an ipynb URL of the notebook that you want to import.          |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **cluster_id**              | It is the ID of the cluster to which the notebook is assigned. If you specify this parameter, then the         |
|                             | notebook is imported with the attached cluster.                                                                |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------
**Syntax to use for importing a notebook that is on an AWS S3 bucket.**

.. sourcecode:: bash

   curl -X "POST" -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:multipart/form-data" -H "Accept: application/json" \
    -F "name"="<Name>" -F "location"="<Location>" -F "note_type"="<Note Type>" -F "url"="<S3 location in the URL
    format/valid-JSON-URL/ipynb-URL>" \
     "https://api.qubole.com/api/v1.2/notebooks/import"

**Syntax to use for importing a notebook that is on a local hard disk.**

.. note:: For adding the file location in the local hard disk, you must start it with ``@/``.
          For example, ``"file":"@/home/spark...``.


.. sourcecode:: bash

   curl   -X "POST" -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: multipart/form-data" -H "Accept: application/json" \
   -F "name"="<Name>" -F  "location"="<Location>" -F "note_type"="<Note Type>" -F  "file"="<local hard disk location>"
   -F  "nbaddmode"="import-from-computer" \
   "https://api.qubole.com/api/v1.2/notebooks/import"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is an example to import a Spark notebook from an AWS S3 bucket.

.. sourcecode:: bash

   curl -X "POST" -H "X-AUTH-TOKEN:$AUTH_TOKEN" -H "Content-Type:multipart/form-data" -H "Accept: application/json" \
   -F "name"="SparkNote" -F "location"="Users/user1@qubole.com" -F "note_type"="spark"
   -F "url"="https://s3.amazonaws.com/notebook-samples/spark_examples" \
   "https://api.qubole.com/api/v1.2/notebooks/import"

Here is an example to import a Spark notebook from the local hard disk.

.. sourcecode:: bash

   curl -X "POST" -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: multipart/form-data" -H "Accept: application/json" \
   -F "name"="SparkNote1" -F  "location"="Users/user2@qubole.com" -F "note_type"="spark" -F  "file"="@/home/spark/SparkNoteb.ipynb"
   -F  "nbaddmode"="import-from-computer" \
   "https://api.qubole.com/api/v1.2/notebooks/import"
