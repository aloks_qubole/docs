.. _create-notebook-api:

=================
Create a Notebook
=================

..  http:post:: /api/v1.2/notebooks/

Use this API to create a notebook.  To know how to create a notebook using the **Notebooks** UI, see :ref:`create-notebook`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values. Presto is not
          currently supported on all Cloud platforms; see :ref:`os-version-support`.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **name**                    | It is the name of the notebook. It is a string and can accept alpha-numerical characters.                      +
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| location                    | It is the location of the folder. By default, it goes to **Users/current_user_email_id** folders. For more     |
|                             | information on notebook folders, see :ref:`folders-notebooks`. The accepted folder locations are:              |
|                             | **Users/current_user_email_id**, **Common**, and **Users**. The default location is                            |
|                             | **Users/current_user_email_id** and it is equivalent to **My Home** on the **Notebooks** UI. You need          |
|                             | privileges to create/edit notebooks in **Common** and **Users**. For more information, see                     |
|                             | :ref:`manage-folder-permissions`.                                                                              |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **note_type**               | It is the type of notebook. The values are ``hive``, ``deep_learning`` ``spark`` and ``presto``.               |
|                             | Hive and Deep Learning are beta features. The Hive notebook beta-feature is available by default. To enable    |
|                             | Deep Learning on a QDS account, create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__. |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| cluster_id                  | It is the cluster ID to which the notebook gets assigned with. You must assign the notebook with a cluster     |
|                             | to use it even though it is not mandatory to assign a notebook with the cluster when you create it. Assign     |
|                             | a cluster only with the corresponding type of notebook that is assign a Spark note with a Spark cluster and a  |
|                             | Presto notebook with a Presto cluster.                                                                         |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"<Name>", "location":"<Location>", "note_type": "<Note Type>", "cluster_id":"<Cluster ID"}' \
   "https://api.qubole.com/api/v1.2/notebooks"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................
Here is an example to create a Spark notebook and assign it to a cluster with its ID as 4001.

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"Spark", "location":"Users/user@abc.com/Notebooks", "note_type": "spark", "cluster_id":"4001"}' \
   "https://api.qubole.com/api/v1.2/notebooks"
