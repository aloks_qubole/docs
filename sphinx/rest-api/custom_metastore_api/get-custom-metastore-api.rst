.. _get-custom-metastore-api:

==========================
View the Custom Metastores
==========================

.. http:get:: /api/v1.2/custom_metastores

Use this API to see the custom metastores that are connected to the QDS.

For information on how to connect/edit to a metastore through the QDS UI, see :ref:`custom-hive-metastore`.

.. note:: This feature is not enabled by default. To enable it, create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.

Parameters
----------
None

Request API Syntax
------------------
Here is the API syntax for the REST API call that can be considered as an example too as there are no parameters in the
data payload.

.. sourcecode:: bash

   curl -X GET -H "X-AUTH-TOKEN:<AUTH-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
   "https://api.qubole.com/api/v1.2/custom_metastores/"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.
