.. _custom-metastore-api:

####################
Custom Metastore API
####################
Qubole supports connecting to a custom metastore through the **Explore** UI if you do not want to use Qubole Hive
metastore. For more information, see :ref:`custom-hive-metastore`. If any custom metastore is connected to QDS, you can
view them through the REST API that is given below.

.. note:: This feature is not enabled by default. To enable it, create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    connect-to-custom-metastore-api
    get-custom-metastore-api