.. _create-folder-api:

===============
Create a Folder
===============
..  http:post:: /api/v1.2/folders/

Use this API to create a notebook or dashboard folder. For more information, see :ref:`folders-notebooks` and
:ref:`dashboards-index`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **name**                    | It is the name of the folder. It is a string and can accept alpha-numerical characters.                        |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **location**                | It is the location of the folder. By default, it goes to **Users/current_user_email_id** folders. For more     |
|                             | information on notebook folders, see :ref:`folders-notebooks`. The accepted folder locations are:              |
|                             | **Users/current_user_email_id**, **Common**, and **Users/other_user_email_id** based on permissions. The       |
|                             | default location is   **Users/current_user_email_id** and it is equivalent to **My Home** on the **Notebooks** |
|                             | UI. You need  privileges to create/edit notebooks in **Common** and **Users/other_user_email_id**. For more    |
|                             | information, see :ref:`manage-folder-permissions`.                                                             |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **type**                    | It denotes if the type is **Notebooks** or **Dashboards**. Its value must be ``notes`` for a Notebook folder   |
|                             | and ``notebook_dashboards`` for a Dashboard folder.                                                            |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+


Request API Syntax
------------------

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"<Name>", "location":"<Location>", "type": "<notes/notebook_dashboards>"}' \ "https://api.qubole.com/api/v1.2/folders"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Requests
...................
Here is an example to create a notebook folder.

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"Note1", "location":"Users/user@qubole.com/Notebooks", "type": "notes"}' \ "https://api.qubole.com/api/v1.2/folders"

Here is an example to create a Dashboard folder.

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"Note1", "location":"Users/user@qubole.com/DashboardUser", "type": "notebook_dashboards"}' \ "https://api.qubole.com/api/v1.2/folders"