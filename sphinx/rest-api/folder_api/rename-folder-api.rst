.. _refresh-rename-folder-api:

===============
Rename a Folder
===============

.. http:put:: /api/v1.2/folders/rename

Use this API to rename a Notebook/Dashboard folder.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **name**                    | It is the new name to the folder.                                                                              |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **folder_id**               | It is the ID of the folder that you want to rename.                                                            |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **location**                | It is the current location of the folder.                                                                      |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **type**                    | It denotes if the type is **Notebooks** or **Dashboards**. Its value must be ``notes`` for a Notebook folder   |
|                             | and ``notebook_dashboards`` for a Dashboard folder.                                                            |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

   curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"<New Folder Name>","folder_id":"<folder_id>","location":"<Folder Location>","type":"<notes/notebook_dashboards>"}' \
    "https://api.qubole.com/api/v1.2/folders/rename"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Requests
...................
Here is a sample API call to rename the **Spark1** Notebook folder.

.. sourcecode:: bash

   curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"SparkManage","folder_id":"12","location":"Users/user1@qubole.com/Spark1","type":"notes"}' \
   "https://api.qubole.com/api/v1.2/folders/rename"

Here is a sample API call to rename the **Sparkuser** Dashboard folder.

.. sourcecode:: bash

   curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"name":"SparkDashuser","folder_id":"14","location":"Users/user1@qubole.com/Sparkuser","type":"notebook_dashboards"}' \
   "https://api.qubole.com/api/v1.2/folders/rename"