.. _folder-api-index:

##########
Folder API
##########

This section explains how to create, edit, move, rename and delete a folder for notebooks and dashboards through REST
API calls. For more information on:

* **Notebooks** UI, see :ref:`notebook-index`
* **Dashboards** UI, see :ref:`dashboards-index`

This section covers:

.. toctree::
    :maxdepth: 1
    :titlesonly:


    create-folder-api
    rename-folder-api
    delete-move-folder-api
    set-folder-policy-api
    get-folder-policy-api
