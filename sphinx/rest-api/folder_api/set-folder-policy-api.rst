.. _set-folder-policy-api:

===================
Set a Folder Policy
===================
..  http:put:: /api/v1.2/folders/policy

Use this API to set access permissions for a Notebook/Dashboard folder. For more information, see :ref:`folders-notebooks`.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group/a user with allowed ``manage`` permissions on folders.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------+------------------------------------------------------------------------------------------------+
| Parameter           | Description                                                                                    |
+=====================+================================================================================================+
| **location**        | It is the current location of the folder.                                                      |
+---------------------+------------------------------------------------------------------------------------------------+
| **source_type**     | For a folder, the source type is ``Folder``.                                                   |
+---------------------+------------------------------------------------------------------------------------------------+
| **type**            | It denotes if the type is **Notebooks** or **Dashboards**. Its value must be ``notes`` for a   |
|                     | Notebook folder and ``notebook_dashboards`` for a Dashboard folder.                            |
+---------------------+------------------------------------------------------------------------------------------------+
| **policy**          | Array of policies to be assigned to a cluster. Each policy include following parameters:       |
|                     |                                                                                                |
|                     | .. note:: Escape the values of policy elements and corresponding values except the             |
|                     |           **user ID value** and **group ID value**.                                            |
|                     |                                                                                                |
|                     | * **action**: Name of the action with particular resource. The actions can be ``read``,        |
|                     |   ``write``, ``manage``, or ``all``. The actions imply as given below:                         |
|                     |                                                                                                |
|                     |   - ``read``: This action is set to allow/deny a user/group to view the folder.                |
|                     |   - ``write``: This action is set to allow/deny a user/group to edit, rename, or move the      |
|                     |     folder.                                                                                    |
|                     |   - ``manage``: This action is set to allow/deny a user/group to manage the folder permissions.|
|                     |   - ``all``: This action is set to allow/deny a user/group to do read/edit/delete the folder.  |
|                     |     It gets the lowest priority always. That is ``read``, ``write``, and ``manage`` actions    |
|                     |     get precedence over the ``all`` action.                                                    |
|                     |                                                                                                |
|                     | * **access**: It is set to allow or deny actions. Its value is either `allow` or `deny`.       |
|                     | * **condition**: Array of user IDs and group IDs that have to be assigned with this policy:    |
|                     |                                                                                                |
|                     |   - **qbol_users**: An array of IDs of the user for whom this policy needs to be set. But      |
|                     |     these IDs are not similar to user IDs and create a ticket with                             |
|                     |     `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to get the user IDs.              |
|                     |   - **qbol_groups**: An array of IDs of the groups for whom this policy needs to be set.       |
|                     |                                                                                                |
+---------------------+------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"location":"<FolderLocation>", "type": "<notes/notebook_dashboards>", "source_type":"Folder",
          "policy": "[{\"access\":\"<Access>\",\"condition\":{\"qbol_users\":[<User ID>]},\"action\":[\"<Actions>\"]},
                     \"action\":[\"<Actions>\"]}, {\"access\":\"<Access>\",
                     \"condition\":{\"qbol_groups\":[<Group ID>] \"action\":[\"<Actions>\"]}, {\"access\":\"<Access>\"}]"}' \
    "https://api.qubole.com/api/api/v1.2/folders/policy"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Requests
...................
Here is a sample API call to assign permissions to the **SparkNotes** Notebook folder.

.. sourcecode:: bash

    curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name":"SparkNotes", "type":"notes", "location":"Users/user1@qubole.com/SparkNotes",
    "source_type":"Folder","policy":"[{\"access\":\"allow\", \"condition\":{\"qbol_users\":[12902]},\"action\":[\"read\",\"write\"]},
    {\"condition\":{\"qbol_groups\":[129]},\"access\":\"deny\",\"action\":[\"all\"]}]"}' \
    "https://api.qubole.com/api/v1.2/folders/policy"

Here is a sample API call to assign permissions to **SparkStatus** Dashboard folder.

.. sourcecode:: bash

     curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
     -d '{"name":"SparkStatus", "type":"notebook_dashboards", "location":"Users/user1@qubole.com/SparkStatus","source_type":"Folder","
     policy":"[{\"access\":\"allow\",\"condition\":  {\"qbol_users\":[12902]},\"action\":[\"read\",\"write\"]},
     {\"condition\":{\"qbol_groups\":[129]},\"access\":\"deny\",\"action\":[\"all\"]}]"}' \
     "https://api.qubole.com/api/v1.2/folders/policy"