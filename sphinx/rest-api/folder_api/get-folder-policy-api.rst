.._get-folder-policy:

====================
View a Folder Policy
====================

.. http:get:: /api/v1.2/folders/policy

Use this API to see the permissions of a Notebook/Dashboard folder.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------+------------------------------------------------------------------------------------------------+
| Parameter           | Description                                                                                    |
+=====================+================================================================================================+
| **location**        | It is the current location of the folder.                                                      |
+---------------------+------------------------------------------------------------------------------------------------+
| **source_type**     | For a folder, the source type is ``Folder``.                                                   |
+---------------------+------------------------------------------------------------------------------------------------+
| **type**            | It denotes if the type is **Notebooks** or **Dashboards**. Its value must be ``notes`` for a   |
|                     | Notebook folder and ``notebook_dashboards`` for a Dashboard folder.                            |
+---------------------+------------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN: <API-TOKEN>" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"location":"<FolderLocation>", "type": "<notes/notebook_dashboards>", "source_type":"Folder"}' \
    "https://api.qubole.com/api/v1.2/folders/policy"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Requests
...................
Here is a sample API call to view permissions of the **SparkNotes** Notebook folder.

.. sourcecode:: bash

    curl -i -X GET -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name":"SparkNotes", "type":"notes", "location":"Users/user1@qubole.com/SparkNotes", "source_type":"Folder"}' \
    "https://api.qubole.com/api/v1.2/folders/policy"

Here is a sample API call to view permissions of the **SparkStatus** Dashboard folder.

.. sourcecode:: bash

     curl -i -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
     -d '{"name":"SparkStatus", "type":"notebook_dashboards", "location":"Users/user1@qubole.com/SparkStatus",
          "source_type":"Folder"}' \
     "https://api.qubole.com/api/v1.2/folders/policy