.. _delete-move-folder-api:

========================
Move and Delete a Folder
========================
You can move and delete a Notebook/Dashboard folder.

Move a Folder
--------------
.. http:post:: /api/v1.2/folders/move

Use this API to move the folder to a different location.

Required Role
.............
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
..........

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **destination_location**    | It is the folder location to which you want to move this folder to which write/manage permission is granted.   |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **location**                | It is the current location of the folder.                                                                      |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **type**                    | It denotes if the type is **Notebooks** or **Dashboards**. Its value must be ``notes`` for a Notebook folder   |
|                             | and ``notebook_dashboards`` for a Dashboard folder.                                                            |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
..................

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"destination_location":"<Destined Folder>", "location":"<Current Folder Location>", "type":"<notes/notebook_dashboards>"}' \
   "https://api.qubole.com/api/v1.2/folders/move"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
~~~~~~~~~~~~~~~~~~
Here is a sample API call to move the Notebook folder to a new destination.

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"destination_location":"Users/user1@qubole.com/Spark1", "location":"Users/user2@qubole.com/Spark1", "type":"notes"}' \
   "https://api.qubole.com/api/v1.2/folders/move"

Delete a Folder
---------------

.. http:post:: api/api/v1.2/folders/delete

Required Role
.............
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows submitting a command. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
..........

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values.

+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter                   | Description                                                                                                    |
+=============================+================================================================================================================+
| **folder_id**               | It is the ID of the folder that you want to delete.                                                            |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **location**                | It is the current location of the folder.                                                                      |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+
| **type**                    | It denotes if the type is **Notebooks** or **Dashboards**. Its value must be ``notes`` for a Notebook folder   |
|                             | and ``notebook_dashboards`` for a Dashboard folder.                                                            |
+-----------------------------+----------------------------------------------------------------------------------------------------------------+

Request API Syntax
..................

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"folder_id":"<folder_id>","location":"<Folder Location>","type":"<notes/notebook_dashboards>"}' \
   "https://api.qubole.com/api/v1.2/folders/delete"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
~~~~~~~~~~~~~~~~~~
Here is a sample API call to delete the Notebook folder that has 12 as its ID.

.. sourcecode:: bash

   curl -i -X POST -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
   -d '{"folder_id":"12","location":"Users/user1@qubole.com/Spark1","type":"notes"}' \
   "https://api.qubole.com/api/v1.2/folders/delete"