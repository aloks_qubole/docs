.. _list-tables-in-a-dbtap:

List Tables in a DbTap
======================

.. http:get::   /api/v1.2/db_taps/<db-tap-id>/tables

Use this API to get a list of all tables in a data store.

+-----------------------+------------------------------------+
| Resource URI          | db\_taps/\ *id*/tables             |
+-----------------------+------------------------------------+
| Request Type          | GET                                |
+-----------------------+------------------------------------+
| Supporting Versions   | v1.2                               |
+-----------------------+------------------------------------+
| Return Value          | JSON Array of tables in a DbTap.   |
+-----------------------+------------------------------------+

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a specific DbTap's details.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
~~~~~~~

::

    curl -i -X GET -H "Content-Type: application/json" -H "Accept: application/json" -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    https://api.qubole.com/api/v1.2/db_taps/${DBTAPID}/tables

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

::

    ["CUSTOMER","LINEITEM","NATION","ORDERS","PART","PARTSUPP","REGION","SUPPLIER"]