.. _delete-a-dbtap:

Delete a DbTap
==============

.. http:delete::   /api/v1.2/db_taps/(int:dbtap_id)/

Use this API to delete a data store. See :ref:`db-tap` for more information.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows deleting a DbTap. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Response
--------
The response contains a JSON object with the status of the operation.


Example
-------

**Goal:** delete a :ref:`db-tap` having id ``123``

.. sourcecode:: bash

    curl -i -X DELETE -H "Content-Type: application/json" \
    	-H "Accept: application/json" -H "X-AUTH-TOKEN:$X_AUTH_TOKEN"  \
    	https://api.qubole.com/api/v1.2/db_taps/123

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Response**

.. sourcecode:: http

	 HTTP/1.1 200 OK
	 Content-Type: application/json; charset=utf-8

	 {"status":"deleted"}