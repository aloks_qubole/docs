.. _list-dbtaps:

List DbTaps
===========

.. http:get::   /api/v1.2/db_taps/

Use this API to list data stores.

+-----------------------+-------------------------+
| Resource URI          | db\_taps                |
+-----------------------+-------------------------+
| Request Type          | GET                     |
+-----------------------+-------------------------+
| Supporting Versions   | v1.2                    |
+-----------------------+-------------------------+
| Return Value          | JSON array of DbTaps.   |
+-----------------------+-------------------------+

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing all DbTaps. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
~~~~~~~

::

     curl -i -X GET -H "Content-Type: application/json" -H "Accept: application/json" -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
     https://api.qubole.com/api/v1.2/db_taps/

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

::

    {
      "paging_info": {
        "next_page": null,
        "per_page": 10,
        "previous_page": null
      },
      "db_taps": [
        {
          "active": true,
          "account_id": 00000,
          "user_id": 1,
          "db_user": "root",
          "db_passwd": "",
          "db_location":"us-east-1",
          "db_type":"mysql",
          "db_name": "jenkins",
          "created_at": "2012-12-27T17:20:32Z",
          "db_host": "localhost",
          "port": null,
          "id": 1
        },
        {
          "active": true,
          "account_id": 00000,
          "user_id": 1,
          "db_user": "doc_writer",
          "db_passwd": "",
          "db_name": "doc_example",
          "created_at": "2013-03-15T10:02:42Z",
          "db_location":"us-east-1",
          "db_type":"postgresql",
          "db_host": "localhost",
          "port": null,
          "id": 3
        }
      ]
    }