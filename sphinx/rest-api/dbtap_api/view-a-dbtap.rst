.. _view-a-dbtap:

View a DbTap
============

.. http:get::   /api/v1.2/db_taps/<db-tap-id>

Use this API to view a data store.

+-----------------------+---------------------------------------+
| Resource URI          | db\_taps/\ *id*                       |
+-----------------------+---------------------------------------+
| Request Type          | GET                                   |
+-----------------------+---------------------------------------+
| Supporting Versions   | v1.2                                  |
+-----------------------+---------------------------------------+
| Return Value          | Json object representing the DbTap.   |
+-----------------------+---------------------------------------+

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing a specific DbTap's details.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Example
~~~~~~~

::

    curl -i -X GET -H "Content-Type: application/json" -H "Accept: application/json" -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    https://api.qubole.com/api/v1.2/db_taps/${DBTAPID}

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

::

    {
      "account_id": 00000,
      "active": true,
      "db_user": "doc_writer",
      "user_id": 1,
      "db_location":"us-east-1",
      "db_type":"mysql",
      "db_passwd": "",
      "db_name": "doc_example",
      "created_at": "2013-03-15T10:02:42Z",
      "db_host": "localhost",
      "id": 3,
      "port": null
    }