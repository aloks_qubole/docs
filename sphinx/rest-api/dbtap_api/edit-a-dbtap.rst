.. _edit-a-dbtap:

Edit a DbTap
============

.. http:put::   /api/v1.2/db_taps/<db-tap-id>

Use this API to edit a data store.

+-----------------------+-----------------------------------------------+
| Resource URI          | db\_taps/\ *id*                               |
+-----------------------+-----------------------------------------------+
| Request Type          | PUT                                           |
+-----------------------+-----------------------------------------------+
| Supporting Versions   | v1.2                                          |
+-----------------------+-----------------------------------------------+
| Return Value          | Json object representing the updated DbTap.   |
+-----------------------+-----------------------------------------------+

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows editing a DbTap. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values. Presto is not
          currently supported on all Cloud platforms; see :ref:`os-version-support`.

+--------------------+-----------------------------------------------------------------------------------------------------------+
| Parameter          | Description                                                                                               |
+====================+===========================================================================================================+
| catalog_name       | This parameter is mandatory to make the data stores accessible through Presto and Spark clusters. The     |
|                    | `catalog_name` can contain lower-case alphabets and numerals. Its value can be ``NULL`` if you do not     |
|                    | want to export data source to Presto or Spark clusters. This parameter is supported only for MySQL,       |
|                    | Postgres, Snowflake, and Redshift type of data stores through Presto/Spark.                               |
|                    |                                                                                                           |
|                    | .. note:: Apart from adding the ``catalog name``, create a ticket with                                    |
|                    |           `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable this feature.                |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| name               | Use this parameter to add a name to the data store. If you do not add a name, then Qubole adds the name   |
|                    | as a combination of ``db_host`` and ``db_name``.                                                          |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| db_name            | Database Name. This is the data store name that is created in the QDS.                                    |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| db_host            | IP address or hostname of the data store.                                                                 |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| db_user            | User name to login to the data store.                                                                     |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| db_passwd          | Password to login to the data store.                                                                      |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| port               | TCP Port to connect on.                                                                                   |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| db_type            | Type of database. Valid values are ``mysql``, ``vertica``, ``mongo``, ``postgresql``, and ``redshift.``   |
|                    | The default value is ``mysql``.                                                                           |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| db_location        | Location of database. Valid values are ``us-east-1``, ``us-west-2``, ``ap-southeast-1``, ``eu-west-1``,   |
|                    | and ``on-premise``.                                                                                       |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| gateway\_ip        | IP address or hostname of the gateway machine.                                                            |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| gateway_port       | The default port is 22. Specify a non-default port to connect to the gateway machine.                     |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| gateway_username   | User name to login to the gateway machine.                                                                |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| gateway_private_key| Private key for the aforementioned user to login to the gateway machine. If you add the private key,      |
|                    | you must add the associated public key to the bastion node as described in :ref:`clusters-in-vpc`.        |
+--------------------+-----------------------------------------------------------------------------------------------------------+
| skip_validation    | Set it to ``true`` to skip the data store validation when you create or edit a data store.                |
|                    | It is set to ``false`` by default.                                                                        |
+--------------------+-----------------------------------------------------------------------------------------------------------+

Example
-------

::

    curl -i -X PUT -H "Content-Type: application/json" -H "Accept: application/json" -H "X-AUTH-TOKEN: $AUTH_TOKEN" \
    -d '{"port":3306}'  \ https://api.qubole.com/api/v1.2/db_taps/${DBTAPID}

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

::

    {
      "account_id": 00000,
      "active": true,
      "db_user": "doc_writer",
      "user_id": 1,
      "db_passwd": "",
      "db_name": "doc_example",
      "created_at": "2013-03-15T10:02:42Z",
      "db_type":"mysql",
      "db_location":"us-east-1"
      "db_host": "localhost",
      "id": 3,
      "port": 3306
    }