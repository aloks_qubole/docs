.. _create-a-dbtap:

Create a DbTap
==============

.. http:post::   /api/v1.2/db_taps/

Use this API to create a data store (formerly known as DbTap). :ref:`db-tap-add-data-store` describes how to add a data
store in the **Explore** UI page.

+-----------------------+-----------------------------------------------------+
| Resource URI          | db\_taps/                                           |
+-----------------------+-----------------------------------------------------+
| Request Type          | POST                                                |
+-----------------------+-----------------------------------------------------+
| Supporting Versions   | v1.2                                                |
+-----------------------+-----------------------------------------------------+
| Return Value          | Json object representing the newly created DbTap.   |
+-----------------------+-----------------------------------------------------+

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a DbTap. See
  :ref:`manage-groups` and :ref:`manage-roles` for more information.


Parameters
----------

.. note:: Parameters marked in **bold** below are mandatory. Others are optional and have default values. Presto is not
          currently supported on all Cloud platforms; see :ref:`os-version-support`.

+---------------------------+--------------------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                                            |
+===========================+========================================================================================================+
| catalog_name              | This parameter is mandatory to make the data stores accessible through Presto and Spark clusters. The  |
|                           | `catalog_name` can contain lower-case alphabets or numerals. Its value can be ``NULL`` if you do not   |
|                           | want to export data source to Presto or Spark clusters. This parameter is supported for MySQL,         |
|                           | Postgres, Snowflake, and Redshift type of data stores only through Presto/Spark.                       |
|                           |                                                                                                        |
|                           | .. note:: Apart from adding the ``catalog name``, create a ticket with                                 |
|                           |           `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable this feature.             |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| name                      | Use this parameter to add a name to the data store. If you do not add a name, then Qubole adds the     |
|                           | name as a combination of ``db_host`` and ``db_name``.                                                  |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **db\_name**              | Database Name. This is the data store name that is created in the QDS.                                 |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **db\_host**              | IP address or hostname of the data store.                                                              |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **db\_user**              | User name to login to the data store.                                                                  |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **db\_passwd**            | Password to login to the data store.                                                                   |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **port**                  | TCP Port to connect on. If not specified, then the default port for the datastore type is used.        |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **db\_type**              | Type of database. Valid values are ``mysql``, ``vertica``, ``mongo``, ``postgresql``, and ``redshift``.|
|                           | The default value is ``mysql``.                                                                        |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **db\_location**          | Location of database. Valid values are ``us-east-1``, ``us-west-2``, ``ap-southeast-1``, ``eu-west-1``,|
|                           | and ``on-premise``.                                                                                    |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **gateway\_ip**           | IP address or hostname of the gateway machine.                                                         |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| gateway_port              | The default port is 22. Specify a non-default port to connect to the gateway machine.                  |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| **gateway\_username**     | User name to login to the gateway machine.                                                             |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| gateway\_private_key      | Private key for the aforementioned user to login to the gateway machine. If you add the private key,   |
|                           | you must add the associated public key to the bastion node as described in :ref:`clusters-in-vpc`.     |
+---------------------------+--------------------------------------------------------------------------------------------------------+
| skip_validation           | Set it to ``true`` to skip the data store validation when you create or edit a data store.             |
|                           | It is set to ``false`` by default.                                                                     |
+---------------------------+--------------------------------------------------------------------------------------------------------+

Note
~~~~

* Gateway parameters (`gateway\_ip`, `gateway\_username`, `gateway\_private_key`) can be specified only if `db_location` is 'on-premise'.
* If you do not want to use a gateway machine to access your data store, you need not specify any of the gateway parameters.
* Though the gateway parameters are optional, if any one of the gateway parameters is specified then all three must be specified.
* Port 22 must be open on the gateway machine and it must have access to the data store.

Example
-------

.. sourcecode:: bash

    payload:
    {
      "db_name":"doc_example",
      "db_host":"localhost",
      "db_user":"doc_writer",
      "db_passwd":"",
      "db_type":"mysql",
      "db_location":"us-east-1"
    }

.. sourcecode:: bash

    curl -i -X POST -H "Content-Type: application/json" -H "Accept: application/json" -H "X-AUTH-TOKEN: $AUTH_TOKEN"
    -d @payload https://api.qubole.com/api/v1.2/db_taps/

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

**Sample Response**

.. sourcecode:: bash

    {
      "account_id": 00000,
      "active": true,
      "db_user": "doc_writer",
      "user_id": 1,
      "db_passwd": "",
      "db_name": "doc_example",
      "created_at": "2013-03-15T10:02:42Z",
      "db_host": "localhost",
      "db_location":"us-east-1",
      "db_type":"mysql"
      "id": 3,
      "port": null
    }

Take a note of the DbTap id (in this case 3). It is used in later examples.

::

    export DBTAPID=3


Example 2
---------
Here is an example to create a data store that you can access through a Presto cluster.

.. sourcecode:: bash

    Payload
        {
          "db_name":"dbtap1",
          "catalog_name":"mydbtap1",
          "db_host":"sample.rds.amazon.com",
          "db_user":"user1",
          "db_passwd":"password",
          "db_type":"mysql",
          "db_location":"<region>"
        }


.. sourcecode:: bash

    curl -i -X POST -H "Content-Type: application/json" -H "Accept: application/json" -H "X-AUTH-TOKEN: {Auth_Token} \
    -d @Payload \ https://api.qubole.com/api/v1.2/db_taps/

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.