.. _dbtap-api:

#########
DbTap API
#########
It is often useful to import or export data to and from data stores other than Cloud storage. For example, you may want
to run a periodic Hive query to summarize a dataset and export the results to a MySQL database; or you may want to import
data from a relational database into Hive. You can identify an external data store for such purposes.

For more information, see :ref:`db-tap`. The following APIs help you to create, edit, view, and delete data stores.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-a-dbtap
    delete-a-dbtap
    edit-a-dbtap
    list-dbtaps
    list-tables-in-a-dbtap
    view-a-dbtap
