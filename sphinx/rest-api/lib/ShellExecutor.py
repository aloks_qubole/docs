# -*- coding: utf-8 -*-
import subprocess
from qds_sdk.qubole import Qubole
import json
from qds_sdk.commands import Command
from time import sleep
import re


class ShellExecutor:

    def __init__(self, cfg):
        self.myenv = {}
        self.api_token = cfg['auth_token']
        self. api_url = cfg['api_url']
        self.version = cfg['version']

    def execute(self, cmd):
        timeout = 600     #10mins
        #if its export command just set the environment variable
        if"export" in cmd:
            self.set_env(cmd)
            return
        cmd_new = self.create_command(cmd)
        res = str(subprocess.Popen(cmd_new, shell=True, stdout=subprocess.PIPE, env=self.myenv).stdout.read())
        try:
            result = json.loads(res)
        except ValueError:
            #if its a view log command
            if "logs" in cmd_new and "error" not in res:
                return {'status': 'done'}
            return {'status': "failed:"+str(res)}
        if 'error' in result:
            return {'status': "error: "+str(result['error'])}
        #wait till the command is complete untill timeout
        elif 'status' in result and 'progress' in result:
            while result['status'] not in ['error', 'done', 'cancelled'] and timeout > 0:
                result = json.loads(str(Command.find(result['id'])))
                sleep(Qubole.poll_interval)
                timeout = timeout - Qubole.poll_interval
                if timeout <= 0:
                    Command.find(id=result['id']).cancel()
                    return {'status': "Timeout exceeded "}
        return result

    def create_command(self, cmd):
        #replace the token
        cmd = re.sub(r'\$AUTH_TOKEN\b', self.api_token, cmd)
        #replace the base url
        cmd = re.sub('https://api.qubole.com/api/', self.api_url, cmd)
        #replace the version if not mentioned to latest
        cmd = re.sub('/\$\{V}', '/'+self.version, cmd)
        #replace the new pig path
        return cmd

    def set_env(self, data):
        data = data.split("=")
        key = data[0].split(" ")[1]
        value = data[1].strip()
        self.myenv[key] = value.strip()