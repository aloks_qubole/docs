import random
import string
from qds_sdk.cluster import *
from qds_sdk.scheduler import *
from qds_sdk.actions import *
import time

def random_sequence(size=5):
    """
    Returns a random string of `size` length with a mix of ascii and digits
    """
    return ''.join(
        random.choice(
            string.ascii_lowercase + string.digits
        ) for _ in range(size))

def wait_for_cluster(cluster_id, state="UP"):

    Cluster.start(cluster_id_label=cluster_id)
    timeout = 900
    while Cluster.status(cluster_id)['state'] != state and timeout > 0:
        timeout = timeout - 5
        time.sleep(10)

def get_action(scheduler):

    sch =Scheduler.find(id=scheduler)
    actions = sch.list_actions()
    while len(actions) == 0:
        time.sleep(2)
        actions = sch.list_actions()
    return actions

def wait_for_action_completion(id):

    act = Action.find(id)
    while not (act.status() == "cancelled" or act.status() == "done"):
        time.sleep(Qubole.poll_interval)
        act = Action.find(act.id)
    return act.id

