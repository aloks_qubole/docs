.. _assign-unassign-role-api:

===============================
Assign and Unassign Group Roles
===============================
A role can be assigned to a specific group to perform certian functions such as administrator and modifier. Similarly,
a role can be unassigned from a group if it does not require the role function.

Assign a Role to a Group
------------------------
.. http:put:: /api/v1.2/groups/<qbol_group_id>/roles/<role-id/name>/assign

This API is used to assign a specific role to a group.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows assigning roles to a group.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **<qubole_group_id>**     | Qubole group ID to which a role is to be assigned                                        |
+---------------------------+------------------------------------------------------------------------------------------+
| **<role-id/name>**        | ID or name of the role that is to be assigned to a group                                 |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the syntax of the Request API.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/<qubole-group-id>/roles/<role-id/name>/assign"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/105/roles/19/assign"

Sample Response
...............

.. sourcecode:: bash

    Success
    {"status":"done"}


Unassign a Role from a Group
----------------------------
When an assinged role is not required by a group, remove it. To remove an assigned role from a group, use this API:

.. http:put:: /api/v1.2/groups/<qubole_group_id>/roles/<role-id/name>/unassign

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows unassigning roles from a group.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **<qubole_group_id>**     | Qubole group ID from which a specific role must be removed                               |
+---------------------------+------------------------------------------------------------------------------------------+
| **<role-id/name>**        | ID/name of the role that must be unassigned from a group                                 |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the syntax of the Request API.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/<qubole-group-id>/roles/<role-id/name>/unassign"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/105/roles/19/unassign"

Sample Response
...............

.. sourcecode:: bash

    Success
    {"status":"done"}
