.. _add-users-in-group-api:

==============================
Add Users to an Existing Group
==============================
.. http:put:: /api/v1.2/groups/<qubole-group-id>/qbol_users/<user-email-address>/add

This API is used to add users to an existing group on QDS.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows adding users to a group.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.


Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------------------+------------------------------------------------------------------------------------------------------+
| Parameter                             | Description                                                                                          |
+=======================================+======================================================================================================+
| **<qubole_group_id>**                 | ID/name of the Qubole group                                                                          |
+---------------------------------------+------------------------------------------------------------------------------------------------------+
| **<user-email-address>**              | An array of Qubole users' email addresses, who are already members of a Qubole account.              |
+---------------------------------------+------------------------------------------------------------------------------------------------------+

Request Syntax
--------------
Here is the request API syntax.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/my_group_name/qbol_users/<<userID1,userID4,..., userIDN>/<user1-emailaddress1,
             useremailaddress2,....,useremailaddressN>>/add"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
--------------
Here is a sample request.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/Users/qbol_users/71,72/add"


Sample Response
...............

.. sourcecode:: bash

    Success
    {"status":"done"}

