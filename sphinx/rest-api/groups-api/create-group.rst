.. _create-group-api:

==============
Create a Group
==============

.. http:post:: /api/v1.2/groups

This API is used to create a group on QDS.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows creating a group.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+------------+------------------------------------------------------------------------------------------------------------+
| Parameter  | Description                                                                                                |
+============+============================================================================================================+
| **name**   | Name of the group that must be unique in the Qubole account.                                               |
+------------+------------------------------------------------------------------------------------------------------------+
| members    | An array of Qubole users' email addresses, who are already members of the Qubole account.                  |
+------------+------------------------------------------------------------------------------------------------------------+
| roles      | An array of Qubole role IDs or role names. Once a Qubole group is created, roles are attached to the group.|
+------------+------------------------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the request API syntax.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name":"<group-name>","members":"<member1,member2,..","roles":"<role1>,<role2>,..."}' \
    "https://api.qubole.com/api/v1.2/groups"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............

Here is a sample request.

.. sourcecode:: bash

    curl -X POST -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{"name":"my_group_name","members":"71,72","roles":"1,2"}' \ "https://api.qubole.com/api/v1.2/groups"

Sample Response
...............

.. sourcecode:: bash

    Success
    {"status":"done"}