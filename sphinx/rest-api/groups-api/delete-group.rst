.. _delete-group-api:

==============
Delete a Group
==============

.. http:delete:: /api/v1.2/groups/<group-id>

This API is used to delete an existing group on QDS.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows deleting a group.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.


Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **<group-id>**            | ID/name of the group that needs to be deleted                                            |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the request API syntax.

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/<group-id>"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request.

.. sourcecode:: bash

    curl -X DELETE -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/2192"

Sample Response
...............

.. sourcecode:: bash

    Success
    "status":"success"