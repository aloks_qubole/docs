.. _delete-users-from-group-api:

===================================
Delete Users from an Existing Group
===================================

.. http:put:: /api/v1.2/groups/<qubole-group-id/name>/qbol_users/<user-email-address>/remove

This API is used to delete users in an existing group on QDS.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows deleting users from a group.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------------------+------------------------------------------------------------------------------------------+
| Parameter                             | Description                                                                              |
+=======================================+==========================================================================================+
| **<qubole_group_id/name**>            | ID or name of the group that contains users, who must be deleted.                        |
+---------------------------------------+------------------------------------------------------------------------------------------+
| **<user-email-address>**              | An array of Qubole users’ email addresses, who are part of the Qubole group.             |
+---------------------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------
Here is the API request syntax.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/<qubole-group-id/name>/qbol_users/<<user-id>/<user-email-address>>/remove"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample Request
..............
Here is a sample request.

.. sourcecode:: bash

    curl -X PUT -H "X-AUTH-TOKEN: $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    "https://api.qubole.com/api/v1.2/groups/106/qbol_users/73,74/remove"