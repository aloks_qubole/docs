.. _groups-api-index:

==========
Groups API
==========

.. toctree::
    :maxdepth: 1
    :titlesonly:

    create-group
    add-users
    assign-unassign-role-api
    list-role
    list-users-in-group
    delete-group
    delete-users
