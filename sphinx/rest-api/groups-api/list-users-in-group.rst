.. _list-users-in-group-api:

===============================
List Users in an Existing Group
===============================

.. http:get:: /api/v1.2/groups/<group-id/name>/qbol_users

This API is used to list all users in a given Qubole group.

Required Role
-------------
The following roles can make this API call:

* A user who is part of the system-user/system-admin group.
* A user invoking this API must be part of a group associated with a role that allows viewing users in a specific group.
  See :ref:`manage-groups` and :ref:`manage-roles` for more information.

Parameters
----------
.. note:: Parameters marked in **bold** are mandatory. Others are optional and have default values.

+---------------------------+------------------------------------------------------------------------------------------+
| Parameter                 | Description                                                                              |
+===========================+==========================================================================================+
| **<group-id/name>**       | Quole group's ID or name                                                                 |
+---------------------------+------------------------------------------------------------------------------------------+

Request API Syntax
------------------

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/<group-id/name>/qbol_users"

.. note:: The above syntax uses https://api.qubole.com as the endpoint. Qubole provides other endpoints to access QDS that
          are described in :ref:`qubole-endpoints`.

Sample API Request
..................

.. sourcecode:: bash

    curl -X GET -H "X-AUTH-TOKEN:  $AUTH_TOKEN" -H "Content-Type: application/json" -H "Accept: application/json" \
    -d '{}' \ "https://api.qubole.com/api/v1.2/groups/20/qbol_users"