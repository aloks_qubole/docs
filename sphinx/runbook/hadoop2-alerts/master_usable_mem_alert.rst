.. _master-usable-memory-alert:

=======================
master_usable_mem_alert
=======================

**Alert Name**: master_usable_mem_alert

**Alert Condition**: The condition that triggers the alert is ``avg(last_5m):avg:mem_free{*} by {host} < 2000000000``.

**Alert Explanation**: This alert indicates that the average free memory in the master node is less than 2 GB.

**Resolution**:

You can use one of the following methods to resolve:

* **Method 1**: Follow these steps:

  1. Use the AWS instance type with larger memory and set it as the master node.
  2. Try moving a part of the workload to another cluster.

* **Method 2**: Follow these steps:

  1. Log into the master node and check which process is using the maximum memory.
  2. Run the ``top`` command to check the percentage of memory being used by each running process.
  3. Run the ``ps`` (process status) command to see more information about each process.
  4. Based on which process is consuming the maximum memory, take appropriate steps as required.