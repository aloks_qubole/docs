.. _master-cpu-utilize-alert:

============================
master_cpu_utilization_alert
============================

**Alert Name**: master_cpu_utilization_alert

**Alert Condition**: The condition that triggers the alert is ``avg(last_5m):avg:cpu_user{*} by {host} > 80``.

**Alert Explanation**: The alert indicates that the average CPU utilized in the master node is greater than 80 percent.

**Resolution**:

You can use one of the following methods to resolve:

* **Method 1** - Follow these steps:

  1. Use the AWS instance type with larger CPU capacity and set it as the master node.
  2. Try moving a part of the workload to another cluster.

* **Method 2** - Follow these steps:

  1. Log into the master node and check which process is using the maximum CPU.
  2. Run the ``top`` command to check the CPU percentage being used by each running process.
  3. Run the ``ps`` (process status) command to see more information about each process.
  4. Based on which process is consuming the maximum CPU, take appropriate steps as required.