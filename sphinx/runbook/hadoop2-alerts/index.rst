.. _hadoop2-runbook-index:

============================
Runbooks for Hadoop 2 Alerts
============================
This section lists a sample runbook for a Hadoop 2 alert, which describes the steps required to find the HDFS disk space,
the master node CPU/memory utilization, and the liveness of the NameNode/ResourceManger.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    hdfs_full_on_cluster.rst
    master_cpu_utilization_alert.rst
    master_usable_mem_alert.rst
    nn_liveness.rst
    rm_liveness.rst