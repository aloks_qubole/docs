.. _runbook-alerts-engines:

=======================
QDS Alerts and Runbooks
=======================

This guide provides runbooks for a system alert and for Hadoop 2, Notebook, Application History Server (AHS), and Hive alerts.

Qubole will add runbooks for YARN, HDFS, and Presto at a later time.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    system-alerts/index.rst
    hadoop2-alerts/index.rst
    notebook-alerts/index.rst
    hive-alerts/index.rst
    ahs-alerts/index.rst

