.. _hs2-alerts-index:

Runbooks for Hive Server 2 Alerts
===============================================

This section lists runbooks for Hive Server 2  s alerts, which describes the steps to perform when an alert is triggered.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    hive-hs2-cms-gc-time.rst
    hive-hs2-g1-oldgen-gc-time.rst
    hive-hs2-liveness.rst
    hive-hs2-memory-usage.rst
    hive-hs2-perm-gen-usage.rst
    hive-hs2-query-failures.rst
    hive-hs2-response.rst
