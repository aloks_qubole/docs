.. hive-hs2-perm-gen-usage:

hive-hs2-perm-gen-usage
=======================
This runbook shows steps to address an excessive level of PermGen memory usage.

**Alert Name**: HS2 Perm Gen Usage

**Alert Message**: "HS2 PermGen Usage over 80%"

**Alert Explanation**: This alert indicates that PermGen memory usage is greater than 80%.

**Resolution**:

* By default, Qubole assigns 1024 MB to PermGen. If this usage alert appears repeatedly, increase the size of PermGen memory and restart HiveServer2.
* Contact the #escalation-hive or #solutions channel for further support.
