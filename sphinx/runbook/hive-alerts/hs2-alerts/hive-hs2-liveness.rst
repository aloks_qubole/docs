.. hive-hs2-liveness:

hive-hs2-liveness
=================
This runbook shows steps to perform when a Hive Server 2 daemon is no longer live.

**Alert Name**: HS2 Liveness

**Alert Message**: "HS2 daemon is not live"

**Alert Explanation**: This alert indicates that the HS2 daemon has stopped running.

**Resolution**:

* Check the Hive logs for evident errors and a reason for the shutdown or failure.
* If the monit status says “execution failed,” then monit failed to restart the process. Restart the process.
