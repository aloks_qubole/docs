.. hive-hs2-g1-oldgen-gc-time:

hive-hs2-g1-oldgen-gc-time
==========================
This runbook shows steps to take when G1 garbage collection time is excessive.

**Alert Name**: HS2 G1 OLDGEN GC time

**Alert Message**: "HS2 GC time over 7 seconds"

**Alert Explanation**: The alert indicates that G1 garbage collection time is greater than seven seconds.

**Resolution**:

* Check the dashboards "HS2 Memory Usage" and "HS2 GC Time." Look for "hive.hs2.memory.pools_CMS-Perm-Gen_usage" trend along with heap trends. If this alert appears repeatedly, it indicates that the JVM is working most of the time on garbage collection and is still not able to free up memory.
* Based on the above, configure or alter your workloads accordingly with the help of Dev teams.
* Restart the HiveServer2 process if failures are not contained.
