.. hive-hs2-response:

hive-hs2-response
=================
This runbook shows steps to perform when a Hive Server 2 daemon has stopped responding.

**Alert Name**: HS2 Responsiveness

**Alert Message**: "HS2 daemon stopped responding"

**Alert Explanation**: This alert indicates that the HS2 daemon has stopped responding.

**Resolution**:

* Check “Active Queries” to understand the active load on the system.
* Check the “System metrics.”.
* Check the “HS2 GC Time” and “Memory Usage” dashboards.
* Restart the HiveServer2 process.
