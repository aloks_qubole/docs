.. _hive-alerts-index:

Runbooks for Hive Alerts
========================

This section lists runbooks for two kinds of Hive alerts, Hive Server 2 and Hive Metastore Server alerts.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    hs2-alerts/index.rst
    hms-alerts/index.rst
