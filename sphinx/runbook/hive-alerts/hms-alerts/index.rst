.. _hms-alerts-index:

Runbooks for Hive Metastore Server (HMS) Alerts
===============================================

This section lists runbooks for Hive Metastore Server alerts, which describes the steps to perform when an alert is triggered.

.. toctree::
    :maxdepth: 1
    :titlesonly:

    hive-hms-memory-used.rst
    hive-hms-liveness.rst
    hive-hms-responsiveness.rst

