.. hive-hms-responsiveness:

HMS Responsiveness
==================
This runbook shows steps to perform when the Hive Metastore Server is not responding.

* Check **Active Queries** to understand the active load on the system.

* Check the **System metrics** as well.

* Check the **Memory Usage** dashboards. This can help in determining the cause.

* Restart the HMS process.

Logs
----
* HMS logs are available on the master node: “/media/ephemeral0/logs/hive1.2/hive_ms.log”

* Look for any evident errors(do basic grep and count of errors).

* Look at the metrics dashboards for more information.

Restart of Process
------------------
* ``sudo monit summary`` for checking the status of the process.

* ``sudo monit stop metastore1_2`` for stopping the process.

* ``sudo monit start metastore1_2`` for starting the process.

