.. _whats-new-r53:

===========
What is New
===========
The new features and enhancements are listed in the section below.

.. note:: The label (in blue) that is against the description indicates the launch state, availability, and the default
          state of the feature/enhancement. For more information, click the label.

          Unless stated otherwise, features are generally available, available as self-service and enabled by default.

Cluster Management
..................

* Qubole supports the ``x1e`` instances.
* QDS supports the AWS Canada and AWS London Regions.

  :ref:`Learn more  <acmr53>`.

* QDS has added the cluster health monitoring enhancements that are available through the Datadog monitoring service. :ref:`Learn more  <acmr53-enhancements>`. :ref:`Beta <availability-features>`

Hive
....

* The metastore consistency check (MSCK) result is displayed only in **Logs** instead of the **Results** tab of the **Analyze** UI. :ref:`Disabled  <availability-features>`
* Qubole encrypts the Hive metastore passwords. :ref:`Beta, Via Support <availability-features>`
* When Tez is the execution engine in Hive queries, QDS provides an account-level configuration to limit the number of
  AWS API calls. :ref:`Beta <availability-features>`
* A Datadog dashboard for the Hive Metastore Server (HMS) is added for Hive, Spark, and Presto clusters. :ref:`Beta <availability-features>`

  :ref:`Learn more <hiver53-enhancements>`.

Presto
......

* **Presto 0.193** version is **generally available**. Detailed notes about the 0.193 release are available as a blog post at `Introducing Presto 0.193 in QDS <https://www.qubole.com/blog/introducing-presto-0-193-qds/>`__.
* The **Qubole Presto Server Bootstrap** is an alternative to the :ref:`Node Bootstrap Script <nodebootstrapscript>`
  to install external jars such as `presto-udfs <https://github.com/qubole/presto-udfs>`__ before the Presto Server is
  started. This prevents the additional restart of the Presto Server that happens when you install such jars through the Node
  Bootstrap Script which causes query failures during the node startup. It is only supported in Presto 0.180 and later versions. :ref:`Disabled <availability-features>`

  :ref:`Learn more on the Qubole Presto Server Bootstrap <presto-server-bootstrap>`.
* Qubole Presto now supports providing full access to the S3 bucket owner for files written to the bucket by other users. :ref:`Disabled  <availability-features>`
* A new FastPath for queries executed by the QDS Drivers. The FastPath minimizes the QDS level time for Presto queries.
  It is only supported on Presto 0.180. With the Presto FastPath, the overall command latencies within QDS should come
  down to around 1.5 seconds when the cluster is active. :ref:`Beta, Via Support <availability-features>`

  :ref:`Learn more <presto-r53>`.

* The file-based authentication feature has been enhanced to accept only the hashed passwords for each user. Earlier, the plain passwords
  were accepted and stored as-is on the cluster which posed a security threat. MD5, SHA1, Unix Crypt, and BCrypt hashed
  passwords are supported. Qubole recommends using MD5 and BCrypt as SHA1 and Unix Crypt are less secure. :ref:`Learn more <prestor53-enhancements>`.

Spark
......

* Support for Apache Spark 2.3.1, which is the latest version of Spark.
* Qubole features and performance enhancements on Spark:

  * Improvement in S3 listing performance
    :ref:`Beta, Via Support, Disabled  <availability-features>`
  * Disallow creation of Spark clusters with low memory instances
    :ref:`Beta, Via Support, Disabled  <availability-features>`
  * Integration of DDL commands with Snowflake through Spark
  * Rolling and aggregation of Spark executor and driver logs into remote S3
    :ref:`Beta, Via Support, Disabled  <availability-features>`

  :ref:`Learn more<sparkr53>`.

Data Analytics
..............

* Partition columns are now marked distinctly on the **Analyze** page’s **Tables** tab.
* You can now view and edit Hive commands greater than 126KB (which were earlier uploaded to Amazon S3) from the UI. :ref:`Beta, Via Support, Disabled  <availability-features>`

  :ref:`Learn more <analyze-explorer53>`.

Data Engineering
................

* For Workflow commands, you can now download subcommand logs as well as results from both the UI and the API. This allows you
  to get logs and results for subcommands that were run even if the command failed as a whole. :ref:`Beta, Via Support, Disabled  <availability-features>`

  :ref:`Learn more <data-engineering-r53-index>`.
