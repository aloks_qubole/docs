.. _release-notes-r53-index:

=================
Version R53 (AWS)
=================
Qubole provides Qubole Data Service (QDS) on Amazon Web Service (AWS), Microsoft Azure, and Oracle Cloud Infrastructure (OCI).

:ref:`hotfixes-post-r52-a` provides the list of hotfixes and enhancements done post version R52.

For details on what has changed in this version, see:

.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr53.rst
    Engines/index.rst
    acmr53.rst
    Applications/index.rst
    otherQDS.rst
    hotfixes-postr52.rst
    enhancements-post-r53.rst