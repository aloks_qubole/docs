.. _enhancements-post-r53:

=====================================
Enhancements After 5th September 2018
=====================================

Release Version 53.33.0
-----------------------

* **ACM-3640**: Qubole now supports ``c5d``, ``m5d``, ``r5``, ``r5d``, and ``z1d`` instances. These new instances do not
  support Hadoop 1 clusters. The ``c5d.large`` instance does not support Presto and Spark clusters.

  Qubole does not support ``r5.large``, ``r5d.large``, and ``z1d.large`` instances.
