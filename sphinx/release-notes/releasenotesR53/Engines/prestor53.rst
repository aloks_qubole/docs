.. _presto-r53:

======
Presto
======

New Features
------------

* **PRES-2080**: The **Presto 0.193** version is **generally available**. Detailed information around the 0.193 release are available as a blog post at `Introducing Presto 0.193 in QDS <https://www.qubole.com/blog/introducing-presto-0-193-qds/>`__ .
* **PRES-1351**: The **Qubole Presto Server Bootstrap** is an alternative to :ref:`Node Bootstrap Script<nodebootstrapscript>`
  to install external jars such as `presto-udfs <https://github.com/qubole/presto-udfs>`__ before the Presto Server is
  started. This prevents the additional restart of the Presto Server that happens when you install such jars through the Node
  Bootstrap Script which causes query failures during the node startup. It is only supported in Presto 0.180 and later
  versions. :ref:`Disabled <availability-features>`
* **PRES-1862**: Qubole Presto now supports providing full access to the S3 bucket owner for files written to the bucket
  by other users. When it is enabled, the S3 bucket owner gets complete permissions over the files written into the
  bucket. :ref:`Disabled  <availability-features>` :ref:`Cluster Restart Required <cluster-restart-op>`

  The configuration is:

  ``catalog/hive.properties:``

  ``hive.bucket-owner-full-control=true``

* **PRES-2028**: A new FastPath for queries executed by the QDS Drivers. The FastPath minimizes the QDS level time for
  Presto queries. It is only supported on Presto 0.180. With the Presto FastPath, the overall command latencies within
  QDS should come down to around 1.5 seconds when the cluster is active. :ref:`Beta, Via Support <availability-features>` :ref:`Cluster Restart Required <cluster-restart-op>`

Bug Fixes
---------

* **PRES-1443**: In Presto autoscaling, the spot ratio was maintained in all nodes. The Presto autoscaling logic is
  fixed to maintain the spot ratio only within the autoscaled nodes.
* **PRES-1567**: The Hive authorization check was not available for SHOW COLUMNS when the Hive authorization is enabled.
  Qubole has introduced the authorization check for SHOW COLUMNS.
* **PRES-1760**: There was an incorrect conversion of the timestamp fields. A new configuration property to automatically convert
  timestamp fields from the server timezone to the client timezone is added. The client timezone can be specified through
  the cluster configuration property, ``client-session-time-zone``. :ref:`Disabled  <availability-features>`  :ref:`Cluster Restart Required <cluster-restart-op>`

  Example:

  ``config.properties:``

  ``client-session-time-zone=Asia/Kolkata``

  It is helpful when you are in a different timezone than the Presto Server in which case the timestamp fields in the output
  would be displayed in the server timezone if this configuration is not set.

* **PRES-1778**: Some queries might show failed status in the Qubole UI without a stack trace in logs. The Presto client
  is fixed to bubble up errors in the commit phase.
* **PRES-1850**: ``Error- inputFormat should not be accessed from a null StorageFormat``. Presto can query tables backed
  by the storage handlers.
* **PRES-2014**: The queries with brackets around them combined results for some columns. It has been resolved now.

.. _prestor53-enhancements:

Enhancements
------------

* **PRES-1477**: Backported the OS Fix for handling the complex type in the optimized Parquet reader to Qubole Presto 0.193.
* **PRES-1523**: A fix is back-ported to honor ``skip.header.line.count`` and ``skip.footer.line.count`` in the SELECT queries
  from the Hive tables for the Text and Avro files.
* **PRES-1783**: A Datadog alert is added to monitor Presto query planning time, which gets triggered if it exceeds two
  minutes. :ref:`Beta, Via Support <availability-features>`

* **PRES-1829**: Presto can extract the username from the source name header for the authentication purpose.
* **PRES-1915**: Changes in the file-based authentication:

  - The file-based authentication feature has been enhanced to accept only the hashed passwords for each user. Earlier, the plain
    passwords were accepted and stored as-is on the cluster which posed a security threat. MD5, SHA1, Unix Crypt, and
    BCrypt hashed passwords are supported. Qubole recommends using MD5 and BCrypt as SHA1 and Unix Crypt are less secure.
  - The file-based authentication accepts the input file as a collection of the username:password lines rather than a JSON.

* **PRES-1928**: Qubole ensures that Presto would not start if the Qubole Presto Server bootstrap is specified through ``bootstrap.properties``
  or the ``bootstrap-file-path`` in Presto overrides and when running the Qubole Presto Server bootstrap fails.
* **PRES-1939**:  Qubole Presto supports a customer delimiter in the INSERT OVERWRITE DIRECTORY syntax while writing
  results to S3. The customer delimiter must be an ASCII value.
