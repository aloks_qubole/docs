.. _schedulerr53:

=========
Scheduler
=========

New Features
------------

* **SCHED-281**: You can now view pending reruns for a scheduled job. You can also delete a pending rerun.



Enhancements
------------

* **SCHED-207**: For scheduled jobs, a cluster is not terminated before the idle timeout even when there are no jobs
  running on it. :ref:`Beta, Via Support <availability-features>`

* **SCHED-211**: The Stop label has been renamed to Kill on the **Scheduler** page.
* **SCHED-203**: The List Schedules API now has an additional ``created_at`` field which represents the time when that
  schedule/periodic job was created.
* **SCHED-247**: You can now search for a scheduled job by name by using the API. Pass the ``name`` parameter to search for
  a job by name. The name parameter must be at least 3 characters long.

