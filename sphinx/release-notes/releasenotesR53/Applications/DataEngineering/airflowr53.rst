.. _airflowr53:

=======
Airflow
=======


Bug Fixes
---------

* **AIR-176**: ACL mismatches are caused when 2 Airflow clusters, running within the same account, use the same session
  information of a user.
* **AIR-186**: Different Airflow clusters from the same QDS accounts are writing process and task logs to the same
  location on Amazon S3. :ref:`Cluster Restart Required <cluster-restart-op>`

