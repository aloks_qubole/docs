.. _workflowr53:

========
Workflow
========

New Features
------------

* **EAM-492**: For Workflow commands, you can now download subcommand logs as well as results from both the UI and the API.
  This allows you to get logs and results for subcommands that were run even if the command failed as a whole. :ref:`Beta, Via Support, Disabled  <availability-features>`
  For more information, see :ref:`download-analyze-results`.