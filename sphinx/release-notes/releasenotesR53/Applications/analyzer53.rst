.. _analyze-explorer53:

==============
Data Analytics
==============

New Features
------------

* **AN-24**: You can now disable the **Composer Auto-suggestions** option from the UI (**Control Panel** >
  **My Preferences** > **Analyze Interface**), and do not need to contact Qubole Support for the same. You can also enable this feature from
  the **Analyze** page (only) for SQL commands. For more information, see :ref:`manage-preferences`.
* **AN-353**: SQL formatting (already available for Hive commands) is now available for the Presto, Spark (SQL type only),
  Redshift, DB Query, and Query Export command types on the **Analyze** page.
* **AN-126**: You can now view and edit Hive commands greater than 126KB (which were earlier uploaded to Amazon S3) from
  the UI. :ref:`Beta, Via Support, Disabled  <availability-features>`

Bug Fixes
---------

* **AN-751**: The **Archive** button, which was always visible earlier on the **Analyze** page’s **Workspace** tab, is hidden if you
  do not have the permission to archive saved queries. Contact the administrator of your user account to obtain this permission.
* **AN-746**: Avoid accidental runs of a subquery on the **Analyze** page by adding a confirmation step on
  clicking the **Run Selected** button.
* **AN-572**: The cluster label search and filter on the **Analyze** page’s **History** tab now returns only exact matches allowing you
  to locate commands faster. Earlier, the filter also returned partial matches.
* **AN-986**: Added support to run Presto commands using the query path from the **Templates** page.
* **AN-771**: The UI appears misaligned due to long space-less commands.
* **EAM-998**: Headers are not printed in the Command Results API  if the inline parameter is set to *true*.
* **EAM-1052**: Bookkeeping issues caused commands to be marked as failed despite the underlying MR jobs succeeding.

Enhancements
------------

* **AN-1076**: Saved queries (previously available only to the first user of the account) are now available to any new user
  who joins an account by using the sign-up link or by invitation.
* **AN-1348**: You now receive more relevant table name suggestions on the **Analyze** page’s code composer pane. This is
  due to the increased preference given to previous commands run from the page.
* **AN-559**: Partition columns are now marked distinctly on the **Analyze** page's **Tables** tab.
* **AN-1082**: The ``additional options`` field is now available for the **Data Import** and **Data Export** command types
  (drop-down menu options under Command) on the **Templates** page.
* **INFRA-941**: For commands that are waiting in queue to execute, Qubole explicitly indicates that the command is
  throttled with its position in the queue so that users can distinguish these from commands that are already running.
  For more information, see :ref:`analyze-qpal` and :ref:`view-command-status`.
