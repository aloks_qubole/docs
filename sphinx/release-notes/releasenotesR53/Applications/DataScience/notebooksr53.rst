.. _notebooksr53:

=========
Notebooks
=========

Bug Fixes
---------

* **ZEP-2468**: Empty paragraphs ran with the **Run All** option in the Notebooks and Dashboard UI.
* **ZEP-2272**: A scatter chart caused the Notebooks UI to freeze due to a bug in the Javascript library that was used for displaying the plot.
* **ZEP-2119**: The cluster did not consider the Spark setting ``spark.driver.extraJavaOptions`` defined at the notebook level through the Interpreter settings. Users can now override this option in the **Interpreter** page of the Notebooks UI.
* **ZEP-2561**: In the case of a scheduled notebook, when the user attempted to change the cluster associated with such notebook, an uninformative error message was displayed. Now the relevant information is displayed in the error message.
* **ZEP-2525**: Users were not able to download the paragraph results in Chrome. The ``fetch output`` operation failed with the ``Failed - Network error`` message when the output was larger than 1 MB.
* **ZEP-2446**: Users were not able to view the Hive Notebooks output.
* **ZEP-1800**: R plots were not displayed in the Notebooks UI, when package management was enabled.
* **ZEP-2178**: When viewing Notebooks in the offline mode, the UI failed to respond because of large notebooks. Now the output size limit is set to prune the size of the output automatically.

Enhancements
------------

* **ZEP-2108**: Users can access the source notebooks from the Dashboard UI. Users can click on the **View Notebook** option on the Dashboard UI to open the source notebook. The source notebook is displayed in a new tab and in the open state.
* **ZEP-1980**: The number of max executors defined in the Spark Interpreter settings by ``spark.max.executors`` was set to 10 by default. Now the resource manager can handle this setting automatically.
  :ref:`Beta, Via Support, Disabled  <availability-features>`

* **ZEP-2840**: For the new accounts, Zeppelin internal cron scheduler is deprecated because the internal scheduler could not start the cluster and therefore was not able to run the notebook. Also, the cron scheduler was not visible in the **Scheduler** page. Users of new accounts can schedule their notebooks by using the Qubole scheduler.
* **ZEP-2715**: Notebooks now support Spark 2.3.1.