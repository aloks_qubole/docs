.. _AdminChangesr53:

==============
Administration
==============


Bug Fixes
---------

* **AD-835**: Recent cluster instances are being skipped on the **Reports** page due to a discrepancy with time fields.

Enhancements
------------

* **AD-1134**: The Cluster Usage Report API now has an additional ``last_computed_time`` field that signifies the time when
  the reported usage value was calculated. Additionally, a message displaying the time the usage value was calculated
  appears in all the places where compute usage details were provided (such as QCUH for AWS).

