.. _release-notes-r54-index-oci:

====================
Version R54 (Oracle)
====================
This section of the Release Notes describes new and changed capabilities of the Qubole Data Service (QDS) on Oracle Cloud Infrastructure (OCI), as of Release Version R54.


.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr54-oci.rst
    Engines/index.rst
    acmr54-oci.rst
    Applications/index.rst
    otherQDS-oci.rst
    hotfixes-postr53-oci.rst
