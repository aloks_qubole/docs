.. _acmr54-oci:

==================
Cluster Management
==================

Bug Fixes
---------

* **ACM-3553**: Fixes the following problems:

  - Active and transitioning clusters were not displayed on the UI if there were more than 100 clusters
    in an account and the active clusters were not among the first 100 clusters.
  - Searching for cluster IDs or labels was limited to the first 100 clusters in an account.
    On the **Clusters** page, search and filters now work across all clusters in the account.
  - The number of clusters displayed on the clusters page is now limited to 30 clusters at a time. Click the **Load More**
    button at the bottom of the list to see more clusters, including all active and transitioning clusters.
    :ref:`Via Support <availability-features>`



