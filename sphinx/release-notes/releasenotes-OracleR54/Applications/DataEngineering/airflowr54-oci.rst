.. _airflowr54-oci:

=======
Airflow
=======

New Features
------------

* **AIR-195**: Users can now push their DAG files to a remote bucket store in the ``dags/plugins`` folder. Airflow
  keeps track of these remote folders and will fetch any changes.

  :ref:`Via Support, Disabled <availability-features>`. :ref:`Cluster Restart Required <cluster-restart-op>`

* **AIR-163**: The Monit ``httpd`` dashboard is now accessible from the QDS UI for Airflow clusters. You can view the status and resource
  utilization of various process running on the cluster. A cluster administrator can start, stop, and restart these processes from
  the UI. :ref:`Cluster Restart Required <cluster-restart-op>`




