.. _AdminChangesr54-oci:

==============
Administration
==============

New Features
------------


* **AD-158**: QDS has a new *Service* user type. This user is automatically validated and immediately added to the account.
  Any user with user-management permissions can create a Service user and update the authentication token. Service
  users cannot log in to the QDS UI and can use only the APIs;and they cannot be added as regular users in any other account.

* **EAM-1337**: A QDS Administrator can enable or disable *Data Preview* from the
  **Manage Roles** page of the QDS UI. Disabling Data Preview prevents your Hive table data from being previewed on the **Explore**
  page (Rows tab) and on the **Analyze** page (where this feature is provided for hot tables).



Improvements
------------

* **AD-1315**: Qubole has addressed inconsistencies in the Accounts API and has made changes to the **v2** version. The version **latest**
  also reflects the new changes as it points to the newest version. The current version, **v1.2**, remains as is to maintain
  backward compatibility.

* **AD-1353**: You can now enable cookie consent per GDPR guidelines for visitors from the EU region. Additionally, you can
  allow visitors to change their cookie preferences at any time.

* **AD-1547**: Cloud agent features are now enabled by default for all new and existing accounts. The **Enable/Purchase Cloud
  Agent** option has been removed from the **Super Admin/Account Edition** and **Payment and Subscription** pages.

* **AD-1244**:  A new field, *authentication_token_updated_at*, has been added to the API response for getting the users of a
  particular QDS account. This field reflects the UTC timestamp when the user's authentication token was last updated.
  It returns blank if the timestamp is not available.

* **AD-1264**: The **Start a Cloud Agents Free Trial API** has been deprecated.

* **AD-1286**: A new *created_at* field is now available for each user in the response to the API for viewing users.
  This field reports the UTC timestamp for when the user was added to the QDS account.


Bug Fixes
---------

* **EAM-1031**: A better error message now appears when you try to delete a data store which has active or suspended scheduled jobs.

* **EAM-451**: Added support for using a Hive Metastore Server endpoint as a custom metastore. :ref:`Disabled <availability-features>`
