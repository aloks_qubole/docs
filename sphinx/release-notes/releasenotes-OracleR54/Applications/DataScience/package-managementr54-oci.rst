.. _package-management-r54-oci:

==================
Package Management
==================

Improvements
------------
* **ZEP-2600**: Packages that have failed to install can now be deleted permanently.
