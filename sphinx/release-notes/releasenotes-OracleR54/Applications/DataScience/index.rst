.. _data-science-r54-index-oci:

============
Data Science
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    notebooksr54-oci.rst
    package-managementr54-oci.rst
