.. _applicationsr54-index-oci:

============
Applications
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    adminr54-oci.rst
    analyzer54-oci.rst
    DataEngineering/index.rst
    DataScience/index.rst
