.. _hadoop-r54-oci:

========
Hadoop 2
========

Improvements
------------

* **HADTWO-1650**: To meet security requirements, Oracle JDK 1.7 version is removed from cluster nodes that use OpenJDK-7
  (version 1.7.0.161).
