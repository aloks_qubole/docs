.. _presto-r54-oci:

======
Presto
======


.. _prestonoteGA-oci:

Presto Notebooks are Generally Available
----------------------------------------
**PRES-1996**: Presto Notebooks are generally available, with these changes:

* QDS has implemented a native Presto Interpreter; this shows progress as a percentage in the Notebook UI.
* Concurrency is supported. You can specify maximum concurrency using ``zeppelin.presto.maxConcurrency``
  when you create the interpreter; the default is 10.

  .. note:: This feature will be enabled for use in the next few days.

.. _presto208-oci:

Presto 0.208 is Supported
-------------------------
**PRES-2169**: The latest supported version is Presto 0.208, which supports file-based authentication and FastPath. It does not
support Dynamic Filtering. :ref:`Beta <availability-features>`. :ref:`Cluster Restart Required <cluster-restart-op>`



Bug Fixes
---------


* **PRES-2026**: Makes planning-time improvements to speed up plan generation for queries that have complex predicates
  or operate on tables with many partitions.
* **PRES-2150**: Resolves an issue that caused queries using
  memory counters to fail intermittently with the error ``Future should be done``.
* **PRES-2168**: Fixes a Dynamic Filtering bug related to concurrency.
* **PRES-2232**: Resolves a problem that caused failures in queries using the ``system.jdbc`` schema with ``PacketTooBigException``,
  when there were too many tables in one of the Hive schemas.
