.. _otherQDSChangesr54-oci:

====================
Other Changes in QDS
====================

New Features
------------
* **SEC-1523**: Provides Apache Ranger integration for Hive workloads to help security administrators define fine-grained data-access policies for users and groups.

* **SEC-1476**: Security administrators can define and enforce RBAC policies across multiple QDS artifacts that contain data and metadata, such as commands, data stores connections, data previews, and results.

* **SEC-1937**: Explore, Analyze, and SQL Service interfaces honor role-based access to *Data Preview* as configured in the QDS UI under **Control Panel > Roles**.
