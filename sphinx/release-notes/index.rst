.. _Release-Notes-index:

#############
Release Notes
#############

.. toctree::
    :maxdepth: 1
    :titlesonly:

    releasenotesR55/index.rst
    releasenotes-AzureR55/index.rst
    releasenotes-OracleR55/index.rst
    releasenotesR54/index.rst
    releasenotes-AzureR54/index.rst
    releasenotes-OracleR54/index.rst
    releasenotesR53/index.rst
    releasenotes-AzureR53/index.rst
    releasenotes-OracleR53/index.rst
    releasenotesR52/index.rst
    releasenotes-AzureR52/index.rst
    releasenotes-OracleR52/index.rst
    earlier-versions.rst
