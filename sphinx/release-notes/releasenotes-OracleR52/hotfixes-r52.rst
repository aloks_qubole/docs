.. _hotfixes-post-r50-oracle:

=================================
Hot-fixes Since 23rd January 2018
=================================

Release Version: 50.84.0
------------------------
* **GDPR Readiness**

  As part of GDPR readiness, Qubole has updated its privacy policy. The updated privacy policy now states how Qubole will
  collect, use, disclose, and share personal data that belongs to QDS users.

  As part of the **Sign up** process, when personal information such as name, email ID, company, title, location, and phone
  number is collected from a new user, he will be asked to give consent to Qubole's **Privacy Policy** and **Terms of Service**,
  by selecting the associated checkbox before proceeding further in the **Sign up** process.

  An existing user, during a fresh login, will be asked to provide consent to Qubole for processing his personal data
  by selecting the **Privacy Policy** and **Terms of Service** checkboxes. This will be required to successfully log into
  the QDS platform. The consent will be collected using a one-time pop-up which will not be shown to users during subsequent
  sign-ins. The same pop-up requesting for consent will be displayed when a user is invited to join QDS.

Release Version: 50.81.0
------------------------

* **AN-874**: Fixes a problem that caused tables not to be displayed under the **Analyze** > **Tables** when Hive
  authorization was enabled.

Release Version: 50.73.0
------------------------

* **INFRA-778**: Fixes a problem that caused live logs for running queries not to be displayed.


Release Version: 50.60.0
------------------------

* **EAM-1004**: Fixes a problem that caused a command or query to be abruptly terminated.

Release Version: 50.45.0
------------------------

* **EAM-911**: The Ruby SAML version has been upgraded to the latest 1.7.2 version.


Release Version: 50.38.0
------------------------

* **EAM-857**: Qubole Scheduler has introduced a new re-run limit per scheduled job; it is applicable to all scheduled
  jobs. Its default value is 20. When the scheduled job re-runs exceed the limit, you will see this error message:
  ``A maximum of 20 reruns are allowed for scheduled job in your account: #<account_number>``.

  You can increase the number of re-runs for a QDS account by creating a ticket with Qubole Support. The new value will
  apply to all the jobs that run in the  account.

Release Version: 50.29.0
------------------------

* **HADTWO-1341**: Fixes a problem that caused all re-tries to fail after a client running on another cluster node
  tried and failed to contact any daemon running on the master. Address resolution is no longer done on the client side.
