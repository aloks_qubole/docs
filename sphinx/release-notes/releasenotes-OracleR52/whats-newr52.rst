.. _whats-new-oracle:

===========================
What's New in QDS on Oracle
===========================

This section calls out the most significant changes and improvements. For more information see:

.. toctree::
    :maxdepth: 1
    :titlesonly:

    beta-supportr52.rst
    Engines/index.rst
    Infrastructure/index.rst
    Applications/index.rst
    otherQDS.rst
    hotfixes-r52.rst


.. _Airflow-new-oracle:

Support for Airflow
-------------------

As of this release Airflow version 1.8 is supported on Oracle. Support is not enabled by default; create a ticket with
`Quobole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable it.


.. _PM-new-oracle:

Changes in Package Management
-----------------------------
Package Management is available for open Beta in this release, and has been improved in the following ways: |BetaImageP|

  .. |BetaImageP| image:: ../rel-images/OpenBetaSupport.png

* The Package Management UI supports uninstalling packages.
* The **Control Panel** of the QDS UI supports configuring account-level ACLs for package management.
* ACLs can also be configured via an API.
* You can delete an environment.

Details :ref:`here <package-management-r52-oracle>`.


.. _acm-new-oracle:

Changes in Cluster Management
-----------------------------

These improvements are |GA1|.

.. |GA1| image:: ../rel-images/GA.png


* QDS clusters support newer Dense and Standard flavors of VM and BM instance families.
* You can now select multiple availability-domain/subnet pairs in the QDS UI.
* QDS supports soft enforcement of cluster permissions at the object level. When you select one permission in the
  **Manage Permissions** dialog for a cluster, additional cluster permissions are automatically selected.
  You can disable those additional permissions if you decide to, but Qubole strongly recommends you accept them.
  See the :ref:`documentation <soft-enforcement-cluster-permissions>` for more information.

Changes in Data Management
--------------------------

These improvements are |GA|.

.. |GA| image:: ../rel-images/GA.png


* The problem of output write failures after partial file writes to object storage has been fixed.


.. _hive-new-oracle:

Changes in Hive
---------------

These improvements are |GA2|.

.. |GA2| image:: ../rel-images/GA.png

* Running Hive 2.1 on the Master node (**Hive on Master**) is still supported but is no longer the default.


.. _note-new-oracle:

Changes in Notebooks
--------------------

These improvements are |GA3|.

.. |GA3| image:: ../rel-images/GA.png

* For help in debugging TTransport exceptions, a hyperlink to the FAQ that contains the solution has been added in
  the paragraph output.

* The UI provides better scrolling while a notebook is being loaded.

See also :ref:`beta-support`.

Changes in Presto
-----------------

* QDS supports Presto 0.193 on Presto clusters |OpenBeta|.

  .. |OpenBeta| image:: ../rel-images/OpenBetaSelfService2.png

  Improvements include autoscaling; user-defined functions (UDFs); a faster Rubix client; and the Kinesis connector.

Changes in Spark
----------------

* QDS now supports Spark 2.2.1; this version is reflected as 2.2 latest (2.2.1) in the Spark cluster UI. All 2.2.0 clusters are automatically upgraded to 2.2.1 in accordance with Qubole’s Spark versioning policy.

.. note::

   Spark 2.2.1 as the latest version will be rolled out in a patch after the R52 release.

See also :ref:`beta-support`.


GDPR Readiness
--------------
As part of GDPR readiness, Qubole has updated its privacy policy. The updated privacy policy explains how Qubole will
collect, use, disclose, and share personal data that belongs to QDS users.

As part of the **Sign up** process, when personal information such as name, email ID, company, title, location, and phone
number is collected from a new user, he will be asked to give consent to Qubole's **Privacy Policy** and **Terms of Service**,
by selecting the associated checkbox before proceeding further in the **Sign up** process.

An existing user, during a fresh login, will be asked to provide consent to Qubole for processing his personal data
by selecting the **Privacy Policy** and **Terms of Service** checkboxes. This will be required to successfully log into
the QDS platform. The consent will be collected using a one-time pop-up which will not be shown to users during subsequent
sign-ins. The same pop-up requesting for consent will be displayed when a user is invited to join QDS.

