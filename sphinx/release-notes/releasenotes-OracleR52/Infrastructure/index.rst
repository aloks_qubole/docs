.. _infrastructurer52-oracle-index:

=========================
Changes in Infrastructure
=========================


.. toctree::
    :maxdepth: 2
    :titlesonly:

    acmr52.rst
    command-infrar52.rst