.. _acmr52-oracle:

==================
Cluster Management
==================

New Features
............

* **ACM-2159**: QDS clusters support newer Dense and Standard types of VM and BM instance families.

Bug Fixes
.........

* **ACM-2060**: The cluster termination reason is now reported correctly as ``Inactivity`` instead of ``No Master found``.
* **ACM-2470**: Cluster start race conditions are fixed.
* **SQOOP-151**: ``dbimport`` and ``dbexport`` jobs now succeed, whether the tables are based on a default
  or non-default schema. (Formerly, jobs would fail if the tables were not based on the default schema).


Improvements
............

* **MUL-688**: The Bastion node does not need public IP SSH access in the security group. Instead, SSG access to the
  loopback address (``localhost``) is required.
* **MUL-712**: You can now select multiple availability-domain/subnet pairs in the QDS UI.