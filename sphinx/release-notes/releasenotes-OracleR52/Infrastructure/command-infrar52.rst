.. _command-infrar52-oracle:

==================
Command Processing
==================

Command Latency Improvements
----------------------------
Command submission and processing overhead has been reduced by around 10 seconds for larger queries.
This improvement particularly benefits latency-sensitive use cases, those from the UI as well as from ODBC/JDBC drivers.