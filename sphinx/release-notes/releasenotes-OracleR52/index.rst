.. _release-notes-r52-Oracle-index:

====================
Version R52 (Oracle)
====================

This section of the Release Notes describes new and changed capabilities for Qubole Data Service (QDS) on  Oracle, as of Release Version R52.

(:ref:`hotfixes-post-r50-oracle` provides a list of hotfixes since version R50.)

Availability of New Capabilities
--------------------------------

Qubole introduces new capabilities and improvements at the following levels of access:

* General Availability |GA|

  .. |GA| image:: ../rel-images/GA.png

* Open-Beta Access. This is further subdivided into:

  - Available on the QDS UI and/or API - open-beta |OpenBeta|.

    .. |OpenBeta| image:: ../rel-images/OpenBetaSelfService2.png

  - Available via Qubole Support - open-Beta, via Support (Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
    to enable the features in your QDS account |BetaImage|.)

    .. |BetaImage| image:: ../rel-images/OpenBetaSupport.png

For details of what's changed in this version, see:

.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr52.rst
    beta-supportr52.rst
