.. _appsr52-oracle-index:

=======================
Changes in Applications
=======================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    adminr52.rst
    analyzer52.rst
    DataEngineering/index.rst
    DataScience/index.rst