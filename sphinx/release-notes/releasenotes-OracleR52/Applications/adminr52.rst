.. _AdminChangesr52-oracle:

==============
Administration
==============

New Features
------------

* **EAM-715**: A new user group named **everyone** will be created for each account, with no associated roles.
  The group will be mapped to all the users of that account, so any role or policies associated with this group will
  be applied to all the users of the account. Modifying users for this group is not allowed but you can add and delete roles
  to and from the group.

Bug Fixes
---------

* **AD-630**: The issue in which the API token was not visible to non-system users and non-system admins has been resolved.
* **AD-884**: CPU counters that were missing from the **Reports** page have been added.

Enhancements
------------


* **AD-538**: A user can see the ID of the cluster which was used to execute a command in the **All Commands** report. This
  applies to all command types except ``db_export``, ``db_import``, and ``db_tap_query``.
* **AD-784**: QDS environments have an idle session timeout of 1 week (10800 minutes). The tooltip that incorrectly
  showed it as 1440 minutes has been corrected.
* **EAM-425**: Workload Scaling Limits has been added to the list of available cloud agents.
* **EAM-690**: System admins of a Qubole account will receive an email notification whenever there is any change in the
  **Allow Qubole access** setting.