.. _hive-r52-oracle:

====
Hive
====


Bug Fixes
---------

* **HIVE-3032**: Fixes a problem that caused INSERT OVERWRITEs to fail when ``hive.allow.move.on.s3`` was set to ``true`` and the table name contained ``=`` . This problem occurred when the table location path was longer or shorter than the partition path generated (dynamically when ``hive.allow.move.on.s3`` was set to ``true``) and resulted in invalid partition specs.
* **HIVE-3035**: Fixes a problem that causes a dynamic write to a partitioned table to fail if a partition already
  exists with a custom location. The fix consists of skipping the exception block if ``hive.qubole.dynpart.move.check``
  is set to false.
* **HIVE-3041**: Fix for proper handling of Thrift metastore exceptions which were causing GRANT, CREATE, and DROP
  commands to hang (in Hive 2.1.1) or fail with a null error (in Hive 1.2) when the object already existed in the metastore.
* **HIVE-3042**: Fixes a problem that caused a Hive query to hang during job submission.
* **HIVE-3133**: Fix to handle empty vector column batches in the FLOAT and DOUBLE reader when you use the Vectorized
  Reader.
* **HIVE-3166**: Fixes a problem that occurred in reading a Hive bootstrap script.
* **HIVE-3172**: Fixes a NPE (Null Pointer Exception) in the Historical SQL Operations section of the HiveServer2 web UI.

Enhancements
------------
* **HIVE-3030**: Tez can now handle a scenario where a single key-value pair size does not fit into any of the buffer blocks.
  This issue caused some Hive queries to fail with ``BufferOverFlowError`` when Tez was used.
* **HIVE-3167**: Fixes a race condition which caused queries to fail with ``State Transition Exception``.
  Related Open Source JIRAs: `HIVE-15563 <https://issues.apache.org/jira/browse/HIVE-15563>`__,
  `HIVE-17352 <https://issues.apache.org/jira/browse/HIVE-17352>`__.