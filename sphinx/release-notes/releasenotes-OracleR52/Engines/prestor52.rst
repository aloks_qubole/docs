.. _presto-oracle-r52:

======
Presto
======

New Features
------------

* **PRES-1393**: QDS now supports *Dynamic Filter*, an optimization to improve the performance of JOIN
  queries, and specifically to optimize hash JOINs in Presto; it can make such JOINs significantly faster.
  It is not enabled by default. You can enable the Dynamic Filter:

  - As a session-level property by setting this property -
    ``set session dynamic_filtering = true.``
  - As a Presto override in the Presto cluster by setting ``experimental.dynamic-filtering-enabled=true``. It requires a
    cluster restart for the configuration to be effective.

* **PRES-1696**: GeoSpatial functions from 0.193 have been back-ported into 0.180 in QDS. The
  ``hammingDistance`` string function has also been ported.

* QDS supports Presto 0.193 on Presto clusters |OpenBeta|.

  .. |OpenBeta| image:: ../../rel-images/OpenBetaSelfService2.png

  Improvements include autoscaling; user-defined functions (UDFs); a faster Rubix client; and the Kinesis connector.

