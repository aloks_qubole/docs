.. _hadoop-r52-oracle:

========
Hadoop 2
========

Bug Fixes
---------

* **HADTWO-1357**: A problem has been fixed that caused ``DistCp`` when used with dynamic strategy not to update the
  ``chunkFilePath`` and other static variables any time after the first job. This occurred when
  ``DistCp::run()`` was used; a single copy succeeded, but subsequent jobs finished with reported success but without any actual copying.
