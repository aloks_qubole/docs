.. _command-infrar53-azure:

=========================================
Command Processing and Cluster Management
=========================================

Command Processing
==================

Enhancements
------------

* **INFRA-941**: For commands that are waiting to execute, QDS explicitly indicates that the command is
  throttled, with its position in the queue, so that users can distinguish it from commands that are already running.

Cluster Management
==================

* **MUL-690**: Allows launching multiple clusters in a single Azure Resource Group.

