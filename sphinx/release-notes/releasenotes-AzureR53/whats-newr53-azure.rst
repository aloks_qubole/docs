.. _whats-new-r53-azure:

==========
What's New
==========

Important new features and improvements are as follows.



Azure-Specific Improvements
...........................

* Presto connector for Power BI - allows ad-hoc Presto queries from `Power BI <https://powerbi.microsoft.com/en-us/get-started/?&OCID=AID719832_SEM_UIEP1g7f&lnkd=Google_PowerBI_Brand&gclid=EAIaIQobChMIqeHc796V3QIVVYezCh3a3g0AEAAYASAAEgIdx_D_BwE>`__ in Direct Query mode.

* Networking improvements:

  * Support for accelerated networking on instances that support it.

  * Static IP support - you can specify static public and private addresses for the Master node of a cluster. Create
    a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ to enable this capability.

* Resource-group improvements - allow you to use a single default resource group for all clusters, or to bring up a given cluster in a given resource group.

* Data-in-transit encryption - you can enable SSL communication among cluster nodes, and HTTPS encryption between the cluster and Azure Blob storage.

* Support for Spark Snowflake connector, with support for Snowflake DDL statements.


.. note:: The label (in blue) that is against the description indicates the launch state, availability, and the default
          state of the feature/enhancement. For more information, click the label.

          Unless stated otherwise, features are generally available, available as self-service and enabled by default.

Hive
....

* The metastore consistency check (MSCK) result is displayed only in **Logs** instead of the **Results** tab of the **Analyze** UI. :ref:`Disabled  <availability-features>`


  :ref:`Learn more <hive-r53-azure>`.

Presto
......

* **Presto 0.193** is **generally available**. Detailed notes about the 0.193 release are available as a blog post at `Introducing Presto 0.193 in QDS <https://www.qubole.com/blog/introducing-presto-0-193-qds/>`__.
* File-based authentication has been improved to accept only hashed passwords. MD5, SHA1, Unix Crypt, and BCrypt hashed
  passwords are supported. Qubole recommends using MD5 and BCrypt as SHA1 and Unix Crypt are less secure.
* Presto supports extracting the username from the source name header to authenticate direct connections to the Presto Server
  (supported only for file-based and LDAP authentication).

  :ref:`Learn more <presto-r53-azure>`.

Spark
.....

* Qubole Spark 2.3.1 is the first release of Apache Spark 2.3.x on Qubole. It is displayed as 2.3 latest (2.3.1) in the
  Spark Version field of the **Create New Cluster** page on QDS UI.

  :ref:`Learn more <sparkr53-azure>`.

Data Analytics
..............

* QDS displays partition columns in the **Table Explorer** (and in the **Explore** UI page).
* You can now view and edit Hive commands greater than 126KB from the UI. :ref:`Beta, Via Support, Disabled  <availability-features>`

  :ref:`Learn more <analyze-explorer53-azure>`.

Other Improvements in QDS
.........................

* You can now discover new product features and enhancements easily from a **What's New** pane that has been
  integrated into the UI.

* Use the **Feedback button** (top right corner) from any screen to provide *faster* feedback and share ideas for enhancements
  and new features with the Qubole engineering team.
