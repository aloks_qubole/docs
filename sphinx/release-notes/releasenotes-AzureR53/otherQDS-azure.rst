.. _otherQDSChangesr53-azure:

====================
Other Changes in QDS
====================


Bug Fixes
---------

* **MUL-720**: Upgraded the Azure Blob storage SDK to fix a bug with writing files to Blob storage using SAS tokens.

New Features
------------
* **GROW-84**:You can now discover new product features and enhancements easily from a **What's New** pane that has been
  integrated into the UI.


Enhancements
------------
* **GROW-93**: Use the **Feedback button** (top right corner) from any screen to provide feedback *faster* and share ideas for enhancements
  and new features with the Qubole engineering team.
