.. _analyze-explorer53-azure:

==============
Data Analytics
==============

New Features
------------

* **AN-24**: You can now disable the **Composer Suggestions and Autocomplete** option from the QDS UI (**Control Panel** >
  **My Preferences** > **Analyze Interface**), and do not need to contact Qubole Support to do it. You can also do this from
  the **Analyze** page, for SQL commands only.
* **AN-353**: You can now format SQL for the Hive, Presto, Spark (SQL type only), Redshift, DB query and query export types on the **Analyze** page.
* **AN-126**: You can now view and edit Hive commands greater than 126KB (which were earlier uploaded to Amazon S3) from
  the UI. :ref:`Beta, Via Support, Disabled  <availability-features>`

Bug Fixes
---------

* **AN-751**: The **Archive** button, which was always visible earlier on the **Analyze** page’s **Workspace** tab, is hidden if you
  do not have the permission to archive saved queries. Contact the administrator of your user account to obtain this permission.
* **AN-746**: Avoid accidental runs of a subquery on the **Analyze** page by adding a confirmation step on
  clicking the **Run Selected** button.
* **AN-572**: The cluster label search and filter on the **Analyze** page’s **History** tab now returns only exact matches allowing you
  to locate commands faster. Earlier, the filter also returned partial matches.
* **AN-986**: Added support to run Presto commands using the query path from the **Templates** page.
* **AN-771**: The UI appears misaligned due to long space-less commands.
* **EAM-998**: Headers are not printed in the Command Results API  if the inline parameter is set to *true*.
* **EAM-1052**: Bookkeeping issues caused commands to be marked as failed despite the underlying MR jobs succeeding.

Enhancements
------------

* **AN-1076**: Saved queries (previously available only to the first user of the account) are now available to any new user
  who joins an account by using the sign-up link or by invitation.
* **AN-1082**: An **Additional Options** field is available for Data Import and Data Export commands in the **Command Templates** UI.
* **AN-1348**: You now receive more relevant table name suggestions on the **Analyze** page’s code composer pane. This is
  due to the increased preference given to previous commands run from the page.
* **AN-559**: Partition columns are now marked distinctly in the **Analyze** page's **Tables** tab.
* **EAM-1082**: The ``additional options`` field is now available for the **Data Import** and **Data Export** command types
  (drop-down menu options under Command) on the **Templates** page.

