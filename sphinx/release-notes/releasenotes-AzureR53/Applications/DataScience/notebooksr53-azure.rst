.. _notebooksr53-azure:

=========
Notebooks
=========

Bug Fixes
---------

* **ZEP-2468**: Empty paragraphs ran with the **Run All** option in the Notebooks and Dashboard UI.
* **ZEP-2625**: You can now successfully download a large paragraph in Chrome.
* **ZEP-2626**:The Spark interpreter property ``zeppelin.interpreter.default`` will no longer be a default property for new Spark interpreters.
* **ZEP-2272**: A scatter chart caused the Notebooks UI to freeze due to a bug in the Javascript library that was used for displaying the plot.
* **ZEP-2119**: The cluster did not consider the Spark setting ``spark.driver.extraJavaOptions`` defined at the notebook level through the Interpreter settings. Users can now override this option in the **Interpreter** page of the Notebooks UI.
* **ZEP-2525**: Users were not able to download the paragraph results in Chrome. The “fetch output” operation failed with the "Failed - Network error" message when the output was larger than 1 MB.
* **ZEP-2446**: Users were not able to view the Hive Notebooks output.
* **ZEP-2178**: When you viewed very large notebooks in offline mode, the UI failed to respond. Now results are hidden for paragraphs that exceed a limit that is configurable at the account level.

Enhancements
------------

* **ZEP-2108**: You can see a dashboard's source notebook from the Dashboard UI by clicking on **View Notebook**. The source notebook is displayed in a new tab and in the open state.
* **ZEP-2840**: Zeppelin's internal cron scheduler is no longer supported for new accounts because the internal scheduler could not start the cluster and run the notebook. Users of new accounts can schedule their notebooks by using the Qubole scheduler.
* **ZEP-2715**: Notebooks now support Spark 2.3.1.