.. _data-science-r53-index-azure:

============
Data Science
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    notebooksr53-azure.rst
