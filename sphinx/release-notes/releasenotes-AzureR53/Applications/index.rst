.. _appicationsr54-index-azure:

=======================
Changes in Applications
=======================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    adminr53-azure.rst
    analyzer53-azure.rst
    DataEngineering/index.rst
    DataScience/index.rst