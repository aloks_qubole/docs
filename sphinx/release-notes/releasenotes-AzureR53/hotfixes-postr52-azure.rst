.. _hotfixes-post-r52-azure:

==============================
Hot-fixes After 13th June 2018
==============================

Release Version: 52.51.0
------------------------
* **INFRA-1028**: Less than 0.05% of commands in QDS could end up in an unknown state under extremely high
  bursts in command submissions from customer(s).

Release Version: 52.37.0
------------------------

* **INFRA-1014**: In a few cases, commands were failing despite successful execution by the engine.

Release Version: 52.36.0
------------------------

* **INFRA-977**: Less than 0.05% of commands in QDS could end up in an unknown state under extremely high
  bursts in command submissions from customer(s).

Release Version: 52.29.1
------------------------
* **HADTWO-1545**: Parallel NodeManager starts in Hadoop2 that led to task failures.

Release Version: 52.29.0
------------------------
* **AN-745**: Slowness experienced while viewing tables in the **Explore** UI.

Release Version: 52.22.0
------------------------
* **ACM-2872**: Error while loading the Cluster UI page.

Release Version: 52.20.0
------------------------

* **SPAR-2726**: Snowflake writes on Spark 2.1 clusters were failing because of problems in the latest Snowflake Jars.
  The Snowflake JDBC and Spark-Snowflake jars have been reverted for Spark 2.1 clusters. The changed versions are:

  .. sourcecode:: bash

        snowflake-jdbc: Reverted from 3.5.3 to 3.4.0
        spark-snowflake_2.11: Reverted from 2.3.0 to 2.2.8


Release Version: 52.17.0
------------------------

* **QTEZ-329**: Tez can now handle a scenario where a single key-value pair does not fit into any of the buffer
  blocks. This resolved the issue that caused a Hive query to fail with ``BufferOverFlowError`` when Tez was used.

Release Version: 52.16.0
------------------------

* **EAM-1184**: When the cluster label was not present in the API payload, a Spark command was submitted to the default
  cluster instead of the last-used spark cluster. This issue has been resolved.
* **PRES-1974**: The problem of Presto queries failing because of comments in the query has been resolved.


Release Version: 52.10.0
------------------------

* **AN-1134**: The problem that caused sub-command status icons for **Workflow** commands not to be visible has been resolved.
  The icons are visible now.