.. _release-notes-r53-azure-index:

===================
Version R53 (Azure)
===================
This section of the Release Notes describes new and changed capabilities for Qubole Data Service (QDS) on Microsoft Azure, as of Release Version R53.

.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr53-azure.rst
    Engines/index.rst
    Infrastructure/index.rst
    Applications/index.rst
    otherQDS-azure.rst
    hotfixes-postr52-azure.rst