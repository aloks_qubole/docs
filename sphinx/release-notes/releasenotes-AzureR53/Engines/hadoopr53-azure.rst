.. _hadoop-r53-azure:

========
Hadoop 2
========


* **HADTWO-1449**: Qubole Hadoop version 2.6.0-qds-0.4.14 is available from AWS S3 via Maven. See the `POM.xml <https://github.com/qubole/qubole-jar-test/blob/master/hadoop2/pom.xml>`__
  for an example of how to use it.