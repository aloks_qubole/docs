.. _engines-azure-r53-index:

==================
Changes in Engines
==================


.. toctree::
    :maxdepth: 1
    :titlesonly:

    hadoopr53-azure.rst
    hiver53-azure.rst
    prestor53-azure.rst
    sparkr53-azure.rst