.. _hadoop-r54-azure:

========
Hadoop 2
========

Improvements
------------

* **HADTWO-1650**: To meet security requirements, Oracle JDK 1.7 version is removed from cluster nodes that use OpenJDK-7
  (version 1.7.0.161).
* **HADTWO-1598**: Ports **HADOOP-15547** from open source. Produces up to a tenfold improvement in the listing performance of the WASB
  file system.