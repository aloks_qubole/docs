.. _engines-r54-index-azure:

=======
Engines
=======


.. toctree::
    :maxdepth: 1
    :titlesonly:

    hadoopr54-azure.rst
    hiver54-azure.rst
    prestor54-azure.rst
    sparkr54-azure.rst
