.. _presto-r54-azure:

======
Presto
======


.. _prestonoteGA-azure:

Presto Notebooks are Generally Available
----------------------------------------
**PRES-1996**: Presto Notebooks are generally available, with these changes:

* QDS has implemented a native Presto Interpreter; this shows progress as a percentage in the Notebook UI.
* Concurrency is supported. You can specify maximum concurrency using ``zeppelin.presto.maxConcurrency``
  when you create the interpreter; the default is 10.

.. _presto208-azure:

Presto 0.208 is Supported
-------------------------
**PRES-2169**: The latest supported version is Presto 0.208, which supports file-based authentication, FastPath, and
Dynamic Filtering. :ref:`Beta <availability-features>` :ref:`Cluster Restart Required <cluster-restart-op>`

Enhancements
------------
* **PRES-2087**: Qubole now allows you to gracefully decommission problematic worker nodes (if any) during autoscaling
  in a Presto cluster. For more information, see :ref:`decommission-presto-worker-node`.

RubiX is Supported (Beta)
-------------------------
RubiX caching provides faster read performance for recently accessed files. This is a :ref:`Beta <availability-features>`
feature, disabled by default. Enable it from the **Clusters** page of the QDS UI.

Enhancements
------------
* **PRES-2087**: Qubole now allows you to gracefully decommission problematic worker nodes (if any) during autoscaling
  in a Presto cluster. For more information, see :ref:`decommission-presto-worker-node`.

Bug Fixes
---------
* **PRES-2026**: Makes planning-time improvements to speed up plan generation for queries that have complex predicates
  or operate on tables with many partitions.
* **PRES-2150**: Resolves an issue that caused queries using
  memory counters to fail intermittently with the error ``Future should be done``.
* **PRES-2168**: Fixes a Dynamic Filtering bug related to concurrency.
* **PRES-2232**: Resolves a problem that caused failures in queries using the ``system.jdbc`` schema with ``PacketTooBigException``,
  when there were too many tables in one of the Hive schemas.
* **PRES-2276**: Fixes a problem that caused multiple output files to be created even when the output was sorted.
* **PRES-2287**: Fixes a problem that caused ``DESC`` commands to fail with ``Access denied`` when a custom
  ``SystemAccessControl`` package was used.

