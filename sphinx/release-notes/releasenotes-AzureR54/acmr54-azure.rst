.. _acmr54-azure:

==================
Cluster Management
==================

New Features
------------

* **ACM-3777**: Allows you to use a static IP address for access to a cluster's master node. Enable it
  from the **Clusters** page of the QDS UI. You can specify a static public IP name, a static interface name, or both.

Bug Fixes
---------

* **ACM-3553**: Fixes the following problems:

  - Active and transitioning clusters were not displayed on the UI if there were more than 100 clusters
    in an account and the active clusters were not among the first 100 clusters.
  - Searching for cluster IDs or labels was limited to the first 100 clusters in an account.
    On the **Clusters** page, search and filters now work across all clusters in the account.
  - The number of clusters displayed on the clusters page is now limited to 30 clusters at a time. Click the **Load More**
    button at the bottom of the list to see more clusters, including all active and transitioning clusters.
    :ref:`Via Support <availability-features>`

* **ACM-3484**: Updates the Azure CLI to version 2.0.44, fixing a problem that caused CLI commands not to work on
  cluster nodes n some cases.


