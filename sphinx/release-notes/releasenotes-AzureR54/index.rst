.. _release-notes-r54-index-azure:

===================
Version R54 (Azure)
===================
This section of the Release Notes describes new and changed capabilities of the Qubole Data Service (QDS) on Microsoft Azure, as of Release Version R54.


.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr54-azure.rst
    Engines/index.rst
    acmr54-azure.rst
    Applications/index.rst
    otherQDS-azure.rst
    hotfixes-postr53-azure.rst
