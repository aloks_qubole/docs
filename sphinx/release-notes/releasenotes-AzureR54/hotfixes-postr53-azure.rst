.. _hotfixes-post-r53-azure:

==================================
Hot-fixes After 5th September 2018
==================================


Release Version: 53.27.0
------------------------

* **EAM-1262**: QDS provides more-descriptive error messages in case of failure when you're adding a data store from the
  **Explore** page of the UI.

Release Version: 53.24.0
------------------------

* **PRES-2026**: Makes planning-time improvements to speed up plan generation for queries that have complex predicates
  or operate on tables with many partitions.
