.. _package-management-r54-azure:

==================
Package Management
==================

Improvements
------------
* **ZEP-2600**: Packages that have failed to install can now be deleted permanently.