.. _data-science-r54-index-azure:

============
Data Science
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    notebooksr54-azure.rst
    package-managementr54-azure.rst