.. _notebooksr54-azure:

=========
Notebooks
=========



New Features
------------

.. _diff-formats-azure:

Export Notebooks in Different Formats
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**ZEP-2716**: You can export notebooks as PNG, PDF, or HTML.  Exported notebooks can be used for
cases such as publishing on a web page, or sending as an email attachment. You can export a notebook even when the cluster is down.
You can also download notebooks using the API (``/api/v1.2/commands`` with the ``notebookConvertCommand`` command type).

When exporting a notebook, you can choose to show or hide the code in the notebook.

If a notebook fails to render within 3 minutes, the download or email option fails.

:ref:`Beta <availability-features>` :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`


.. _spark-status-azure:

Spark Application Status on the Notebooks Page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**ZEP-2735** and **ZEP-2184**: You can use the **Health** widget on the **Notebooks** page of the QDS UI to monitor
the status of the Spark application. You can also see the status (down, starting, or running)
of the Spark interpreter that is bound to the Notebook.
:ref:`Beta <availability-features>`. :ref:`Via Support <availability-features>`. :ref:`Disabled  <availability-features>`

.. _cluster-status-azure:

Cluster Status on the Notebooks Page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**ZEP-2733**: The **Cluster** widget on the
**Notebooks** page displays real-time information about the cluster's health. For example, if the cluster is terminated,
you can see why. :ref:`Beta <availability-features>`. :ref:`Via Support <availability-features>`. :ref:`Disabled  <availability-features>`

Improvements
------------

* **ZEP-2879**: Scala libraries such as ``org.scala-lang:scala-library`` and ``org.scala-lang:scala-reflect`` are excluded by default.
  You do not have to explicitly exclude them when adding artifacts in the interpreter property.
* **ZEP-2736**: The Notebook Interpreter bindings are now available on the **Interpreters** page, rather than the Notebooks page.
  :ref:`Via Support <availability-features>`. :ref:`Disabled  <availability-features>`
* **ZEP-2585**: You can now import and access Python libraries in JAR files (such as the ``graphframes`` JAR) in PySpark.

Bug Fixes
---------

* **ZEP-2529**: Notebooks that were created using the API in legacy mode now run successfully from the **Analyze** and
  **Scheduler** pages in the QDS UI.
* **ZEP-2863**: Configuration file updates for Spark and Zeppelin setup are now `atomic <https://en.wikipedia.org/wiki/Read-modify-write>`_,
  and the files remain consistent during metastore connection setup.
* **ZEP-2478**: The Notebook stacked bar chart and grouped bar charts are now displayed appropriately whether the cluster
  is up or down. Updated bar charts are displayed whenever the settings are modified.
* **ZEP-2700**: Invalid interpreter JSON in DEFLOC caused Zeppelin to fail to start.
  This fix ensures invalid Interpreter JSON is not propagated to DEFLOC and Zeppelin starts successfully.
  :ref:`Via Support <availability-features>`. :ref:`Disabled  <availability-features>`

