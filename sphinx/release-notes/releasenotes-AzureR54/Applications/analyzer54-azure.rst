.. _analyze-explorer54-azure:

==============
Data Analytics
==============

New Features
------------

* **AN-107**: By specifying whether a command is **public**
  or **private**, you can control which command-runs other users in the account have access to.
  You can also specify a default for command access in future. :ref:`Disabled <availability-features>`

Enhancements
------------

* **AN-1285**: Cluster-management commands are no longer counted towards overall user concurrency, so cluster start
  commands are not throttled.

* **AN-1285**: You can set a maximum command-concurrent-limit percentage
  for all users of an account. This limit is configured at the account level, so all users are subject to the same limitation.
  :ref:`Via Support, Disabled <availability-features>`

Bug Fixes
---------

* **AN-1342**: If the **Refresh Table** command type was the first command in a workflow, the cluster-selection drop-down
  list was hidden for subsequent steps in the workflow edit. The cluster drop-down list
  is now displayed throughout the edit.

