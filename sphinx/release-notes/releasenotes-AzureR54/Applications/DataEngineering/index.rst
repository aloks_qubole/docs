.. _data-engineering-r54-index-azure:

================
Data Engineering
================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    airflowr54-azure.rst
    explorer54-azure.rst
    schedulerr54-azure.rst