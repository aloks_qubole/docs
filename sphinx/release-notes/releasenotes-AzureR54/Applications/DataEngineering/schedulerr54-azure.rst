.. _schedulerr54-azure:

=========
Scheduler
=========

New Features
------------

* **SCHED-312**: Users with *Manage* access for a scheduled job can use the API to change the ownership of that schedule.




