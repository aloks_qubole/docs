.. _explorer54-azure:

=======
Explore
=======

Bug Fixes
---------

* **EAM-1262**: QDS provides more-descriptive error messages in case of failure when you're adding a data store from the
  **Explore** page of the UI.
