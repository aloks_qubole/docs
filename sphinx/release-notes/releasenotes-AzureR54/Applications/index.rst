.. _applicationsr54-index-azure:

============
Applications
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    adminr54-azure.rst
    analyzer54-azure.rst
    DataEngineering/index.rst
    DataScience/index.rst