.. _hotfixes-post-r53-a:

==================================
Hot-fixes After 5th September 2018
==================================

Release Version: 53.46.0
------------------------
* **HADTWO-1736**: The issue in which a lot of delete markers were being created while renaming an existing
  file or creating a new file in the s3a filesystem has a resolution. The issue is seen only in the S3 buckets that
  have versioning enabled. It can be resolved by setting a property at the cluster level or the account level. :ref:`Via Support <availability-features>`
  :ref:`Cluster Restart Required <cluster-restart-op>` - for the cluster-level setting.

Release Version: 53.35.0
------------------------

* **AN-714**: The Query Export feature on the **Analyze** page has now been made optional and is disabled by default.  :ref:`Disabled <availability-features>`

Release Version: 53.28.0
------------------------

* **INFRA-1040**: The formatting issues in the **Analyze** page’s **Results** tab have been resolved.

Release Version: 53.27.0
------------------------

* **EAM-1262**: While adding data stores on the **Explore** page, more descriptive error messages appear in case of failures.

* **AIR-65**: Added support in QuboleOperator to pass the *use_customer_cluster, customer_cluster_label*, and *additional_options* parameters while submitting DBImport and DBExport commands.

Release Version: 53.24.0
------------------------

* **PRES-2026**: The queries with complex predicate and working on tables with lots of partitions were stuck in the PLANNING
  stage. So, the query planning is improved to make such queries’ execution faster.
* **RUB-74**: As part of the cluster health monitoring, Qubole evicts files from the local filesystem when RubiX cache
  is full.