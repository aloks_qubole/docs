.. _analyze-explorer54:

==============
Data Analytics
==============

New Features
------------

* **AN-107**: Control which command runs other users in the account have access to by specifying whether a command is **public**
  or **private**. For ease of use, you can also define a default for command access in the future. :ref:`Disabled <availability-features>`

Enhancements
------------

* **AN-1285**: Cluster management commands are no longer counted towards overall user concurrency. As a result, cluster start
  commands are not slow due to throttling anymore.

* **INFRA-775**: QDS has introduced a functionality where customers can request setting a maximum command concurrent limit percentage
  for all users of an account. As this limit is configured at the account level, all users get the same limitation.
  :ref:`Via Support, Disabled <availability-features>`

Bug Fixes
---------

* **AN-714**: The **Query Export** feature on the **Analyze** page has now been made optional and is disabled by default.
  :ref:`Disabled <availability-features>`

* **AN-1342**: If the Refresh Table command type was picked as the first command in a workflow, the cluster selection drop-down
  list was hidden for subsequent steps while editing the workflow. This bug was fixed by allowing the cluster drop-down list
  to be displayed at all times while editing workflow commands.

* **AN-655**: When landing on Analyze data from the **Explore** page, a select * default query was being populated with no limit on the
  number of results returned. To prevent customers from unintentionally running the query on the entire dataset, Qubole
  has set a limit on the number of results returned by executing the default query.

* **INFRA-1040**: The formatting issues in the **Analyze** page’s **Results** tab have been resolved.
