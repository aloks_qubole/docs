.. _AdminChangesr54:

==============
Administration
==============

New Features
------------

* **AD-448** **QCUH Dashboard**: Qubole has introduced a new feature to view and track QCUH usage for an account on an hourly, daily, weekly,
  and monthly basis. Navigate to **Usage > QCUH Analysis** to view these details for your account.

* **AD-158**: Qubole has introduced a new *Service* user type. This user is automatically validated and immediately added to the account.
  Any user having user management permissions can create a *Service* user and update the authentication token. However, note that these
  users cannot login from the UI and can be used only through APIs. Additionally, they cannot be added as regular users in any other account
  after being created as *Service* users.

* **EAM-1337**: The Administrator can now configure an additional resource, *Data Preview*, while managing resources on the
  **Manage Roles** page. Disabling  *Data Preview* protects your HIVE table data from being previewed on the **Explore**
  page (Rows tab) and on the **Analyze** page (where this feature is provided for hot tables).



Enhancements
------------

* **AD-1315**: Qubole has addressed inconsistencies in the Accounts API and has made changes to the **v2** version. The version **latest**
  also reflects the new changes as it points to the newest version. The current version, **v1.2**, remains as is to maintain
  backward compatibility.

* **AD-402**: A new parameter, *status*, has now been added for retrieving the All Commands report through the API. You can
  filter this parameter based on the command's status. The available values are: Error, Running, Waiting, Cancelled, and
  Cancelling.

* **AD-1353**: You can now enable cookie consent per GDPR guidelines for visitors from the EU region. Additionally, you can
  allow visitors to change their cookie preferences at any time.

* **AD-1547**: Cloud agent features are now enabled by default for all new and existing accounts. The **Enable/Purchase Cloud
  Agent** option has been removed from the **Super Admin/Account Edition** and **Payment and Subscription** pages.

* **AD-1244**:  A new field, *authentication_token_updated_at*, has been added to the API response for getting the users of a
  particular QDS account. This field reflects the UTC timestamp when the user's authentication token was last updated.
  It returns blank if the timestamp is not available.

* **AD-1264**: The **Start a Cloud Agents Free Trial API** has been deprecated.

* **AD-1286**: While viewing users for Account APIs, a new *created_at* field is now available for each user in the response.
  This field reflects the UTC timestamp when the user was added to the QDS account.

* **AD-1277**: The **Reports** and **Status** pages will now retain data for the last 90 days only. Earlier, there was no time limit
  set for supporting data.


Bug Fixes
---------

* **AD-1515**: When cloning an account, the application now associates it with a new external ID. This is available for customers
  using the IAM Role as the access mode. :ref:`Via Support, Disabled <availability-features>`

* **AD-1227**: Default tables are now being created for IAM roles when you configure your account settings with valid storage settings.

* **EAM-995**: Roles and group changes for users are now being reflected correctly as the caching problem has been resolved.

* **EAM-1031**: A corrected error message now appears when trying to delete a data store which has active or suspended scheduled jobs.

* **EAM-451**: Added support to use an HMS (Hive Metastore Server) endpoint as a custom metastore. :ref:`Disabled <availability-features>`