.. _deeplearningr54:

=============
Deep Learning
=============

Enhancements
------------

* **DS-223**: Qubole now supports setting CPU instances as master node on DeepLearning clusters.
* **DS-224**: The first-time import of TensorFlow is now faster.
* **DS-225**: Qubole now supports the AWS Mumbai Region for launching DeepLearning clusters.