.. _data-science-r54-index:

============
Data Science
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    notebooksr54.rst
    deeplearningr54.rst
    package-managementr54.rst