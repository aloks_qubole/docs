.. _package-management-r54:

==================
Package Management
==================

Enhancements
------------
* **ZEP-2600**: Packages, which have failed to install in the Package Manager can now be deleted permanently.