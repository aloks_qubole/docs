.. _notebooksr54:

=========
Notebooks
=========

In this release, Qubole provides various new features and enhancements for Notebooks.

New Features
------------

.. _diff-formats:

Export Notebooks in Different Formats
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**ZEP-2716**: You can export notebooks in the following formats: PNG, PDF, and HTML.  Exported notebooks can be used for various use cases, such as publishing them on a web page, and sending as an email attachment to business users or colleagues.
You can also download notebooks by using the command API (``/api/v1.2/commands`` with ``notebookConvertCommand`` command type). You can export the notebook even when the cluster is down.
When exporting the notebook, you can choose to show or hide the code in the exported notebook.
If a notebook fails to render within 3 minutes, then the download or email option fails.
:ref:`Beta <availability-features>` :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`


.. _spark-status:

Spark Application Status on the Notebooks Page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**ZEP-2735 and ZEP-2184**: For Spark users, the status of notebooks depends on the health of clusters and Spark applications. Now, you can view the status of the Spark application on the Notebooks page. The health widget on the Notebooks page tracks the status of the Spark application and displays real-time information about the application. You can also view the status of the Spark interpreter that is bound to the notebook. Spark interpreter status includes down, starting, and running. :ref:`Beta <availability-features>` :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`

.. _cluster-status:

Cluster Status on the Notebooks Page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**ZEP-2733**: You can view the cluster status on the Notebooks page. The cluster widget on the Notebooks page displays the real-time information about the cluster health. For example, if the cluster is terminated, you can view the status to understand the reason for cluster termination. :ref:`Beta <availability-features>` :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`

Enhancements
------------

* **ZEP-2879**: Scala libraries, such as, ``org.scala-lang:scala-library, org.scala-lang:scala-reflect`` are excluded by default. Therefore, you do not have to explicitly exclude them when adding artifacts in the interpreter property.
* **ZEP-2736**: The Notebook Interpreter bindings are now available on the Interpreters page. Earlier, the link to the Interpreter bindings was displayed on the Notebooks page. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`
* **ZEP-2585**: You can now import and access python libraries present in jars (such as graphframes jar) in PySpark.

Bug Fixes
---------

* **ZEP-2769**: %knitr Interpreter is now supported with Spark 2.3.1. Therefore, you can run the knitr commands with Spark 2.3.1.
* **ZEP-2529**: Notebooks that are created by using the API in the legacy mode run successfully from the Analyze and Scheduler UI.
* **ZEP-1811**: Dependencies specified in interpreters are reloaded after the zeppelin server is restarted.
* **ZEP-2863**: Hadoop config files updates are now atomic for Spark and Zeppelin setup to keep the files consistent during metastore connection setup.
* **ZEP-2478**: The Notebook stacked bar chart was not displayed correctly. Now, the Notebook stacked bar chart is displayed appropriately when the cluster is up or down. The grouped bar chart is also displayed correctly. Whenever the settings are modified, the updated grouped and stacked bar charts are displayed.
* **ZEP-2700**: Invalid interpreter json in defloc caused Zeppelin start to fail. With this fix, invalid interpreter json is not synchronized to defloc and zeppelin starts successfully. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`

