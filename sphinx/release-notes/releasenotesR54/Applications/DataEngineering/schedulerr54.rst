.. _schedulerr54:

=========
Scheduler
=========

New Features
------------

* **SCHED-312**: Users having *Manage* access for a scheduled job can change the ownership of that schedule through the APIs.




