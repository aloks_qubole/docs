.. _explorer54:

=========
Explore
=========

Bug Fixes
---------

* **EAM-1262**: While adding data stores on the **Explore** page, more descriptive error messages appear in case of failures.
