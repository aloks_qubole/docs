.. _release-notes-r54-index:

=================
Version R54 (AWS)
=================
Qubole provides Qubole Data Service (QDS) on Amazon Web Service (AWS), Microsoft Azure, and Oracle Cloud Infrastructure (OCI).

:ref:`hotfixes-post-r53-a` provides the list of hotfixes done post version R53.

For details on what has changed in this version, see:

.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr54.rst
    Engines/index.rst
    acmr54.rst
    Applications/index.rst
    otherQDS.rst
    hotfixes-postr53.rst
