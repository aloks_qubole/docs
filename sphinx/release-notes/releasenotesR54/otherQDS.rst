.. _otherQDSChangesr54:

====================
Other Changes in QDS
====================

New Features
------------
* **SEC-1523**: Ranger integration for Hive workloads to help security administrators define fine-grained data access
  policies at row and column-level across users and user groups.

* **SEC-1476**: The security administrator can define and enforce RBAC policies across multiple Qubole artifacts that contain
  data and metadata such as commands, data store connections, data previews, and results.

* **SEC-1937**: Explore, Analyze and SQL Service interfaces honor the role-based access to Data Preview that is configured
  in the **Control Panel > Roles** page.

