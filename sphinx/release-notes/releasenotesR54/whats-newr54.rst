.. _whats-new-r54:

===========
What is New
===========
The new features and enhancements are listed in the section below.

.. note:: The label (in blue) that is against the description indicates the launch state, availability, and the default
          state of the feature/enhancement. For more information, click the label.

          Unless stated otherwise, features are generally available, available as self-service and enabled by default.

Cluster Management
------------------

* Qubole now supports ``c5d``, ``m5d``, ``r5``, ``r5d``, and ``z1d`` instances. :ref:`Learn more <newr54AWSinstances>`.
* QDS has introduced a new cluster console providing granular visibility, enhanced governance and ease of use with
  features such as an ability to track a specific cluster’s activity, view history of its configuration changes, cluster
  usage, and instances. :ref:`Learn more <cluster-governance>`. :ref:`Beta <availability-features>`
* QDS supports configuring multiple subnets in a VPC when clusters are configured with all compositions including the
  heterogeneous clusters. :ref:`Learn more <multiple-subnet-comp>`. :ref:`Cluster Restart Required <cluster-restart-op>`

Hive
----

* Hive 2.1 version is now generally available. :ref:`Learn more <hive2.1GA>`. :ref:`Cluster Restart Required <cluster-restart-op>`
* Qubole has added HA Proxy in the cluster master node for balancing the load when there are multiple connections between
  the cluster and the Qubole-managed Hive Metastore. This removes a single point of failure and provides more stability.
  :ref:`Learn more <HAProxyHive>`. :ref:`Via Support <availability-features>`

Hadoop 1
--------
Hadoop 1 as-a-service is deprecated from this QDS version. Qubole will support Hadoop 1 on existing clusters until 31
December 2018. Creating a new Hadoop 1 cluster or cloning an existing Hadoop 1 cluster is not supported through the
API and the UI. :ref:`Learn more <hadoop1r54>`.

Presto
------

* Qubole has added a new feature to automatically retry the unsuccessful Presto queries (if possible) when the nodes are
  being added as part of autoscaling. :ref:`Learn more <prestoqueryretry>`. :ref:`Disabled  <availability-features>` :ref:`Cluster Restart Required <cluster-restart-op>`
* Presto Notebooks are now generally available. :ref:`Learn more <prestonoteGA>`.
* The latest supported version is Presto 0.208.  :ref:`Learn more <presto208>`. :ref:`Beta <availability-features>` :ref:`Cluster Restart Required <cluster-restart-op>`

Spark
-----

* Qubole Spark supports RubiX distributed file caching system. :ref:`Learn more <rubix>`. :ref:`Beta <availability-features>` :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`
* Qubole Spark provides Dynamic Filtering for join query performance improvement. :ref:`Learn more <dyn-filtering>`. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`
* Sparklens experimental open service tool is available on http://sparklens.qubole.net. :ref:`Learn more <sparklens_rel>`.
* Parquet footer metadata caching to improve query execution performance. :ref:`Learn more <meta_caching>`. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`
* Proactive cleanup of shuffle block data to enable faster downscaling of nodes. :ref:`Learn more <shuffle_cleanup>`. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`
* Auto-scaling is enabled by default for clusters. The default value for the maximum number of
  auto-scaling nodes has been increased from 2 to 10 for a new Spark cluster.  :ref:`Learn more <node-autoscaling>`.
* Large Spark SQL commands are now supported in API and Analyze page. :ref:`Learn more <large_sql>`. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`
* Qubole Spark command subtypes are now supported with script files containing macros. :ref:`Learn more <temp-macros>`. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`

Deprecated Spark Versions
.........................

In this release, the following Spark versions are deprecated: 1.5.1, 1.6.0, 1.6.1, 2.0.0, and 2.1.0.
Qubole will continue to support Spark 1.6.2 and latest maintenance versions of each minor version in Spark 2.x. See `version support documentation <https://docs.qubole.com/en/latest/admin-guide/osversionsupport.html>`_.


Spark Structured Streaming
--------------------------

* Qubole provides comprehensive support for Kinesis connector in Structured Streaming.

  * Support to read from Kinesis Source in micro-batch streaming and continuous streaming modes.
  * Support to ingest data into Kinesis.
  * Support for IAM roles in Kinesis Connector.

  :ref:`Learn more <kinesis-connector>`

* Support to write a structured streaming query to a Spark data source table. :ref:`Learn more <spark-table>`.
* Streaming query progress graphs are now displayed in notebooks. :ref:`Learn more <progress-graphs>`. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`
* Direct writes for checkpointing is supported. :ref:`Learn more <direct-writes>`. :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`


Notebooks
---------

* Notebooks can be exported as PNG, PDF, and HTML files. :ref:`Learn more <diff-formats>`. :ref:`Beta <availability-features>` :ref:`Disabled  <availability-features>`
* Cluster Status is available on the **Notebooks** page. :ref:`Learn more <cluster-status>`. :ref:`Beta <availability-features>` :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`
* Spark application status is available on the **Notebooks** page. :ref:`Learn more <spark-status>`. :ref:`Beta <availability-features>` :ref:`Via Support <availability-features>` :ref:`Disabled  <availability-features>`


Administration
..............

* Administrators can now track usage on Qubole using the QCUH dashboard. :ref:`Beta <availability-features>`

* Qubole has introduced a new *Service* user type. :ref:`Beta, Via Support, Disabled <availability-features>`

* Administrators can now configure an additional resource, *Data Preview*, on HIVE tables while managing resources on the **Manage Roles** page.

:ref:`Learn more <AdminChangesr54>`.


Data Analytics
..............

* QDS has introduced a functionality where customers can request setting a maximum command concurrent limit percentage for all users
  of an account. :ref:`Via Support, Disabled <availability-features>`

:ref:`Learn more <analyze-explorer54>`.

Data Engineering
................


**Explore**

* Qubole has enabled support for AWS Aurora-MySQL RDS as a:

  * Custom metastore in Qubole.
  * Database backend for Airflow contents.


**Airflow**

* Python 3.5 is now supported on Airflow clusters. You can now manage add-on packages using package management. :ref:`Beta, Via Support, Disabled <availability-features>`

* Qubole now enables you to monitor the health of Airflow clusters using integrated Monit, and lets you turn-on/turn-off certain services. :ref:`Cluster Restart Required <cluster-restart-op>`

* Continuous development, integration, and deployment of Airflow DAGs with the Qubole UI. :ref:`Beta, Via Support, Disabled <availability-features>` :ref:`Cluster Restart Required <cluster-restart-op>`

:ref:`Learn more <data-engineering-r54-index>`.

Security
........

* Apache Ranger integration for Hive workloads to help security administrators in defining fine-grained data access
  policies across users and user groups.

* Security administrators can define and enforce RBAC policies across multiple Qubole artifacts that contain data and
  metadata such as commands, data stores connections, data previews, and results.

:ref:`Learn more <otherQDSChangesr54>`.