.. _rubixr54:

=====
RubiX
=====

Bug Fixes
---------

* **RUB-74**: As part of the cluster health monitoring, Qubole evicts files from the local filesystem when RubiX cache
  is full.