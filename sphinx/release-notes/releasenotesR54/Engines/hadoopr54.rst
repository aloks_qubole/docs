.. _hadoop-r54:

========
Hadoop 2
========

Enhancements
------------

* **HADTWO-1342**: After a job fails, the error message that is displayed now states the actual cause of the job failure in
  the Application UI as opposed to the previous generic message that stated that the job was killed by a user.
* **HADTWO-1364**: Qubole now proactively removes the nodes that are marked as ``UNHEALTHY`` from the cluster.
* **HADTWO-1533**: The number of nodes terminated due to the spot node loss is published as a metric and it is displayed on
  the Datadog.
* **HADTWO-1650**: The Oracle JDK 1.7 version is removed from the clusters nodes that use OpenJDK-7 (version 1.7.0.161)
  as part of the security requirement.

Bug Fixes
---------

* **HADTWO-1682** and **HADTWO-1736**: The issue in which a lot of delete markers were being created while renaming an existing
  file or creating a new file in the s3a filesystem has a resolution. The issue is seen only in the S3 buckets that
  have versioning enabled. It can be resolved by setting a property at the cluster level or the account level. :ref:`Via Support <availability-features>`
  :ref:`Cluster Restart Required <cluster-restart-op>` - for the cluster-level setting.