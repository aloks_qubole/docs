.. _hadoop1r54:

========
Hadoop 1
========

Deprecation of Hadoop 1 as-a-service
------------------------------------
Hadoop 1 as-a-service is deprecated from this QDS version. Qubole will support Hadoop 1 on the existing clusters until
31 December 2018. Creating a new Hadoop 1 cluster or cloning an existing Hadoop 1 cluster is not supported through the
API and the UI.

If you are using Hadoop 1 clusters, then you must plan to migrate the workloads to Hadoop 2 clusters. Creating a
new Hadoop 2 (Hive) cluster is one of the easiest ways to migrate the workload. As an alternative option, you can migrate the
workloads to a Presto or a Spark cluster. For more information, contact the **Qubole Support** or the Customer Success Manager
associated with your account.