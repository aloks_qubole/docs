.. _presto-r53-oracle:

======
Presto
======

Bug Fixes
---------

* **PRES-1760**: Incorrect conversion of the time-stamp fields. A new configuration property to automatically convert
  timestamp fields from the server timezone to the client timezone has been added. The client time zone can be specified via
  the cluster configuration property ``client-session-time-zone``. :ref:`Disabled  <availability-features>`  :ref:`Cluster Restart Required <cluster-restart-op>`

  Example:

  ``config.properties:``

  ``client-session-time-zone=Asia/Kolkata``

  This is helpful when you are in a different time zone from the Presto Server; in that case the time-stamp fields in the output
  are displayed in the server's time zone if this property is not set.

* **PRES-1778**: Some queries reported a ``failed`` status in the QDS UI without a stack trace in the logs. The Presto client
  has been fixed to bubble up errors in the commit phase.
* **PRES-1850**: ``Error- inputFormat should not be accessed from a null StorageFormat``. Presto can now query tables backed
  by storage handlers.
* **PRES-2014**: Queries with brackets combined results for some columns.

Enhancements
------------


* **PRES-1829**: Presto can extract the username from the source name header for authentication purposes.
* **PRES-1915**: Changes in file-based authentication:

  - File-based authentication has been enhanced to accept only hashed passwords for each user. In earlier releases, plain-text
    passwords were accepted and stored as-is on the cluster, which posed a security risk. MD5, SHA1, Unix Crypt, and
    BCrypt hashed passwords are supported. Qubole recommends using MD5 and BCrypt as SHA1 and Unix Crypt are less secure.
  - File-based authentication now accepts username:password lines, rather than JSON, in the input file.

* **PRES-1939**:  Qubole Presto supports the use of a custom delimiter in the INSERT OVERWRITE DIRECTORY syntax when writing
  results to Cloud storage. The delimiter must be an ASCII value.
* **PRES-2080**: The **Presto 0.193** version is **generally available**.
