.. _command-infrar53-oracle:

=========================================
Command Processing and Cluster Management
=========================================

Command Processing
==================

Enhancements
------------

* **INFRA-941**: For commands that are waiting to execute, QDS explicitly indicates that the command is
  throttled, with its position in the queue, so that users can distinguish it from commands that are already running.

Cluster Management
==================

* **ACM-2614**: Resolve race condition between cluster auto-terminate due to inactivity and execution of command.

* **ACM-2974**: Permissions for CloudMan debug log files have been fixed so they continue to work after rotation of the logs.

* **ACM-2992**: Ganglia dependencies fixed for Oracle OCI clusters.

* **ACM-3167**: Added support for new instance types for OCI to **us-phoenix-1** and **eu-frankfurt-1** regions. The following new instance types are now supported:

  - VM.Standard2.4

  - VM.Standard2.8

  - VM.Standard2.16

  - VM.Standard2.24

  - VM.DenselO2.8

  - VM.DenselO2.16

  - VM.DenselO2.24

  - BM.Standard2.52

  - BM.DenselO2.52


