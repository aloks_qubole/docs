
.. _interfacesr53-oracle-index:

=======================
Changes in Applications
=======================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    adminr53-oracle.rst
    analyzer53-oracle.rst
    DataEngineering/index.rst
    DataScience/index.rst
