.. _analyze-explorer53-oracle:

==============
Data Analytics
==============

New Features
------------

* **AN-353**: You can now format SQL for the Hive, Presto, Spark (SQL type only), Redshift, DB query and query export types on the **Analyze** page.
* **AN-126**: You can now view and edit Hive commands greater than 126KB (which were earlier uploaded to Amazon S3) from
  the UI. :ref:`Beta, Via Support, Disabled  <availability-features>`

Bug Fixes
---------

* **EAM-1052**: Bookkeeping issues caused commands to be marked as failed despite the underlying MR jobs succeeding.

Enhancements
------------

* **AN-1076**: Saved queries (previously available only to the first user of the account) are now available to any new user
  who joins an account by using the sign-up link or by invitation.
* **AN-1082**: An **Additional Options** field is available for Data Import and Data Export commands in the **Command Templates** UI.
* **AN-559**: Partition columns are now marked distinctly in the **Analyze** page's **Tables** tab.
* **EAM-1082**: The ``additional options`` field is now available for the **Data Import** and **Data Export** command types
  (drop-down menu options under Command) on the **Templates** page.

