.. _airflowr53-oracle:

=======
Airflow
=======


Bug Fixes
---------

* **AIR-176**: ACL mismatches were caused when two Airflow clusters, running within the same account, used the same session
  information of a user.
* **AIR-186**: Different Airflow clusters from the same QDS accounts were writing process and task logs to the same
  location in Blob storage. :ref:`Cluster Restart Required <cluster-restart-op>`
* **AIR-195**: You can now push a DAG to a remote storage location using ``dags/plugins``. Airflow monitors these locations and fetches changes.
