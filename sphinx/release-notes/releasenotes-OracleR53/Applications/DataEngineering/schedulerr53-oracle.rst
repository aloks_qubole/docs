.. _schedulerr53-oracle:

=========
Scheduler
=========

New Features
------------

* **SCHED-281**: You can now view pending reruns for a scheduled job. You can also delete a pending rerun.



Enhancements
------------

* **SCHED-247**: You can now search for a scheduled job by name, using the ``name`` parameter in the API. The value must
  be at least 3 characters long.
