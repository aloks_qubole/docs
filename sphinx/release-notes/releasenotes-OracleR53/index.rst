.. _release-notes-r53-oracle-index:

=============================
Version R53 (Oracle)
=============================
This section of the Release Notes describes new and changed capabilities for Qubole Data Service (QDS) on Oracle Cloud Infrastructure, as of Release Version R53.

.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr53-oracle.rst
    Engines/index.rst
    Infrastructure/index.rst
    Applications/index.rst
    otherQDS-oracle.rst
    hotfixes-postr52-oracle.rst
