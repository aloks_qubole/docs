.. _acmr52-azure:

==================
Cluster Management
==================

* **ACM-2134**: QDS clusters now support B Series, M Series, L Series, E Series, and B v3 series instances.
* **ACM-2195**: QDS clusters now support NCv2 and ND series GPU instances.

Bug Fixes
.........

* **ACM-2060**: The cluster termination reason is now reported correctly as ``Inactivity`` instead of ``No Master found``.
* **ACM-2470**: Cluster start race conditions are fixed.
* **MUL-637**:  Pricing details for Azure instance types are now visible in the UI.
* **SQOOP-151**: ``dbimport`` and ``dbexport`` jobs now succeed, whether the tables are based on a default
  or non-default schema. (Formerly, jobs would fail if the tables were not based on the default schema).


Improvements
............

* **MUL-658**: QDS is supported in the Azure Netherlands region.
* **MUL-688**: The Bastion node does not need public IP SSH access in the security group. Instead, SSG access to the
  loopback address (``localhost``) is required.
