.. _infrastructurer52-azure-index:

=========================
Changes in Infrastructure
=========================


.. toctree::
    :maxdepth: 2
    :titlesonly:

    acmr52.rst
    command-infrar52.rst