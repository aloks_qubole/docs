.. _package-management-r52-azure:

==================
Package Management
==================


New Features
------------
* **ZEP-2232**: Package Management is available as an open beta feature with this release.
* **ZEP-1833**: The Package Management UI supports uninstalling packages.

Enhancements
------------
* **ZEP-1869**: You can install Python and R packages in the `conda-forge <https://conda-forge.org/>`__ channel.
* **ZEP-1997**: QDS supports access control lists (ACL) for Package Management.
* **ZEP-2029**: The QDS **Control Panel** supports configuring account-level ACLs for Package Management Environments.
* **ZEP-2104**: Package Management supports deleting an environment.
