.. _notebooksr52-azure:

=========
Notebooks
=========

Bug Fixes
---------

* **ZEP-1571**: Fixes related to the interpreter’s **Edit** button.


Improvements
------------

* **ZEP-1488**: Dashboards can now be refreshed periodically.
* **ZEP-1552**: Fixes problems such as a malformed ``interpreter.json``, and interpreters or repositories getting lost,
  by bringing the Zeppelin service up faster.
* **ZEP-1875**: Provides ``Example Notebooks`` link from the ``Permalink`` option in the ``Settings`` dropdown.
* **ZEP-1929**: Qubole has improved the notebook's initial load time by pre-caching the notebook resources.
* **ZEP-1974**: Existing Spark interpreters have been made more compact. Properties whose values are not explicitly overridden
  are removed from the interpreters and the values of those properties are set to the cluster defaults.
* **ZEP-2004**: To help debugging the TTransport exception, a hyperlink to the FAQ that contains the solution has
  been added in the paragraph output.
* **ZEP-2224**: Improvements to prevent interpreters from being reset in error.
* **ZEP-2251**: The UI provides better scrolling while a notebook is being loaded.

