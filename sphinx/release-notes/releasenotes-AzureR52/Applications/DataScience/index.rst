.. _data-science-r52-azure-index:

============
Data Science
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    notebooksr52.rst
    package-managementr52.rst