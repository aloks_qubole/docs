.. _engines-r52-azure-index:

==================
Changes in Engines
==================


.. toctree::
    :maxdepth: 1
    :titlesonly:

    hadoopr52.rst
    hiver52.rst
    prestor52.rst
    sparkr52.rst