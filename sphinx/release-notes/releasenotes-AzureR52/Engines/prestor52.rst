.. _presto-azure-r52:

======
Presto
======

New Features
------------

* **PRES-1393**: QDS now supports *Dynamic Filter*, an optimization to improve the performance of JOIN
  queries, and specifically to optimize hash JOINs in Presto; it can make such JOINs significantly faster.
  It is not enabled by default. You can enable the Dynamic Filter:

  - As a session-level property by setting this property -
    ``set session dynamic_filtering = true.``
  - As a Presto override in the Presto cluster by setting ``experimental.dynamic-filtering-enabled=true``. It requires a
    cluster restart for the configuration to be effective.

* **PRES-1696**: GeoSpatial functions from 0.193 have been back-ported into 0.180 in QDS. The
  ``hammingDistance`` string function has also been ported.

* QDS supports Presto 0.193 on Presto clusters |OpenBeta|.

  .. |OpenBeta| image:: ../../rel-images/OpenBetaSelfService2.png

  Improvements include autoscaling; user-defined functions (UDFs); a faster Rubix client; and the Kinesis connector.

Improvements
............

* **PRES-1371**: The Postgres JAR version has been downgraded to 9.3-1102-jdbc41 in 0.180 to maintain
  compatibility with Redshift.

* **PRES-1453**: Support for JOIN reordering based on table statistics has been added. This means that tables are automatically
  joined in the order that provides the best query performance. JOIN reordering is not enabled by default and is supported only
  in Presto 0.180 or later versions. Enable JOIN reordering:

  - As a session-level property by setting ``qubole_reorder_joins = true``.
  - As a Presto override in the Presto cluster by setting ``qubole-reorder-joins=true``.
