.. _otherQDSChangesr52-azure:

====================
Other Changes in QDS
====================

Bug Fixes
---------
* **AD-824**:  Fixes a problem in the unlock flow that caused the wrong page to open when you clicked the unlock link for the second time.
* **MUL-697**: Fixes a problem in the **Explore** section of the QDS UI that resulted in an incomplete listing of folders
  and files for an Azure Container.

Improvements
............

* QDS environments have an idle session timeout of 1 week (10800 minutes). The tooltip that said it was 1440 minutes
  has been corrected.
* **AN-344**: The **Analyze** query editor supports Spark clusters with the ``clouddistcp`` option when you select the
  Hadoop command type.
* **AN-693**: A new icon set shows whether a command has succeeded or failed.
* **EAM-672**: Significantly reduces the time to return results for small queries and small commands
  (especially Presto Commands).
