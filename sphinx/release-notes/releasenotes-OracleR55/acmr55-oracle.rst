.. _acmr55-Oracle:

==================
Cluster Management
==================

Master/Worker Terminology is used in the Cluster UI
---------------------------------------------------
**ACM-4086**: **Master**/**Worker** terminology is now used in the QDS UI instead of **Master**/**Slave**.


Improvements
------------

* **ACM-3890**: You can configure Hive 2.3 (Beta) under the **Configuration** tab of the **Clusters** page in the QDS UI,
  and also via the cluster API.
* **ACM-4174**: QDS now supports
  `Oracle Cloud instance free-form tags <https://docs.cloud.oracle.com/iaas/Content/Identity/Concepts/taggingoverview.htm>`__.
* **ACM-4324**: QDS now supports a round-robin option to help allocate resources evenly in clusters configured to use
  multiple avialability domains. :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`.


Bug Fix
-------
* **ACM-4021**: The maximum size of an Oracle block volume has been increased to 32768 GB.
