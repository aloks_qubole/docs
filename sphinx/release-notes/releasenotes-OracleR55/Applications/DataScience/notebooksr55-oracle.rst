.. _notebooksr55-Oracle:

========================
Notebooks and Dashboards
========================



New Features
------------


Export Dashboards in Different Formats
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**ZEP-3031**: You can export dashboards in the following formats: PNG, PDF, and HTML. Exported dashboards can be used
for cases such as publishing on a web page, or sending as an email attachment.
You can export a dashboard even when the cluster is down.  If a dashboard fails to render within 3 minutes, the
download or email option fails.

The results size limit is not applicable to an exported dashboard. :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`.

Improvements
------------

* **ZEP-2734**: When the cluster is offline, you can click on the **Run** button to start it, and run the paragraphs in
  a notebook. :ref:`Via Support <availability-features>`, :ref:`Disabled <availability-features>`.
* **ZEP-2664**: Notebooks automatically trim the output stored in memory, showing only the first ten and the last ten rows.
  This helps prevent issues such as freezing of notebooks, Zeppelin server crashes, etc.
  :ref:`Via Support <availability-features>`, :ref:`Disabled <availability-features>`.
* **ZEP-2880**: When you update or modify the dashboard-refresh configuration, the label details of the cluster attached
  to the dashboard are also updated if they were modified.

Bug Fixes
---------

* **ZEP-3109**: Implements optimizations to handle hundreds of clusters and hundreds of notebooks, helping to ensure
  that notebooks do not freeze or become unresponsive even at high volumes.


