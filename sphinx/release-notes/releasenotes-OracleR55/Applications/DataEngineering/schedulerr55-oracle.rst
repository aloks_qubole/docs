.. _schedulerr55-Oracle:

=========
Scheduler
=========

Improvements
------------

* **SCHED-311** A QDS system administrator can now change the owner of any scheduled job created through the QDS UI.





