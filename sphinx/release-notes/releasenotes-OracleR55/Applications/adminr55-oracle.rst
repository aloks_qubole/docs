.. _AdminChangesr55-Oracle:

==============
Administration
==============


Bug Fixes
---------

* **AD-1196**: Prevents inviting a user to a group which does not exist in that account. User addition now fails with:
  *Group doesn't exist*. *Enter a valid group name or create a new one.*

* **AD-1039**: Fixes a problem that caused negative or incorrect values to appear under **Nodes** in the **Cluster Usage**
  report.
