.. _release-notes-r55-index-Oracle:

====================
Version R55 (Oracle)
====================

This section of the Release Notes describes new and changed capabilities of the Qubole Data Service (QDS) on
Oracle OCI, as of Release Version R55.


.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr55-oracle.rst
    Engines/index.rst
    acmr55-oracle.rst
    Applications/index.rst
