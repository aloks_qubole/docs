.. _hadoop-r55-Oracle:

========
Hadoop 2
========

Bug Fix
-------

* **HADTWO-1847**: Fixed an issue that prevented upscaling when none of the nodes in the minimum set were healthy.
