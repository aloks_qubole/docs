.. _hive-r55-Oracle:

====
Hive
====


Hive 2.3.4 (Beta)
-----------------

**QHIVE-3438**: Hive 2.3.4 (Beta) is available. When this version runs on the QDS server, it uses Java 8;
it is also compatible with Java 7 when it runs on the Master node or HiveServer 2. :ref:`Beta <availability-features>`

You can configure this version from the **Configuration**
tab of the **Clusters** page in the QDS UI, and via the cluster API. :ref:`Cluster Restart Required <cluster-restart-op>`

For more information, see :ref:`use-hive-versions`.


Multi-line Column Data in Hive Query Results
-----------------------------------------------
**QHIVE-2650**: Query results that had columns with multiple lines of data did not display correctly in the UI.
QDS now supports newline (``\n``) and carriage return (``\r``) characters in Hive query results
by escaping them in the Hive result set and then un-escaping them in the UI. You can enable this new capability by
contacting Qubole Support. :ref:`Via Support <availability-features>`

After this feature has been enabled, even a simple SELECT query requires a cluster start.


Hive Logs are Available in the Analyze UI
------------------------------------------
**QHIVE-3367**: A detailed log for each Hive query executed using HiveServer2 or Hive on Master is uploaded to a
subdirectory in the default location in Cloud object storage within a few minutes of query completion.  Individual log
files are created for each query at ``*/media/ephemeral0/hive_query_logs*`` along with the existing logs. QDS displays the
location of the logs under the **Logs** tab of the **Analyze** page in the QDS UI. :ref:`Via Support <availability-features>`.


Bug Fixes
---------

* **QHIVE-3528**: Fixes a problem that caused a table to be corrupted when the total number of buckets in a partition
  was greater than the expected count. This problem occurred when ``hive.qubole.dynpart.use.prefix`` was enabled for
  the account. Hive will now create empty buckets during an INSERT OVERWRITE operation only if the buckets are needed in
  partitions that are being written to. :ref:`Via Support <availability-features>`.
* **QHIVE-3560**: Fixes a race condition that occurred when multiple commands were trying to create downloaded
  resource directories with the same name.
* **QHIVE-3600**: Fixes a problem that caused a query to fail with a ``FileNotFoundException`` when ``hive.optimize.skewjoin``
  was enabled and ``hive.auto.convert.join`` was disabled.
