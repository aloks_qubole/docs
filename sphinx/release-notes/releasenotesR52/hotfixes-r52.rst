.. _hotfixes-post-r50:

=================================
Hot-fixes Since 23rd January 2018
=================================

Release Version: 50.84.0
------------------------
* **GDPR Readiness**

  As part of GDPR readiness, Qubole has updated its privacy policy. The updated privacy policy now states how Qubole will
  collect, use, disclose, and share personal data that belongs to QDS users.

  As part of the **Sign up** process, when personal information such as name, email ID, company, title, location, and phone
  number is collected from a new user, he will be asked to give consent to Qubole's **Privacy Policy** and **Terms of Service**,
  by selecting the associated checkbox before proceeding further in the **Sign up** process.

  An existing user of an existing account, during a fresh login, will be asked to provide consent to Qubole for processing
  his personal data by selecting the **Privacy Policy** checkbox. This will be required to successfully log into the QDS platform. The
  consent will be collected using a one-time pop-up which will not be shown to users during subsequent sign-ins.

  The same pop-up with the **Privacy Policy** checkbox requesting for consent will be displayed when a user is invited to join an
  existing QDS account.

Release Version: 50.81.0
------------------------

* **AN-874**: The issue where tables were not displayed in the **Analyze** > **Tables** when Hive authorization was enabled, has
  been resolved now.

Release Version: 50.73.0
------------------------

* **INFRA-778**: The issue in which live logs were not displayed for running queries has been resolved.

Release Version: 50.67.0
------------------------

* **INFRA-550**: The issue in which a completed Presto query still showed running as its status has been resolved.
* **INFRA-558**: The issue in which a Spark Command run on the **Analyze** UI failed to fetch column names has been resolved.

Release Version: 50.66.0
------------------------

* **INFRA-323**: The issue in which a Tez Job that was taking a longer time to execute has been resolved.

Release Version: 50.60.0
------------------------

* **EAM-1004**: The issue in which a command/query was getting abruptly terminated has been resolved.

Release Version: 50.45.0
------------------------

* **EAM-911**: The Ruby SAML version has been upgraded to the latest 1.7.2 version.

Release Version: 50.39.0
------------------------

* **AD-655**: Custom EC2 tags will be visible in the cluster usage report.

Release Version: 50.38.0
------------------------

* **EAM-857**: Qubole Scheduler has introduced a new rerun limit per scheduled job and it is applicable to all scheduled
  jobs. Its default value is 20. When the scheduled job reruns exceed the limit, you get this error message:
  ``A maximum of 20 reruns are allowed for scheduled job in your account: #<account_number>``.

  You can increase the number of reruns for the QDS account by creating a ticket with Qubole Support. The new value is
  applicable to all the jobs of a given QDS account.

Release Version: 50.32.0
------------------------

* **SPAR-2271**: The data store configuration file's permissions in a Spark cluster have been changed.

Release Version: 50.29.0
------------------------

* **HADTWO-1341**: If the client running outside master tried to contact any daemon running on the master and if it failed the
  first time, then in later retries, it always fails to contact the master as it is used to do the wrong address resolution.
  This issue has been fixed by not doing the resolution on the client side always.

Release Version: 50.27.0
------------------------
* **RUB-40**: The RubiX checkbox on the Hadoop 2 (Hive) cluster UI has been removed.

Release Version: 50.22.0
------------------------

* **EAM-567**: The issue in which disabling an account was unsuccessful, has been resolved.
* **HADTWO-1302**: The issue in which the ApplicationMaster tried to launch Containers on the downscaled nodes has been
  resolved. The issue had occurred when two cluster instances came up with the same private IP address.

Release Version: 50.14.7
------------------------

* **PRES-1572**: The issue in which Presto queries taking longer time in the **Planning Stage** on a Presto 0.180 cluster
  and not getting submitted to the cluster has been resolved.
* **PRES-1573**: The errors in fetching AWS S3 credentials have been resolved.

Release Version: 50.12.0
------------------------

* **AN-755**: Fixed an issue where users assigned to public hive role could not see tables in **Explore** and **Analyze** UI.