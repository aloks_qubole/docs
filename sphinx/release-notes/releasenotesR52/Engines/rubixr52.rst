.. _rubixr52:

=====
RubiX
=====

Bug Fixes
---------
* **RUB-28**: Qubole has added support for variants of the S3 cloud file system in RubiX.