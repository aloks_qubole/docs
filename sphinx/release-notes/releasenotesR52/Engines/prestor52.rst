.. _presto-r52:

======
Presto
======
:ref:`presto-new` provides more description on the new features/enhancements.

New Features
------------

* **PRES-1393**: Qubole now supports the Dynamic Filter feature. It is a join optimization to improve performance of JOIN
  queries. It has been introduced to optimize Hash JOINs in Presto which can lead to significant speedup in relevant cases.
  It is not enabled by default and it is supported only in Presto 0.180 or later versions. Enable the Dynamic Filter feature:

  - As a session-level property by setting this property -
    ``set session dynamic_filtering = true.``
  - As a Presto override in the Presto cluster by setting ``experimental.dynamic-filtering-enabled=true``.

* **PRES-1453**: The JOIN REORDER support based on Table statistics has been added. It enables capability to pick optimal
  order for joining tables. It is not enabled by default and it is supported only in Presto 0.180 or later versions.
  Enable the JOIN Reordering:

  - As a session-level property by setting ``qubole_reorder_joins = true``.
  - As a Presto override in the Presto cluster by setting ``qubole-reorder-joins=true``.

* **PRES-1589**: Qubole Presto has added a new Hive connector configuration property, ``hive.skip-corrupt-records`` to skip
  corrupt records in input formats other than ORC, Parquet and RCfile. This configuration can also be set as session
  property as ``hive.skip_corrupt_records=true``. It is supported only in Presto 0.180 or later versions.
* **PRES-1596**: QDS supports Presto 0.193 on Presto clusters |OpenBeta|.

  .. |OpenBeta| image:: ../../rel-images/OpenBetaSelfService2.png

* **PRES-1696**: GeoSpatial functions from 0.193 have been back ported into 0.180 in QDS. Apart from GeoSpatial functions,
  the ``hammingDistance`` string function has also been ported.

Bug Fixes
---------

* **PRES-1315**: The issue in which a user was unable to execute commands on a Presto 0.180 cluster using notebooks has been
  resolved.
* **PRES-1374**: Qubole has added cron jobs to ensure Presto restarts seamlessly after a crash.
* **PRES-1456**: Enabled splitting of output file in the ``CREATE TABLE AS SELECT`` query to prevent creating one big file per
  worker node.

Enhancements
------------

* **PRES-1093**: All Presto connectors are now available in the Presto 0.180 version.
* **PRES-1369**: Qubole Presto can roll back ``INSERT INTO DIRECTORY and INSERT OVERWRITE DIRECTORY`` partial results in case of
  query failures or query cancellation.
* **PRES-1495**: To authenticate direct connections to the cluster Master, basic file-based authentication has been added.
* **PRES-1590**: Qubole has deprecated Presto version 0.142. It will not be available when you create a new clusters or
  switch versions, but existing clusters will continue to work until the configuration is changed.
* **PRES-1599**: Presto 0.180 is the default version of Presto as of this release.
