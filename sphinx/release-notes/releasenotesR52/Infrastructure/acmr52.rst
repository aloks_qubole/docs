.. _acmr52:

==================
Cluster Management
==================
:ref:`acm-new` provides more description on the new features/enhancements.

New Features
------------

* **ACM-2071**, **ACM-2192**, and **ACM-2744**: QDS clusters support C5, H1, and M5 AWS instance types.
* **AD-540**: The **Account SSH Key** field displays the public key of the SSH key pair to use while logging into Bastion
  node for a private subnet.

Bug Fixes
---------

* **ACM-1912**: The issue in which query messages that displayed AMI override error has been resolved.
* **ACM-1945**: Improved cluster start time when the node bootstrap file is not present.
* **ACM-2128**: The cluster startup error in a Hadoop 2 (Hive) cluster has been resolved.
* **ACM-2362** and **ACM-2527**: The cluster configuration UI now shows an error if it is unable to find a VPC in an account.
* **ACM-2569**: The issue where changing node types in a heterogeneous cluster resulted in the clusters page not displaying
  any data has been resolved.

Enhancements
------------

* **Java Upgrade**: As part of a security initiative to upgrade to secure versions of Java, the following changes have
  been made in the cluster AMIs:

   * (Oracle) JDK-8 is upgraded to *1.8.0.161* by default on the cluster AMI.
   * Users now have the option to switch to OpenJDK-7 (version *1.7.0.161*) instead of Oracle JDK-7. Create a ticket with
     `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__ if you want to switch to OpenJDK-7.
   * Java-6 has been removed from cluster AMIs.

* **ACM-2095**: QDS supports soft enforcement of cluster permissions at the object level. On the **Manage Permissions**
  dialog of a specific cluster, when you select one permission, then additional cluster permissions are automatically selected.
  You can still disable those additional permissions in the UI before saving.

  Qubole highly recommends a user to accept the enforced permissions.
* **ACM-2177**: The Spot node acquisition has become faster by up to 10 seconds now.
* **ACM-2234**: Qubole has moved to HVM image for better reliability and performance.

  If you bake custom AMIs,  you can bake the PV AMI type for sometime now but going forward, just baking HVM AMI should
  be sufficient. Qubole’s API would continue to accept PV image for backward compatibility reasons but Qubole would not
  use PV AMIs going forward.
* **ACM-2291**: The default threshold for EBS-based storage upscaling has been **changed to 25% from 15%**. However, existing
  configurations will not be affected.

  In addition, for progress-rate based upscaling, QDS adds a disk if the storage is estimated to get full in **10 minutes**
  instead of the previous 2 minutes.
* **ACM-2420**: Qubole has made multiple optimizations to reduce number of API calls which helps alleviate the API rate-limiting
  problem.
* **ACM-2665**: Qubole has made optimizations to reduce the number of AWS API calls to start/upscale a cluster.
