.. _otherQDSChangesr52:

====================
Other Changes in QDS
====================

Bug Fixes
---------
* **AD-824**: Fixed issues in the unlock flow of the Signin page where the user clicks the unlock link for the second time
  and it opens a wrong page.