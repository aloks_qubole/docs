.. _data-science-r52-index:

============
Data Science
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    notebooksr52.rst
    deep-learningr52.rst
    package-managementr52.rst