.. _package-management-r52:

==================
Package Management
==================
:ref:`PM-new` provides more description on the new features/enhancements.

New Features
------------
* **ZEP-2232**: Package Management is available as an open beta feature.
* **ZEP-1833**: The Package Management UI supports uninstalling packages.

Enhancements
------------
* **ZEP-1869**: A newly created Package Management environment supports conda-forge channel which supports more packages.
* **ZEP-1997**: QDS supports access control lists (ACL) for using the Package Management feature.
* **ZEP-2029**: QDS supports account-level ACL for Package Management Environments in the **Control Panel**.
* **ZEP-2104**: Package Management supports deleting an environment.
