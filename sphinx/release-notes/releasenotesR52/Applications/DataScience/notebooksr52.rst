.. _notebooksr52:

=========
Notebooks
=========
:ref:`note-new` provides more description on the new features/enhancements.

New Features
------------

* **ZEP-1878**: Qubole has improved usability of a markdown paragraph:

  - The editor auto hides and auto runs (whenever a user makes changes) after the markdown paragraph is out of focus.
  - Double clicking a markdown paragraph displays the editor.

* **ZEP-1897**: There is an option at a notebook level to show/hide line numbers for all paragraphs.

Bug Fixes
---------

* **ZEP-1360**: Restart functionality has been added for Spark (%spark) interpreters.
* **ZEP-1393**: The issue in which paragraphs of a Spark notebook disappeared when the associated cluster was switched
  has been resolved.
* **ZEP-1571**: Enhancements related to the interpreter’s **edit** button.
* **ZEP-1673**: The run time of the paragraph has been changed to show only the actual execution time of the paragraph
  and does not include the time spent in pending/ waiting state.
* **ZEP-1846**: The issue in which **Interpreter Settings** that were getting cleared out has been resolved.
* **ZEP-1887**: The issue in which the Scheduler was not displaying notebooks for the Spark command type has been
  resolved now.
* **ZEP-1988**: The issue when a notebook was exported as a PDF resulted in broken charts has been resolved now.
* **ZEP-2010**: While creating a new interpreter, the drop-down list now only lists interpreter types that is only specific
  to the cluster type.
* **ZEP-2052**: Fixed the issue where new detailed job progress in notebook was crashing in the case when number of
  stages in a job exceeded the ``spark.ui.retainedStages`` limit.
* **ZEP-2111**: The issue in which the cluster-start button in a notebook was disabled for a user when the same user
  had cluster’s object-level start permission but did not have the user-level start permission, has been resolved.

Enhancements
------------
* **HIVE-3024**: Hive and Presto Notebooks are displayed with a **Beta** tag in the **Create New Notebook** dialog.
* **ZEP-1055**: QDS will gradually disable Internal Notebook Scheduler present in Notebooks post R52. After that, users
  should use Qubole's Scheduler to schedule notebooks.
* **ZEP-1255**: Spark applications run by zeppelin are now named based on the name of the spark interpreter used.
* **ZEP-1294**: The issue where the buttons of the **Create New Notebook** dialog were disabled, has been resolved.
* **ZEP-1552**: This fixes issues such as interpreter.json getting malformed and interpreters/repositories getting lost
  by making the Zeppelin service come up early. Thus the notebooks are asynchronously loaded.
* **ZEP-1650**: QDS has introduced a new feature to support displaying head of a dataframe as a table using the z.show()
  functionality in notebooks.
* **ZEP-1683**: QDS automatically hides the paragraphs in the Dashboard if the output is empty or user has chosen to hide
  the output.
* **ZEP-1875**: The permalink option is available in **Example Notebook**'s settings drop-down list as it is available in other
  notebooks.
* **ZEP-1893**: Qubole will pre-cache the left navigation content in Notebook and Dashboards so that a user does not have
  to wait for the data to be loaded.
* **ZEP-1922**: A cluster will be selected by default during notebook creation based on notebook type from available
  clusters. The active cluster will be given priority during selection.
* **ZEP-1927**: In the Zeppelin's Interpreters page, a **Log** link is added for each Spark interpreter. If the interpreter
  is not started as part of the current cluster instance, it redirects to the logs folder.
* **ZEP-1929**: Qubole has improved the notebook's initial load time by pre-caching the notebook resources.
* **ZEP-1974**: Existing Spark interpreters have been made compact. Properties whose values are not explicitly overridden
  will be removed from the interpreters. These properties would be picked from the cluster defaults.
* **ZEP-1999**: A new field called Default Language is added in Spark notebooks. Using this field, now a user has the
  flexibility to choose the default supported language for Spark interpreter while creating the notebook. This default
  language will be persisted when the notebook is detached from one cluster to attached to another cluster, and it is also
  persisted when this notebook is imported or exported.
* **ZEP-2004**: For ease of debugging the TTransport exception, a hyperlink to the FAQ that contains the solution has
  been added in the paragraph output.
* **ZEP-2012**: In case of exceptions such as executor memory being more than the threshold, error in connection to the
  Hive metastore and so on, a general NullPointerException (NPE) used to be displayed. Instead of NPE, specific error messages
  will be displayed now in the paragraph output.
* **ZEP-2018** and **ZEP-2224**: QDS has done some enhancements to avoid interpreters from being reset.
* **ZEP-2049**: The default location for a new notebook dashboard is changed to the **Home** folder.
* **ZEP-2077**: Multiple code editor instability related fixes are done.