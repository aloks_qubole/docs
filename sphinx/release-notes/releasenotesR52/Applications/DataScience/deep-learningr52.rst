.. _deep-learningr52:

=============
Deep Learning
=============

Bug Fixes
---------

* **DS-168**: Deep Learning clusters support all instance types of g2, p2, g3, and p3 instance families.
* **DS-179**: The issue in which TensorBoard was not registering on a cluster has been resolved.

Enhancements
------------

* **DS-175**: QDS has upgraded the TensorFlow’s version to 1.7. Qubole has installed the ``nvidia driver version 384.125`` on all
  its nodes along with ``cuda (version - 9.0)`` and ``cudnn (version 7.0)``.
