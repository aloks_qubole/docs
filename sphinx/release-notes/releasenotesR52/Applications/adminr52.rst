.. _AdminChangesr52:

==============
Administration
==============

New Features
------------

* **EAM-715**: A new user group named **everyone** will be created for each account with no roles associated to it.
  The group will be mapped to all the users of that account, thus any role (or policies) associated with this group will
  be applied to all the users for that account. Modifying users for this group is not allowed but one can add/delete roles
  from this group.

Bug Fixes
---------

* **AD-630**: The issue in which the API token was not visible to non-system users and non-system admins has been resolved now.
* **AD-884**: CPU counters missing from the **Reports** page have been added.

Enhancements
------------

* **AD-333**: On the **Control Panel** > **Account Settings**, there is a new field ``datadog_alert_email_list`` added for
  receiving Datadog alerts. This parameter is used to update the default Datadog email that would receive alerts. The alert email
  is applicable only to the new clusters. In case of existing clusters, if you want to change the email, you can change
  it on the Datadog UI.
* **AD-538**: A user can see the ID of cluster which was used to execute command in the **All Commands** report. This is
  applicable for all the command types except db_export, db_import, and db_tap_query.
* **AD-784**: QDS environments will have an idle session timeout of 1 week (10800 minutes). The tool tip that incorrectly
  said it to be 1440 minutes has been corrected now.
* **EAM-425**: Workload Scaling Limits has been added to the list of available cloud agents.
* **EAM-690**: System admins of a Qubole account will receive an email notification, whenever there is any change in the
  **Allow Qubole access** setting.