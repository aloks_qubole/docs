.. _analyze-explorer52:

==============
Data Analytics
==============

Bug Fixes
---------

* **AN-217**: The **Run Selected** option is removed for **Workflow** commands. The complete text gets saved/run even when
  partial text is selected for **Workflow** commands.
* **AN-354**: Qubole has made enhancements in data caching for QDS accounts that have Hive Authorization enabled.
* **AN-311**: On the **Analyze** UI, the cluster selection drop-down is not available for the **Refresh Table** command type in
  the edit mode now. It was visible earlier and the issue has been resolved.
* **AN-487**: In the **Explore** UI, the issue in which the custom path displayed the upload option instead of the download
  option has been resolved.
* **AN-523**: Tags are saved properly when the Create Command Templates API is used.
* **AN-674**: The ``add jar`` statements are formatted correctly when the **Format** button in the SQL composer is clicked.
* **AN-685**: The Notification in the Preview tab of the Analyze UI appears only when data is available.
* **AN-756**: The issue in which the upload button in the **Explore** UI is not displayed when browser window is resized has
  been resolved.
* **EAM-683**: The issue where a *failure* during the data import was incorrectly treated as a *success* has been resolved.

Enhancements
------------

* **AN-344**: Spark clusters are supported when the **s3distcp** option for a **Hadoop** job is selected on the **Analyze** UI.
* **AN-486**: Clicking **All select ...** from the column dropdown in the **Results** pane selects all columns and when
  it is clicked again, all columns gets unselected.
* **AN-693**: A new icon set for command status visually conveys whether the command has succeeded or failed by using tick
  or cross icons as appropriate.
* **EAM-672**: Optimisations on time taken for showing results for small commands (specially Presto commands). The time
  taken for returning results is significantly reduced for small queries.
