.. _airflowr52:

=======
Airflow
=======

New Features
------------

* **AIR-95**: Logs for Airflow ``$AIRFLOW_HOME/logs`` are moved to ``/media/ephemeral`` with this release.
* **AIR-98**: A new **Delete** button is available on the Airflow UI to delete a DAG and the CLI support
  for deleting a DAG has been removed. After a DAG is deleted on the UI, it will take around 5 minutes for the DAG to
  completely disappear from the UI. The value to get the deleted DAG disappear from the UI is configurable using the
  ``scheduler.dag_dir_list_interval`` setting in the ``airflow.cfg`` file. It does not work on sub-DAG operators.
* **AIR-99**: QDS provides a feature to enable a notification email that notifies the user if the Airflow cluster had
  some active DAG runs in the past and has been idle for over a week. If there are no active runs in the DB, a notification
  is sent daily.

Bug Fixes
---------

* **AIR-101**: To receive email alerts related to Airflow process, enable ``_alert_via_email_`` in the ``airflow.cfg`` file.
  After it is enabled, a notification email is sent when the Airflow’s webserver, scheduler, rabbitmq, or worker process
  stops responding.

Enhancements
------------

* **AIR-69**: This distorted UI (Missing toggle button and DAG run info), when the number of DAGs in a
  user's account are more than 25 is fixed.