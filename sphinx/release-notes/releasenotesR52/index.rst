.. _release-notes-r52-index:

=================
Version R52 (AWS)
=================
Qubole provides Qubole Data Service (QDS) on Amazon Web Service (AWS), Microsoft Azure, and Oracle Cloud Infrastructure (OCI).

(:ref:`hotfixes-post-r50` provides the list of hotfixes done post version R50.)

Availability of Features/Enhancements
-------------------------------------
The availability of new features/enhancements is categorized into:

* General Availability |GA|

  .. |GA| image:: ../rel-images/GA.png

* Open-beta access. It is further subdivided into:

  - Available on the QDS UI and/or API - open-beta |OpenBeta|.

    .. |OpenBeta| image:: ../rel-images/OpenBetaSelfService2.png

  - Available via Qubole Support - open-beta, via Support (Create a ticket with `Qubole Support <https://qubole.zendesk.com/hc/en-us>`__
    to enable the features in your QDS account |BetaImage|.)

    .. |BetaImage| image:: ../rel-images/OpenBetaSupport.png

For details on what has changed in this version, see:

.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr52.rst
    Engines/index.rst
    Infrastructure/index.rst
    Applications/index.rst
    otherQDS.rst
    enhancementspostr52.rst
    hotfixes-postr52.rst
    hotfixes-r52.rst
