.. _presto-r55:

======
Presto
======

Presto Strict Mode for Running Expensive Queries
------------------------------------------------
**PRES-339**: The Presto Strict Mode is added in Presto which prohibits:

* Cross join
* Order by without limit
* Querying partitioned table without predicates

You can enable the Presto Strict Mode at a cluster level by setting ``qubole-strict-mode-enabled = true`` in
``config.properties`` through the cluster's Presto overrides. :ref:`Cluster Restart Required <cluster-restart-op>`

The Presto Strict Mode is only supported in Presto 0.193 and later versions. :ref:`Beta, Disabled <availability-features>`

.. _ranger-presto:

Apache Ranger Integration with Presto
-------------------------------------
**PRES-1985**: The Apache Ranger integration with Presto is added for managing granular access control.
Ranger features such as Hive authorization, Row filtering, and Data masking are supported. :ref:`Beta, Disabled <availability-features>`

Enhancements
------------

* **PRES-2473**: The default value of ``hive.information-schema-presto-view-only`` is now set to ``true``. With this change,
  Presto will not try to read Hive views by default. To enable reading Hive views in Presto, add
  ``hive.information-schema-presto-view-only=false`` to Hive catalog properties. :ref:`Cluster Restart Required <cluster-restart-op>`

Bug Fixes
---------
* **PRES-525**: Fixed a bug where multiple output files were created even when the output is sorted. With this change,
  a single output file is created.
* **PRES-1968**: Fixed the issue in Presto queries which failed with the nesting of 101 is too deep error.
* **PRES-2202**: Fixed a problem that caused Datadog queries to fail during downscaling or loss of
  Spot nodes. System and JMX metrics should now report correctly.
* **PRES-2207**: Fix to improve planning time when querying against a table with large number of partitions.
* **PRES-2225**: Fixed the bug where intermittent NaNs were displayed in the Presto UI.
* **PRES-2244**: The error in Presto 0.193 that occurred while reading the Avro schema from S3 is resolved.
* **PRES-2377**: Fixed the bug where performing the Update and Push operation on a Presto cluster broke autoscaling after
  a modified configuration is pushed to a running cluster.
* **PRES-2383**: The fix resolves the issue where Presto queries that had datatypes with spaces as a member of ROW type
  were unsuccessful.
* **PRES-2390**: As part of the fix, the predicate pushdown support is added for the DATE type in Parquet reader, where
  DATEs are encoded with INT32 values to Presto 0.193 and 0.208 versions. This change also fixes an issue where filtering
  against DATE columns may result in an exception due to type mismatch in Presto 0.208.

For a list of bug fixes between versions R54 and R55, see :ref:`apiqubole`.
