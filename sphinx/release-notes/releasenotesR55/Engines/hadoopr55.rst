.. _hadoop-r55:

========
Hadoop 2
========

Bug Fixes
---------

* **HADTWO-1803**: Fixed the issue where YARN application logs are not accessible after the cluster is terminated when
  SSE-KMS is used.
* **HADTWO-1847**: Fixed the issue where upscaling did not happen when all minimum nodes went to an unhealthy state.