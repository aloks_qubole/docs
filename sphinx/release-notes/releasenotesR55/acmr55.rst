.. _acmr55:

==================
Cluster Management
==================

Master/Worker Terminology is used on the Cluster UI
---------------------------------------------------
**ACM-4086**: The Master/Worker terminology naming convention is now used in the QDS UI instead of the Master/Slave
terminology that was used before.

Qubole Supports New Instances
-----------------------------
* **ACM-3436**: QDS now supports ``c5n``, ``m5a``, and ``r5a`` instances.

Enhancements
------------
* **ACM-3545**: Qubole now provides a feature to avoid the cluster from starting whenever the master node's Elastic IP
  address fails. :ref:`Via Support <availability-features>`
* **ACM-3714**: While creating a **Hadoop 2 (Hive)** cluster, you can configure the Pig version through the **Clusters**
  UI while creating a cluster and you can also configure through the cluster API (:ref:`Cluster Restart Required <cluster-restart-op>`). :ref:`Via Support <availability-features>`
* **ACM-3848**: For clusters in a VPC with multiple subnets, Qubole chooses a subnet that has sufficient IPs to launch
  the master node and the minimum number of nodes.
* **ACM-3833**: Qubole now supports saving the node bootstrap on SSE-KMS encrypted buckets.
* **ACM-3890**: You can configure 2.3 Hive (beta) version in the **Configuration** tab of the **Clusters** UI while creating a cluster.
  You can also configure 2.3 Hive (beta) through the cluster API.
* **ACM-3891**: As part of a specific cluster's governance, the master node is not counted in the node time chart, that is
  node chart only shows worker nodes.
* **ACM-4157**: The **Maximum Price Percentage** option has replaced the **Maximum Bid Price** option on the **Clusters** UI.
* **TOOLS-978**: In the AMIs, ``nscd`` is installed for DNS caching and the default value of TTL is changed to 30 seconds
  from 1 hour for host entries in their configuration.

Bug Fixes
---------
* **ACM-4062**: The size of log files of the httpd web server (used by Ganglia) was growing larger, which may cause
  disk space issues in long-running clusters. To avoid such disk-space issues, the httpd web server logs are rotated.

For a list of bug fixes between versions R54 and R55, see :ref:`apiqubole`.