.. _release-notes-r55-index:

=================
Version R55 (AWS)
=================

Qubole provides Qubole Data Service (QDS) on Amazon Web Service (AWS), Microsoft Azure, and Oracle Cloud Infrastructure (OCI).

For details on what has changed in this version, see:

.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr55.rst
    Engines/index.rst
    acmr55.rst
    Applications/index.rst
