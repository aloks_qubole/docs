.. _notebooksr55:

========================
Notebooks and Dashboards
========================

In this release, Qubole provides various new features and enhancements for Notebooks and Dashboards.

New Features
------------

.. _diff-formats-dashboard:

Export Dashboards in Different Formats
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**ZEP-3031**: You can export dashboards in the following formats: PNG, PDF, and HTML. Exported dashboards can be used for various use cases, such as publishing them on a web page, and sending as an email attachment to business users or colleagues. You can also download dashboards by using the command API (``/api/v1.2/commands`` with ``notebookConvertCommand`` command type). You can export the dashboard even when the cluster is down.  If a dashboard fails to render within 3 minutes, then the download or email option fails.
The results size limit is not applicable on the exported dashboard. :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`.

Enhancements
------------

* **ZEP-2734**: When the cluster is offline, you can click on the Run button to start the cluster, and run the paragraphs in the notebook. :ref:`Via Support <availability-features>`, :ref:`Disabled <availability-features>`.
* **ZEP-2664**: Notebooks automatically trim the output stored in memory, showing only the first 10 and the last 10 rows. As a result, the occurrence of UI issues, such as freezing of notebooks, zeppelin server crash, etc. are minimized. :ref:`Via Support <availability-features>`, :ref:`Disabled <availability-features>`.
* **ZEP-2880**: When you update or modify the dashboard refresh configuration, the label details of the cluster that is attached to the dashboard are also updated if they were modified.

Bug Fixes
---------

* **ZEP-3047**: The **Interpreters** page was not displayed intermittently. Now, you can view the **Interpreters** page without any delay.
* **ZEP-3109**: Various components of the **Notebooks** page are optimized to handle 100s of clusters and 1000s of notebooks. As a result, notebooks do not
  freeze or become unresponsive with large number of clusters and notebooks.

For a list of bug fixes between versions R54 and R55, see :ref:`apiqubole`.


