.. _AdminChangesr55:

==============
Administration
==============


Enhancements
------------

* **AD-1817**: Accounts are now sorted alphabetically in the **Account** drop-down list (upper-right corner of the screen).
  This makes it easier for users to locate their accounts when the list of accounts is large.

* **AD-1704**: QDS now displays cluster tags for deleted clusters when you select **Cluster Usage** from the **Report Type** drop-down
  list (under the **Generate Report** section) on the **Usage** page.

* **AD-1694**: You can now view long cluster labels (which were previously truncated) when you select **Cluster Usage** from
  the **Report Type** drop-down list (under the **Generate Report** section) on the **Usage** page.

* **AD-334**: The **Account Settings** tab now displays concurrent command limits at the account and user levels. The **Account Level Concurrent**
  **Command Limit** provides the maximum number of concurrent commands that you can run for an account. The **User Level Concurrent Command Limit** ( :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>` )
  provides the maximum number of concurrent commands that can be run at the user-level for the account. :ref:`Learn more <acc-settings>`

* **AD-243**: QDS has introduced a new public API which you can use to reset your own authentication token. Account
  administrators can also use this to reset the authentication token of **all** other users in the account (including system-administrators). :ref:`Learn more <reset-token-api>`

* **AD-1834**: The list of email IDs configured in the **Email List for Account Updates** field (under **Notifications**) of the
  **Account Settings** page will now receive notifications related to releases, account configuration, and feature changes.

Bug Fixes
---------

* **AD-1196**: While inviting a user to a group which does not exist in that account, user addition fails, and the following error
  message is displayed:
  *Group doesn't exist*. *Enter a valid group name or create a new one.*

* **AD-1607**: Creating an account with an incorrect/invalid AWS region does not fall back to ``us-east-1`` by default.
  An error message is displayed, and account creation fails.

* **AD-1538**: Earlier, when an account was already set up with an IAM Role and the Role ARN was updated, it failed
  irrespective of whether the **Push Compute Settings to All Clusters** checkbox was selected. The application has been enhanced
  such that Role ARN updation is allowed only if the **Push Compute Settings to All Clusters** checkbox is cleared.

* **AD-1039**: On generating the **Cluster Usage** report, negative or incorrect values do not appear under **Nodes** now.

* **AD-1801**: Earlier, API commands to retrieve the **Cluster Usage** report where the end date was a future date resulted in
  displaying data from the last 30 days. Currently, the API displays usage from the start date to the end date. (Effectively,
  from the start date to the current date as data from the current to future date will be empty).

For a list of bug fixes between versions R54 and R55, see :ref:`apiqubole`.