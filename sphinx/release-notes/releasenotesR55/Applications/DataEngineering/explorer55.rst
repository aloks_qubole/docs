.. _explorer55:

=======
Explore
=======

Enhancements
------------

* **EAM-1550**: Qubole supports configuring the thrift socket connection timeout according to the required value
  based on the schema table count. It can only be configured through the Qubole Support team. :ref:`Via Support <availability-features>`

* **EAM-1469**: Qubole has deprecated Hadoop 1 and you cannot use it to export or import data (``dbexport``/``dbimport``).
  Set ``use_customer_cluster`` option to ``true`` for running the query on a specific Hadoop 2 cluster and choose a
  Hadoop 2 cluster to run these commands.

* **EAM-876** ``retry_delay`` is added as a parameter to  denote time interval between the retries when a job fails.


Bug Fixes
---------
For a list of bug fixes between versions R54 and R55, see :ref:`apiqubole`.