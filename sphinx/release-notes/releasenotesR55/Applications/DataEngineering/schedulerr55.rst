.. _schedulerr55:

=========
Scheduler
=========

Enhancements
------------

* **SCHED-311** The **System Admin** can now change the owner of any scheduled job created through the QDS UI.

Bug Fixes
---------
For a list of bug fixes between versions R54 and R55, see :ref:`apiqubole`.




