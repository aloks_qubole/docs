.. _sqoopr55:

=====
Sqoop
=====

New Features
------------

* **SQOOP-228 & SQOOP-216**: The Sqoop version has been upgraded to 1.4.7 (:ref:`Cluster Restart Required <cluster-restart-op>`). :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`
