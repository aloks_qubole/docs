.. _whats-new-r55:

===========
What is New
===========
The new features and enhancements are listed in the corresponding tabs below.

.. note:: The label (in blue) that is against the description indicates the launch state, availability, and the default
          state of the feature/enhancement. For more information, click the label.

          Unless stated otherwise, features are generally available, available as self-service and enabled by default.

.. tabs::

   .. tab:: Administration

      * QDS has introduced a new public API which you can use to reset your own authentication token. Account administrators can use
        this to reset the authentication token of **all** other users in the account (including system-administrators).

      * Accounts are now sorted alphabetically in the **Account** drop-down list.

      * The **Account Settings** tab now displays concurrent command limits at the account and user levels.

      * QDS now displays cluster tags for deleted clusters.

      :ref:`Learn more <AdminChangesr55>`.

   .. tab:: Cluster Management

      * The Master/Worker terminology naming convention is now used in the QDS UI instead of the Master/Slave
        terminology that was used before.
      * QDS now supports ``c5n``, ``m5a``, and ``r5a`` instances. :ref:`Cluster Restart Required <cluster-restart-op>`

      :ref:`Learn more about the complete list of changes <acmr55>`.

   .. tab:: Data Engineering

      * To monitor the running DAGs on Airflow, Prometheus is integrated for real-time monitoring and dashboard using Grafana (:ref:`Cluster Restart Required <cluster-restart-op>`). :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`

        :ref:`Learn more <airflowr55>`

      * You can now monitor the metrics with your own setup of Prometheus.
      * Any changes to the DAG Explorer will now immediately reflect on the Airflow cluster after the changes are saved. :ref:`Cluster Restart Required <cluster-restart-op>`
      * Qubole Airflow clusters now support the Airflow REST API.
      * Apache Airflow v1.10.0 is now supported on QDS. While creating an Airflow cluster, you can run the new Airflow
        version using the **Airflow Version** drop-down on the cluster UI. :ref:`Learn more <airflowr55>`
      * The Sqoop version has been upgraded to 1.4.7 (:ref:`Cluster Restart Required <cluster-restart-op>`). :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`

      :ref:`Learn more about the complete list of changes <data-engineering-r55-index>`.

   .. tab:: Hive

      * Hive 2.3.4 (beta) is available. You can configure it while creating a Hadoop 2 (Hive) cluster (:ref:`Cluster Restart Required <cluster-restart-op>`). :ref:`Beta <availability-features>`

        :ref:`Learn more <hive-2.3-rn>`.

      * Hadoop 2 (Hive) clusters now support Pig 0.17 version (:ref:`Cluster Restart Required <cluster-restart-op>`).  :ref:`Beta <availability-features>`

        :ref:`Learn more <pig-0.17-h2-cluster-rn>`.

      * Multi-column data from a Hive query result is displayed in the **Results** tab of the **Analyze** UI page. :ref:`Via Support <availability-features>`

        :ref:`Learn more <hive-logs-rn>`.

      * A detailed log for a specific Hive query that is executed using HiveServer2 or Hive-on-master is now available.  :ref:`Via Support <availability-features>`

        :ref:`Learn more <multi-column-rn>`.

      :ref:`Learn more about the complete list of changes <hive-enh-r55>`.

   .. tab:: Notebooks and Dashboards

      * Dashboards can be exported as PNG, PDF, and HTML files.  :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`.

        :ref:`Learn more <diff-formats-dashboard>`.

   .. tab:: Presto

      * Qubole has added a Presto Strict Mode for running expensive Presto queries (:ref:`Cluster Restart Required <cluster-restart-op>`). :ref:`Beta, Disabled <availability-features>`


        :ref:`Learn more <presto-r55>`.

      * Qubole has added the Apache Ranger integration with Presto for managing granular access control.  :ref:`Beta, Disabled <availability-features>`

        :ref:`Learn more <ranger-presto>`.


      :ref:`Learn more about the complete list of changes <ranger-presto>`.

   .. tab:: Spark

      * Amazon S3 Select is now integrated with Spark on Qubole, and works for CSV and JSON data sources and tables. This integration improves TPCDS query performance. :ref:`Via Support <availability-features>`.

        :ref:`Learn more <s3_select>`.

      * Python UDF pushdown is optimized to improve join performance by pushing down UDF when the joined output is larger than individual tables. :ref:`Learn more <udf-pushdown>`.



       .. .. * Direct distributed writes to S3 from executors provides improved SQL support. :ref:`Learn more <dist-write>`.



      * Multitenant Qubole Job History Server that serves log and history of Spark jobs run on terminated cluster is now upgraded to Spark 2.3. :ref:`Learn more <shs>`.




      * Spark on Qubole now supports Hive Admin commands to allow users to grant privileges such as (SELECT, UPDATE, INSERT and DELETE.) to other users or roles. :ref:`Learn more <hive-auth>`.

