.. _release-notes-r55-index-azure:

===================
Version R55 (Azure)
===================

This section of the Release Notes describes new and changed capabilities of the Qubole Data Service (QDS) on Microsoft
Azure, as of Release Version R55.


.. toctree::
    :maxdepth: 2
    :titlesonly:

    whats-newr55-azure.rst
    Engines/index.rst
    acmr55-azure.rst
    Applications/index.rst
