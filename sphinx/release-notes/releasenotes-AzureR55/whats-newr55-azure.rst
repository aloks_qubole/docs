.. _whats-new-r55-Azure:

==========
What's New
==========

Important new features and improvements are as follows.

.. note:: Blue text next to a description in these Release Notes indicates the launch state, availability, and default
          state of the item. For more information, click the label. Unless otherwise stated, features are generally available,
          available as self-service (without intervention by Qubole support), and enabled by default.



Azure-Specific Improvements
---------------------------

* Presto FastPath optimizations reduce QDS driver command latency.

* RubiX caching on Spark clusters improves query performance. :ref:`Via Support <availability-features>`,
  :ref:`Disabled  <availability-features>`

* QDS Azure now supports Airflow :ref:`package management <package-management>`.

Other Important Improvements
----------------------------

Other important QDS improvements appear under the tabs below:

.. tabs::


   .. tab:: Cluster Management

      * **Master**/**Worker** terminology is now used in the QDS UI instead of **Master**/**Slave**.

      :ref:`See all the changes <acmr55-azure>`.

   .. tab:: Data Engineering

      * You can use your own Prometheus setup for metrics.
      * QDS now supports Apache Airflow v1.10.0. You can choose it from the **Airflow Version** drop-down list
        on the **Clusters** page of the QDS UI. :ref:`Learn more <airflowr55-azure>`
      * Sqoop has been upgraded to 1.4.7 (:ref:`Cluster Restart Required <cluster-restart-op>`).
        :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`

      :ref:`See all the changes <data-engineering-r55-azure-index>`.

   .. tab:: Hive

      * Hive 2.3.4 (Beta) is available. You can choose it when you configure a Hadoop 2 (Hive) cluster.
        (:ref:`Cluster Restart Required <cluster-restart-op>`). :ref:`Beta <availability-features>`.

      * Multi-column data from a Hive query result is displayed in the **Results** tab of the **Analyze** UI page.
        :ref:`Via Support <availability-features>`.

      * A detailed log is now available for each Hive query that is executed using HiveServer2 or Hive on Master.
        :ref:`Via Support <availability-features>`.

      :ref:`See all the changes <hive-r55-azure>`.

   .. tab:: Presto

      * QDS now supports Presto **Strict Mode** to help guard against unduly expensive Presto queries.
        (:ref:`Cluster Restart Required <cluster-restart-op>`). :ref:`Beta, Disabled <availability-features>`

      * QDS now supports Apache Ranger integration with Presto for granular access control.
        :ref:`Beta, Disabled <availability-features>`

      :ref:`See all the changes <presto-r55-azure>`.

   .. tab:: Spark

      * The multi-tenant Qubole Job History Server has been upgraded to Spark 2.3 (2.3.1 by default).  This server
        makes available the logs and history of Spark jobs that ran on clusters that have since been terminated.

      * Spark on Qubole now supports Hive Admin commands that allow users to grant privileges such as SELECT, UPDATE,
        INSERT and DELETE to other users or roles.

      :ref:`See all the changes <spark-r55-azure>`.

