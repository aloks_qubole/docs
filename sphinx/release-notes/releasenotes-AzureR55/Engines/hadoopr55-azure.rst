.. _hadoop-r55-azure:

========
Hadoop 2
========

Bug Fix
-------

* **HADTWO-1847**: Fixed an issue that prevented upscaling when none of the  nodes in the minimum set were healthy.
