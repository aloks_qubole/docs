.. _presto-r55-azure:

======
Presto
======

Presto Strict Mode for Running Expensive Queries
------------------------------------------------
**PRES-339**: Presto **Strict Mode** prohibits:

* Cross join
* ORDER BY without LIMIT
* Querying a partitioned table without predicates

You can enable Strict Mode for a Presto cluster by setting ``qubole-strict-mode-enabled = true`` under
``config.properties`` in the **Override Presto Settings** field under the **Advanced Configuration** tab of the **Clusters**
page in the QDS UI. :ref:`Cluster Restart Required <cluster-restart-op>`.

Strict Mode is supported only in Presto 0.193 and later versions. :ref:`Beta, Disabled <availability-features>`.

Apache Ranger Integration with Presto
-------------------------------------
**PRES-1985**: Apache Ranger integration with Presto provides granular access control.
Ranger features such as Hive authorization, row filtering, and data masking are supported. :ref:`Beta, Disabled <availability-features>`.

Improvements
------------

* **PRES-2473**: ``hive.information-schema-presto-view-only`` is now set to ``true`` by default. This means that
  Presto will not try to read Hive views by default. To enable reading Hive views in Presto, add
  ``hive.information-schema-presto-view-only=false`` under ``catalog/hive-properties`` in the **Override Presto Settings**
  field under the **Advanced Configuration** tab of the **Clusters** page in the QDS UI.
  :ref:`Cluster Restart Required <cluster-restart-op>`.

Bug Fixes
---------

* **PRES-1968**: Fixed a problem that caused Presto queries to fail with the error ``nesting of 101 is too deep``.
* **PRES-2225**: Fixed a problem that caused intermittent ``NaN`` values to be displayed in the Presto UI.
