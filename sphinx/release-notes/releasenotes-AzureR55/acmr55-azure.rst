.. _acmr55-azure:

==================
Cluster Management
==================

Master/Worker Terminology is used in the Cluster UI
---------------------------------------------------
**ACM-4086**: **Master**/**Worker** terminology is now used in the QDS UI instead of **Master**/**Slave**.


Improvements
------------

* **ACM-3890**: You can configure Hive 2.3 (Beta) under the **Configuration** tab of the **Clusters** page in the QDS UI,
  and also via the cluster API.
* **ACM-3885**: You can enable RubiX caching for Spark on Azure. Use the check box under the **Advanced** tab of the
  **Clusters** page in the QDS UI.
* **ACM-3328**: QDS now validates your Azure compute credentials when you enter them in the **Control Panel** of the QDS
  UI, making sure your account has the proper permissions to manage the Azure resources your QDS clusters will need.

Bug Fixes
---------
* **ACM-4079**: The maximum size of an Azure data disk has been increased to 4095 GB.
