.. _sqoopr55-azure:

=====
Sqoop
=====

New Features
------------

* **SQOOP-228** & **SQOOP-216**: Sqoop has been upgraded to version 1.4.7 (:ref:`Cluster Restart Required <cluster-restart-op>`).
  :ref:`Via Support <availability-features>`, :ref:`Disabled  <availability-features>`.
