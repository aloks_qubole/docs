.. _data-engineering-r55-azure-index:

================
Data Engineering
================

.. toctree::
    :maxdepth: 1
    :titlesonly:

    airflowr55-azure.rst
    schedulerr55-azure.rst
    sqoopr55-azure.rst
