.. _applications-r55-azure-index:

============
Applications
============

.. toctree::
    :maxdepth: 1
    :titlesonly:

    adminr55-azure.rst
    DataEngineering/index.rst
    DataScience/index.rst
