.. _video-tutorials-index:

######################
Qubole Video Tutorials
######################

.. toctree::
    :maxdepth: 1

For a complete list of Qubole Education services including free public courses, self service education and in-application
walk throughs, see `Qubole Education <https://www.qubole.com/education/>`__.

The following video tutorials demonstrate several QDS features and components.

**1. Qubole Clusters and Structure**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/q_YDHuYduNE" frameborder="0" allowfullscreen></iframe>


**2. Qubole Cluster Composition**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/DxDdAy8t74k" frameborder="0" allowfullscreen></iframe>



**3. Qubole Cluster AutoScaling**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/d7CSvzmy8PU" frameborder="0" allowfullscreen></iframe>



**4. Cloud Instance Types**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/h9qqGkU51VM" frameborder="0" allowfullscreen></iframe>



**5. On Demand and Spot Instances**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/GxOzmKBfRr8" frameborder="0" allowfullscreen></iframe>



**6. Qubole Clusters**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/WzYtIwfwsWI" frameborder="0" allowfullscreen></iframe>



**7. Qubole Notebooks**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/a338s4Y5qQM" frameborder="0" allowfullscreen></iframe>



**8. Qubole Communication**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/olvD4ZM-XhI" frameborder="0" allowfullscreen></iframe>



**9. Qubole Templates**

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/DUSvPvvvDFM" frameborder="0" allowfullscreen></iframe>