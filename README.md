### Installation

 
See https://qubole.atlassian.net/wiki/spaces/KB/pages/51085321/Sphinx+Doc+Framework



### Deploying

The master branch is deployed at http://www.qubole.com/documentation.
A new build is initiated automatically on every push to the master branch.
**So, please be super careful while making changes to the master branch.**

### Resources

Our docs are reST (reStructuredText, *.rst) files with some Sphinx-specific directives.
To learn reST and Sphinx (documentation framework), refer to:

http://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html

http://sphinx-doc.org/rest.html

#### Themes
While changing the theme, ensure to add the new theme in sphinx/dependencies.pip.
The dependencies file (is known as the Requirements file) must be added in **Advanced Settings** > **Requirements file** of the
ReadTheDocs account.

The **_static** folder contains the theme override CSS file.
The **_templates** folder contains the template overriding files.

For any kind of issue, either post a question on StackOverflow or GitHub or refer to the related questions in their Forums.
As there are advanced users of ReadTheDocs who would guide you with workarounds.